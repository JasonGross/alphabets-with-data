

Summary:Short Summary:
Correct: 168
Incorrect: 32
Percent Correct: 84

Same Correct: 40
Same Incorrect: 21
Percent Same Correct: 65

Different Correct: 128
Different Incorrect: 11
Percent Different Correct: 92

Long Summary:
Task 0: correctly said that (atemayar, 25, a1vzhyp8yvxkz) and (atemayar, 25, avlbkm6qpb1s) are the same in 5724 ms.
Task 1: correctly said that (atemayar, 10, azdw6062ia38) and (inuktitut, 2, ay0bjcgydhix) are not the same in 2162 ms.
Task 2: correctly said that (tagalog, 1, a1i92aacrkacw) and (atemayar, 12, a3m0kt6kjezd1) are not the same in 3048 ms.
Task 3: incorrectly said that (atemayar, 10, azdw6062ia38) and (atemayar, 6, avlbkm6qpb1s) are the same in 2232 ms.
Task 4: incorrectly said that (tagalog, 15, a1l2fzyn31zvf) and (atemayar, 10, a31xqftzcsia2) are the same in 1568 ms.
Task 5: incorrectly said that (tagalog, 6, a12rfyr8dbn79) and (tagalog, 6, a1ehrxjplsq0r) are not the same in 2386 ms.
Task 6: incorrectly said that (myanmar, 4, a1stvgjqfl5kk) and (atemayar, 6, a3m0kt6kjezd1) are the same in 1851 ms.
Task 7: incorrectly said that (korean, 3, aav4pzxdoebd) and (atemayar, 12, ax5gghhj0o6g) are the same in 1796 ms.
Task 8: correctly said that (inuktitut, 12, a2qnqd7m8zfbo) and (inuktitut, 12, a1manxzopzden) are the same in 1775 ms.
Task 9: correctly said that (myanmar, 34, a4858n7ggst4) and (atemayar, 10, agst3co6usf8) are not the same in 4367 ms.
Task 10: correctly said that (sylheti, 4, a2du3880jbrov) and (sylheti, 4, ay0bjcgydhix) are the same in 1821 ms.
Task 11: correctly said that (myanmar, 8, ay0bjcgydhix) and (tagalog, 6, a3mv1jtwep7xn) are not the same in 1619 ms.
Task 12: correctly said that (korean, 17, a3dfh6dpi11ip) and (tagalog, 6, a1j8s7giyto4a) are not the same in 1332 ms.
Task 13: correctly said that (sylheti, 3, atvyxlwe5n03) and (inuktitut, 2, a2pfktghg1ofp) are not the same in 1347 ms.
Task 14: correctly said that (tagalog, 1, ajf66jwen7bx) and (inuktitut, 11, a2ernxe6jlm4x) are not the same in 1437 ms.
Task 15: correctly said that (myanmar, 34, a1aegunf6e081) and (atemayar, 25, a31e6jchki335) are not the same in 1385 ms.
Task 16: correctly said that (atemayar, 25, aljsxt6v0a30) and (inuktitut, 2, avlbkm6qpb1s) are not the same in 1105 ms.
Task 17: correctly said that (tagalog, 6, a1j8s7giyto4a) and (korean, 3, a2ernxe6jlm4x) are not the same in 1244 ms.
Task 18: incorrectly said that (sylheti, 4, a6fjbvuzrhm6) and (korean, 3, a1v0p79fynz9j) are the same in 1245 ms.
Task 19: correctly said that (atemayar, 12, a3m0kt6kjezd1) and (tagalog, 15, ae0ibwt7asth) are not the same in 3463 ms.
Task 20: incorrectly said that (tagalog, 1, a3m0kt6kjezd1) and (tagalog, 1, a2du3880jbrov) are not the same in 1653 ms.
Task 21: correctly said that (inuktitut, 11, a1s3njxhehcip) and (tagalog, 15, a1ehrxjplsq0r) are not the same in 1362 ms.
Task 22: incorrectly said that (atemayar, 10, a3m0kt6kjezd1) and (myanmar, 16, a1aegunf6e081) are the same in 3212 ms.
Task 23: correctly said that (inuktitut, 2, a3qat9qkqumi2) and (myanmar, 34, amynpl5xzw6q) are not the same in 1448 ms.
Task 24: correctly said that (atemayar, 10, ax5gghhj0o6g) and (atemayar, 25, a31e6jchki335) are not the same in 2795 ms.
Task 25: correctly said that (atemayar, 12, a31xqftzcsia2) and (atemayar, 12, awl8tng8e81d) are the same in 1510 ms.
Task 26: correctly said that (myanmar, 8, a1foggtlgo7n4) and (atemayar, 6, ae0ibwt7asth) are not the same in 1338 ms.
Task 27: correctly said that (inuktitut, 12, a3noo9k3y1yu5) and (myanmar, 34, adc8vbkoocrr) are not the same in 1239 ms.
Task 28: correctly said that (myanmar, 34, a1foggtlgo7n4) and (myanmar, 34, a1clejhbx9yyn) are the same in 1292 ms.
Task 29: correctly said that (atemayar, 6, ax5gghhj0o6g) and (atemayar, 6, aljsxt6v0a30) are the same in 2081 ms.
Task 30: correctly said that (atemayar, 6, a23psx2z4f65q) and (atemayar, 6, a1vzhyp8yvxkz) are the same in 1317 ms.
Task 31: correctly said that (sylheti, 4, a6fjbvuzrhm6) and (inuktitut, 12, a1manxzopzden) are not the same in 1537 ms.
Task 32: correctly said that (atemayar, 25, ae0ibwt7asth) and (atemayar, 25, a31e6jchki335) are the same in 1423 ms.
Task 33: correctly said that (tagalog, 1, aljsxt6v0a30) and (myanmar, 8, a6fjbvuzrhm6) are not the same in 1304 ms.
Task 34: correctly said that (atemayar, 25, a1z59eb5sfxpy) and (korean, 39, aav4pzxdoebd) are not the same in 1304 ms.
Task 35: incorrectly said that (tagalog, 15, a1yovlluv1c9h) and (tagalog, 15, ae0ibwt7asth) are not the same in 1378 ms.
Task 36: correctly said that (atemayar, 6, aljsxt6v0a30) and (inuktitut, 2, a2pfktghg1ofp) are not the same in 2066 ms.
Task 37: correctly said that (inuktitut, 2, a315xddd8j4x8) and (myanmar, 8, a1s3njxhehcip) are not the same in 1271 ms.
Task 38: correctly said that (atemayar, 6, aljsxt6v0a30) and (korean, 3, a1oh82lwr5hbg) are not the same in 1407 ms.
Task 39: correctly said that (tagalog, 10, a1j8s7giyto4a) and (inuktitut, 2, a315xddd8j4x8) are not the same in 1470 ms.
Task 40: correctly said that (tagalog, 15, a2du3880jbrov) and (tagalog, 15, a3qat9qkqumi2) are the same in 2927 ms.
Task 41: correctly said that (myanmar, 34, a4858n7ggst4) and (myanmar, 16, a2pfktghg1ofp) are not the same in 4397 ms.
Task 42: correctly said that (sylheti, 13, a1qku9mccvl35) and (sylheti, 13, a1l2fzyn31zvf) are the same in 2114 ms.
Task 43: correctly said that (tagalog, 6, asu813kb7bv1) and (myanmar, 16, ajf66jwen7bx) are not the same in 1139 ms.
Task 44: correctly said that (tagalog, 6, ae0ibwt7asth) and (tagalog, 6, asu813kb7bv1) are the same in 1080 ms.
Task 45: incorrectly said that (atemayar, 6, a1poymbentexx) and (atemayar, 6, a1qku9mccvl35) are not the same in 1226 ms.
Task 46: correctly said that (sylheti, 13, a3209gno9duil) and (inuktitut, 12, avlbkm6qpb1s) are not the same in 2500 ms.
Task 47: correctly said that (sylheti, 15, a1foggtlgo7n4) and (tagalog, 15, a3mv1jtwep7xn) are not the same in 1183 ms.
Task 48: correctly said that (sylheti, 15, amynpl5xzw6q) and (atemayar, 12, aljsxt6v0a30) are not the same in 3482 ms.
Task 49: incorrectly said that (atemayar, 25, a31xqftzcsia2) and (atemayar, 25, a31e6jchki335) are not the same in 3540 ms.
Task 50: correctly said that (inuktitut, 12, a12xm86d2nbih) and (inuktitut, 12, a1j8s7giyto4a) are the same in 3299 ms.
Task 51: correctly said that (korean, 17, aj6upe9mlarg) and (tagalog, 1, a12rfyr8dbn79) are not the same in 1269 ms.
Task 52: correctly said that (tagalog, 10, a1s3njxhehcip) and (tagalog, 10, a2du3880jbrov) are the same in 2247 ms.
Task 53: correctly said that (myanmar, 34, a1umhcrj9zfdp) and (myanmar, 34, a1stvgjqfl5kk) are the same in 1559 ms.
Task 54: correctly said that (atemayar, 25, a1vzhyp8yvxkz) and (atemayar, 25, a1qku9mccvl35) are the same in 1414 ms.
Task 55: correctly said that (myanmar, 34, a1stvgjqfl5kk) and (myanmar, 34, ay0bjcgydhix) are the same in 1445 ms.
Task 56: correctly said that (atemayar, 10, a2pfktghg1ofp) and (inuktitut, 2, a1qku9mccvl35) are not the same in 1219 ms.
Task 57: correctly said that (tagalog, 15, a3m0kt6kjezd1) and (tagalog, 15, a1s3njxhehcip) are the same in 2282 ms.
Task 58: correctly said that (atemayar, 12, a1z59eb5sfxpy) and (korean, 4, a2ernxe6jlm4x) are not the same in 1748 ms.
Task 59: correctly said that (inuktitut, 3, a2pfktghg1ofp) and (inuktitut, 3, a315xddd8j4x8) are the same in 1259 ms.
Task 60: correctly said that (tagalog, 6, amynpl5xzw6q) and (sylheti, 15, a6fjbvuzrhm6) are not the same in 2017 ms.
Task 61: correctly said that (inuktitut, 12, a2ernxe6jlm4x) and (tagalog, 15, a1ehrxjplsq0r) are not the same in 1636 ms.
Task 62: correctly said that (tagalog, 1, amynpl5xzw6q) and (myanmar, 16, a1umhcrj9zfdp) are not the same in 1596 ms.
Task 63: incorrectly said that (korean, 39, a1oh82lwr5hbg) and (korean, 39, a3bb99mn3zy37) are not the same in 1682 ms.
Task 64: correctly said that (sylheti, 15, amynpl5xzw6q) and (korean, 39, a3mv1jtwep7xn) are not the same in 1469 ms.
Task 65: correctly said that (korean, 39, a2px0aywxngce) and (inuktitut, 2, avlbkm6qpb1s) are not the same in 1250 ms.
Task 66: correctly said that (korean, 39, a2nm1qzuh14ls) and (inuktitut, 12, a1w3fo0d7cf5t) are not the same in 1351 ms.
Task 67: correctly said that (inuktitut, 3, a1manxzopzden) and (myanmar, 8, a3j1pp96x14u0) are not the same in 3674 ms.
Task 68: correctly said that (sylheti, 15, a3pkun3onkucn) and (myanmar, 8, a4858n7ggst4) are not the same in 1809 ms.
Task 69: correctly said that (myanmar, 16, a4858n7ggst4) and (inuktitut, 2, a2ernxe6jlm4x) are not the same in 1276 ms.
Task 70: incorrectly said that (tagalog, 6, a2du3880jbrov) and (tagalog, 6, a1i92aacrkacw) are not the same in 1428 ms.
Task 71: correctly said that (tagalog, 10, aljsxt6v0a30) and (atemayar, 6, ae0ibwt7asth) are not the same in 1045 ms.
Task 72: correctly said that (myanmar, 16, a1ehrxjplsq0r) and (myanmar, 16, a1clejhbx9yyn) are the same in 2492 ms.
Task 73: correctly said that (atemayar, 12, a6fjbvuzrhm6) and (inuktitut, 12, a12rfyr8dbn79) are not the same in 1329 ms.
Task 74: correctly said that (korean, 4, a3d7xwp0gand5) and (korean, 4, a1umhcrj9zfdp) are the same in 1137 ms.
Task 75: correctly said that (sylheti, 4, a2du3880jbrov) and (korean, 17, aw82oruamauz) are not the same in 1164 ms.
Task 76: correctly said that (atemayar, 10, a1z59eb5sfxpy) and (myanmar, 4, a1umhcrj9zfdp) are not the same in 1320 ms.
Task 77: correctly said that (myanmar, 8, a1foggtlgo7n4) and (korean, 3, a3d7xwp0gand5) are not the same in 1363 ms.
Task 78: correctly said that (myanmar, 16, aljsxt6v0a30) and (myanmar, 16, adc8vbkoocrr) are the same in 1300 ms.
Task 79: correctly said that (sylheti, 13, avlbkm6qpb1s) and (atemayar, 12, azdw6062ia38) are not the same in 3878 ms.
Task 80: incorrectly said that (atemayar, 25, a2pfktghg1ofp) and (atemayar, 25, a31e6jchki335) are not the same in 1518 ms.
Task 81: correctly said that (tagalog, 1, asu813kb7bv1) and (sylheti, 13, a2du3880jbrov) are not the same in 1244 ms.
Task 82: incorrectly said that (sylheti, 4, a3pkun3onkucn) and (sylheti, 4, aljsxt6v0a30) are not the same in 1130 ms.
Task 83: correctly said that (korean, 4, a3mv1jtwep7xn) and (myanmar, 8, adc8vbkoocrr) are not the same in 1089 ms.
Task 84: correctly said that (inuktitut, 11, a2ernxe6jlm4x) and (korean, 39, a3bb99mn3zy37) are not the same in 1201 ms.
Task 85: correctly said that (myanmar, 4, a6fjbvuzrhm6) and (korean, 39, a1oh82lwr5hbg) are not the same in 1153 ms.
Task 86: correctly said that (korean, 3, a2akud1gdstsd) and (tagalog, 10, a3qat9qkqumi2) are not the same in 1194 ms.
Task 87: correctly said that (inuktitut, 3, a2pgu4pi93b1q) and (atemayar, 12, a1z59eb5sfxpy) are not the same in 2049 ms.
Task 88: incorrectly said that (korean, 39, a2px0aywxngce) and (korean, 39, a1oh82lwr5hbg) are not the same in 1065 ms.
Task 89: correctly said that (sylheti, 13, a3np21apfa18b) and (korean, 17, a3bb99mn3zy37) are not the same in 1398 ms.
Task 90: correctly said that (inuktitut, 3, a3noo9k3y1yu5) and (inuktitut, 3, a1z59eb5sfxpy) are the same in 1491 ms.
Task 91: correctly said that (atemayar, 6, a1j8s7giyto4a) and (tagalog, 10, aljsxt6v0a30) are not the same in 1174 ms.
Task 92: incorrectly said that (korean, 39, aj6upe9mlarg) and (korean, 39, a3mv1jtwep7xn) are not the same in 1036 ms.
Task 93: incorrectly said that (atemayar, 12, a1z59eb5sfxpy) and (atemayar, 12, avlbkm6qpb1s) are not the same in 1325 ms.
Task 94: correctly said that (tagalog, 15, a3qat9qkqumi2) and (sylheti, 15, a1a25pyfoa9ge) are not the same in 895 ms.
Task 95: correctly said that (atemayar, 12, a6fjbvuzrhm6) and (atemayar, 25, avlbkm6qpb1s) are not the same in 965 ms.
Task 96: correctly said that (tagalog, 6, asu813kb7bv1) and (tagalog, 15, a1yovlluv1c9h) are not the same in 1577 ms.
Task 97: correctly said that (myanmar, 16, avlbkm6qpb1s) and (sylheti, 15, a6fjbvuzrhm6) are not the same in 4702 ms.
Task 98: correctly said that (tagalog, 15, a1ehrxjplsq0r) and (myanmar, 16, aljsxt6v0a30) are not the same in 1550 ms.
Task 99: correctly said that (tagalog, 15, a1i92aacrkacw) and (korean, 3, a5379luit3pc) are not the same in 1120 ms.
Task 100: correctly said that (atemayar, 12, ae0ibwt7asth) and (inuktitut, 11, avlbkm6qpb1s) are not the same in 1526 ms.
Task 101: correctly said that (atemayar, 6, a6fjbvuzrhm6) and (sylheti, 13, a3r9q9wqm1nyz) are not the same in 1065 ms.
Task 102: incorrectly said that (myanmar, 16, a4858n7ggst4) and (myanmar, 16, a3qat9qkqumi2) are not the same in 2377 ms.
Task 103: correctly said that (korean, 17, a2px0aywxngce) and (inuktitut, 2, a315xddd8j4x8) are not the same in 960 ms.
Task 104: correctly said that (tagalog, 6, a3qat9qkqumi2) and (atemayar, 12, a1manxzopzden) are not the same in 850 ms.
Task 105: correctly said that (atemayar, 6, a1j8s7giyto4a) and (myanmar, 4, adc8vbkoocrr) are not the same in 819 ms.
Task 106: correctly said that (tagalog, 1, a3mv1jtwep7xn) and (inuktitut, 2, a2ernxe6jlm4x) are not the same in 991 ms.
Task 107: incorrectly said that (inuktitut, 12, a2pfktghg1ofp) and (myanmar, 8, a2pfktghg1ofp) are the same in 3237 ms.
Task 108: incorrectly said that (korean, 39, aav4pzxdoebd) and (myanmar, 8, a1aegunf6e081) are the same in 3029 ms.
Task 109: correctly said that (inuktitut, 2, a1w3fo0d7cf5t) and (inuktitut, 2, a2qnqd7m8zfbo) are the same in 2953 ms.
Task 110: correctly said that (myanmar, 4, amynpl5xzw6q) and (myanmar, 4, a1umhcrj9zfdp) are the same in 3174 ms.
Task 111: correctly said that (sylheti, 3, a30xq7555spo8) and (inuktitut, 11, a315xddd8j4x8) are not the same in 1676 ms.
Task 112: correctly said that (sylheti, 4, a3pkun3onkucn) and (sylheti, 13, a6fjbvuzrhm6) are not the same in 923 ms.
Task 113: correctly said that (tagalog, 10, avlbkm6qpb1s) and (tagalog, 10, a12rfyr8dbn79) are the same in 1630 ms.
Task 114: correctly said that (korean, 3, a3dfh6dpi11ip) and (korean, 17, aw82oruamauz) are not the same in 1673 ms.
Task 115: correctly said that (myanmar, 16, a2pfktghg1ofp) and (sylheti, 4, a3m0kt6kjezd1) are not the same in 1498 ms.
Task 116: correctly said that (myanmar, 4, a1foggtlgo7n4) and (tagalog, 6, a1ehrxjplsq0r) are not the same in 1075 ms.
Task 117: correctly said that (inuktitut, 11, a2qnqd7m8zfbo) and (inuktitut, 3, a1qku9mccvl35) are not the same in 937 ms.
Task 118: correctly said that (korean, 4, ae0ibwt7asth) and (atemayar, 6, avlbkm6qpb1s) are not the same in 2634 ms.
Task 119: correctly said that (inuktitut, 2, a1j8s7giyto4a) and (sylheti, 13, a1s3njxhehcip) are not the same in 1099 ms.
Task 120: correctly said that (atemayar, 10, agst3co6usf8) and (atemayar, 10, ae0ibwt7asth) are the same in 1307 ms.
Task 121: correctly said that (tagalog, 15, a3qat9qkqumi2) and (inuktitut, 3, a2ernxe6jlm4x) are not the same in 1116 ms.
Task 122: correctly said that (tagalog, 15, a1yovlluv1c9h) and (tagalog, 15, a1a25pyfoa9ge) are the same in 1882 ms.
Task 123: correctly said that (tagalog, 1, a3qat9qkqumi2) and (tagalog, 1, aghrkle9vf6z) are the same in 1349 ms.
Task 124: correctly said that (korean, 4, a1udqv4pfypkn) and (tagalog, 10, a1i92aacrkacw) are not the same in 1951 ms.
Task 125: correctly said that (tagalog, 10, aghrkle9vf6z) and (inuktitut, 12, a1s3njxhehcip) are not the same in 1721 ms.
Task 126: correctly said that (sylheti, 13, avlbkm6qpb1s) and (atemayar, 25, awl8tng8e81d) are not the same in 1251 ms.
Task 127: correctly said that (atemayar, 25, avlbkm6qpb1s) and (tagalog, 1, amynpl5xzw6q) are not the same in 931 ms.
Task 128: correctly said that (atemayar, 25, a31e6jchki335) and (sylheti, 3, aljsxt6v0a30) are not the same in 811 ms.
Task 129: correctly said that (korean, 39, aav4pzxdoebd) and (korean, 3, a1manxzopzden) are not the same in 921 ms.
Task 130: correctly said that (myanmar, 4, ajf66jwen7bx) and (inuktitut, 12, ae0ibwt7asth) are not the same in 1138 ms.
Task 131: incorrectly said that (myanmar, 34, a1umhcrj9zfdp) and (sylheti, 15, a3209gno9duil) are the same in 1414 ms.
Task 132: correctly said that (myanmar, 34, amynpl5xzw6q) and (sylheti, 3, atvyxlwe5n03) are not the same in 870 ms.
Task 133: correctly said that (myanmar, 34, ajf66jwen7bx) and (inuktitut, 3, a1manxzopzden) are not the same in 836 ms.
Task 134: correctly said that (atemayar, 6, a1z59eb5sfxpy) and (atemayar, 6, ax5gghhj0o6g) are the same in 1265 ms.
Task 135: incorrectly said that (myanmar, 8, a1foggtlgo7n4) and (myanmar, 8, aljsxt6v0a30) are not the same in 2169 ms.
Task 136: correctly said that (inuktitut, 2, a3qat9qkqumi2) and (atemayar, 10, a31e6jchki335) are not the same in 888 ms.
Task 137: correctly said that (tagalog, 6, aghrkle9vf6z) and (atemayar, 10, ax5gghhj0o6g) are not the same in 3565 ms.
Task 138: correctly said that (atemayar, 12, aljsxt6v0a30) and (inuktitut, 3, a1manxzopzden) are not the same in 848 ms.
Task 139: correctly said that (atemayar, 10, a31xqftzcsia2) and (atemayar, 10, a2pfktghg1ofp) are the same in 1304 ms.
Task 140: correctly said that (inuktitut, 2, a12xm86d2nbih) and (korean, 17, a2nm1qzuh14ls) are not the same in 1064 ms.
Task 141: correctly said that (korean, 39, a2px0aywxngce) and (myanmar, 8, a1clejhbx9yyn) are not the same in 892 ms.
Task 142: correctly said that (myanmar, 4, a4858n7ggst4) and (sylheti, 3, atvyxlwe5n03) are not the same in 760 ms.
Task 143: correctly said that (sylheti, 3, a30xq7555spo8) and (sylheti, 3, a1foggtlgo7n4) are the same in 1079 ms.
Task 144: correctly said that (tagalog, 1, a12rfyr8dbn79) and (tagalog, 15, a1l2fzyn31zvf) are not the same in 868 ms.
Task 145: correctly said that (myanmar, 8, a1aegunf6e081) and (myanmar, 8, ajf66jwen7bx) are the same in 959 ms.
Task 146: correctly said that (korean, 39, a1v0p79fynz9j) and (sylheti, 3, a3np21apfa18b) are not the same in 1826 ms.
Task 147: correctly said that (atemayar, 12, azdw6062ia38) and (sylheti, 4, amynpl5xzw6q) are not the same in 1170 ms.
Task 148: correctly said that (myanmar, 34, a3pkun3onkucn) and (atemayar, 25, awl8tng8e81d) are not the same in 1019 ms.
Task 149: correctly said that (korean, 39, a3d7xwp0gand5) and (tagalog, 10, a1j8s7giyto4a) are not the same in 948 ms.
Task 150: correctly said that (myanmar, 4, aljsxt6v0a30) and (korean, 17, aav4pzxdoebd) are not the same in 1137 ms.
Task 151: correctly said that (sylheti, 15, a1qku9mccvl35) and (tagalog, 15, a1j8s7giyto4a) are not the same in 1089 ms.
Task 152: incorrectly said that (atemayar, 25, a1z59eb5sfxpy) and (atemayar, 25, a1yovlluv1c9h) are not the same in 1898 ms.
Task 153: correctly said that (tagalog, 10, a1i92aacrkacw) and (tagalog, 1, aljsxt6v0a30) are not the same in 1031 ms.
Task 154: correctly said that (korean, 17, a3bb99mn3zy37) and (myanmar, 16, a2pfktghg1ofp) are not the same in 1000 ms.
Task 155: correctly said that (atemayar, 10, avlbkm6qpb1s) and (sylheti, 3, a3np21apfa18b) are not the same in 1274 ms.
Task 156: correctly said that (inuktitut, 3, a3qat9qkqumi2) and (atemayar, 6, a1vzhyp8yvxkz) are not the same in 914 ms.
Task 157: incorrectly said that (atemayar, 12, a2pfktghg1ofp) and (atemayar, 12, aljsxt6v0a30) are not the same in 1206 ms.
Task 158: correctly said that (myanmar, 34, a1foggtlgo7n4) and (korean, 3, a5379luit3pc) are not the same in 761 ms.
Task 159: correctly said that (atemayar, 6, a1manxzopzden) and (tagalog, 15, a12rfyr8dbn79) are not the same in 1088 ms.
Task 160: correctly said that (atemayar, 12, a1j8s7giyto4a) and (myanmar, 8, a1stvgjqfl5kk) are not the same in 1155 ms.
Task 161: correctly said that (sylheti, 4, a1l2fzyn31zvf) and (sylheti, 4, a1s3njxhehcip) are the same in 2127 ms.
Task 162: correctly said that (atemayar, 10, a23psx2z4f65q) and (atemayar, 10, a1vzhyp8yvxkz) are the same in 1274 ms.
Task 163: incorrectly said that (korean, 4, aw82oruamauz) and (korean, 4, aav4pzxdoebd) are not the same in 4847 ms.
Task 164: incorrectly said that (korean, 3, a1udqv4pfypkn) and (korean, 3, a1v0p79fynz9j) are not the same in 1056 ms.
Task 165: correctly said that (inuktitut, 12, a315xddd8j4x8) and (myanmar, 4, amynpl5xzw6q) are not the same in 1071 ms.
Task 166: correctly said that (myanmar, 34, a3pkun3onkucn) and (sylheti, 13, a3np21apfa18b) are not the same in 1293 ms.
Task 167: incorrectly said that (korean, 4, a1oh82lwr5hbg) and (korean, 4, a1umhcrj9zfdp) are not the same in 2900 ms.
Task 168: correctly said that (sylheti, 13, a4858n7ggst4) and (korean, 3, a1v0p79fynz9j) are not the same in 762 ms.
Task 169: incorrectly said that (inuktitut, 2, a1w3fo0d7cf5t) and (inuktitut, 11, a1manxzopzden) are the same in 4641 ms.
Task 170: incorrectly said that (inuktitut, 12, a2qnqd7m8zfbo) and (inuktitut, 12, a1manxzopzden) are not the same in 1217 ms.
Task 171: correctly said that (tagalog, 6, a1j8s7giyto4a) and (sylheti, 13, a3np21apfa18b) are not the same in 921 ms.
Task 172: correctly said that (korean, 4, a2akud1gdstsd) and (atemayar, 10, a1z59eb5sfxpy) are not the same in 847 ms.
Task 173: correctly said that (korean, 3, a1umhcrj9zfdp) and (myanmar, 8, aljsxt6v0a30) are not the same in 729 ms.
Task 174: correctly said that (atemayar, 12, a6fjbvuzrhm6) and (atemayar, 12, a1yovlluv1c9h) are the same in 2123 ms.
Task 175: correctly said that (sylheti, 15, a1j8s7giyto4a) and (tagalog, 1, aljsxt6v0a30) are not the same in 1104 ms.
Task 176: correctly said that (myanmar, 8, adc8vbkoocrr) and (tagalog, 6, a1i92aacrkacw) are not the same in 868 ms.
Task 177: correctly said that (atemayar, 6, avlbkm6qpb1s) and (sylheti, 4, a3m0kt6kjezd1) are not the same in 853 ms.
Task 178: correctly said that (sylheti, 3, a6fjbvuzrhm6) and (atemayar, 25, a1z59eb5sfxpy) are not the same in 987 ms.
Task 179: correctly said that (atemayar, 10, a1vzhyp8yvxkz) and (korean, 4, a2px0aywxngce) are not the same in 3156 ms.
Task 180: correctly said that (sylheti, 3, a1a25pyfoa9ge) and (myanmar, 16, aljsxt6v0a30) are not the same in 1203 ms.
Task 181: correctly said that (tagalog, 15, a1ehrxjplsq0r) and (sylheti, 13, a4858n7ggst4) are not the same in 1155 ms.
Task 182: correctly said that (myanmar, 34, a6fjbvuzrhm6) and (sylheti, 3, a3pkun3onkucn) are not the same in 2074 ms.
Task 183: correctly said that (myanmar, 8, aljsxt6v0a30) and (tagalog, 1, a12rfyr8dbn79) are not the same in 889 ms.
Task 184: correctly said that (tagalog, 15, amynpl5xzw6q) and (tagalog, 15, a1l2fzyn31zvf) are the same in 1260 ms.
Task 185: correctly said that (inuktitut, 12, a3qat9qkqumi2) and (atemayar, 25, agst3co6usf8) are not the same in 933 ms.
Task 186: correctly said that (atemayar, 10, a1vzhyp8yvxkz) and (atemayar, 10, a2pfktghg1ofp) are the same in 1190 ms.
Task 187: correctly said that (atemayar, 12, azdw6062ia38) and (tagalog, 6, a1ehrxjplsq0r) are not the same in 1674 ms.
Task 188: correctly said that (sylheti, 15, avlbkm6qpb1s) and (myanmar, 8, amynpl5xzw6q) are not the same in 759 ms.
Task 189: incorrectly said that (tagalog, 1, ajf66jwen7bx) and (tagalog, 1, a1s3njxhehcip) are not the same in 4051 ms.
Task 190: correctly said that (inuktitut, 3, a3noo9k3y1yu5) and (korean, 3, a3dfh6dpi11ip) are not the same in 1170 ms.
Task 191: correctly said that (inuktitut, 3, a2pgu4pi93b1q) and (inuktitut, 3, a1qku9mccvl35) are the same in 1487 ms.
Task 192: correctly said that (myanmar, 8, a6fjbvuzrhm6) and (myanmar, 8, a1umhcrj9zfdp) are the same in 1764 ms.
Task 193: correctly said that (myanmar, 34, a1s3njxhehcip) and (sylheti, 13, a30xq7555spo8) are not the same in 986 ms.
Task 194: correctly said that (inuktitut, 12, a2pgu4pi93b1q) and (sylheti, 13, a3m0kt6kjezd1) are not the same in 2677 ms.
Task 195: correctly said that (inuktitut, 2, ae0ibwt7asth) and (tagalog, 6, a1l2fzyn31zvf) are not the same in 762 ms.
Task 196: correctly said that (myanmar, 16, a1aegunf6e081) and (sylheti, 13, a4858n7ggst4) are not the same in 777 ms.
Task 197: incorrectly said that (atemayar, 10, a1j8s7giyto4a) and (myanmar, 34, a3j1pp96x14u0) are the same in 2134 ms.
Task 198: correctly said that (korean, 4, a2nm1qzuh14ls) and (korean, 4, a1manxzopzden) are the same in 1097 ms.
Task 199: correctly said that (sylheti, 15, aljsxt6v0a30) and (atemayar, 12, aljsxt6v0a30) are not the same in 1275 ms.

Duration: 23m 7s 237ms
Comments: Interesting! I'm surprised I managed to get so many right it seemed like milliseconds that was on the screen. :) 