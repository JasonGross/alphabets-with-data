

Summary:Short Summary:
Correct: 181
Incorrect: 19
Percent Correct: 90

Same Correct: 45
Same Incorrect: 5
Percent Same Correct: 90

Different Correct: 136
Different Incorrect: 14
Percent Different Correct: 90

Long Summary:
Task 0: incorrectly said that (inuktitut, 12, avlbkm6qpb1s) and (myanmar, 8, a1ehrxjplsq0r) are the same in 16009 ms.
Task 1: incorrectly said that (sylheti, 4, a3pkun3onkucn) and (korean, 39, a1s3njxhehcip) are the same in 4018 ms.
Task 2: correctly said that (atemayar, 25, awl8tng8e81d) and (myanmar, 16, a1aegunf6e081) are not the same in 7719 ms.
Task 3: correctly said that (korean, 39, aw82oruamauz) and (sylheti, 13, a1foggtlgo7n4) are not the same in 3910 ms.
Task 4: correctly said that (korean, 17, a1umhcrj9zfdp) and (korean, 17, aav4pzxdoebd) are the same in 1359 ms.
Task 5: correctly said that (atemayar, 25, a2pfktghg1ofp) and (inuktitut, 2, a3qat9qkqumi2) are not the same in 1817 ms.
Task 6: correctly said that (myanmar, 8, a1j8s7giyto4a) and (tagalog, 10, a1s3njxhehcip) are not the same in 985 ms.
Task 7: incorrectly said that (tagalog, 6, a1l2fzyn31zvf) and (tagalog, 6, a12rfyr8dbn79) are not the same in 1467 ms.
Task 8: incorrectly said that (korean, 39, a1udqv4pfypkn) and (atemayar, 10, aljsxt6v0a30) are the same in 2151 ms.
Task 9: correctly said that (sylheti, 3, atvyxlwe5n03) and (atemayar, 12, azdw6062ia38) are not the same in 1281 ms.
Task 10: incorrectly said that (inuktitut, 12, ae0ibwt7asth) and (inuktitut, 12, a3qat9qkqumi2) are not the same in 1373 ms.
Task 11: incorrectly said that (tagalog, 1, ay0bjcgydhix) and (sylheti, 4, a2du3880jbrov) are the same in 444 ms.
Task 12: correctly said that (myanmar, 8, ay0bjcgydhix) and (sylheti, 3, a30xq7555spo8) are not the same in 874 ms.
Task 13: correctly said that (atemayar, 25, a1j8s7giyto4a) and (atemayar, 25, a1z59eb5sfxpy) are the same in 883 ms.
Task 14: correctly said that (inuktitut, 12, a12rfyr8dbn79) and (inuktitut, 12, a1j8s7giyto4a) are the same in 946 ms.
Task 15: correctly said that (inuktitut, 3, ay0bjcgydhix) and (myanmar, 4, a4858n7ggst4) are not the same in 1846 ms.
Task 16: correctly said that (myanmar, 4, a6fjbvuzrhm6) and (korean, 17, a3bb99mn3zy37) are not the same in 1553 ms.
Task 17: correctly said that (tagalog, 6, a3qat9qkqumi2) and (tagalog, 6, asu813kb7bv1) are the same in 1028 ms.
Task 18: correctly said that (myanmar, 16, a4858n7ggst4) and (tagalog, 10, a1s3njxhehcip) are not the same in 1040 ms.
Task 19: correctly said that (inuktitut, 3, a1z59eb5sfxpy) and (sylheti, 15, a1a25pyfoa9ge) are not the same in 1024 ms.
Task 20: correctly said that (myanmar, 16, a1aegunf6e081) and (tagalog, 15, aljsxt6v0a30) are not the same in 1389 ms.
Task 21: correctly said that (tagalog, 6, ajf66jwen7bx) and (korean, 4, a3mv1jtwep7xn) are not the same in 836 ms.
Task 22: incorrectly said that (sylheti, 15, a1qku9mccvl35) and (atemayar, 25, a31xqftzcsia2) are the same in 1530 ms.
Task 23: correctly said that (atemayar, 10, a1yovlluv1c9h) and (atemayar, 12, a1manxzopzden) are not the same in 1123 ms.
Task 24: correctly said that (tagalog, 6, a1j8s7giyto4a) and (tagalog, 6, amynpl5xzw6q) are the same in 1012 ms.
Task 25: incorrectly said that (sylheti, 3, a1qku9mccvl35) and (sylheti, 15, a3209gno9duil) are the same in 1159 ms.
Task 26: correctly said that (atemayar, 10, a2pfktghg1ofp) and (atemayar, 10, a23psx2z4f65q) are the same in 1088 ms.
Task 27: correctly said that (inuktitut, 2, avlbkm6qpb1s) and (tagalog, 15, a1s3njxhehcip) are not the same in 823 ms.
Task 28: correctly said that (atemayar, 10, ae0ibwt7asth) and (korean, 4, a2akud1gdstsd) are not the same in 1257 ms.
Task 29: correctly said that (tagalog, 6, a1i92aacrkacw) and (sylheti, 3, a3pkun3onkucn) are not the same in 795 ms.
Task 30: correctly said that (atemayar, 25, awl8tng8e81d) and (korean, 3, ae0ibwt7asth) are not the same in 1960 ms.
Task 31: correctly said that (atemayar, 12, a31xqftzcsia2) and (myanmar, 34, a4858n7ggst4) are not the same in 798 ms.
Task 32: correctly said that (tagalog, 10, a1j8s7giyto4a) and (myanmar, 16, a1stvgjqfl5kk) are not the same in 661 ms.
Task 33: correctly said that (inuktitut, 12, a2pfktghg1ofp) and (atemayar, 25, a1yovlluv1c9h) are not the same in 616 ms.
Task 34: correctly said that (atemayar, 25, a2pfktghg1ofp) and (sylheti, 15, a1s3njxhehcip) are not the same in 926 ms.
Task 35: correctly said that (korean, 4, ae0ibwt7asth) and (korean, 4, aav4pzxdoebd) are the same in 789 ms.
Task 36: correctly said that (atemayar, 25, a1z59eb5sfxpy) and (tagalog, 10, a3m0kt6kjezd1) are not the same in 1192 ms.
Task 37: correctly said that (tagalog, 6, a1a25pyfoa9ge) and (tagalog, 6, a1ehrxjplsq0r) are the same in 1076 ms.
Task 38: incorrectly said that (inuktitut, 12, a1j8s7giyto4a) and (inuktitut, 12, a2pfktghg1ofp) are not the same in 1076 ms.
Task 39: correctly said that (inuktitut, 2, a12rfyr8dbn79) and (myanmar, 16, a3j1pp96x14u0) are not the same in 972 ms.
Task 40: incorrectly said that (inuktitut, 12, a2ernxe6jlm4x) and (inuktitut, 12, aljsxt6v0a30) are not the same in 1397 ms.
Task 41: correctly said that (inuktitut, 2, avlbkm6qpb1s) and (korean, 17, a3bb99mn3zy37) are not the same in 693 ms.
Task 42: correctly said that (tagalog, 10, a1l2fzyn31zvf) and (tagalog, 10, a1s3njxhehcip) are the same in 1514 ms.
Task 43: correctly said that (myanmar, 8, a1stvgjqfl5kk) and (myanmar, 34, a1ehrxjplsq0r) are not the same in 902 ms.
Task 44: correctly said that (myanmar, 34, a6fjbvuzrhm6) and (inuktitut, 11, a1s3njxhehcip) are not the same in 1543 ms.
Task 45: correctly said that (sylheti, 13, aljsxt6v0a30) and (sylheti, 13, a4858n7ggst4) are the same in 1143 ms.
Task 46: correctly said that (tagalog, 15, aghrkle9vf6z) and (inuktitut, 11, a1qku9mccvl35) are not the same in 1069 ms.
Task 47: correctly said that (tagalog, 10, a3m0kt6kjezd1) and (myanmar, 4, adc8vbkoocrr) are not the same in 1441 ms.
Task 48: incorrectly said that (myanmar, 34, a1ehrxjplsq0r) and (sylheti, 3, a1a25pyfoa9ge) are the same in 866 ms.
Task 49: correctly said that (atemayar, 6, a31xqftzcsia2) and (atemayar, 6, a1yovlluv1c9h) are the same in 1547 ms.
Task 50: correctly said that (sylheti, 13, a3r9q9wqm1nyz) and (korean, 3, a3d7xwp0gand5) are not the same in 1108 ms.
Task 51: correctly said that (atemayar, 25, ax5gghhj0o6g) and (sylheti, 13, a2du3880jbrov) are not the same in 904 ms.
Task 52: correctly said that (korean, 39, ae0ibwt7asth) and (inuktitut, 3, a2pgu4pi93b1q) are not the same in 679 ms.
Task 53: correctly said that (myanmar, 4, amynpl5xzw6q) and (inuktitut, 2, a1z59eb5sfxpy) are not the same in 585 ms.
Task 54: correctly said that (myanmar, 4, a2pfktghg1ofp) and (myanmar, 4, aljsxt6v0a30) are the same in 1019 ms.
Task 55: correctly said that (myanmar, 8, a1umhcrj9zfdp) and (tagalog, 6, ay0bjcgydhix) are not the same in 638 ms.
Task 56: correctly said that (myanmar, 8, aljsxt6v0a30) and (atemayar, 25, a23psx2z4f65q) are not the same in 2296 ms.
Task 57: correctly said that (korean, 3, a39ivbdm1ekue) and (sylheti, 4, a1l2fzyn31zvf) are not the same in 985 ms.
Task 58: correctly said that (inuktitut, 11, a1manxzopzden) and (inuktitut, 11, ay0bjcgydhix) are the same in 746 ms.
Task 59: correctly said that (atemayar, 10, a1qku9mccvl35) and (myanmar, 16, a1ehrxjplsq0r) are not the same in 655 ms.
Task 60: correctly said that (atemayar, 6, avlbkm6qpb1s) and (myanmar, 34, a1s3njxhehcip) are not the same in 935 ms.
Task 61: correctly said that (tagalog, 1, ajf66jwen7bx) and (tagalog, 1, a3qat9qkqumi2) are the same in 860 ms.
Task 62: correctly said that (sylheti, 3, a30xq7555spo8) and (myanmar, 4, a1umhcrj9zfdp) are not the same in 1277 ms.
Task 63: incorrectly said that (atemayar, 10, ae0ibwt7asth) and (korean, 17, a39ivbdm1ekue) are the same in 877 ms.
Task 64: incorrectly said that (sylheti, 4, ay0bjcgydhix) and (tagalog, 1, a12rfyr8dbn79) are the same in 1182 ms.
Task 65: correctly said that (atemayar, 10, a1vzhyp8yvxkz) and (myanmar, 8, a3pkun3onkucn) are not the same in 1042 ms.
Task 66: correctly said that (myanmar, 16, a1j8s7giyto4a) and (korean, 17, ae0ibwt7asth) are not the same in 753 ms.
Task 67: correctly said that (korean, 3, a3d7xwp0gand5) and (korean, 17, a2akud1gdstsd) are not the same in 520 ms.
Task 68: correctly said that (tagalog, 10, asu813kb7bv1) and (korean, 39, a1udqv4pfypkn) are not the same in 476 ms.
Task 69: correctly said that (tagalog, 6, a1i92aacrkacw) and (myanmar, 34, a1s3njxhehcip) are not the same in 2193 ms.
Task 70: correctly said that (korean, 3, aw82oruamauz) and (inuktitut, 11, a1j8s7giyto4a) are not the same in 1054 ms.
Task 71: correctly said that (tagalog, 1, ay0bjcgydhix) and (tagalog, 1, a1a25pyfoa9ge) are the same in 1422 ms.
Task 72: correctly said that (korean, 3, aj6upe9mlarg) and (sylheti, 13, a1qku9mccvl35) are not the same in 682 ms.
Task 73: correctly said that (atemayar, 25, azdw6062ia38) and (atemayar, 25, ae0ibwt7asth) are the same in 1190 ms.
Task 74: correctly said that (tagalog, 10, a3m0kt6kjezd1) and (sylheti, 4, a2du3880jbrov) are not the same in 1383 ms.
Task 75: correctly said that (myanmar, 34, a3qat9qkqumi2) and (myanmar, 34, a3j1pp96x14u0) are the same in 880 ms.
Task 76: correctly said that (tagalog, 6, a3mv1jtwep7xn) and (atemayar, 12, awl8tng8e81d) are not the same in 868 ms.
Task 77: incorrectly said that (korean, 17, a3d7xwp0gand5) and (sylheti, 3, a2du3880jbrov) are the same in 1299 ms.
Task 78: correctly said that (sylheti, 4, a1s3njxhehcip) and (tagalog, 10, a1yovlluv1c9h) are not the same in 606 ms.
Task 79: correctly said that (inuktitut, 3, a1manxzopzden) and (atemayar, 12, azdw6062ia38) are not the same in 1292 ms.
Task 80: correctly said that (inuktitut, 3, ay0bjcgydhix) and (inuktitut, 3, ae0ibwt7asth) are the same in 1315 ms.
Task 81: correctly said that (inuktitut, 12, a1w3fo0d7cf5t) and (tagalog, 6, a1a25pyfoa9ge) are not the same in 1463 ms.
Task 82: correctly said that (atemayar, 12, a1qku9mccvl35) and (myanmar, 16, ay0bjcgydhix) are not the same in 653 ms.
Task 83: correctly said that (tagalog, 1, a3mv1jtwep7xn) and (myanmar, 16, a2pfktghg1ofp) are not the same in 588 ms.
Task 84: correctly said that (korean, 3, a2akud1gdstsd) and (korean, 3, aj6upe9mlarg) are the same in 920 ms.
Task 85: correctly said that (myanmar, 4, a1s3njxhehcip) and (myanmar, 4, a3j1pp96x14u0) are the same in 793 ms.
Task 86: correctly said that (sylheti, 15, a1j8s7giyto4a) and (tagalog, 10, a1ehrxjplsq0r) are not the same in 1043 ms.
Task 87: correctly said that (inuktitut, 11, a2qnqd7m8zfbo) and (tagalog, 15, a3qat9qkqumi2) are not the same in 624 ms.
Task 88: correctly said that (korean, 4, aw82oruamauz) and (atemayar, 25, avlbkm6qpb1s) are not the same in 828 ms.
Task 89: correctly said that (sylheti, 13, a1l2fzyn31zvf) and (inuktitut, 12, a1j8s7giyto4a) are not the same in 547 ms.
Task 90: correctly said that (korean, 4, a3mv1jtwep7xn) and (tagalog, 15, aljsxt6v0a30) are not the same in 629 ms.
Task 91: correctly said that (inuktitut, 2, a12rfyr8dbn79) and (korean, 39, a1umhcrj9zfdp) are not the same in 418 ms.
Task 92: correctly said that (korean, 3, a2px0aywxngce) and (tagalog, 10, a2du3880jbrov) are not the same in 485 ms.
Task 93: correctly said that (sylheti, 3, a1a25pyfoa9ge) and (sylheti, 3, a1j8s7giyto4a) are the same in 1120 ms.
Task 94: correctly said that (korean, 4, a2akud1gdstsd) and (sylheti, 3, a1s3njxhehcip) are not the same in 538 ms.
Task 95: correctly said that (tagalog, 15, a3qat9qkqumi2) and (sylheti, 15, a3m0kt6kjezd1) are not the same in 786 ms.
Task 96: correctly said that (inuktitut, 2, a3noo9k3y1yu5) and (tagalog, 15, a12rfyr8dbn79) are not the same in 485 ms.
Task 97: correctly said that (tagalog, 10, avlbkm6qpb1s) and (tagalog, 6, a1i92aacrkacw) are not the same in 680 ms.
Task 98: correctly said that (myanmar, 8, ajf66jwen7bx) and (myanmar, 4, ajf66jwen7bx) are not the same in 620 ms.
Task 99: correctly said that (sylheti, 15, a1l2fzyn31zvf) and (sylheti, 13, amynpl5xzw6q) are not the same in 710 ms.
Task 100: correctly said that (korean, 3, a2nm1qzuh14ls) and (atemayar, 25, awl8tng8e81d) are not the same in 1235 ms.
Task 101: correctly said that (myanmar, 16, avlbkm6qpb1s) and (inuktitut, 2, a3mv1jtwep7xn) are not the same in 725 ms.
Task 102: correctly said that (sylheti, 4, a6fjbvuzrhm6) and (sylheti, 4, a1s3njxhehcip) are the same in 880 ms.
Task 103: correctly said that (myanmar, 8, a1umhcrj9zfdp) and (korean, 17, a39ivbdm1ekue) are not the same in 946 ms.
Task 104: correctly said that (sylheti, 15, a6fjbvuzrhm6) and (sylheti, 3, a1l2fzyn31zvf) are not the same in 411 ms.
Task 105: correctly said that (atemayar, 6, a1j8s7giyto4a) and (tagalog, 15, a1j8s7giyto4a) are not the same in 497 ms.
Task 106: correctly said that (tagalog, 15, a3qat9qkqumi2) and (atemayar, 12, a1manxzopzden) are not the same in 470 ms.
Task 107: correctly said that (myanmar, 16, a1umhcrj9zfdp) and (korean, 4, a1v0p79fynz9j) are not the same in 507 ms.
Task 108: correctly said that (sylheti, 4, a1qku9mccvl35) and (sylheti, 4, ay0bjcgydhix) are the same in 1987 ms.
Task 109: correctly said that (tagalog, 10, a3mv1jtwep7xn) and (tagalog, 10, a1i92aacrkacw) are the same in 1377 ms.
Task 110: correctly said that (myanmar, 4, ajf66jwen7bx) and (atemayar, 6, a1poymbentexx) are not the same in 975 ms.
Task 111: correctly said that (tagalog, 10, aljsxt6v0a30) and (sylheti, 3, a3m0kt6kjezd1) are not the same in 528 ms.
Task 112: correctly said that (tagalog, 10, a2du3880jbrov) and (korean, 3, a39ivbdm1ekue) are not the same in 489 ms.
Task 113: correctly said that (korean, 4, a3bb99mn3zy37) and (korean, 4, a1udqv4pfypkn) are the same in 820 ms.
Task 114: correctly said that (atemayar, 12, a2pfktghg1ofp) and (atemayar, 12, a31xqftzcsia2) are the same in 1143 ms.
Task 115: correctly said that (korean, 17, a2akud1gdstsd) and (korean, 17, aav4pzxdoebd) are the same in 777 ms.
Task 116: correctly said that (inuktitut, 2, a1w3fo0d7cf5t) and (inuktitut, 2, a2qnqd7m8zfbo) are the same in 1292 ms.
Task 117: incorrectly said that (tagalog, 10, a12rfyr8dbn79) and (tagalog, 15, asu813kb7bv1) are the same in 732 ms.
Task 118: incorrectly said that (atemayar, 6, a31xqftzcsia2) and (atemayar, 12, agst3co6usf8) are the same in 1014 ms.
Task 119: correctly said that (korean, 39, a5379luit3pc) and (korean, 39, a39ivbdm1ekue) are the same in 494 ms.
Task 120: correctly said that (korean, 4, a1v0p79fynz9j) and (atemayar, 6, a1poymbentexx) are not the same in 1156 ms.
Task 121: correctly said that (sylheti, 13, avlbkm6qpb1s) and (sylheti, 13, a4858n7ggst4) are the same in 828 ms.
Task 122: correctly said that (inuktitut, 11, a2pfktghg1ofp) and (sylheti, 13, a3np21apfa18b) are not the same in 1287 ms.
Task 123: correctly said that (inuktitut, 11, aljsxt6v0a30) and (inuktitut, 3, a315xddd8j4x8) are not the same in 743 ms.
Task 124: correctly said that (tagalog, 1, amynpl5xzw6q) and (sylheti, 3, aljsxt6v0a30) are not the same in 1078 ms.
Task 125: incorrectly said that (atemayar, 12, a1poymbentexx) and (korean, 17, a1umhcrj9zfdp) are the same in 905 ms.
Task 126: correctly said that (myanmar, 8, amynpl5xzw6q) and (myanmar, 16, a1stvgjqfl5kk) are not the same in 652 ms.
Task 127: correctly said that (atemayar, 25, a6fjbvuzrhm6) and (inuktitut, 11, a3noo9k3y1yu5) are not the same in 554 ms.
Task 128: correctly said that (inuktitut, 11, a2qnqd7m8zfbo) and (myanmar, 8, ay0bjcgydhix) are not the same in 626 ms.
Task 129: correctly said that (tagalog, 10, a3mv1jtwep7xn) and (inuktitut, 3, a315xddd8j4x8) are not the same in 455 ms.
Task 130: correctly said that (myanmar, 8, a1clejhbx9yyn) and (myanmar, 34, amynpl5xzw6q) are not the same in 976 ms.
Task 131: correctly said that (myanmar, 34, a4858n7ggst4) and (myanmar, 34, a1ehrxjplsq0r) are the same in 860 ms.
Task 132: correctly said that (atemayar, 12, avlbkm6qpb1s) and (inuktitut, 3, ay0bjcgydhix) are not the same in 995 ms.
Task 133: correctly said that (sylheti, 4, a1s3njxhehcip) and (atemayar, 12, a31e6jchki335) are not the same in 768 ms.
Task 134: correctly said that (sylheti, 3, a4858n7ggst4) and (sylheti, 3, a1a25pyfoa9ge) are the same in 755 ms.
Task 135: correctly said that (inuktitut, 2, avlbkm6qpb1s) and (sylheti, 13, ay0bjcgydhix) are not the same in 562 ms.
Task 136: correctly said that (inuktitut, 3, a1z59eb5sfxpy) and (sylheti, 15, a1foggtlgo7n4) are not the same in 369 ms.
Task 137: correctly said that (myanmar, 16, a3qat9qkqumi2) and (myanmar, 16, ajf66jwen7bx) are the same in 778 ms.
Task 138: correctly said that (inuktitut, 3, a1z59eb5sfxpy) and (korean, 17, a1s3njxhehcip) are not the same in 769 ms.
Task 139: correctly said that (atemayar, 12, aljsxt6v0a30) and (atemayar, 6, a3m0kt6kjezd1) are not the same in 999 ms.
Task 140: correctly said that (inuktitut, 12, a1manxzopzden) and (korean, 4, aj6upe9mlarg) are not the same in 977 ms.
Task 141: correctly said that (korean, 3, a1s3njxhehcip) and (inuktitut, 11, ae0ibwt7asth) are not the same in 633 ms.
Task 142: correctly said that (sylheti, 13, a1foggtlgo7n4) and (tagalog, 10, ay0bjcgydhix) are not the same in 568 ms.
Task 143: correctly said that (atemayar, 10, avlbkm6qpb1s) and (atemayar, 10, a1poymbentexx) are the same in 795 ms.
Task 144: correctly said that (atemayar, 10, a1z59eb5sfxpy) and (inuktitut, 2, a2ernxe6jlm4x) are not the same in 539 ms.
Task 145: correctly said that (korean, 17, a1umhcrj9zfdp) and (myanmar, 4, ay0bjcgydhix) are not the same in 617 ms.
Task 146: correctly said that (tagalog, 6, a1ehrxjplsq0r) and (atemayar, 25, a31xqftzcsia2) are not the same in 563 ms.
Task 147: correctly said that (atemayar, 25, awl8tng8e81d) and (tagalog, 6, a1i92aacrkacw) are not the same in 446 ms.
Task 148: correctly said that (sylheti, 3, a3209gno9duil) and (inuktitut, 11, a1j8s7giyto4a) are not the same in 406 ms.
Task 149: correctly said that (atemayar, 6, a6fjbvuzrhm6) and (korean, 3, a1s3njxhehcip) are not the same in 704 ms.
Task 150: incorrectly said that (korean, 4, a2px0aywxngce) and (korean, 4, a2nm1qzuh14ls) are not the same in 1151 ms.
Task 151: correctly said that (sylheti, 3, a1qku9mccvl35) and (atemayar, 6, a1manxzopzden) are not the same in 582 ms.
Task 152: correctly said that (korean, 3, a2px0aywxngce) and (korean, 17, aj6upe9mlarg) are not the same in 601 ms.
Task 153: correctly said that (atemayar, 25, a1z59eb5sfxpy) and (tagalog, 6, a1i92aacrkacw) are not the same in 456 ms.
Task 154: correctly said that (sylheti, 13, a1foggtlgo7n4) and (tagalog, 10, a2du3880jbrov) are not the same in 400 ms.
Task 155: correctly said that (korean, 3, a2nm1qzuh14ls) and (sylheti, 4, avlbkm6qpb1s) are not the same in 451 ms.
Task 156: correctly said that (atemayar, 6, a31xqftzcsia2) and (sylheti, 15, a3r9q9wqm1nyz) are not the same in 464 ms.
Task 157: correctly said that (korean, 17, aav4pzxdoebd) and (korean, 17, aw82oruamauz) are the same in 933 ms.
Task 158: correctly said that (inuktitut, 12, a2ernxe6jlm4x) and (inuktitut, 2, a12rfyr8dbn79) are not the same in 612 ms.
Task 159: correctly said that (tagalog, 15, ay0bjcgydhix) and (inuktitut, 2, ay0bjcgydhix) are not the same in 448 ms.
Task 160: correctly said that (sylheti, 4, a1l2fzyn31zvf) and (sylheti, 13, a1qku9mccvl35) are not the same in 744 ms.
Task 161: correctly said that (tagalog, 1, a1ehrxjplsq0r) and (tagalog, 15, a2du3880jbrov) are not the same in 633 ms.
Task 162: correctly said that (sylheti, 3, a1j8s7giyto4a) and (myanmar, 8, aljsxt6v0a30) are not the same in 493 ms.
Task 163: correctly said that (sylheti, 3, a3r9q9wqm1nyz) and (inuktitut, 11, a1w3fo0d7cf5t) are not the same in 440 ms.
Task 164: correctly said that (atemayar, 10, azdw6062ia38) and (inuktitut, 11, a2pfktghg1ofp) are not the same in 838 ms.
Task 165: correctly said that (myanmar, 34, a1foggtlgo7n4) and (atemayar, 12, a1yovlluv1c9h) are not the same in 1046 ms.
Task 166: correctly said that (korean, 17, a1s3njxhehcip) and (inuktitut, 2, a12rfyr8dbn79) are not the same in 524 ms.
Task 167: correctly said that (tagalog, 10, avlbkm6qpb1s) and (korean, 39, a3bb99mn3zy37) are not the same in 797 ms.
Task 168: correctly said that (atemayar, 10, a1yovlluv1c9h) and (atemayar, 12, agst3co6usf8) are not the same in 1381 ms.
Task 169: correctly said that (sylheti, 4, atvyxlwe5n03) and (myanmar, 34, a1foggtlgo7n4) are not the same in 559 ms.
Task 170: correctly said that (tagalog, 10, amynpl5xzw6q) and (tagalog, 10, aljsxt6v0a30) are the same in 919 ms.
Task 171: correctly said that (sylheti, 3, a4858n7ggst4) and (sylheti, 3, aljsxt6v0a30) are the same in 996 ms.
Task 172: correctly said that (sylheti, 3, a4858n7ggst4) and (sylheti, 13, a3r9q9wqm1nyz) are not the same in 722 ms.
Task 173: correctly said that (sylheti, 13, a3r9q9wqm1nyz) and (sylheti, 13, aljsxt6v0a30) are the same in 1085 ms.
Task 174: correctly said that (atemayar, 10, a1z59eb5sfxpy) and (korean, 3, aav4pzxdoebd) are not the same in 1519 ms.
Task 175: correctly said that (tagalog, 10, a1ehrxjplsq0r) and (korean, 39, a2ernxe6jlm4x) are not the same in 563 ms.
Task 176: correctly said that (inuktitut, 3, ae0ibwt7asth) and (inuktitut, 3, a12xm86d2nbih) are the same in 820 ms.
Task 177: correctly said that (tagalog, 6, a3qat9qkqumi2) and (tagalog, 6, aljsxt6v0a30) are the same in 730 ms.
Task 178: correctly said that (tagalog, 10, a1l2fzyn31zvf) and (tagalog, 6, a2du3880jbrov) are not the same in 589 ms.
Task 179: correctly said that (myanmar, 16, a1stvgjqfl5kk) and (sylheti, 3, a30xq7555spo8) are not the same in 495 ms.
Task 180: correctly said that (inuktitut, 3, a1manxzopzden) and (inuktitut, 2, a12xm86d2nbih) are not the same in 801 ms.
Task 181: correctly said that (myanmar, 16, a2pfktghg1ofp) and (myanmar, 16, a1ehrxjplsq0r) are the same in 1209 ms.
Task 182: correctly said that (tagalog, 1, aljsxt6v0a30) and (tagalog, 1, a2du3880jbrov) are the same in 1773 ms.
Task 183: correctly said that (sylheti, 13, a1j8s7giyto4a) and (myanmar, 4, a1umhcrj9zfdp) are not the same in 742 ms.
Task 184: correctly said that (tagalog, 6, a12rfyr8dbn79) and (myanmar, 34, a1foggtlgo7n4) are not the same in 679 ms.
Task 185: correctly said that (korean, 17, a1udqv4pfypkn) and (atemayar, 25, a6fjbvuzrhm6) are not the same in 834 ms.
Task 186: correctly said that (korean, 4, a1umhcrj9zfdp) and (korean, 39, a2px0aywxngce) are not the same in 641 ms.
Task 187: correctly said that (sylheti, 4, a1qku9mccvl35) and (sylheti, 4, a1j8s7giyto4a) are the same in 925 ms.
Task 188: correctly said that (atemayar, 10, ae0ibwt7asth) and (inuktitut, 11, aljsxt6v0a30) are not the same in 469 ms.
Task 189: correctly said that (inuktitut, 2, ay0bjcgydhix) and (inuktitut, 11, a1s3njxhehcip) are not the same in 473 ms.
Task 190: correctly said that (inuktitut, 3, a2ernxe6jlm4x) and (inuktitut, 3, a3noo9k3y1yu5) are the same in 841 ms.
Task 191: correctly said that (inuktitut, 11, a2ernxe6jlm4x) and (inuktitut, 11, aljsxt6v0a30) are the same in 1028 ms.
Task 192: correctly said that (inuktitut, 3, a1manxzopzden) and (myanmar, 8, aljsxt6v0a30) are not the same in 894 ms.
Task 193: correctly said that (korean, 4, a3dfh6dpi11ip) and (myanmar, 4, a1umhcrj9zfdp) are not the same in 1607 ms.
Task 194: incorrectly said that (atemayar, 10, a2pfktghg1ofp) and (atemayar, 25, a31e6jchki335) are the same in 1001 ms.
Task 195: correctly said that (inuktitut, 11, avlbkm6qpb1s) and (korean, 3, a39ivbdm1ekue) are not the same in 762 ms.
Task 196: correctly said that (inuktitut, 12, a1z59eb5sfxpy) and (myanmar, 34, a2pfktghg1ofp) are not the same in 578 ms.
Task 197: correctly said that (korean, 4, a1v0p79fynz9j) and (sylheti, 15, a1l2fzyn31zvf) are not the same in 497 ms.
Task 198: correctly said that (atemayar, 6, a1yovlluv1c9h) and (korean, 17, a2nm1qzuh14ls) are not the same in 544 ms.
Task 199: correctly said that (tagalog, 15, a12rfyr8dbn79) and (korean, 4, a1v0p79fynz9j) are not the same in 663 ms.

Duration: 15m 36s 600ms
Comments: 