

Summary:Short Summary:
Correct: 164
Incorrect: 36
Percent Correct: 82

Same Correct: 30
Same Incorrect: 24
Percent Same Correct: 55

Different Correct: 134
Different Incorrect: 12
Percent Different Correct: 91

Long Summary:
Task 0: incorrectly said that (atemayar, 12, a1vzhyp8yvxkz) and (atemayar, 12, a23psx2z4f65q) are not the same in 3979 ms.
Task 1: correctly said that (sylheti, 15, a3np21apfa18b) and (sylheti, 4, a3209gno9duil) are not the same in 2876 ms.
Task 2: correctly said that (myanmar, 34, a1ehrxjplsq0r) and (atemayar, 10, agst3co6usf8) are not the same in 6943 ms.
Task 3: incorrectly said that (tagalog, 1, a1yovlluv1c9h) and (korean, 17, a2px0aywxngce) are the same in 1437 ms.
Task 4: correctly said that (sylheti, 4, a3m0kt6kjezd1) and (myanmar, 16, a1j8s7giyto4a) are not the same in 2757 ms.
Task 5: incorrectly said that (atemayar, 10, a31xqftzcsia2) and (korean, 17, a2px0aywxngce) are the same in 1334 ms.
Task 6: incorrectly said that (atemayar, 6, a1poymbentexx) and (atemayar, 12, a3m0kt6kjezd1) are the same in 1321 ms.
Task 7: correctly said that (korean, 4, a1oh82lwr5hbg) and (sylheti, 15, a6fjbvuzrhm6) are not the same in 1860 ms.
Task 8: correctly said that (korean, 4, a3d7xwp0gand5) and (tagalog, 15, a1j8s7giyto4a) are not the same in 1371 ms.
Task 9: correctly said that (sylheti, 13, a2du3880jbrov) and (korean, 39, aav4pzxdoebd) are not the same in 4334 ms.
Task 10: correctly said that (sylheti, 4, aljsxt6v0a30) and (korean, 4, a1v0p79fynz9j) are not the same in 2597 ms.
Task 11: correctly said that (korean, 17, aw82oruamauz) and (korean, 17, a3dfh6dpi11ip) are the same in 1111 ms.
Task 12: correctly said that (korean, 4, a1udqv4pfypkn) and (korean, 4, a3d7xwp0gand5) are the same in 1702 ms.
Task 13: correctly said that (sylheti, 3, amynpl5xzw6q) and (tagalog, 10, a1s3njxhehcip) are not the same in 1913 ms.
Task 14: correctly said that (korean, 39, aw82oruamauz) and (korean, 39, a39ivbdm1ekue) are the same in 1762 ms.
Task 15: correctly said that (myanmar, 4, a1j8s7giyto4a) and (myanmar, 4, adc8vbkoocrr) are the same in 2662 ms.
Task 16: incorrectly said that (inuktitut, 3, a1qku9mccvl35) and (inuktitut, 3, a1w3fo0d7cf5t) are not the same in 2175 ms.
Task 17: incorrectly said that (tagalog, 15, amynpl5xzw6q) and (myanmar, 4, a1foggtlgo7n4) are the same in 1480 ms.
Task 18: incorrectly said that (atemayar, 6, a1qku9mccvl35) and (atemayar, 6, a31e6jchki335) are not the same in 1389 ms.
Task 19: incorrectly said that (atemayar, 10, a1manxzopzden) and (korean, 17, a3mv1jtwep7xn) are the same in 1185 ms.
Task 20: correctly said that (atemayar, 25, ax5gghhj0o6g) and (myanmar, 34, a4858n7ggst4) are not the same in 1325 ms.
Task 21: correctly said that (atemayar, 25, a6fjbvuzrhm6) and (atemayar, 6, a1j8s7giyto4a) are not the same in 1344 ms.
Task 22: correctly said that (korean, 4, a2ernxe6jlm4x) and (inuktitut, 11, a3noo9k3y1yu5) are not the same in 1482 ms.
Task 23: correctly said that (atemayar, 10, a31e6jchki335) and (sylheti, 13, a1j8s7giyto4a) are not the same in 1498 ms.
Task 24: correctly said that (inuktitut, 2, a1qku9mccvl35) and (tagalog, 6, a1a25pyfoa9ge) are not the same in 1421 ms.
Task 25: correctly said that (tagalog, 10, amynpl5xzw6q) and (atemayar, 10, a1j8s7giyto4a) are not the same in 1170 ms.
Task 26: correctly said that (inuktitut, 11, a12rfyr8dbn79) and (inuktitut, 11, a1z59eb5sfxpy) are the same in 1512 ms.
Task 27: correctly said that (myanmar, 4, a2pfktghg1ofp) and (myanmar, 34, a1clejhbx9yyn) are not the same in 1607 ms.
Task 28: correctly said that (sylheti, 3, ay0bjcgydhix) and (sylheti, 3, amynpl5xzw6q) are the same in 2020 ms.
Task 29: correctly said that (korean, 39, a39ivbdm1ekue) and (sylheti, 13, a3209gno9duil) are not the same in 1509 ms.
Task 30: correctly said that (tagalog, 6, a12rfyr8dbn79) and (tagalog, 6, ay0bjcgydhix) are the same in 1235 ms.
Task 31: correctly said that (sylheti, 15, a3209gno9duil) and (tagalog, 1, a3qat9qkqumi2) are not the same in 1062 ms.
Task 32: correctly said that (korean, 3, a1s3njxhehcip) and (atemayar, 25, a1poymbentexx) are not the same in 1142 ms.
Task 33: incorrectly said that (myanmar, 16, a1s3njxhehcip) and (myanmar, 16, a1clejhbx9yyn) are not the same in 1771 ms.
Task 34: correctly said that (tagalog, 10, a3m0kt6kjezd1) and (myanmar, 4, a1clejhbx9yyn) are not the same in 1210 ms.
Task 35: correctly said that (sylheti, 15, aljsxt6v0a30) and (korean, 17, a3mv1jtwep7xn) are not the same in 1451 ms.
Task 36: correctly said that (myanmar, 34, avlbkm6qpb1s) and (inuktitut, 11, aljsxt6v0a30) are not the same in 1060 ms.
Task 37: correctly said that (myanmar, 16, amynpl5xzw6q) and (sylheti, 3, a1l2fzyn31zvf) are not the same in 1455 ms.
Task 38: correctly said that (sylheti, 15, a3m0kt6kjezd1) and (tagalog, 6, asu813kb7bv1) are not the same in 1133 ms.
Task 39: incorrectly said that (atemayar, 10, a23psx2z4f65q) and (korean, 17, ae0ibwt7asth) are the same in 1673 ms.
Task 40: incorrectly said that (tagalog, 15, ajf66jwen7bx) and (tagalog, 15, a3mv1jtwep7xn) are not the same in 983 ms.
Task 41: incorrectly said that (atemayar, 12, a1poymbentexx) and (atemayar, 12, azdw6062ia38) are not the same in 1175 ms.
Task 42: correctly said that (inuktitut, 11, a2pfktghg1ofp) and (myanmar, 16, amynpl5xzw6q) are not the same in 1571 ms.
Task 43: correctly said that (korean, 39, aw82oruamauz) and (myanmar, 16, a2pfktghg1ofp) are not the same in 1140 ms.
Task 44: incorrectly said that (korean, 17, aav4pzxdoebd) and (korean, 17, a2nm1qzuh14ls) are not the same in 1337 ms.
Task 45: correctly said that (korean, 17, aav4pzxdoebd) and (korean, 17, aw82oruamauz) are the same in 1362 ms.
Task 46: incorrectly said that (korean, 3, aav4pzxdoebd) and (korean, 17, aw82oruamauz) are the same in 1313 ms.
Task 47: correctly said that (atemayar, 6, a3m0kt6kjezd1) and (atemayar, 25, azdw6062ia38) are not the same in 1153 ms.
Task 48: correctly said that (tagalog, 1, a12rfyr8dbn79) and (atemayar, 6, a31xqftzcsia2) are not the same in 1234 ms.
Task 49: correctly said that (korean, 39, a5379luit3pc) and (inuktitut, 3, a3noo9k3y1yu5) are not the same in 1400 ms.
Task 50: incorrectly said that (sylheti, 4, a1qku9mccvl35) and (sylheti, 4, ay0bjcgydhix) are not the same in 1340 ms.
Task 51: correctly said that (myanmar, 4, a6fjbvuzrhm6) and (korean, 4, a2akud1gdstsd) are not the same in 1007 ms.
Task 52: correctly said that (inuktitut, 2, a1manxzopzden) and (sylheti, 13, a1j8s7giyto4a) are not the same in 1634 ms.
Task 53: incorrectly said that (korean, 4, a2akud1gdstsd) and (atemayar, 12, a1vzhyp8yvxkz) are the same in 1228 ms.
Task 54: incorrectly said that (tagalog, 10, a1l2fzyn31zvf) and (tagalog, 10, aghrkle9vf6z) are not the same in 1169 ms.
Task 55: correctly said that (inuktitut, 12, a1manxzopzden) and (myanmar, 4, a3pkun3onkucn) are not the same in 1496 ms.
Task 56: correctly said that (myanmar, 16, a1umhcrj9zfdp) and (atemayar, 25, a1j8s7giyto4a) are not the same in 997 ms.
Task 57: correctly said that (sylheti, 4, a2du3880jbrov) and (sylheti, 13, aljsxt6v0a30) are not the same in 802 ms.
Task 58: incorrectly said that (myanmar, 8, a3pkun3onkucn) and (myanmar, 8, a1ehrxjplsq0r) are not the same in 815 ms.
Task 59: correctly said that (korean, 39, a2px0aywxngce) and (tagalog, 1, a1a25pyfoa9ge) are not the same in 1201 ms.
Task 60: correctly said that (sylheti, 15, amynpl5xzw6q) and (inuktitut, 2, a315xddd8j4x8) are not the same in 986 ms.
Task 61: correctly said that (sylheti, 4, aljsxt6v0a30) and (inuktitut, 3, a1manxzopzden) are not the same in 1080 ms.
Task 62: correctly said that (korean, 3, aj6upe9mlarg) and (atemayar, 25, awl8tng8e81d) are not the same in 895 ms.
Task 63: correctly said that (inuktitut, 11, ae0ibwt7asth) and (tagalog, 10, ay0bjcgydhix) are not the same in 1228 ms.
Task 64: correctly said that (inuktitut, 3, aljsxt6v0a30) and (atemayar, 12, avlbkm6qpb1s) are not the same in 884 ms.
Task 65: correctly said that (atemayar, 6, aljsxt6v0a30) and (korean, 39, a2ernxe6jlm4x) are not the same in 921 ms.
Task 66: correctly said that (korean, 4, aj6upe9mlarg) and (korean, 4, a1umhcrj9zfdp) are the same in 877 ms.
Task 67: correctly said that (inuktitut, 12, a1qku9mccvl35) and (myanmar, 34, a3j1pp96x14u0) are not the same in 876 ms.
Task 68: correctly said that (sylheti, 13, a3np21apfa18b) and (inuktitut, 2, avlbkm6qpb1s) are not the same in 736 ms.
Task 69: correctly said that (inuktitut, 2, a1j8s7giyto4a) and (myanmar, 34, a3j1pp96x14u0) are not the same in 755 ms.
Task 70: incorrectly said that (sylheti, 4, avlbkm6qpb1s) and (sylheti, 4, a1l2fzyn31zvf) are not the same in 1392 ms.
Task 71: correctly said that (myanmar, 34, a3j1pp96x14u0) and (korean, 17, a2akud1gdstsd) are not the same in 800 ms.
Task 72: correctly said that (korean, 3, a2nm1qzuh14ls) and (korean, 3, a2akud1gdstsd) are the same in 1252 ms.
Task 73: correctly said that (myanmar, 34, ay0bjcgydhix) and (sylheti, 15, a1j8s7giyto4a) are not the same in 1194 ms.
Task 74: correctly said that (myanmar, 34, a3qat9qkqumi2) and (korean, 17, a5379luit3pc) are not the same in 1010 ms.
Task 75: correctly said that (tagalog, 6, a1s3njxhehcip) and (inuktitut, 11, a2qnqd7m8zfbo) are not the same in 1723 ms.
Task 76: correctly said that (inuktitut, 12, a1z59eb5sfxpy) and (inuktitut, 12, a1manxzopzden) are the same in 1442 ms.
Task 77: incorrectly said that (atemayar, 6, azdw6062ia38) and (atemayar, 6, a3m0kt6kjezd1) are not the same in 959 ms.
Task 78: incorrectly said that (atemayar, 12, a1manxzopzden) and (atemayar, 12, a1j8s7giyto4a) are not the same in 1697 ms.
Task 79: correctly said that (inuktitut, 12, a3mv1jtwep7xn) and (sylheti, 4, a4858n7ggst4) are not the same in 985 ms.
Task 80: correctly said that (myanmar, 34, a1s3njxhehcip) and (atemayar, 25, a1manxzopzden) are not the same in 1386 ms.
Task 81: correctly said that (sylheti, 13, a3pkun3onkucn) and (myanmar, 8, aljsxt6v0a30) are not the same in 1052 ms.
Task 82: incorrectly said that (korean, 39, a1v0p79fynz9j) and (sylheti, 15, aljsxt6v0a30) are the same in 1252 ms.
Task 83: correctly said that (atemayar, 25, a31xqftzcsia2) and (korean, 39, a2px0aywxngce) are not the same in 1241 ms.
Task 84: correctly said that (tagalog, 6, a1a25pyfoa9ge) and (atemayar, 6, a1qku9mccvl35) are not the same in 1132 ms.
Task 85: incorrectly said that (atemayar, 12, awl8tng8e81d) and (atemayar, 12, a1yovlluv1c9h) are not the same in 1057 ms.
Task 86: correctly said that (sylheti, 15, a3pkun3onkucn) and (inuktitut, 3, a1qku9mccvl35) are not the same in 955 ms.
Task 87: correctly said that (inuktitut, 12, a12xm86d2nbih) and (inuktitut, 11, a3noo9k3y1yu5) are not the same in 1698 ms.
Task 88: incorrectly said that (myanmar, 4, aljsxt6v0a30) and (myanmar, 4, a6fjbvuzrhm6) are not the same in 982 ms.
Task 89: incorrectly said that (myanmar, 16, a1aegunf6e081) and (myanmar, 16, avlbkm6qpb1s) are not the same in 1371 ms.
Task 90: correctly said that (atemayar, 12, ax5gghhj0o6g) and (korean, 17, a1oh82lwr5hbg) are not the same in 921 ms.
Task 91: correctly said that (atemayar, 25, a2pfktghg1ofp) and (atemayar, 25, a23psx2z4f65q) are the same in 1087 ms.
Task 92: correctly said that (atemayar, 10, a1vzhyp8yvxkz) and (myanmar, 16, a6fjbvuzrhm6) are not the same in 1066 ms.
Task 93: correctly said that (inuktitut, 2, a1qku9mccvl35) and (korean, 39, a1oh82lwr5hbg) are not the same in 1016 ms.
Task 94: correctly said that (inuktitut, 12, ae0ibwt7asth) and (inuktitut, 2, a3mv1jtwep7xn) are not the same in 1034 ms.
Task 95: correctly said that (myanmar, 8, a3qat9qkqumi2) and (sylheti, 13, a4858n7ggst4) are not the same in 1137 ms.
Task 96: correctly said that (myanmar, 16, a3j1pp96x14u0) and (atemayar, 12, avlbkm6qpb1s) are not the same in 1051 ms.
Task 97: correctly said that (myanmar, 16, a1s3njxhehcip) and (sylheti, 4, a4858n7ggst4) are not the same in 863 ms.
Task 98: correctly said that (atemayar, 10, a1z59eb5sfxpy) and (sylheti, 15, a1l2fzyn31zvf) are not the same in 965 ms.
Task 99: correctly said that (korean, 4, a2nm1qzuh14ls) and (korean, 4, a39ivbdm1ekue) are the same in 1270 ms.
Task 100: correctly said that (korean, 17, a1s3njxhehcip) and (korean, 17, a1umhcrj9zfdp) are the same in 1031 ms.
Task 101: correctly said that (sylheti, 3, a3r9q9wqm1nyz) and (myanmar, 8, a4858n7ggst4) are not the same in 902 ms.
Task 102: correctly said that (sylheti, 3, avlbkm6qpb1s) and (myanmar, 4, a3j1pp96x14u0) are not the same in 923 ms.
Task 103: correctly said that (tagalog, 1, aljsxt6v0a30) and (tagalog, 15, ay0bjcgydhix) are not the same in 742 ms.
Task 104: correctly said that (korean, 39, a1v0p79fynz9j) and (korean, 4, a2nm1qzuh14ls) are not the same in 685 ms.
Task 105: correctly said that (korean, 39, a1manxzopzden) and (myanmar, 34, a1ehrxjplsq0r) are not the same in 1478 ms.
Task 106: correctly said that (sylheti, 13, ay0bjcgydhix) and (sylheti, 15, a6fjbvuzrhm6) are not the same in 1035 ms.
Task 107: correctly said that (korean, 39, aj6upe9mlarg) and (sylheti, 3, a3np21apfa18b) are not the same in 791 ms.
Task 108: correctly said that (tagalog, 1, aghrkle9vf6z) and (tagalog, 15, amynpl5xzw6q) are not the same in 958 ms.
Task 109: correctly said that (tagalog, 6, amynpl5xzw6q) and (tagalog, 1, ay0bjcgydhix) are not the same in 869 ms.
Task 110: correctly said that (korean, 39, a5379luit3pc) and (tagalog, 6, a1l2fzyn31zvf) are not the same in 870 ms.
Task 111: correctly said that (myanmar, 4, amynpl5xzw6q) and (sylheti, 13, a6fjbvuzrhm6) are not the same in 1080 ms.
Task 112: incorrectly said that (atemayar, 25, a1qku9mccvl35) and (korean, 17, a1manxzopzden) are the same in 1354 ms.
Task 113: correctly said that (inuktitut, 2, ae0ibwt7asth) and (tagalog, 15, a1l2fzyn31zvf) are not the same in 2107 ms.
Task 114: correctly said that (sylheti, 13, a1l2fzyn31zvf) and (inuktitut, 12, a1qku9mccvl35) are not the same in 951 ms.
Task 115: correctly said that (atemayar, 10, a3m0kt6kjezd1) and (myanmar, 16, a1s3njxhehcip) are not the same in 1117 ms.
Task 116: correctly said that (korean, 3, a1udqv4pfypkn) and (inuktitut, 11, ae0ibwt7asth) are not the same in 1019 ms.
Task 117: correctly said that (sylheti, 15, a4858n7ggst4) and (sylheti, 15, a1foggtlgo7n4) are the same in 1078 ms.
Task 118: correctly said that (atemayar, 25, agst3co6usf8) and (sylheti, 3, avlbkm6qpb1s) are not the same in 963 ms.
Task 119: correctly said that (myanmar, 16, a1umhcrj9zfdp) and (tagalog, 6, ajf66jwen7bx) are not the same in 1106 ms.
Task 120: correctly said that (sylheti, 13, a1l2fzyn31zvf) and (sylheti, 13, amynpl5xzw6q) are the same in 1368 ms.
Task 121: incorrectly said that (inuktitut, 12, a315xddd8j4x8) and (inuktitut, 12, a12rfyr8dbn79) are not the same in 769 ms.
Task 122: correctly said that (myanmar, 34, a1stvgjqfl5kk) and (atemayar, 25, agst3co6usf8) are not the same in 951 ms.
Task 123: correctly said that (tagalog, 10, a1i92aacrkacw) and (atemayar, 25, a1j8s7giyto4a) are not the same in 1145 ms.
Task 124: correctly said that (myanmar, 16, ajf66jwen7bx) and (tagalog, 6, ajf66jwen7bx) are not the same in 995 ms.
Task 125: correctly said that (korean, 17, aav4pzxdoebd) and (myanmar, 4, a1j8s7giyto4a) are not the same in 1063 ms.
Task 126: correctly said that (korean, 3, aj6upe9mlarg) and (korean, 3, a2nm1qzuh14ls) are the same in 832 ms.
Task 127: incorrectly said that (tagalog, 1, a1j8s7giyto4a) and (korean, 39, a1s3njxhehcip) are the same in 1218 ms.
Task 128: incorrectly said that (korean, 4, a3mv1jtwep7xn) and (atemayar, 12, a23psx2z4f65q) are the same in 1228 ms.
Task 129: incorrectly said that (sylheti, 4, aljsxt6v0a30) and (sylheti, 4, amynpl5xzw6q) are not the same in 924 ms.
Task 130: correctly said that (korean, 39, a1oh82lwr5hbg) and (atemayar, 12, a31xqftzcsia2) are not the same in 1371 ms.
Task 131: correctly said that (korean, 17, a39ivbdm1ekue) and (myanmar, 8, a3qat9qkqumi2) are not the same in 952 ms.
Task 132: correctly said that (myanmar, 16, amynpl5xzw6q) and (inuktitut, 3, a12rfyr8dbn79) are not the same in 691 ms.
Task 133: correctly said that (tagalog, 15, a2du3880jbrov) and (korean, 17, a1udqv4pfypkn) are not the same in 1295 ms.
Task 134: correctly said that (tagalog, 1, a3mv1jtwep7xn) and (sylheti, 3, a1foggtlgo7n4) are not the same in 914 ms.
Task 135: correctly said that (sylheti, 3, a30xq7555spo8) and (sylheti, 3, avlbkm6qpb1s) are the same in 918 ms.
Task 136: correctly said that (atemayar, 12, a2pfktghg1ofp) and (sylheti, 15, avlbkm6qpb1s) are not the same in 944 ms.
Task 137: correctly said that (atemayar, 25, a3m0kt6kjezd1) and (atemayar, 25, a2pfktghg1ofp) are the same in 1202 ms.
Task 138: correctly said that (sylheti, 13, a6fjbvuzrhm6) and (inuktitut, 2, ay0bjcgydhix) are not the same in 851 ms.
Task 139: incorrectly said that (tagalog, 10, a1j8s7giyto4a) and (tagalog, 10, aghrkle9vf6z) are not the same in 1012 ms.
Task 140: correctly said that (myanmar, 34, a1ehrxjplsq0r) and (korean, 4, a1oh82lwr5hbg) are not the same in 938 ms.
Task 141: correctly said that (atemayar, 12, avlbkm6qpb1s) and (sylheti, 15, amynpl5xzw6q) are not the same in 1539 ms.
Task 142: correctly said that (inuktitut, 12, avlbkm6qpb1s) and (tagalog, 1, aljsxt6v0a30) are not the same in 1260 ms.
Task 143: correctly said that (tagalog, 15, ay0bjcgydhix) and (atemayar, 6, awl8tng8e81d) are not the same in 1003 ms.
Task 144: correctly said that (inuktitut, 11, a1s3njxhehcip) and (inuktitut, 12, a1s3njxhehcip) are not the same in 976 ms.
Task 145: correctly said that (tagalog, 6, a1l2fzyn31zvf) and (myanmar, 16, amynpl5xzw6q) are not the same in 964 ms.
Task 146: correctly said that (tagalog, 1, a1a25pyfoa9ge) and (atemayar, 12, a31xqftzcsia2) are not the same in 1090 ms.
Task 147: correctly said that (inuktitut, 12, a2pgu4pi93b1q) and (inuktitut, 12, a2pfktghg1ofp) are the same in 1095 ms.
Task 148: correctly said that (myanmar, 34, adc8vbkoocrr) and (korean, 39, a1oh82lwr5hbg) are not the same in 725 ms.
Task 149: correctly said that (myanmar, 4, a1clejhbx9yyn) and (korean, 3, aw82oruamauz) are not the same in 1045 ms.
Task 150: correctly said that (tagalog, 1, a1i92aacrkacw) and (myanmar, 8, a4858n7ggst4) are not the same in 956 ms.
Task 151: correctly said that (myanmar, 34, a1s3njxhehcip) and (myanmar, 34, a3pkun3onkucn) are the same in 887 ms.
Task 152: correctly said that (tagalog, 6, ae0ibwt7asth) and (korean, 3, aj6upe9mlarg) are not the same in 951 ms.
Task 153: correctly said that (sylheti, 13, a3m0kt6kjezd1) and (sylheti, 13, a3pkun3onkucn) are the same in 922 ms.
Task 154: correctly said that (atemayar, 12, awl8tng8e81d) and (myanmar, 4, a1s3njxhehcip) are not the same in 1040 ms.
Task 155: correctly said that (inuktitut, 12, a12rfyr8dbn79) and (inuktitut, 12, a2ernxe6jlm4x) are the same in 1005 ms.
Task 156: correctly said that (myanmar, 4, a3pkun3onkucn) and (tagalog, 15, a3qat9qkqumi2) are not the same in 1068 ms.
Task 157: correctly said that (atemayar, 25, avlbkm6qpb1s) and (myanmar, 16, a1clejhbx9yyn) are not the same in 768 ms.
Task 158: correctly said that (tagalog, 10, a1j8s7giyto4a) and (sylheti, 15, a1a25pyfoa9ge) are not the same in 1554 ms.
Task 159: correctly said that (sylheti, 13, a1foggtlgo7n4) and (tagalog, 15, ajf66jwen7bx) are not the same in 959 ms.
Task 160: correctly said that (sylheti, 4, a3r9q9wqm1nyz) and (inuktitut, 11, a2ernxe6jlm4x) are not the same in 1367 ms.
Task 161: correctly said that (atemayar, 10, a1j8s7giyto4a) and (atemayar, 10, a3m0kt6kjezd1) are the same in 1181 ms.
Task 162: correctly said that (korean, 3, ae0ibwt7asth) and (atemayar, 25, a31xqftzcsia2) are not the same in 1064 ms.
Task 163: correctly said that (inuktitut, 11, a2pfktghg1ofp) and (sylheti, 15, a1a25pyfoa9ge) are not the same in 1131 ms.
Task 164: correctly said that (myanmar, 16, a3j1pp96x14u0) and (atemayar, 12, ax5gghhj0o6g) are not the same in 1083 ms.
Task 165: correctly said that (atemayar, 10, a1manxzopzden) and (tagalog, 10, ajf66jwen7bx) are not the same in 965 ms.
Task 166: incorrectly said that (tagalog, 1, a3mv1jtwep7xn) and (tagalog, 1, a1l2fzyn31zvf) are not the same in 1024 ms.
Task 167: correctly said that (inuktitut, 12, a1qku9mccvl35) and (atemayar, 25, avlbkm6qpb1s) are not the same in 1348 ms.
Task 168: correctly said that (tagalog, 1, ajf66jwen7bx) and (myanmar, 8, a1s3njxhehcip) are not the same in 1028 ms.
Task 169: incorrectly said that (sylheti, 3, a1qku9mccvl35) and (sylheti, 3, a1j8s7giyto4a) are not the same in 1778 ms.
Task 170: correctly said that (inuktitut, 11, a1manxzopzden) and (korean, 39, a1s3njxhehcip) are not the same in 1183 ms.
Task 171: correctly said that (korean, 4, aav4pzxdoebd) and (tagalog, 1, avlbkm6qpb1s) are not the same in 1173 ms.
Task 172: correctly said that (tagalog, 15, a2du3880jbrov) and (atemayar, 10, a1poymbentexx) are not the same in 943 ms.
Task 173: correctly said that (tagalog, 15, a1a25pyfoa9ge) and (tagalog, 15, a1ehrxjplsq0r) are the same in 983 ms.
Task 174: correctly said that (inuktitut, 12, a2qnqd7m8zfbo) and (atemayar, 12, a2pfktghg1ofp) are not the same in 764 ms.
Task 175: correctly said that (korean, 4, a2nm1qzuh14ls) and (tagalog, 10, a3qat9qkqumi2) are not the same in 765 ms.
Task 176: correctly said that (tagalog, 6, a3m0kt6kjezd1) and (inuktitut, 11, a2qnqd7m8zfbo) are not the same in 963 ms.
Task 177: correctly said that (myanmar, 34, a6fjbvuzrhm6) and (myanmar, 34, a1j8s7giyto4a) are the same in 1253 ms.
Task 178: correctly said that (atemayar, 6, a6fjbvuzrhm6) and (tagalog, 10, a1l2fzyn31zvf) are not the same in 791 ms.
Task 179: correctly said that (tagalog, 10, a1a25pyfoa9ge) and (tagalog, 15, aljsxt6v0a30) are not the same in 1021 ms.
Task 180: correctly said that (inuktitut, 3, aljsxt6v0a30) and (myanmar, 4, a3pkun3onkucn) are not the same in 1261 ms.
Task 181: incorrectly said that (myanmar, 8, adc8vbkoocrr) and (myanmar, 8, a4858n7ggst4) are not the same in 859 ms.
Task 182: correctly said that (myanmar, 16, ay0bjcgydhix) and (myanmar, 34, a1aegunf6e081) are not the same in 1358 ms.
Task 183: correctly said that (korean, 3, aav4pzxdoebd) and (korean, 3, a1manxzopzden) are the same in 1307 ms.
Task 184: incorrectly said that (atemayar, 12, aljsxt6v0a30) and (atemayar, 12, a1vzhyp8yvxkz) are not the same in 808 ms.
Task 185: correctly said that (tagalog, 15, a3qat9qkqumi2) and (korean, 17, a2nm1qzuh14ls) are not the same in 1093 ms.
Task 186: correctly said that (inuktitut, 2, a2ernxe6jlm4x) and (korean, 3, a2nm1qzuh14ls) are not the same in 1021 ms.
Task 187: correctly said that (tagalog, 10, a3m0kt6kjezd1) and (korean, 4, a39ivbdm1ekue) are not the same in 1200 ms.
Task 188: correctly said that (korean, 4, a1s3njxhehcip) and (inuktitut, 12, a2qnqd7m8zfbo) are not the same in 971 ms.
Task 189: correctly said that (korean, 39, aw82oruamauz) and (inuktitut, 11, a315xddd8j4x8) are not the same in 1097 ms.
Task 190: correctly said that (korean, 4, a2px0aywxngce) and (atemayar, 25, awl8tng8e81d) are not the same in 954 ms.
Task 191: correctly said that (inuktitut, 2, a12rfyr8dbn79) and (atemayar, 10, a1z59eb5sfxpy) are not the same in 931 ms.
Task 192: correctly said that (inuktitut, 2, a1w3fo0d7cf5t) and (inuktitut, 2, a1j8s7giyto4a) are the same in 1199 ms.
Task 193: correctly said that (korean, 3, a3bb99mn3zy37) and (korean, 3, a1udqv4pfypkn) are the same in 1642 ms.
Task 194: incorrectly said that (inuktitut, 11, a1j8s7giyto4a) and (inuktitut, 11, a3qat9qkqumi2) are not the same in 989 ms.
Task 195: correctly said that (korean, 4, a2ernxe6jlm4x) and (korean, 39, a3bb99mn3zy37) are not the same in 996 ms.
Task 196: correctly said that (atemayar, 6, a1z59eb5sfxpy) and (atemayar, 25, a1manxzopzden) are not the same in 1085 ms.
Task 197: correctly said that (sylheti, 13, a1foggtlgo7n4) and (atemayar, 12, a1qku9mccvl35) are not the same in 788 ms.
Task 198: correctly said that (atemayar, 25, a1vzhyp8yvxkz) and (korean, 3, a39ivbdm1ekue) are not the same in 1285 ms.
Task 199: correctly said that (sylheti, 3, a1foggtlgo7n4) and (sylheti, 3, a30xq7555spo8) are the same in 1108 ms.

Duration: 22m 9s 345ms
Comments: Thanks. It was very interesting. Please inform me when you have your hit  next time.