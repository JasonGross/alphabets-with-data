

Summary:Short Summary:
Correct: 131
Incorrect: 69
Percent Correct: 65

Same Correct: 14
Same Incorrect: 39
Percent Same Correct: 26

Different Correct: 117
Different Incorrect: 30
Percent Different Correct: 79

Long Summary:
Task 0: correctly said that (tagalog, 15, avlbkm6qpb1s) and (tagalog, 1, a1a25pyfoa9ge) are not the same in 17226 ms.
Task 1: correctly said that (inuktitut, 2, avlbkm6qpb1s) and (atemayar, 25, agst3co6usf8) are not the same in 11688 ms.
Task 2: correctly said that (atemayar, 10, azdw6062ia38) and (tagalog, 1, ae0ibwt7asth) are not the same in 1348 ms.
Task 3: correctly said that (atemayar, 12, azdw6062ia38) and (atemayar, 25, azdw6062ia38) are not the same in 1398 ms.
Task 4: correctly said that (atemayar, 12, awl8tng8e81d) and (sylheti, 15, a4858n7ggst4) are not the same in 1611 ms.
Task 5: correctly said that (inuktitut, 11, ay0bjcgydhix) and (atemayar, 6, azdw6062ia38) are not the same in 894 ms.
Task 6: incorrectly said that (korean, 3, a2nm1qzuh14ls) and (korean, 3, a2px0aywxngce) are not the same in 1307 ms.
Task 7: correctly said that (myanmar, 8, a1umhcrj9zfdp) and (tagalog, 10, a3qat9qkqumi2) are not the same in 1116 ms.
Task 8: correctly said that (sylheti, 13, a30xq7555spo8) and (myanmar, 8, a3qat9qkqumi2) are not the same in 1331 ms.
Task 9: correctly said that (korean, 3, a3bb99mn3zy37) and (myanmar, 4, a1aegunf6e081) are not the same in 1403 ms.
Task 10: correctly said that (tagalog, 10, ajf66jwen7bx) and (korean, 4, a1s3njxhehcip) are not the same in 7155 ms.
Task 11: correctly said that (inuktitut, 11, a1z59eb5sfxpy) and (myanmar, 8, amynpl5xzw6q) are not the same in 4760 ms.
Task 12: incorrectly said that (tagalog, 1, a1yovlluv1c9h) and (tagalog, 1, a12rfyr8dbn79) are not the same in 5689 ms.
Task 13: correctly said that (tagalog, 1, a1l2fzyn31zvf) and (inuktitut, 11, a2qnqd7m8zfbo) are not the same in 3840 ms.
Task 14: correctly said that (atemayar, 10, a23psx2z4f65q) and (sylheti, 4, a3pkun3onkucn) are not the same in 2267 ms.
Task 15: correctly said that (tagalog, 6, ay0bjcgydhix) and (sylheti, 15, a3np21apfa18b) are not the same in 2638 ms.
Task 16: correctly said that (tagalog, 6, aghrkle9vf6z) and (atemayar, 10, a23psx2z4f65q) are not the same in 2940 ms.
Task 17: incorrectly said that (myanmar, 16, ajf66jwen7bx) and (myanmar, 16, adc8vbkoocrr) are not the same in 3115 ms.
Task 18: incorrectly said that (tagalog, 1, a1s3njxhehcip) and (tagalog, 1, a1yovlluv1c9h) are not the same in 5259 ms.
Task 19: correctly said that (myanmar, 34, a4858n7ggst4) and (atemayar, 12, agst3co6usf8) are not the same in 2109 ms.
Task 20: incorrectly said that (inuktitut, 2, ae0ibwt7asth) and (korean, 3, aav4pzxdoebd) are the same in 5821 ms.
Task 21: incorrectly said that (tagalog, 6, a1j8s7giyto4a) and (korean, 4, a2ernxe6jlm4x) are the same in 4754 ms.
Task 22: correctly said that (sylheti, 15, a3pkun3onkucn) and (sylheti, 15, a3m0kt6kjezd1) are the same in 4972 ms.
Task 23: correctly said that (atemayar, 12, ae0ibwt7asth) and (korean, 17, a1umhcrj9zfdp) are not the same in 4871 ms.
Task 24: incorrectly said that (myanmar, 16, a1aegunf6e081) and (sylheti, 13, a3r9q9wqm1nyz) are the same in 3433 ms.
Task 25: correctly said that (inuktitut, 2, a1qku9mccvl35) and (korean, 4, aj6upe9mlarg) are not the same in 3595 ms.
Task 26: correctly said that (tagalog, 10, a1ehrxjplsq0r) and (tagalog, 15, ae0ibwt7asth) are not the same in 4663 ms.
Task 27: correctly said that (myanmar, 34, aljsxt6v0a30) and (tagalog, 10, asu813kb7bv1) are not the same in 4208 ms.
Task 28: incorrectly said that (atemayar, 25, a1z59eb5sfxpy) and (sylheti, 3, avlbkm6qpb1s) are the same in 6719 ms.
Task 29: correctly said that (inuktitut, 12, ay0bjcgydhix) and (myanmar, 16, a3qat9qkqumi2) are not the same in 11224 ms.
Task 30: correctly said that (sylheti, 13, a1s3njxhehcip) and (sylheti, 3, ay0bjcgydhix) are not the same in 7453 ms.
Task 31: correctly said that (sylheti, 15, a1foggtlgo7n4) and (myanmar, 16, a2pfktghg1ofp) are not the same in 16940 ms.
Task 32: incorrectly said that (sylheti, 15, a3209gno9duil) and (sylheti, 15, avlbkm6qpb1s) are not the same in 4068 ms.
Task 33: correctly said that (tagalog, 6, ae0ibwt7asth) and (myanmar, 34, a1aegunf6e081) are not the same in 2271 ms.
Task 34: correctly said that (korean, 17, a39ivbdm1ekue) and (tagalog, 10, a1ehrxjplsq0r) are not the same in 5253 ms.
Task 35: correctly said that (tagalog, 15, a3m0kt6kjezd1) and (korean, 4, a2ernxe6jlm4x) are not the same in 4536 ms.
Task 36: correctly said that (korean, 3, aj6upe9mlarg) and (korean, 3, ae0ibwt7asth) are the same in 7114 ms.
Task 37: correctly said that (myanmar, 4, a3qat9qkqumi2) and (sylheti, 3, a3pkun3onkucn) are not the same in 3561 ms.
Task 38: incorrectly said that (tagalog, 15, a1yovlluv1c9h) and (sylheti, 3, a6fjbvuzrhm6) are the same in 3243 ms.
Task 39: incorrectly said that (atemayar, 12, agst3co6usf8) and (atemayar, 12, a1yovlluv1c9h) are not the same in 11452 ms.
Task 40: correctly said that (inuktitut, 11, a1qku9mccvl35) and (myanmar, 34, a6fjbvuzrhm6) are not the same in 7485 ms.
Task 41: correctly said that (inuktitut, 3, a1s3njxhehcip) and (inuktitut, 3, a12rfyr8dbn79) are the same in 2684 ms.
Task 42: incorrectly said that (inuktitut, 12, a1j8s7giyto4a) and (inuktitut, 12, ae0ibwt7asth) are not the same in 5922 ms.
Task 43: incorrectly said that (inuktitut, 2, a12rfyr8dbn79) and (inuktitut, 2, aljsxt6v0a30) are not the same in 6711 ms.
Task 44: correctly said that (inuktitut, 2, a1manxzopzden) and (sylheti, 3, a1qku9mccvl35) are not the same in 17256 ms.
Task 45: incorrectly said that (sylheti, 15, a1s3njxhehcip) and (tagalog, 6, a1i92aacrkacw) are the same in 2697 ms.
Task 46: incorrectly said that (korean, 39, a1s3njxhehcip) and (inuktitut, 3, ae0ibwt7asth) are the same in 2106 ms.
Task 47: incorrectly said that (sylheti, 13, a1foggtlgo7n4) and (tagalog, 10, ajf66jwen7bx) are the same in 2610 ms.
Task 48: incorrectly said that (sylheti, 3, a2du3880jbrov) and (myanmar, 34, amynpl5xzw6q) are the same in 2575 ms.
Task 49: incorrectly said that (atemayar, 6, a23psx2z4f65q) and (korean, 17, a2ernxe6jlm4x) are the same in 8829 ms.
Task 50: incorrectly said that (atemayar, 10, aljsxt6v0a30) and (atemayar, 10, a1z59eb5sfxpy) are not the same in 5260 ms.
Task 51: correctly said that (sylheti, 3, aljsxt6v0a30) and (korean, 39, a1umhcrj9zfdp) are not the same in 8700 ms.
Task 52: correctly said that (atemayar, 25, a23psx2z4f65q) and (sylheti, 4, a1j8s7giyto4a) are not the same in 5589 ms.
Task 53: correctly said that (atemayar, 25, a3m0kt6kjezd1) and (korean, 3, a1oh82lwr5hbg) are not the same in 4191 ms.
Task 54: correctly said that (atemayar, 10, a6fjbvuzrhm6) and (tagalog, 1, a1l2fzyn31zvf) are not the same in 10856 ms.
Task 55: correctly said that (atemayar, 6, agst3co6usf8) and (inuktitut, 11, a1s3njxhehcip) are not the same in 3859 ms.
Task 56: correctly said that (inuktitut, 3, a12xm86d2nbih) and (korean, 17, a1v0p79fynz9j) are not the same in 5059 ms.
Task 57: correctly said that (sylheti, 13, a30xq7555spo8) and (sylheti, 13, a4858n7ggst4) are the same in 14792 ms.
Task 58: correctly said that (atemayar, 12, ae0ibwt7asth) and (atemayar, 12, a1j8s7giyto4a) are the same in 7226 ms.
Task 59: incorrectly said that (korean, 3, a3d7xwp0gand5) and (korean, 3, a1manxzopzden) are not the same in 9963 ms.
Task 60: correctly said that (inuktitut, 12, a12rfyr8dbn79) and (myanmar, 8, a3pkun3onkucn) are not the same in 7867 ms.
Task 61: incorrectly said that (inuktitut, 11, a2pgu4pi93b1q) and (inuktitut, 11, a315xddd8j4x8) are not the same in 8530 ms.
Task 62: correctly said that (myanmar, 4, a1foggtlgo7n4) and (myanmar, 4, a1aegunf6e081) are the same in 4567 ms.
Task 63: correctly said that (atemayar, 6, ax5gghhj0o6g) and (sylheti, 4, a1l2fzyn31zvf) are not the same in 9033 ms.
Task 64: incorrectly said that (tagalog, 15, a1j8s7giyto4a) and (sylheti, 3, a1a25pyfoa9ge) are the same in 8027 ms.
Task 65: incorrectly said that (sylheti, 4, a6fjbvuzrhm6) and (sylheti, 4, amynpl5xzw6q) are not the same in 4120 ms.
Task 66: incorrectly said that (sylheti, 4, a1l2fzyn31zvf) and (inuktitut, 11, a2ernxe6jlm4x) are the same in 4332 ms.
Task 67: correctly said that (atemayar, 10, a23psx2z4f65q) and (inuktitut, 2, a1manxzopzden) are not the same in 13644 ms.
Task 68: correctly said that (inuktitut, 12, a1qku9mccvl35) and (korean, 3, a2akud1gdstsd) are not the same in 15484 ms.
Task 69: correctly said that (korean, 17, a3bb99mn3zy37) and (sylheti, 3, atvyxlwe5n03) are not the same in 16107 ms.
Task 70: correctly said that (myanmar, 16, a1clejhbx9yyn) and (tagalog, 10, a1yovlluv1c9h) are not the same in 4813 ms.
Task 71: correctly said that (tagalog, 6, avlbkm6qpb1s) and (inuktitut, 2, a3noo9k3y1yu5) are not the same in 38742 ms.
Task 72: correctly said that (sylheti, 3, amynpl5xzw6q) and (sylheti, 3, a1s3njxhehcip) are the same in 7847 ms.
Task 73: incorrectly said that (sylheti, 4, a1qku9mccvl35) and (sylheti, 13, a3209gno9duil) are the same in 2407 ms.
Task 74: correctly said that (inuktitut, 3, a1qku9mccvl35) and (myanmar, 16, a2pfktghg1ofp) are not the same in 5436 ms.
Task 75: incorrectly said that (inuktitut, 11, a1s3njxhehcip) and (inuktitut, 11, ay0bjcgydhix) are not the same in 5456 ms.
Task 76: correctly said that (inuktitut, 2, a1manxzopzden) and (tagalog, 10, a1s3njxhehcip) are not the same in 7998 ms.
Task 77: incorrectly said that (tagalog, 10, a12rfyr8dbn79) and (tagalog, 10, a1s3njxhehcip) are not the same in 6342 ms.
Task 78: correctly said that (myanmar, 8, aljsxt6v0a30) and (myanmar, 34, ay0bjcgydhix) are not the same in 9890 ms.
Task 79: correctly said that (myanmar, 4, a3pkun3onkucn) and (tagalog, 6, ae0ibwt7asth) are not the same in 17007 ms.
Task 80: correctly said that (tagalog, 15, a3mv1jtwep7xn) and (tagalog, 15, ay0bjcgydhix) are the same in 9298 ms.
Task 81: correctly said that (inuktitut, 3, a3noo9k3y1yu5) and (inuktitut, 12, a2qnqd7m8zfbo) are not the same in 4483 ms.
Task 82: correctly said that (inuktitut, 11, a2pfktghg1ofp) and (inuktitut, 11, a1qku9mccvl35) are the same in 2913 ms.
Task 83: incorrectly said that (atemayar, 12, a1z59eb5sfxpy) and (korean, 17, a1umhcrj9zfdp) are the same in 3662 ms.
Task 84: correctly said that (atemayar, 6, a31xqftzcsia2) and (atemayar, 25, awl8tng8e81d) are not the same in 4834 ms.
Task 85: incorrectly said that (tagalog, 10, a3m0kt6kjezd1) and (korean, 17, a3dfh6dpi11ip) are the same in 3083 ms.
Task 86: correctly said that (korean, 39, a1v0p79fynz9j) and (myanmar, 8, a1foggtlgo7n4) are not the same in 6123 ms.
Task 87: correctly said that (tagalog, 6, ay0bjcgydhix) and (inuktitut, 3, avlbkm6qpb1s) are not the same in 7987 ms.
Task 88: correctly said that (inuktitut, 2, a2pfktghg1ofp) and (inuktitut, 12, a3mv1jtwep7xn) are not the same in 6194 ms.
Task 89: correctly said that (inuktitut, 3, a1manxzopzden) and (myanmar, 8, a2pfktghg1ofp) are not the same in 11970 ms.
Task 90: correctly said that (sylheti, 13, a6fjbvuzrhm6) and (atemayar, 6, a1yovlluv1c9h) are not the same in 2860 ms.
Task 91: correctly said that (atemayar, 10, a1vzhyp8yvxkz) and (sylheti, 15, a3pkun3onkucn) are not the same in 5654 ms.
Task 92: correctly said that (inuktitut, 2, ay0bjcgydhix) and (myanmar, 4, a3j1pp96x14u0) are not the same in 5549 ms.
Task 93: correctly said that (tagalog, 15, a3qat9qkqumi2) and (myanmar, 8, a1umhcrj9zfdp) are not the same in 3316 ms.
Task 94: correctly said that (korean, 4, a3mv1jtwep7xn) and (tagalog, 15, asu813kb7bv1) are not the same in 4391 ms.
Task 95: correctly said that (sylheti, 13, a1foggtlgo7n4) and (sylheti, 13, a2du3880jbrov) are the same in 11374 ms.
Task 96: correctly said that (tagalog, 10, amynpl5xzw6q) and (sylheti, 3, a1s3njxhehcip) are not the same in 5735 ms.
Task 97: correctly said that (tagalog, 15, a1ehrxjplsq0r) and (atemayar, 12, a31xqftzcsia2) are not the same in 11818 ms.
Task 98: correctly said that (inuktitut, 2, a3mv1jtwep7xn) and (sylheti, 15, atvyxlwe5n03) are not the same in 11541 ms.
Task 99: correctly said that (atemayar, 12, azdw6062ia38) and (sylheti, 3, a1a25pyfoa9ge) are not the same in 7711 ms.
Task 100: correctly said that (myanmar, 8, aljsxt6v0a30) and (sylheti, 4, aljsxt6v0a30) are not the same in 4001 ms.
Task 101: incorrectly said that (korean, 39, a1udqv4pfypkn) and (korean, 39, a5379luit3pc) are not the same in 4055 ms.
Task 102: correctly said that (sylheti, 15, a1j8s7giyto4a) and (inuktitut, 3, a1z59eb5sfxpy) are not the same in 10638 ms.
Task 103: correctly said that (inuktitut, 11, a1j8s7giyto4a) and (korean, 17, a3d7xwp0gand5) are not the same in 4329 ms.
Task 104: correctly said that (atemayar, 6, a2pfktghg1ofp) and (korean, 4, ae0ibwt7asth) are not the same in 4549 ms.
Task 105: incorrectly said that (tagalog, 1, a3mv1jtwep7xn) and (tagalog, 1, a1l2fzyn31zvf) are not the same in 5830 ms.
Task 106: correctly said that (atemayar, 6, a3m0kt6kjezd1) and (tagalog, 15, a3qat9qkqumi2) are not the same in 3385 ms.
Task 107: incorrectly said that (tagalog, 6, a1ehrxjplsq0r) and (tagalog, 6, a1s3njxhehcip) are not the same in 5349 ms.
Task 108: correctly said that (tagalog, 10, a3qat9qkqumi2) and (inuktitut, 11, a12xm86d2nbih) are not the same in 4592 ms.
Task 109: correctly said that (tagalog, 6, ajf66jwen7bx) and (atemayar, 12, a1yovlluv1c9h) are not the same in 8987 ms.
Task 110: incorrectly said that (atemayar, 6, a31e6jchki335) and (myanmar, 4, a1clejhbx9yyn) are the same in 3111 ms.
Task 111: incorrectly said that (atemayar, 25, a1z59eb5sfxpy) and (atemayar, 25, a31e6jchki335) are not the same in 6020 ms.
Task 112: correctly said that (sylheti, 4, ay0bjcgydhix) and (korean, 17, a2akud1gdstsd) are not the same in 8016 ms.
Task 113: correctly said that (inuktitut, 3, avlbkm6qpb1s) and (atemayar, 25, a23psx2z4f65q) are not the same in 11014 ms.
Task 114: correctly said that (tagalog, 10, amynpl5xzw6q) and (myanmar, 16, adc8vbkoocrr) are not the same in 38657 ms.
Task 115: incorrectly said that (tagalog, 15, amynpl5xzw6q) and (tagalog, 15, a2du3880jbrov) are not the same in 2554 ms.
Task 116: correctly said that (inuktitut, 3, a3qat9qkqumi2) and (tagalog, 1, a1j8s7giyto4a) are not the same in 5241 ms.
Task 117: correctly said that (inuktitut, 3, a1s3njxhehcip) and (korean, 17, ae0ibwt7asth) are not the same in 8717 ms.
Task 118: correctly said that (korean, 4, a1v0p79fynz9j) and (sylheti, 4, a3r9q9wqm1nyz) are not the same in 5266 ms.
Task 119: correctly said that (korean, 4, a3dfh6dpi11ip) and (myanmar, 16, avlbkm6qpb1s) are not the same in 5389 ms.
Task 120: incorrectly said that (sylheti, 15, a3209gno9duil) and (myanmar, 4, ay0bjcgydhix) are the same in 2729 ms.
Task 121: correctly said that (korean, 3, a39ivbdm1ekue) and (tagalog, 15, ay0bjcgydhix) are not the same in 9479 ms.
Task 122: incorrectly said that (sylheti, 15, a3np21apfa18b) and (atemayar, 10, a6fjbvuzrhm6) are the same in 4305 ms.
Task 123: correctly said that (atemayar, 10, a1vzhyp8yvxkz) and (atemayar, 25, a1poymbentexx) are not the same in 2693 ms.
Task 124: correctly said that (korean, 4, a1manxzopzden) and (myanmar, 34, a3j1pp96x14u0) are not the same in 5308 ms.
Task 125: correctly said that (myanmar, 16, a1ehrxjplsq0r) and (tagalog, 15, a12rfyr8dbn79) are not the same in 6414 ms.
Task 126: incorrectly said that (sylheti, 13, avlbkm6qpb1s) and (sylheti, 13, a3np21apfa18b) are not the same in 4852 ms.
Task 127: incorrectly said that (tagalog, 6, amynpl5xzw6q) and (tagalog, 6, avlbkm6qpb1s) are not the same in 9165 ms.
Task 128: correctly said that (korean, 17, a3d7xwp0gand5) and (atemayar, 25, a1vzhyp8yvxkz) are not the same in 6975 ms.
Task 129: correctly said that (sylheti, 4, a1l2fzyn31zvf) and (tagalog, 1, a1ehrxjplsq0r) are not the same in 4101 ms.
Task 130: incorrectly said that (myanmar, 8, a1aegunf6e081) and (myanmar, 8, a4858n7ggst4) are not the same in 3669 ms.
Task 131: correctly said that (inuktitut, 12, a1z59eb5sfxpy) and (myanmar, 34, a1foggtlgo7n4) are not the same in 3509 ms.
Task 132: incorrectly said that (inuktitut, 12, a1s3njxhehcip) and (inuktitut, 12, a2pgu4pi93b1q) are not the same in 5591 ms.
Task 133: incorrectly said that (inuktitut, 3, a2ernxe6jlm4x) and (korean, 17, a2ernxe6jlm4x) are the same in 5880 ms.
Task 134: incorrectly said that (sylheti, 4, a1j8s7giyto4a) and (sylheti, 4, a1s3njxhehcip) are not the same in 5272 ms.
Task 135: correctly said that (sylheti, 15, a1qku9mccvl35) and (sylheti, 15, a1j8s7giyto4a) are the same in 4160 ms.
Task 136: incorrectly said that (myanmar, 8, a3pkun3onkucn) and (sylheti, 3, a1s3njxhehcip) are the same in 11319 ms.
Task 137: correctly said that (tagalog, 15, a1s3njxhehcip) and (sylheti, 4, a1j8s7giyto4a) are not the same in 4468 ms.
Task 138: correctly said that (tagalog, 10, avlbkm6qpb1s) and (myanmar, 4, a1umhcrj9zfdp) are not the same in 2439 ms.
Task 139: correctly said that (myanmar, 34, a1umhcrj9zfdp) and (myanmar, 34, a6fjbvuzrhm6) are the same in 7546 ms.
Task 140: correctly said that (sylheti, 4, a1s3njxhehcip) and (sylheti, 15, a3209gno9duil) are not the same in 4094 ms.
Task 141: incorrectly said that (atemayar, 12, a1poymbentexx) and (atemayar, 12, awl8tng8e81d) are not the same in 5965 ms.
Task 142: correctly said that (myanmar, 4, aljsxt6v0a30) and (korean, 39, aj6upe9mlarg) are not the same in 6032 ms.
Task 143: correctly said that (tagalog, 10, ajf66jwen7bx) and (atemayar, 12, a1vzhyp8yvxkz) are not the same in 4179 ms.
Task 144: correctly said that (inuktitut, 12, a1qku9mccvl35) and (sylheti, 15, ay0bjcgydhix) are not the same in 3006 ms.
Task 145: incorrectly said that (atemayar, 25, a1qku9mccvl35) and (tagalog, 1, a2du3880jbrov) are the same in 3742 ms.
Task 146: correctly said that (inuktitut, 3, a3noo9k3y1yu5) and (sylheti, 3, a4858n7ggst4) are not the same in 6966 ms.
Task 147: correctly said that (korean, 3, a2px0aywxngce) and (korean, 4, a5379luit3pc) are not the same in 6448 ms.
Task 148: correctly said that (myanmar, 8, a1s3njxhehcip) and (korean, 17, a1manxzopzden) are not the same in 3951 ms.
Task 149: correctly said that (tagalog, 15, ajf66jwen7bx) and (tagalog, 10, a1ehrxjplsq0r) are not the same in 7401 ms.
Task 150: incorrectly said that (korean, 3, a1s3njxhehcip) and (korean, 3, aj6upe9mlarg) are not the same in 3591 ms.
Task 151: incorrectly said that (sylheti, 3, a3r9q9wqm1nyz) and (sylheti, 3, a6fjbvuzrhm6) are not the same in 5557 ms.
Task 152: incorrectly said that (myanmar, 34, a3pkun3onkucn) and (inuktitut, 2, a2ernxe6jlm4x) are the same in 4294 ms.
Task 153: incorrectly said that (sylheti, 3, a3np21apfa18b) and (sylheti, 3, a6fjbvuzrhm6) are not the same in 2912 ms.
Task 154: correctly said that (myanmar, 4, a2pfktghg1ofp) and (sylheti, 3, aljsxt6v0a30) are not the same in 4325 ms.
Task 155: correctly said that (myanmar, 4, a3j1pp96x14u0) and (inuktitut, 11, a1s3njxhehcip) are not the same in 4279 ms.
Task 156: incorrectly said that (sylheti, 3, aljsxt6v0a30) and (sylheti, 3, avlbkm6qpb1s) are not the same in 5672 ms.
Task 157: correctly said that (atemayar, 12, agst3co6usf8) and (myanmar, 8, a1j8s7giyto4a) are not the same in 3578 ms.
Task 158: incorrectly said that (myanmar, 34, ay0bjcgydhix) and (myanmar, 34, a1aegunf6e081) are not the same in 5790 ms.
Task 159: incorrectly said that (atemayar, 12, ax5gghhj0o6g) and (tagalog, 6, ajf66jwen7bx) are the same in 9708 ms.
Task 160: correctly said that (inuktitut, 12, a12xm86d2nbih) and (myanmar, 34, a1stvgjqfl5kk) are not the same in 6630 ms.
Task 161: correctly said that (inuktitut, 3, ae0ibwt7asth) and (tagalog, 1, a1j8s7giyto4a) are not the same in 7797 ms.
Task 162: correctly said that (tagalog, 6, amynpl5xzw6q) and (myanmar, 8, a1clejhbx9yyn) are not the same in 3306 ms.
Task 163: correctly said that (myanmar, 34, a1umhcrj9zfdp) and (myanmar, 16, a1stvgjqfl5kk) are not the same in 4049 ms.
Task 164: correctly said that (inuktitut, 3, a2qnqd7m8zfbo) and (myanmar, 8, a1clejhbx9yyn) are not the same in 2650 ms.
Task 165: incorrectly said that (atemayar, 12, aljsxt6v0a30) and (myanmar, 4, a3pkun3onkucn) are the same in 11216 ms.
Task 166: correctly said that (tagalog, 6, a3mv1jtwep7xn) and (myanmar, 16, amynpl5xzw6q) are not the same in 11419 ms.
Task 167: correctly said that (sylheti, 3, a3r9q9wqm1nyz) and (sylheti, 15, a3pkun3onkucn) are not the same in 3831 ms.
Task 168: incorrectly said that (atemayar, 12, avlbkm6qpb1s) and (inuktitut, 2, a3qat9qkqumi2) are the same in 4515 ms.
Task 169: correctly said that (inuktitut, 3, a3mv1jtwep7xn) and (sylheti, 4, a6fjbvuzrhm6) are not the same in 9883 ms.
Task 170: correctly said that (sylheti, 13, aljsxt6v0a30) and (myanmar, 8, a1clejhbx9yyn) are not the same in 7399 ms.
Task 171: incorrectly said that (tagalog, 10, asu813kb7bv1) and (tagalog, 10, a3qat9qkqumi2) are not the same in 5955 ms.
Task 172: incorrectly said that (myanmar, 16, a3pkun3onkucn) and (inuktitut, 11, a315xddd8j4x8) are the same in 8211 ms.
Task 173: incorrectly said that (tagalog, 1, a3mv1jtwep7xn) and (myanmar, 4, amynpl5xzw6q) are the same in 5322 ms.
Task 174: incorrectly said that (atemayar, 10, a1yovlluv1c9h) and (atemayar, 10, a1j8s7giyto4a) are not the same in 4415 ms.
Task 175: correctly said that (sylheti, 3, a1s3njxhehcip) and (korean, 3, ae0ibwt7asth) are not the same in 11763 ms.
Task 176: correctly said that (inuktitut, 3, a1z59eb5sfxpy) and (myanmar, 8, a2pfktghg1ofp) are not the same in 7101 ms.
Task 177: incorrectly said that (inuktitut, 11, ay0bjcgydhix) and (inuktitut, 3, a315xddd8j4x8) are the same in 2068 ms.
Task 178: correctly said that (tagalog, 6, aljsxt6v0a30) and (inuktitut, 3, avlbkm6qpb1s) are not the same in 6142 ms.
Task 179: correctly said that (sylheti, 13, a3209gno9duil) and (atemayar, 6, a2pfktghg1ofp) are not the same in 9408 ms.
Task 180: correctly said that (korean, 4, aj6upe9mlarg) and (korean, 39, a3dfh6dpi11ip) are not the same in 5674 ms.
Task 181: correctly said that (atemayar, 25, a6fjbvuzrhm6) and (myanmar, 34, a3j1pp96x14u0) are not the same in 6622 ms.
Task 182: incorrectly said that (tagalog, 6, avlbkm6qpb1s) and (tagalog, 6, a12rfyr8dbn79) are not the same in 3535 ms.
Task 183: correctly said that (inuktitut, 12, a12xm86d2nbih) and (inuktitut, 2, a1manxzopzden) are not the same in 2269 ms.
Task 184: correctly said that (inuktitut, 3, a1w3fo0d7cf5t) and (inuktitut, 3, a2qnqd7m8zfbo) are the same in 6477 ms.
Task 185: correctly said that (sylheti, 13, a30xq7555spo8) and (atemayar, 12, a1z59eb5sfxpy) are not the same in 4800 ms.
Task 186: incorrectly said that (inuktitut, 2, a2ernxe6jlm4x) and (inuktitut, 2, ae0ibwt7asth) are not the same in 3914 ms.
Task 187: incorrectly said that (myanmar, 8, a4858n7ggst4) and (tagalog, 10, a3mv1jtwep7xn) are the same in 5196 ms.
Task 188: incorrectly said that (tagalog, 6, ajf66jwen7bx) and (tagalog, 6, a1j8s7giyto4a) are not the same in 3133 ms.
Task 189: incorrectly said that (myanmar, 4, a1j8s7giyto4a) and (inuktitut, 2, a12xm86d2nbih) are the same in 1883 ms.
Task 190: correctly said that (inuktitut, 2, a1w3fo0d7cf5t) and (inuktitut, 2, a2pfktghg1ofp) are the same in 1203 ms.
Task 191: correctly said that (sylheti, 4, a3m0kt6kjezd1) and (korean, 3, aj6upe9mlarg) are not the same in 3471 ms.
Task 192: correctly said that (inuktitut, 11, a2ernxe6jlm4x) and (atemayar, 25, awl8tng8e81d) are not the same in 2357 ms.
Task 193: incorrectly said that (korean, 4, a1udqv4pfypkn) and (korean, 4, a2px0aywxngce) are not the same in 3706 ms.
Task 194: incorrectly said that (sylheti, 4, a1s3njxhehcip) and (sylheti, 4, a3np21apfa18b) are not the same in 3548 ms.
Task 195: correctly said that (korean, 17, a2akud1gdstsd) and (myanmar, 16, a1umhcrj9zfdp) are not the same in 8224 ms.
Task 196: incorrectly said that (atemayar, 25, a1manxzopzden) and (atemayar, 25, a6fjbvuzrhm6) are not the same in 4194 ms.
Task 197: incorrectly said that (sylheti, 3, a3209gno9duil) and (sylheti, 3, ay0bjcgydhix) are not the same in 3463 ms.
Task 198: correctly said that (tagalog, 15, amynpl5xzw6q) and (sylheti, 3, avlbkm6qpb1s) are not the same in 9792 ms.
Task 199: correctly said that (inuktitut, 2, a1z59eb5sfxpy) and (inuktitut, 11, a3qat9qkqumi2) are not the same in 7906 ms.

Duration: 50m 29s 539ms
Comments: 