

Summary:Short Summary:
Correct: 176
Incorrect: 24
Percent Correct: 88

Same Correct: 42
Same Incorrect: 16
Percent Same Correct: 72

Different Correct: 134
Different Incorrect: 8
Percent Different Correct: 94

Long Summary:
Task 0: correctly said that (tagalog, 10, aljsxt6v0a30) and (sylheti, 3, a1a25pyfoa9ge) are not the same in 2003 ms.
Task 1: correctly said that (atemayar, 25, a6fjbvuzrhm6) and (tagalog, 15, a3qat9qkqumi2) are not the same in 1619 ms.
Task 2: correctly said that (myanmar, 4, a3qat9qkqumi2) and (sylheti, 15, a3r9q9wqm1nyz) are not the same in 1343 ms.
Task 3: incorrectly said that (inuktitut, 11, a2ernxe6jlm4x) and (inuktitut, 11, a3noo9k3y1yu5) are not the same in 2238 ms.
Task 4: incorrectly said that (tagalog, 6, a1s3njxhehcip) and (tagalog, 6, a3qat9qkqumi2) are the same in 1567 ms.
Task 5: incorrectly said that (korean, 3, a2ernxe6jlm4x) and (korean, 3, a1manxzopzden) are not the same in 1575 ms.
Task 6: correctly said that (atemayar, 6, a1qku9mccvl35) and (inuktitut, 11, a315xddd8j4x8) are not the same in 1486 ms.
Task 7: correctly said that (myanmar, 4, a3pkun3onkucn) and (korean, 4, a2ernxe6jlm4x) are not the same in 1589 ms.
Task 8: correctly said that (tagalog, 1, avlbkm6qpb1s) and (sylheti, 3, a3r9q9wqm1nyz) are not the same in 2061 ms.
Task 9: incorrectly said that (inuktitut, 2, a3noo9k3y1yu5) and (inuktitut, 2, avlbkm6qpb1s) are not the same in 1341 ms.
Task 10: correctly said that (atemayar, 10, azdw6062ia38) and (sylheti, 15, a1j8s7giyto4a) are not the same in 1434 ms.
Task 11: correctly said that (myanmar, 34, a3pkun3onkucn) and (atemayar, 25, a1manxzopzden) are not the same in 1381 ms.
Task 12: correctly said that (tagalog, 1, a3mv1jtwep7xn) and (tagalog, 1, a1s3njxhehcip) are the same in 2299 ms.
Task 13: correctly said that (myanmar, 8, a1umhcrj9zfdp) and (inuktitut, 2, aljsxt6v0a30) are not the same in 1277 ms.
Task 14: correctly said that (korean, 39, a1v0p79fynz9j) and (sylheti, 13, avlbkm6qpb1s) are not the same in 2829 ms.
Task 15: correctly said that (inuktitut, 12, a12rfyr8dbn79) and (inuktitut, 12, a1qku9mccvl35) are the same in 1468 ms.
Task 16: correctly said that (inuktitut, 12, ay0bjcgydhix) and (inuktitut, 12, a3noo9k3y1yu5) are the same in 1246 ms.
Task 17: incorrectly said that (myanmar, 16, a1clejhbx9yyn) and (myanmar, 16, a6fjbvuzrhm6) are not the same in 1623 ms.
Task 18: incorrectly said that (sylheti, 15, a1a25pyfoa9ge) and (sylheti, 15, ay0bjcgydhix) are not the same in 1788 ms.
Task 19: correctly said that (korean, 4, a1umhcrj9zfdp) and (atemayar, 6, a1qku9mccvl35) are not the same in 1291 ms.
Task 20: correctly said that (sylheti, 3, a1qku9mccvl35) and (korean, 4, a1udqv4pfypkn) are not the same in 1536 ms.
Task 21: correctly said that (atemayar, 12, awl8tng8e81d) and (tagalog, 10, a1j8s7giyto4a) are not the same in 1550 ms.
Task 22: correctly said that (atemayar, 6, a3m0kt6kjezd1) and (atemayar, 6, a1j8s7giyto4a) are the same in 2050 ms.
Task 23: correctly said that (inuktitut, 12, a1s3njxhehcip) and (inuktitut, 12, a1j8s7giyto4a) are the same in 1068 ms.
Task 24: correctly said that (inuktitut, 12, a2pgu4pi93b1q) and (sylheti, 13, a1l2fzyn31zvf) are not the same in 1146 ms.
Task 25: incorrectly said that (korean, 4, a2nm1qzuh14ls) and (inuktitut, 2, a3mv1jtwep7xn) are the same in 5588 ms.
Task 26: correctly said that (tagalog, 10, ajf66jwen7bx) and (korean, 39, a1s3njxhehcip) are not the same in 1039 ms.
Task 27: incorrectly said that (myanmar, 16, a1j8s7giyto4a) and (myanmar, 4, ajf66jwen7bx) are the same in 2157 ms.
Task 28: correctly said that (korean, 17, a1udqv4pfypkn) and (sylheti, 4, a3pkun3onkucn) are not the same in 1661 ms.
Task 29: correctly said that (atemayar, 10, agst3co6usf8) and (sylheti, 13, a3np21apfa18b) are not the same in 1204 ms.
Task 30: correctly said that (myanmar, 34, a1s3njxhehcip) and (myanmar, 34, a3j1pp96x14u0) are the same in 1370 ms.
Task 31: correctly said that (korean, 17, a3d7xwp0gand5) and (sylheti, 4, a6fjbvuzrhm6) are not the same in 1540 ms.
Task 32: correctly said that (sylheti, 13, aljsxt6v0a30) and (sylheti, 4, a6fjbvuzrhm6) are not the same in 2248 ms.
Task 33: incorrectly said that (atemayar, 25, ae0ibwt7asth) and (atemayar, 25, a3m0kt6kjezd1) are not the same in 1576 ms.
Task 34: correctly said that (sylheti, 15, ay0bjcgydhix) and (sylheti, 13, a3pkun3onkucn) are not the same in 1051 ms.
Task 35: correctly said that (tagalog, 1, a12rfyr8dbn79) and (inuktitut, 11, ay0bjcgydhix) are not the same in 1188 ms.
Task 36: correctly said that (inuktitut, 11, a12xm86d2nbih) and (inuktitut, 11, a1j8s7giyto4a) are the same in 1371 ms.
Task 37: correctly said that (korean, 4, a2px0aywxngce) and (myanmar, 4, a1ehrxjplsq0r) are not the same in 1387 ms.
Task 38: correctly said that (korean, 4, a1manxzopzden) and (tagalog, 1, a1yovlluv1c9h) are not the same in 1221 ms.
Task 39: incorrectly said that (atemayar, 10, a1manxzopzden) and (atemayar, 10, a31xqftzcsia2) are not the same in 1511 ms.
Task 40: incorrectly said that (atemayar, 10, a6fjbvuzrhm6) and (atemayar, 10, ax5gghhj0o6g) are not the same in 1618 ms.
Task 41: incorrectly said that (myanmar, 16, adc8vbkoocrr) and (myanmar, 4, a3pkun3onkucn) are the same in 1507 ms.
Task 42: correctly said that (inuktitut, 12, a1qku9mccvl35) and (korean, 39, a1oh82lwr5hbg) are not the same in 1302 ms.
Task 43: correctly said that (korean, 3, ae0ibwt7asth) and (sylheti, 4, a4858n7ggst4) are not the same in 1042 ms.
Task 44: correctly said that (atemayar, 12, a1z59eb5sfxpy) and (myanmar, 16, ajf66jwen7bx) are not the same in 1309 ms.
Task 45: correctly said that (tagalog, 15, a3qat9qkqumi2) and (korean, 17, aav4pzxdoebd) are not the same in 1092 ms.
Task 46: correctly said that (tagalog, 15, a3qat9qkqumi2) and (inuktitut, 3, a3qat9qkqumi2) are not the same in 1359 ms.
Task 47: correctly said that (inuktitut, 12, a3noo9k3y1yu5) and (inuktitut, 12, a12xm86d2nbih) are the same in 1158 ms.
Task 48: correctly said that (tagalog, 1, ae0ibwt7asth) and (tagalog, 1, ajf66jwen7bx) are the same in 1278 ms.
Task 49: incorrectly said that (tagalog, 1, a3mv1jtwep7xn) and (tagalog, 1, a3m0kt6kjezd1) are not the same in 1794 ms.
Task 50: correctly said that (korean, 17, ae0ibwt7asth) and (inuktitut, 2, a1s3njxhehcip) are not the same in 1259 ms.
Task 51: correctly said that (inuktitut, 12, a1s3njxhehcip) and (inuktitut, 12, avlbkm6qpb1s) are the same in 1083 ms.
Task 52: correctly said that (sylheti, 3, ay0bjcgydhix) and (korean, 39, a2px0aywxngce) are not the same in 1545 ms.
Task 53: correctly said that (myanmar, 4, a3j1pp96x14u0) and (inuktitut, 3, avlbkm6qpb1s) are not the same in 1450 ms.
Task 54: incorrectly said that (korean, 4, ae0ibwt7asth) and (korean, 39, a1manxzopzden) are the same in 1469 ms.
Task 55: incorrectly said that (myanmar, 16, a1aegunf6e081) and (myanmar, 34, a1j8s7giyto4a) are the same in 1201 ms.
Task 56: correctly said that (inuktitut, 3, a1manxzopzden) and (myanmar, 8, a3pkun3onkucn) are not the same in 1166 ms.
Task 57: correctly said that (inuktitut, 11, a2pfktghg1ofp) and (sylheti, 4, a3np21apfa18b) are not the same in 1398 ms.
Task 58: correctly said that (sylheti, 3, a2du3880jbrov) and (myanmar, 4, a3qat9qkqumi2) are not the same in 1360 ms.
Task 59: incorrectly said that (tagalog, 10, amynpl5xzw6q) and (tagalog, 10, a3qat9qkqumi2) are not the same in 1714 ms.
Task 60: correctly said that (sylheti, 13, atvyxlwe5n03) and (tagalog, 6, avlbkm6qpb1s) are not the same in 1361 ms.
Task 61: correctly said that (korean, 3, a2px0aywxngce) and (myanmar, 34, a1ehrxjplsq0r) are not the same in 826 ms.
Task 62: incorrectly said that (atemayar, 10, a2pfktghg1ofp) and (atemayar, 10, ae0ibwt7asth) are not the same in 1361 ms.
Task 63: correctly said that (inuktitut, 11, a3noo9k3y1yu5) and (myanmar, 4, a6fjbvuzrhm6) are not the same in 1057 ms.
Task 64: correctly said that (sylheti, 15, a4858n7ggst4) and (korean, 4, a1s3njxhehcip) are not the same in 2714 ms.
Task 65: correctly said that (myanmar, 16, a4858n7ggst4) and (atemayar, 25, awl8tng8e81d) are not the same in 1180 ms.
Task 66: correctly said that (myanmar, 4, a1s3njxhehcip) and (sylheti, 15, aljsxt6v0a30) are not the same in 938 ms.
Task 67: correctly said that (myanmar, 4, avlbkm6qpb1s) and (sylheti, 3, a3r9q9wqm1nyz) are not the same in 1119 ms.
Task 68: correctly said that (myanmar, 4, a1stvgjqfl5kk) and (tagalog, 15, a1ehrxjplsq0r) are not the same in 1219 ms.
Task 69: correctly said that (myanmar, 4, ajf66jwen7bx) and (inuktitut, 3, a1w3fo0d7cf5t) are not the same in 1126 ms.
Task 70: correctly said that (sylheti, 13, amynpl5xzw6q) and (sylheti, 13, a4858n7ggst4) are the same in 1520 ms.
Task 71: correctly said that (korean, 4, a3d7xwp0gand5) and (korean, 4, a1oh82lwr5hbg) are the same in 1538 ms.
Task 72: correctly said that (sylheti, 13, a1a25pyfoa9ge) and (atemayar, 12, a3m0kt6kjezd1) are not the same in 1093 ms.
Task 73: correctly said that (atemayar, 10, a1z59eb5sfxpy) and (tagalog, 10, asu813kb7bv1) are not the same in 1271 ms.
Task 74: correctly said that (korean, 39, a39ivbdm1ekue) and (sylheti, 13, a3209gno9duil) are not the same in 1118 ms.
Task 75: correctly said that (atemayar, 10, a1j8s7giyto4a) and (atemayar, 10, a31e6jchki335) are the same in 1027 ms.
Task 76: incorrectly said that (atemayar, 6, a1z59eb5sfxpy) and (korean, 17, a1manxzopzden) are the same in 1336 ms.
Task 77: correctly said that (korean, 39, a1oh82lwr5hbg) and (myanmar, 4, a3pkun3onkucn) are not the same in 1194 ms.
Task 78: correctly said that (inuktitut, 2, a2ernxe6jlm4x) and (inuktitut, 12, aljsxt6v0a30) are not the same in 1541 ms.
Task 79: correctly said that (korean, 39, a3d7xwp0gand5) and (korean, 39, a39ivbdm1ekue) are the same in 1065 ms.
Task 80: correctly said that (atemayar, 25, a1manxzopzden) and (sylheti, 13, a3r9q9wqm1nyz) are not the same in 1018 ms.
Task 81: correctly said that (tagalog, 15, a1ehrxjplsq0r) and (inuktitut, 11, ay0bjcgydhix) are not the same in 1850 ms.
Task 82: incorrectly said that (atemayar, 6, a1qku9mccvl35) and (atemayar, 10, a1vzhyp8yvxkz) are the same in 1331 ms.
Task 83: correctly said that (korean, 4, a1oh82lwr5hbg) and (sylheti, 3, atvyxlwe5n03) are not the same in 1328 ms.
Task 84: correctly said that (atemayar, 10, a1j8s7giyto4a) and (sylheti, 15, a3np21apfa18b) are not the same in 718 ms.
Task 85: correctly said that (korean, 17, aav4pzxdoebd) and (inuktitut, 3, a2ernxe6jlm4x) are not the same in 628 ms.
Task 86: correctly said that (myanmar, 8, ay0bjcgydhix) and (inuktitut, 2, a1z59eb5sfxpy) are not the same in 652 ms.
Task 87: correctly said that (sylheti, 15, a3pkun3onkucn) and (tagalog, 6, a3m0kt6kjezd1) are not the same in 963 ms.
Task 88: correctly said that (atemayar, 25, ax5gghhj0o6g) and (atemayar, 10, a1qku9mccvl35) are not the same in 1110 ms.
Task 89: correctly said that (tagalog, 15, a1i92aacrkacw) and (tagalog, 15, avlbkm6qpb1s) are the same in 1934 ms.
Task 90: correctly said that (sylheti, 15, atvyxlwe5n03) and (sylheti, 13, a1foggtlgo7n4) are not the same in 1481 ms.
Task 91: correctly said that (korean, 3, aav4pzxdoebd) and (atemayar, 12, a1yovlluv1c9h) are not the same in 942 ms.
Task 92: correctly said that (atemayar, 12, a1yovlluv1c9h) and (sylheti, 4, a6fjbvuzrhm6) are not the same in 1183 ms.
Task 93: correctly said that (tagalog, 15, ae0ibwt7asth) and (korean, 4, a3dfh6dpi11ip) are not the same in 718 ms.
Task 94: correctly said that (atemayar, 25, awl8tng8e81d) and (atemayar, 10, a23psx2z4f65q) are not the same in 905 ms.
Task 95: correctly said that (myanmar, 34, ajf66jwen7bx) and (korean, 39, a3d7xwp0gand5) are not the same in 665 ms.
Task 96: correctly said that (myanmar, 8, amynpl5xzw6q) and (atemayar, 6, avlbkm6qpb1s) are not the same in 726 ms.
Task 97: correctly said that (atemayar, 12, agst3co6usf8) and (sylheti, 15, a3209gno9duil) are not the same in 1206 ms.
Task 98: correctly said that (atemayar, 10, a6fjbvuzrhm6) and (myanmar, 16, amynpl5xzw6q) are not the same in 1805 ms.
Task 99: correctly said that (korean, 39, a3mv1jtwep7xn) and (korean, 39, a1umhcrj9zfdp) are the same in 1130 ms.
Task 100: correctly said that (tagalog, 6, aghrkle9vf6z) and (sylheti, 13, a30xq7555spo8) are not the same in 1269 ms.
Task 101: correctly said that (inuktitut, 3, a3noo9k3y1yu5) and (atemayar, 10, a1z59eb5sfxpy) are not the same in 588 ms.
Task 102: correctly said that (myanmar, 4, a3pkun3onkucn) and (myanmar, 4, a1ehrxjplsq0r) are the same in 964 ms.
Task 103: correctly said that (tagalog, 15, amynpl5xzw6q) and (atemayar, 12, aljsxt6v0a30) are not the same in 796 ms.
Task 104: correctly said that (korean, 4, a2ernxe6jlm4x) and (tagalog, 1, a1ehrxjplsq0r) are not the same in 10689 ms.
Task 105: correctly said that (inuktitut, 11, aljsxt6v0a30) and (tagalog, 15, a1i92aacrkacw) are not the same in 804 ms.
Task 106: correctly said that (inuktitut, 11, a12rfyr8dbn79) and (korean, 17, aw82oruamauz) are not the same in 1697 ms.
Task 107: incorrectly said that (myanmar, 34, a1foggtlgo7n4) and (myanmar, 34, a2pfktghg1ofp) are not the same in 857 ms.
Task 108: correctly said that (tagalog, 10, a1i92aacrkacw) and (sylheti, 4, a4858n7ggst4) are not the same in 964 ms.
Task 109: incorrectly said that (sylheti, 15, a1j8s7giyto4a) and (sylheti, 15, a1foggtlgo7n4) are not the same in 2610 ms.
Task 110: incorrectly said that (tagalog, 10, a2du3880jbrov) and (tagalog, 10, a1ehrxjplsq0r) are not the same in 1366 ms.
Task 111: correctly said that (sylheti, 13, a1a25pyfoa9ge) and (tagalog, 1, a1i92aacrkacw) are not the same in 801 ms.
Task 112: correctly said that (sylheti, 3, amynpl5xzw6q) and (sylheti, 15, amynpl5xzw6q) are not the same in 840 ms.
Task 113: correctly said that (atemayar, 6, awl8tng8e81d) and (sylheti, 3, a3m0kt6kjezd1) are not the same in 613 ms.
Task 114: correctly said that (sylheti, 15, amynpl5xzw6q) and (myanmar, 8, a3pkun3onkucn) are not the same in 642 ms.
Task 115: correctly said that (inuktitut, 11, a3mv1jtwep7xn) and (inuktitut, 11, a1manxzopzden) are the same in 1385 ms.
Task 116: correctly said that (korean, 4, ae0ibwt7asth) and (sylheti, 13, a1foggtlgo7n4) are not the same in 737 ms.
Task 117: correctly said that (korean, 3, a2nm1qzuh14ls) and (atemayar, 12, avlbkm6qpb1s) are not the same in 818 ms.
Task 118: correctly said that (inuktitut, 12, a1manxzopzden) and (inuktitut, 12, a2qnqd7m8zfbo) are the same in 1026 ms.
Task 119: correctly said that (inuktitut, 2, a315xddd8j4x8) and (inuktitut, 2, a12rfyr8dbn79) are the same in 1230 ms.
Task 120: correctly said that (sylheti, 13, a1l2fzyn31zvf) and (myanmar, 4, a3pkun3onkucn) are not the same in 1327 ms.
Task 121: correctly said that (tagalog, 15, asu813kb7bv1) and (korean, 17, a1umhcrj9zfdp) are not the same in 609 ms.
Task 122: correctly said that (inuktitut, 11, a315xddd8j4x8) and (sylheti, 3, a2du3880jbrov) are not the same in 637 ms.
Task 123: correctly said that (tagalog, 10, a1l2fzyn31zvf) and (atemayar, 25, a1yovlluv1c9h) are not the same in 598 ms.
Task 124: correctly said that (atemayar, 25, a1z59eb5sfxpy) and (sylheti, 3, atvyxlwe5n03) are not the same in 946 ms.
Task 125: correctly said that (korean, 3, aj6upe9mlarg) and (atemayar, 12, a1z59eb5sfxpy) are not the same in 804 ms.
Task 126: correctly said that (inuktitut, 2, a315xddd8j4x8) and (inuktitut, 3, a1manxzopzden) are not the same in 744 ms.
Task 127: correctly said that (inuktitut, 3, a1j8s7giyto4a) and (inuktitut, 3, a2pfktghg1ofp) are the same in 1229 ms.
Task 128: correctly said that (inuktitut, 11, avlbkm6qpb1s) and (tagalog, 1, aghrkle9vf6z) are not the same in 732 ms.
Task 129: correctly said that (atemayar, 12, a1vzhyp8yvxkz) and (sylheti, 3, a2du3880jbrov) are not the same in 676 ms.
Task 130: correctly said that (tagalog, 10, a1ehrxjplsq0r) and (sylheti, 13, aljsxt6v0a30) are not the same in 1058 ms.
Task 131: correctly said that (korean, 17, a1udqv4pfypkn) and (korean, 17, a3dfh6dpi11ip) are the same in 1224 ms.
Task 132: correctly said that (sylheti, 15, ay0bjcgydhix) and (inuktitut, 11, a2pfktghg1ofp) are not the same in 811 ms.
Task 133: correctly said that (sylheti, 3, a3r9q9wqm1nyz) and (sylheti, 3, a1a25pyfoa9ge) are the same in 983 ms.
Task 134: correctly said that (inuktitut, 3, avlbkm6qpb1s) and (inuktitut, 3, a1qku9mccvl35) are the same in 1168 ms.
Task 135: correctly said that (myanmar, 16, a3qat9qkqumi2) and (atemayar, 12, a2pfktghg1ofp) are not the same in 753 ms.
Task 136: correctly said that (inuktitut, 2, a2qnqd7m8zfbo) and (inuktitut, 2, a2ernxe6jlm4x) are the same in 1858 ms.
Task 137: incorrectly said that (korean, 39, a3d7xwp0gand5) and (korean, 39, a1s3njxhehcip) are not the same in 990 ms.
Task 138: correctly said that (sylheti, 3, a3pkun3onkucn) and (sylheti, 15, atvyxlwe5n03) are not the same in 840 ms.
Task 139: correctly said that (atemayar, 6, a1qku9mccvl35) and (myanmar, 4, a6fjbvuzrhm6) are not the same in 633 ms.
Task 140: correctly said that (korean, 17, a3mv1jtwep7xn) and (inuktitut, 3, a2qnqd7m8zfbo) are not the same in 1550 ms.
Task 141: correctly said that (inuktitut, 2, ae0ibwt7asth) and (tagalog, 1, a3m0kt6kjezd1) are not the same in 749 ms.
Task 142: correctly said that (myanmar, 8, a4858n7ggst4) and (tagalog, 1, a12rfyr8dbn79) are not the same in 773 ms.
Task 143: correctly said that (atemayar, 12, a1vzhyp8yvxkz) and (atemayar, 12, agst3co6usf8) are the same in 1002 ms.
Task 144: correctly said that (tagalog, 10, avlbkm6qpb1s) and (sylheti, 13, a3m0kt6kjezd1) are not the same in 1538 ms.
Task 145: correctly said that (sylheti, 15, a3pkun3onkucn) and (myanmar, 16, adc8vbkoocrr) are not the same in 775 ms.
Task 146: correctly said that (inuktitut, 2, a3noo9k3y1yu5) and (inuktitut, 12, a1j8s7giyto4a) are not the same in 1059 ms.
Task 147: correctly said that (atemayar, 6, aljsxt6v0a30) and (atemayar, 6, a6fjbvuzrhm6) are the same in 1411 ms.
Task 148: correctly said that (sylheti, 15, a3pkun3onkucn) and (myanmar, 8, ay0bjcgydhix) are not the same in 672 ms.
Task 149: correctly said that (atemayar, 12, a31xqftzcsia2) and (sylheti, 13, atvyxlwe5n03) are not the same in 682 ms.
Task 150: correctly said that (sylheti, 3, a1l2fzyn31zvf) and (myanmar, 34, aljsxt6v0a30) are not the same in 1470 ms.
Task 151: incorrectly said that (sylheti, 4, atvyxlwe5n03) and (sylheti, 4, amynpl5xzw6q) are not the same in 1051 ms.
Task 152: correctly said that (sylheti, 13, a2du3880jbrov) and (tagalog, 1, asu813kb7bv1) are not the same in 633 ms.
Task 153: correctly said that (inuktitut, 12, a1w3fo0d7cf5t) and (atemayar, 25, a1qku9mccvl35) are not the same in 729 ms.
Task 154: correctly said that (myanmar, 8, adc8vbkoocrr) and (korean, 4, a2ernxe6jlm4x) are not the same in 563 ms.
Task 155: correctly said that (myanmar, 4, a3j1pp96x14u0) and (sylheti, 15, a1a25pyfoa9ge) are not the same in 874 ms.
Task 156: correctly said that (myanmar, 16, adc8vbkoocrr) and (inuktitut, 12, a2pfktghg1ofp) are not the same in 545 ms.
Task 157: correctly said that (inuktitut, 12, a3mv1jtwep7xn) and (korean, 39, a1oh82lwr5hbg) are not the same in 657 ms.
Task 158: correctly said that (sylheti, 4, a1qku9mccvl35) and (atemayar, 25, a1poymbentexx) are not the same in 877 ms.
Task 159: correctly said that (tagalog, 15, a3qat9qkqumi2) and (atemayar, 6, a1yovlluv1c9h) are not the same in 748 ms.
Task 160: correctly said that (sylheti, 4, avlbkm6qpb1s) and (atemayar, 10, agst3co6usf8) are not the same in 1502 ms.
Task 161: correctly said that (atemayar, 6, avlbkm6qpb1s) and (sylheti, 13, a30xq7555spo8) are not the same in 611 ms.
Task 162: correctly said that (atemayar, 10, a23psx2z4f65q) and (atemayar, 10, azdw6062ia38) are the same in 1044 ms.
Task 163: correctly said that (sylheti, 13, a3m0kt6kjezd1) and (sylheti, 13, a1l2fzyn31zvf) are the same in 928 ms.
Task 164: correctly said that (korean, 4, a1udqv4pfypkn) and (myanmar, 34, avlbkm6qpb1s) are not the same in 788 ms.
Task 165: correctly said that (myanmar, 16, a1aegunf6e081) and (myanmar, 16, a1stvgjqfl5kk) are the same in 1388 ms.
Task 166: correctly said that (atemayar, 6, a1qku9mccvl35) and (sylheti, 15, a1s3njxhehcip) are not the same in 806 ms.
Task 167: correctly said that (atemayar, 6, a1yovlluv1c9h) and (inuktitut, 12, avlbkm6qpb1s) are not the same in 692 ms.
Task 168: correctly said that (atemayar, 6, a1qku9mccvl35) and (inuktitut, 11, a3qat9qkqumi2) are not the same in 552 ms.
Task 169: correctly said that (korean, 17, a1udqv4pfypkn) and (korean, 17, aj6upe9mlarg) are the same in 1327 ms.
Task 170: correctly said that (inuktitut, 3, a2ernxe6jlm4x) and (korean, 3, a39ivbdm1ekue) are not the same in 1406 ms.
Task 171: correctly said that (sylheti, 15, a2du3880jbrov) and (inuktitut, 3, a1qku9mccvl35) are not the same in 616 ms.
Task 172: correctly said that (inuktitut, 2, a12xm86d2nbih) and (korean, 39, a1oh82lwr5hbg) are not the same in 514 ms.
Task 173: correctly said that (myanmar, 4, a1ehrxjplsq0r) and (korean, 4, a1udqv4pfypkn) are not the same in 509 ms.
Task 174: correctly said that (myanmar, 34, a3qat9qkqumi2) and (atemayar, 10, ax5gghhj0o6g) are not the same in 482 ms.
Task 175: correctly said that (korean, 17, a39ivbdm1ekue) and (korean, 17, a3bb99mn3zy37) are the same in 981 ms.
Task 176: correctly said that (sylheti, 3, aljsxt6v0a30) and (sylheti, 3, a1j8s7giyto4a) are the same in 1281 ms.
Task 177: correctly said that (atemayar, 10, ae0ibwt7asth) and (korean, 3, aw82oruamauz) are not the same in 950 ms.
Task 178: correctly said that (myanmar, 16, a1stvgjqfl5kk) and (sylheti, 3, avlbkm6qpb1s) are not the same in 785 ms.
Task 179: correctly said that (korean, 3, a2px0aywxngce) and (inuktitut, 12, a2pfktghg1ofp) are not the same in 580 ms.
Task 180: correctly said that (myanmar, 4, a1ehrxjplsq0r) and (myanmar, 4, aljsxt6v0a30) are the same in 1273 ms.
Task 181: correctly said that (korean, 4, a1oh82lwr5hbg) and (tagalog, 10, asu813kb7bv1) are not the same in 701 ms.
Task 182: correctly said that (myanmar, 34, a3pkun3onkucn) and (korean, 3, a1s3njxhehcip) are not the same in 532 ms.
Task 183: correctly said that (sylheti, 13, a3np21apfa18b) and (korean, 39, a3mv1jtwep7xn) are not the same in 533 ms.
Task 184: correctly said that (korean, 3, a3d7xwp0gand5) and (sylheti, 3, a3r9q9wqm1nyz) are not the same in 933 ms.
Task 185: correctly said that (tagalog, 6, aljsxt6v0a30) and (atemayar, 10, a31xqftzcsia2) are not the same in 672 ms.
Task 186: correctly said that (atemayar, 6, a31xqftzcsia2) and (sylheti, 3, a1a25pyfoa9ge) are not the same in 591 ms.
Task 187: correctly said that (sylheti, 13, a1qku9mccvl35) and (sylheti, 13, a1s3njxhehcip) are the same in 883 ms.
Task 188: correctly said that (myanmar, 16, avlbkm6qpb1s) and (inuktitut, 12, a2pgu4pi93b1q) are not the same in 812 ms.
Task 189: correctly said that (tagalog, 6, a1a25pyfoa9ge) and (tagalog, 6, a3mv1jtwep7xn) are the same in 1312 ms.
Task 190: correctly said that (myanmar, 8, a4858n7ggst4) and (inuktitut, 12, a12xm86d2nbih) are not the same in 927 ms.
Task 191: correctly said that (sylheti, 13, a3r9q9wqm1nyz) and (inuktitut, 12, a315xddd8j4x8) are not the same in 1217 ms.
Task 192: correctly said that (korean, 39, a3dfh6dpi11ip) and (korean, 39, ae0ibwt7asth) are the same in 1236 ms.
Task 193: correctly said that (atemayar, 25, azdw6062ia38) and (sylheti, 4, a1s3njxhehcip) are not the same in 1247 ms.
Task 194: correctly said that (atemayar, 10, a1manxzopzden) and (atemayar, 10, a31xqftzcsia2) are the same in 1080 ms.
Task 195: correctly said that (korean, 17, aj6upe9mlarg) and (korean, 17, a2px0aywxngce) are the same in 1224 ms.
Task 196: correctly said that (sylheti, 4, a3pkun3onkucn) and (sylheti, 4, a2du3880jbrov) are the same in 1030 ms.
Task 197: correctly said that (inuktitut, 11, a12rfyr8dbn79) and (inuktitut, 11, a1j8s7giyto4a) are the same in 1229 ms.
Task 198: correctly said that (sylheti, 3, a30xq7555spo8) and (sylheti, 13, a3np21apfa18b) are not the same in 771 ms.
Task 199: correctly said that (myanmar, 16, a2pfktghg1ofp) and (myanmar, 16, a3pkun3onkucn) are the same in 1462 ms.

Duration: 20m 41s 788ms
Comments: I felt it grew easier over time partially because you knew which symbols were available for selection.