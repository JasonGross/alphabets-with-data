

Summary:Short Summary:
Correct: 171
Incorrect: 29
Percent Correct: 85

Same Correct: 34
Same Incorrect: 11
Percent Same Correct: 75

Different Correct: 137
Different Incorrect: 18
Percent Different Correct: 88

Long Summary:
Task 0: incorrectly said that (sylheti, 17, a1a25pyfoa9ge) and (sylheti, 17, a1qku9mccvl35) are the same in 4058 ms.
Task 1: incorrectly said that (sylheti, 20, atvyxlwe5n03) and (myanmar, 1, a3qat9qkqumi2) are the same in 2444 ms.
Task 2: incorrectly said that (korean, 28, a2nm1qzuh14ls) and (korean, 29, a2nm1qzuh14ls) are the same in 1361 ms.
Task 3: correctly said that (korean, 28, a39ivbdm1ekue) and (tagalog, 5, ay0bjcgydhix) are not the same in 1768 ms.
Task 4: incorrectly said that (sylheti, 17, a30xq7555spo8) and (sylheti, 17, a1s3njxhehcip) are not the same in 2741 ms.
Task 5: incorrectly said that (sylheti, 19, a1qku9mccvl35) and (korean, 39, a1oh82lwr5hbg) are the same in 1736 ms.
Task 6: correctly said that (inuktitut, 1, a12rfyr8dbn79) and (atemayar, 25, ax5gghhj0o6g) are not the same in 1725 ms.
Task 7: correctly said that (sylheti, 20, a3r9q9wqm1nyz) and (atemayar, 6, a1yovlluv1c9h) are not the same in 1194 ms.
Task 8: incorrectly said that (tagalog, 2, a3qat9qkqumi2) and (tagalog, 2, a1yovlluv1c9h) are not the same in 2497 ms.
Task 9: incorrectly said that (myanmar, 31, a1foggtlgo7n4) and (myanmar, 32, a1aegunf6e081) are the same in 1963 ms.
Task 10: correctly said that (sylheti, 28, a1j8s7giyto4a) and (inuktitut, 3, a1j8s7giyto4a) are not the same in 2346 ms.
Task 11: correctly said that (sylheti, 28, a1s3njxhehcip) and (korean, 29, aav4pzxdoebd) are not the same in 1153 ms.
Task 12: incorrectly said that (atemayar, 25, aljsxt6v0a30) and (atemayar, 25, a1manxzopzden) are not the same in 941 ms.
Task 13: correctly said that (atemayar, 10, a1yovlluv1c9h) and (myanmar, 1, ay0bjcgydhix) are not the same in 1338 ms.
Task 14: correctly said that (atemayar, 25, a1manxzopzden) and (atemayar, 25, ae0ibwt7asth) are the same in 1762 ms.
Task 15: correctly said that (sylheti, 20, a3m0kt6kjezd1) and (myanmar, 31, a1clejhbx9yyn) are not the same in 1331 ms.
Task 16: correctly said that (tagalog, 5, asu813kb7bv1) and (inuktitut, 3, ae0ibwt7asth) are not the same in 1383 ms.
Task 17: correctly said that (sylheti, 19, aljsxt6v0a30) and (korean, 39, a2akud1gdstsd) are not the same in 1230 ms.
Task 18: correctly said that (tagalog, 10, ay0bjcgydhix) and (sylheti, 19, a2du3880jbrov) are not the same in 1881 ms.
Task 19: incorrectly said that (tagalog, 6, a1yovlluv1c9h) and (tagalog, 6, a2du3880jbrov) are not the same in 1185 ms.
Task 20: correctly said that (myanmar, 31, aljsxt6v0a30) and (atemayar, 6, a1z59eb5sfxpy) are not the same in 1682 ms.
Task 21: correctly said that (korean, 39, a3mv1jtwep7xn) and (korean, 29, a2ernxe6jlm4x) are not the same in 850 ms.
Task 22: correctly said that (atemayar, 25, avlbkm6qpb1s) and (sylheti, 19, a3m0kt6kjezd1) are not the same in 923 ms.
Task 23: incorrectly said that (tagalog, 2, ay0bjcgydhix) and (myanmar, 25, a1umhcrj9zfdp) are the same in 1296 ms.
Task 24: correctly said that (korean, 29, a5379luit3pc) and (korean, 29, a3d7xwp0gand5) are the same in 1078 ms.
Task 25: correctly said that (korean, 39, a2ernxe6jlm4x) and (inuktitut, 2, a1z59eb5sfxpy) are not the same in 1044 ms.
Task 26: correctly said that (sylheti, 28, a4858n7ggst4) and (korean, 4, aw82oruamauz) are not the same in 985 ms.
Task 27: correctly said that (myanmar, 31, a1umhcrj9zfdp) and (myanmar, 25, avlbkm6qpb1s) are not the same in 1043 ms.
Task 28: correctly said that (inuktitut, 3, a12xm86d2nbih) and (atemayar, 6, a1qku9mccvl35) are not the same in 925 ms.
Task 29: correctly said that (myanmar, 31, amynpl5xzw6q) and (tagalog, 2, a3m0kt6kjezd1) are not the same in 1065 ms.
Task 30: incorrectly said that (korean, 28, a1v0p79fynz9j) and (myanmar, 31, aljsxt6v0a30) are the same in 2064 ms.
Task 31: correctly said that (sylheti, 20, a3m0kt6kjezd1) and (atemayar, 10, azdw6062ia38) are not the same in 1159 ms.
Task 32: correctly said that (sylheti, 19, a3m0kt6kjezd1) and (sylheti, 19, avlbkm6qpb1s) are the same in 1281 ms.
Task 33: incorrectly said that (korean, 28, a1v0p79fynz9j) and (sylheti, 20, a1foggtlgo7n4) are the same in 1177 ms.
Task 34: correctly said that (korean, 39, aw82oruamauz) and (korean, 28, a1manxzopzden) are not the same in 1460 ms.
Task 35: correctly said that (tagalog, 10, a1ehrxjplsq0r) and (korean, 4, a1umhcrj9zfdp) are not the same in 1066 ms.
Task 36: correctly said that (sylheti, 19, a1qku9mccvl35) and (tagalog, 6, a1i92aacrkacw) are not the same in 1124 ms.
Task 37: correctly said that (tagalog, 6, ae0ibwt7asth) and (tagalog, 6, a2du3880jbrov) are the same in 1283 ms.
Task 38: correctly said that (inuktitut, 1, a12xm86d2nbih) and (inuktitut, 1, a2qnqd7m8zfbo) are the same in 1395 ms.
Task 39: correctly said that (inuktitut, 1, a2pgu4pi93b1q) and (atemayar, 10, a3m0kt6kjezd1) are not the same in 964 ms.
Task 40: incorrectly said that (myanmar, 32, a6fjbvuzrhm6) and (atemayar, 23, a3m0kt6kjezd1) are the same in 1572 ms.
Task 41: correctly said that (atemayar, 25, a1manxzopzden) and (myanmar, 32, amynpl5xzw6q) are not the same in 1059 ms.
Task 42: correctly said that (korean, 29, a2nm1qzuh14ls) and (korean, 4, a39ivbdm1ekue) are not the same in 3277 ms.
Task 43: correctly said that (myanmar, 32, a1j8s7giyto4a) and (atemayar, 10, a31e6jchki335) are not the same in 840 ms.
Task 44: correctly said that (tagalog, 6, a1i92aacrkacw) and (korean, 29, a1oh82lwr5hbg) are not the same in 2080 ms.
Task 45: correctly said that (myanmar, 1, a1s3njxhehcip) and (sylheti, 17, aljsxt6v0a30) are not the same in 858 ms.
Task 46: correctly said that (tagalog, 6, a12rfyr8dbn79) and (korean, 28, a2nm1qzuh14ls) are not the same in 950 ms.
Task 47: correctly said that (inuktitut, 3, a1qku9mccvl35) and (korean, 28, a2nm1qzuh14ls) are not the same in 758 ms.
Task 48: correctly said that (tagalog, 5, aljsxt6v0a30) and (tagalog, 2, aghrkle9vf6z) are not the same in 1184 ms.
Task 49: correctly said that (tagalog, 5, a12rfyr8dbn79) and (atemayar, 6, a6fjbvuzrhm6) are not the same in 771 ms.
Task 50: correctly said that (myanmar, 25, a1stvgjqfl5kk) and (sylheti, 19, a1l2fzyn31zvf) are not the same in 1316 ms.
Task 51: correctly said that (myanmar, 1, a2pfktghg1ofp) and (inuktitut, 3, a1qku9mccvl35) are not the same in 731 ms.
Task 52: correctly said that (myanmar, 1, avlbkm6qpb1s) and (tagalog, 6, a1i92aacrkacw) are not the same in 943 ms.
Task 53: correctly said that (inuktitut, 3, avlbkm6qpb1s) and (myanmar, 31, ajf66jwen7bx) are not the same in 1005 ms.
Task 54: correctly said that (atemayar, 23, awl8tng8e81d) and (myanmar, 32, ay0bjcgydhix) are not the same in 1042 ms.
Task 55: correctly said that (atemayar, 25, a1yovlluv1c9h) and (tagalog, 6, a3mv1jtwep7xn) are not the same in 1207 ms.
Task 56: incorrectly said that (myanmar, 32, a3pkun3onkucn) and (tagalog, 5, a3qat9qkqumi2) are the same in 1464 ms.
Task 57: correctly said that (atemayar, 23, a1z59eb5sfxpy) and (atemayar, 23, a1vzhyp8yvxkz) are the same in 1730 ms.
Task 58: correctly said that (atemayar, 6, a31e6jchki335) and (sylheti, 17, a3r9q9wqm1nyz) are not the same in 1247 ms.
Task 59: correctly said that (atemayar, 25, a1yovlluv1c9h) and (inuktitut, 11, a12rfyr8dbn79) are not the same in 908 ms.
Task 60: correctly said that (tagalog, 6, amynpl5xzw6q) and (inuktitut, 11, a1j8s7giyto4a) are not the same in 1293 ms.
Task 61: correctly said that (korean, 28, aj6upe9mlarg) and (myanmar, 25, ay0bjcgydhix) are not the same in 800 ms.
Task 62: incorrectly said that (myanmar, 1, a3j1pp96x14u0) and (myanmar, 1, amynpl5xzw6q) are not the same in 1150 ms.
Task 63: correctly said that (tagalog, 10, a1ehrxjplsq0r) and (sylheti, 28, a3pkun3onkucn) are not the same in 1198 ms.
Task 64: correctly said that (myanmar, 1, avlbkm6qpb1s) and (atemayar, 25, a2pfktghg1ofp) are not the same in 1936 ms.
Task 65: correctly said that (korean, 29, a3mv1jtwep7xn) and (korean, 29, a1v0p79fynz9j) are the same in 1441 ms.
Task 66: correctly said that (inuktitut, 2, a12rfyr8dbn79) and (atemayar, 6, a1qku9mccvl35) are not the same in 768 ms.
Task 67: correctly said that (sylheti, 17, a4858n7ggst4) and (atemayar, 6, a1z59eb5sfxpy) are not the same in 1197 ms.
Task 68: correctly said that (inuktitut, 3, a12rfyr8dbn79) and (korean, 29, a2ernxe6jlm4x) are not the same in 1892 ms.
Task 69: correctly said that (inuktitut, 11, a1w3fo0d7cf5t) and (sylheti, 20, atvyxlwe5n03) are not the same in 2790 ms.
Task 70: correctly said that (atemayar, 25, agst3co6usf8) and (korean, 4, a1v0p79fynz9j) are not the same in 1658 ms.
Task 71: correctly said that (sylheti, 28, a3m0kt6kjezd1) and (tagalog, 10, a1l2fzyn31zvf) are not the same in 788 ms.
Task 72: correctly said that (inuktitut, 3, ae0ibwt7asth) and (korean, 39, aw82oruamauz) are not the same in 882 ms.
Task 73: correctly said that (myanmar, 32, a1ehrxjplsq0r) and (tagalog, 10, ae0ibwt7asth) are not the same in 2721 ms.
Task 74: correctly said that (atemayar, 25, a2pfktghg1ofp) and (myanmar, 32, a1clejhbx9yyn) are not the same in 844 ms.
Task 75: correctly said that (sylheti, 19, a1l2fzyn31zvf) and (sylheti, 19, a1qku9mccvl35) are the same in 1976 ms.
Task 76: correctly said that (korean, 29, aav4pzxdoebd) and (myanmar, 31, ajf66jwen7bx) are not the same in 1369 ms.
Task 77: correctly said that (inuktitut, 1, a2qnqd7m8zfbo) and (atemayar, 6, a23psx2z4f65q) are not the same in 1017 ms.
Task 78: correctly said that (myanmar, 1, a1clejhbx9yyn) and (myanmar, 1, a1s3njxhehcip) are the same in 1562 ms.
Task 79: correctly said that (sylheti, 20, aljsxt6v0a30) and (korean, 4, a1v0p79fynz9j) are not the same in 1471 ms.
Task 80: correctly said that (atemayar, 6, a31e6jchki335) and (sylheti, 20, a2du3880jbrov) are not the same in 1347 ms.
Task 81: correctly said that (tagalog, 10, a1a25pyfoa9ge) and (inuktitut, 3, a1manxzopzden) are not the same in 834 ms.
Task 82: correctly said that (tagalog, 5, ay0bjcgydhix) and (atemayar, 10, a1vzhyp8yvxkz) are not the same in 893 ms.
Task 83: incorrectly said that (korean, 28, a1oh82lwr5hbg) and (korean, 28, aw82oruamauz) are not the same in 1087 ms.
Task 84: correctly said that (sylheti, 20, a1a25pyfoa9ge) and (inuktitut, 11, a2pgu4pi93b1q) are not the same in 1153 ms.
Task 85: correctly said that (sylheti, 20, a3np21apfa18b) and (sylheti, 20, a6fjbvuzrhm6) are the same in 1425 ms.
Task 86: correctly said that (myanmar, 31, ajf66jwen7bx) and (korean, 4, a3d7xwp0gand5) are not the same in 1196 ms.
Task 87: correctly said that (atemayar, 10, a6fjbvuzrhm6) and (inuktitut, 1, a2pgu4pi93b1q) are not the same in 887 ms.
Task 88: correctly said that (tagalog, 5, a1yovlluv1c9h) and (tagalog, 5, ajf66jwen7bx) are the same in 1207 ms.
Task 89: incorrectly said that (korean, 29, a2nm1qzuh14ls) and (tagalog, 5, amynpl5xzw6q) are the same in 2586 ms.
Task 90: correctly said that (inuktitut, 1, a2ernxe6jlm4x) and (sylheti, 28, a1a25pyfoa9ge) are not the same in 2113 ms.
Task 91: correctly said that (korean, 4, a2nm1qzuh14ls) and (tagalog, 5, a3qat9qkqumi2) are not the same in 1139 ms.
Task 92: correctly said that (korean, 4, a3d7xwp0gand5) and (korean, 4, a3dfh6dpi11ip) are the same in 1568 ms.
Task 93: correctly said that (tagalog, 5, aghrkle9vf6z) and (tagalog, 5, avlbkm6qpb1s) are the same in 942 ms.
Task 94: correctly said that (inuktitut, 2, a3noo9k3y1yu5) and (atemayar, 6, a3m0kt6kjezd1) are not the same in 1556 ms.
Task 95: correctly said that (sylheti, 17, a1j8s7giyto4a) and (inuktitut, 2, a1z59eb5sfxpy) are not the same in 2688 ms.
Task 96: correctly said that (atemayar, 23, a23psx2z4f65q) and (atemayar, 10, a31xqftzcsia2) are not the same in 1247 ms.
Task 97: correctly said that (korean, 29, a1v0p79fynz9j) and (inuktitut, 3, a1s3njxhehcip) are not the same in 3919 ms.
Task 98: correctly said that (sylheti, 20, a4858n7ggst4) and (atemayar, 23, a31xqftzcsia2) are not the same in 1112 ms.
Task 99: correctly said that (korean, 28, a3d7xwp0gand5) and (tagalog, 10, a1a25pyfoa9ge) are not the same in 1211 ms.
Task 100: correctly said that (myanmar, 1, a1foggtlgo7n4) and (tagalog, 5, a1s3njxhehcip) are not the same in 1426 ms.
Task 101: correctly said that (tagalog, 6, a2du3880jbrov) and (tagalog, 2, a1s3njxhehcip) are not the same in 1509 ms.
Task 102: correctly said that (myanmar, 1, a1foggtlgo7n4) and (myanmar, 1, a6fjbvuzrhm6) are the same in 1848 ms.
Task 103: correctly said that (inuktitut, 2, a1manxzopzden) and (sylheti, 19, ay0bjcgydhix) are not the same in 1624 ms.
Task 104: correctly said that (korean, 29, a2ernxe6jlm4x) and (inuktitut, 3, a2pgu4pi93b1q) are not the same in 7249 ms.
Task 105: incorrectly said that (sylheti, 28, a3r9q9wqm1nyz) and (sylheti, 28, a30xq7555spo8) are not the same in 1036 ms.
Task 106: correctly said that (sylheti, 20, a3np21apfa18b) and (sylheti, 20, avlbkm6qpb1s) are the same in 1595 ms.
Task 107: correctly said that (inuktitut, 2, a1manxzopzden) and (korean, 4, a2ernxe6jlm4x) are not the same in 1309 ms.
Task 108: correctly said that (myanmar, 31, a1umhcrj9zfdp) and (sylheti, 19, a3np21apfa18b) are not the same in 1141 ms.
Task 109: correctly said that (korean, 39, a2nm1qzuh14ls) and (atemayar, 10, a6fjbvuzrhm6) are not the same in 1078 ms.
Task 110: correctly said that (myanmar, 32, a2pfktghg1ofp) and (atemayar, 10, a1qku9mccvl35) are not the same in 2017 ms.
Task 111: correctly said that (myanmar, 31, a1j8s7giyto4a) and (atemayar, 10, a1manxzopzden) are not the same in 691 ms.
Task 112: correctly said that (korean, 28, a2nm1qzuh14ls) and (atemayar, 6, a2pfktghg1ofp) are not the same in 575 ms.
Task 113: correctly said that (inuktitut, 3, a2ernxe6jlm4x) and (tagalog, 6, aghrkle9vf6z) are not the same in 718 ms.
Task 114: correctly said that (inuktitut, 11, a2pgu4pi93b1q) and (atemayar, 23, a31xqftzcsia2) are not the same in 601 ms.
Task 115: correctly said that (tagalog, 10, ay0bjcgydhix) and (inuktitut, 3, avlbkm6qpb1s) are not the same in 563 ms.
Task 116: correctly said that (inuktitut, 1, a1s3njxhehcip) and (inuktitut, 1, avlbkm6qpb1s) are the same in 1497 ms.
Task 117: correctly said that (myanmar, 1, a1foggtlgo7n4) and (inuktitut, 3, a3noo9k3y1yu5) are not the same in 926 ms.
Task 118: correctly said that (myanmar, 1, ajf66jwen7bx) and (myanmar, 31, avlbkm6qpb1s) are not the same in 999 ms.
Task 119: correctly said that (myanmar, 1, a3j1pp96x14u0) and (atemayar, 23, avlbkm6qpb1s) are not the same in 648 ms.
Task 120: correctly said that (inuktitut, 2, a1j8s7giyto4a) and (korean, 39, aj6upe9mlarg) are not the same in 1602 ms.
Task 121: correctly said that (sylheti, 28, a4858n7ggst4) and (sylheti, 28, a3209gno9duil) are the same in 1437 ms.
Task 122: incorrectly said that (sylheti, 17, atvyxlwe5n03) and (sylheti, 28, a3r9q9wqm1nyz) are the same in 1845 ms.
Task 123: correctly said that (myanmar, 25, a6fjbvuzrhm6) and (inuktitut, 11, a3qat9qkqumi2) are not the same in 1557 ms.
Task 124: correctly said that (sylheti, 20, a3np21apfa18b) and (atemayar, 23, a1j8s7giyto4a) are not the same in 654 ms.
Task 125: correctly said that (sylheti, 19, a1j8s7giyto4a) and (sylheti, 19, a1l2fzyn31zvf) are the same in 1604 ms.
Task 126: correctly said that (sylheti, 28, avlbkm6qpb1s) and (korean, 29, aw82oruamauz) are not the same in 1187 ms.
Task 127: correctly said that (inuktitut, 1, a1manxzopzden) and (korean, 39, aw82oruamauz) are not the same in 802 ms.
Task 128: incorrectly said that (sylheti, 20, a1j8s7giyto4a) and (sylheti, 20, a1qku9mccvl35) are not the same in 3595 ms.
Task 129: correctly said that (sylheti, 19, a6fjbvuzrhm6) and (sylheti, 20, a1l2fzyn31zvf) are not the same in 2620 ms.
Task 130: incorrectly said that (atemayar, 10, aljsxt6v0a30) and (atemayar, 6, a1manxzopzden) are the same in 2066 ms.
Task 131: correctly said that (inuktitut, 1, ae0ibwt7asth) and (korean, 4, ae0ibwt7asth) are not the same in 790 ms.
Task 132: correctly said that (korean, 4, a5379luit3pc) and (myanmar, 25, aljsxt6v0a30) are not the same in 780 ms.
Task 133: incorrectly said that (inuktitut, 3, a1j8s7giyto4a) and (inuktitut, 2, aljsxt6v0a30) are the same in 1898 ms.
Task 134: correctly said that (atemayar, 6, agst3co6usf8) and (atemayar, 6, a23psx2z4f65q) are the same in 1244 ms.
Task 135: correctly said that (tagalog, 5, a1yovlluv1c9h) and (tagalog, 5, a12rfyr8dbn79) are the same in 1285 ms.
Task 136: correctly said that (atemayar, 6, a6fjbvuzrhm6) and (sylheti, 19, a1qku9mccvl35) are not the same in 1067 ms.
Task 137: correctly said that (myanmar, 25, a1stvgjqfl5kk) and (atemayar, 23, a1z59eb5sfxpy) are not the same in 715 ms.
Task 138: correctly said that (atemayar, 10, a31xqftzcsia2) and (korean, 28, a3d7xwp0gand5) are not the same in 3502 ms.
Task 139: correctly said that (inuktitut, 1, a1z59eb5sfxpy) and (inuktitut, 1, a1w3fo0d7cf5t) are the same in 1827 ms.
Task 140: correctly said that (korean, 28, a2ernxe6jlm4x) and (tagalog, 5, a2du3880jbrov) are not the same in 1995 ms.
Task 141: correctly said that (inuktitut, 1, a3noo9k3y1yu5) and (inuktitut, 1, a1s3njxhehcip) are the same in 2920 ms.
Task 142: correctly said that (atemayar, 23, a1vzhyp8yvxkz) and (inuktitut, 1, a1j8s7giyto4a) are not the same in 1130 ms.
Task 143: incorrectly said that (inuktitut, 11, a2pgu4pi93b1q) and (inuktitut, 11, aljsxt6v0a30) are not the same in 905 ms.
Task 144: correctly said that (inuktitut, 3, a3qat9qkqumi2) and (atemayar, 25, a1qku9mccvl35) are not the same in 604 ms.
Task 145: incorrectly said that (sylheti, 20, a3pkun3onkucn) and (sylheti, 17, a4858n7ggst4) are the same in 1559 ms.
Task 146: correctly said that (inuktitut, 1, a2ernxe6jlm4x) and (inuktitut, 1, a3noo9k3y1yu5) are the same in 1369 ms.
Task 147: correctly said that (inuktitut, 1, a2qnqd7m8zfbo) and (sylheti, 19, a1foggtlgo7n4) are not the same in 1519 ms.
Task 148: correctly said that (korean, 39, a5379luit3pc) and (inuktitut, 2, avlbkm6qpb1s) are not the same in 882 ms.
Task 149: correctly said that (myanmar, 1, a2pfktghg1ofp) and (myanmar, 1, a1aegunf6e081) are the same in 1849 ms.
Task 150: correctly said that (sylheti, 17, a3r9q9wqm1nyz) and (tagalog, 6, avlbkm6qpb1s) are not the same in 1537 ms.
Task 151: correctly said that (inuktitut, 3, a3mv1jtwep7xn) and (atemayar, 10, a1poymbentexx) are not the same in 762 ms.
Task 152: correctly said that (inuktitut, 2, a2ernxe6jlm4x) and (inuktitut, 2, ae0ibwt7asth) are the same in 1439 ms.
Task 153: correctly said that (inuktitut, 1, a1manxzopzden) and (inuktitut, 1, a3mv1jtwep7xn) are the same in 854 ms.
Task 154: correctly said that (atemayar, 25, a1j8s7giyto4a) and (atemayar, 25, a1yovlluv1c9h) are the same in 2081 ms.
Task 155: correctly said that (tagalog, 5, avlbkm6qpb1s) and (tagalog, 5, a1s3njxhehcip) are the same in 1136 ms.
Task 156: correctly said that (tagalog, 2, asu813kb7bv1) and (tagalog, 2, a1l2fzyn31zvf) are the same in 1331 ms.
Task 157: correctly said that (inuktitut, 3, a2ernxe6jlm4x) and (tagalog, 5, aljsxt6v0a30) are not the same in 1037 ms.
Task 158: correctly said that (atemayar, 23, a1z59eb5sfxpy) and (tagalog, 2, aghrkle9vf6z) are not the same in 917 ms.
Task 159: correctly said that (myanmar, 25, avlbkm6qpb1s) and (korean, 29, aw82oruamauz) are not the same in 1050 ms.
Task 160: incorrectly said that (myanmar, 1, avlbkm6qpb1s) and (myanmar, 25, a1clejhbx9yyn) are the same in 2334 ms.
Task 161: correctly said that (sylheti, 17, amynpl5xzw6q) and (tagalog, 5, a3mv1jtwep7xn) are not the same in 1292 ms.
Task 162: correctly said that (tagalog, 6, a1a25pyfoa9ge) and (tagalog, 6, asu813kb7bv1) are the same in 4955 ms.
Task 163: incorrectly said that (myanmar, 25, a1clejhbx9yyn) and (myanmar, 31, a1s3njxhehcip) are the same in 1072 ms.
Task 164: correctly said that (myanmar, 1, a4858n7ggst4) and (korean, 29, a2nm1qzuh14ls) are not the same in 4893 ms.
Task 165: correctly said that (sylheti, 20, a1qku9mccvl35) and (tagalog, 2, aghrkle9vf6z) are not the same in 942 ms.
Task 166: correctly said that (korean, 28, a5379luit3pc) and (atemayar, 23, a1qku9mccvl35) are not the same in 865 ms.
Task 167: correctly said that (tagalog, 2, a2du3880jbrov) and (korean, 28, aw82oruamauz) are not the same in 1204 ms.
Task 168: correctly said that (atemayar, 25, a2pfktghg1ofp) and (tagalog, 5, a1s3njxhehcip) are not the same in 1285 ms.
Task 169: correctly said that (inuktitut, 11, avlbkm6qpb1s) and (korean, 39, a1v0p79fynz9j) are not the same in 1555 ms.
Task 170: correctly said that (tagalog, 6, ae0ibwt7asth) and (atemayar, 6, a1poymbentexx) are not the same in 1366 ms.
Task 171: correctly said that (myanmar, 31, amynpl5xzw6q) and (atemayar, 10, a1z59eb5sfxpy) are not the same in 837 ms.
Task 172: correctly said that (myanmar, 25, a1aegunf6e081) and (sylheti, 17, a6fjbvuzrhm6) are not the same in 601 ms.
Task 173: correctly said that (sylheti, 20, a2du3880jbrov) and (inuktitut, 1, a1z59eb5sfxpy) are not the same in 984 ms.
Task 174: correctly said that (korean, 28, a1s3njxhehcip) and (tagalog, 5, ae0ibwt7asth) are not the same in 1050 ms.
Task 175: correctly said that (sylheti, 20, a1j8s7giyto4a) and (sylheti, 20, a3np21apfa18b) are the same in 1968 ms.
Task 176: incorrectly said that (sylheti, 17, ay0bjcgydhix) and (sylheti, 20, aljsxt6v0a30) are the same in 1237 ms.
Task 177: correctly said that (inuktitut, 2, a1qku9mccvl35) and (myanmar, 1, a6fjbvuzrhm6) are not the same in 2072 ms.
Task 178: correctly said that (sylheti, 20, amynpl5xzw6q) and (tagalog, 6, aghrkle9vf6z) are not the same in 773 ms.
Task 179: correctly said that (atemayar, 25, a1vzhyp8yvxkz) and (inuktitut, 3, a1manxzopzden) are not the same in 4304 ms.
Task 180: correctly said that (korean, 29, a1umhcrj9zfdp) and (myanmar, 1, a2pfktghg1ofp) are not the same in 1795 ms.
Task 181: correctly said that (tagalog, 6, ajf66jwen7bx) and (inuktitut, 2, ay0bjcgydhix) are not the same in 664 ms.
Task 182: correctly said that (myanmar, 25, a1j8s7giyto4a) and (tagalog, 6, ay0bjcgydhix) are not the same in 696 ms.
Task 183: correctly said that (sylheti, 19, ay0bjcgydhix) and (inuktitut, 1, a2qnqd7m8zfbo) are not the same in 756 ms.
Task 184: incorrectly said that (tagalog, 10, ajf66jwen7bx) and (tagalog, 10, amynpl5xzw6q) are not the same in 6624 ms.
Task 185: correctly said that (sylheti, 28, amynpl5xzw6q) and (atemayar, 6, a2pfktghg1ofp) are not the same in 675 ms.
Task 186: correctly said that (inuktitut, 11, a12xm86d2nbih) and (atemayar, 6, a1qku9mccvl35) are not the same in 735 ms.
Task 187: correctly said that (atemayar, 6, a1j8s7giyto4a) and (inuktitut, 2, a2ernxe6jlm4x) are not the same in 1827 ms.
Task 188: correctly said that (atemayar, 23, aljsxt6v0a30) and (atemayar, 23, a31e6jchki335) are the same in 1826 ms.
Task 189: incorrectly said that (myanmar, 25, a1foggtlgo7n4) and (myanmar, 25, a1aegunf6e081) are not the same in 2358 ms.
Task 190: correctly said that (atemayar, 6, awl8tng8e81d) and (atemayar, 6, azdw6062ia38) are the same in 1750 ms.
Task 191: correctly said that (sylheti, 17, a1s3njxhehcip) and (inuktitut, 3, a2qnqd7m8zfbo) are not the same in 1701 ms.
Task 192: correctly said that (korean, 28, aw82oruamauz) and (myanmar, 32, a6fjbvuzrhm6) are not the same in 722 ms.
Task 193: correctly said that (sylheti, 20, a30xq7555spo8) and (atemayar, 23, a1z59eb5sfxpy) are not the same in 707 ms.
Task 194: correctly said that (sylheti, 19, a3209gno9duil) and (korean, 28, a1udqv4pfypkn) are not the same in 653 ms.
Task 195: correctly said that (tagalog, 5, a1i92aacrkacw) and (inuktitut, 3, a1j8s7giyto4a) are not the same in 650 ms.
Task 196: correctly said that (korean, 28, a2nm1qzuh14ls) and (korean, 28, aw82oruamauz) are the same in 1531 ms.
Task 197: correctly said that (inuktitut, 11, avlbkm6qpb1s) and (inuktitut, 1, a2pgu4pi93b1q) are not the same in 805 ms.
Task 198: correctly said that (atemayar, 10, a1j8s7giyto4a) and (tagalog, 6, a12rfyr8dbn79) are not the same in 662 ms.
Task 199: correctly said that (inuktitut, 11, a315xddd8j4x8) and (inuktitut, 2, a2ernxe6jlm4x) are not the same in 1142 ms.

Duration: 23m 3s 499ms
Comments: 