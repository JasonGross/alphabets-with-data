

Summary:Short Summary:
Correct: 147
Incorrect: 53
Percent Correct: 73

Same Correct: 45
Same Incorrect: 20
Percent Same Correct: 69

Different Correct: 102
Different Incorrect: 33
Percent Different Correct: 75

Long Summary:
Task 0: correctly said that (sylheti, 17, avlbkm6qpb1s) and (sylheti, 17, a6fjbvuzrhm6) are the same in 2971 ms.
Task 1: correctly said that (atemayar, 10, a1z59eb5sfxpy) and (tagalog, 5, asu813kb7bv1) are not the same in 1856 ms.
Task 2: correctly said that (tagalog, 2, avlbkm6qpb1s) and (tagalog, 2, a1i92aacrkacw) are the same in 3971 ms.
Task 3: correctly said that (sylheti, 28, a3r9q9wqm1nyz) and (myanmar, 31, a3j1pp96x14u0) are not the same in 2155 ms.
Task 4: correctly said that (myanmar, 1, a2pfktghg1ofp) and (korean, 28, a1oh82lwr5hbg) are not the same in 2253 ms.
Task 5: correctly said that (sylheti, 19, avlbkm6qpb1s) and (sylheti, 19, a4858n7ggst4) are the same in 1763 ms.
Task 6: correctly said that (tagalog, 2, ae0ibwt7asth) and (atemayar, 25, a31xqftzcsia2) are not the same in 1725 ms.
Task 7: correctly said that (sylheti, 19, a3m0kt6kjezd1) and (tagalog, 2, aghrkle9vf6z) are not the same in 2485 ms.
Task 8: correctly said that (korean, 29, a2px0aywxngce) and (atemayar, 6, a1j8s7giyto4a) are not the same in 1898 ms.
Task 9: correctly said that (inuktitut, 1, a2pgu4pi93b1q) and (myanmar, 32, a2pfktghg1ofp) are not the same in 2037 ms.
Task 10: correctly said that (tagalog, 5, a12rfyr8dbn79) and (tagalog, 5, a2du3880jbrov) are the same in 2230 ms.
Task 11: correctly said that (atemayar, 23, ax5gghhj0o6g) and (atemayar, 6, a2pfktghg1ofp) are not the same in 1901 ms.
Task 12: correctly said that (tagalog, 2, asu813kb7bv1) and (myanmar, 31, a1ehrxjplsq0r) are not the same in 1719 ms.
Task 13: correctly said that (tagalog, 10, a1i92aacrkacw) and (tagalog, 10, a3m0kt6kjezd1) are the same in 2033 ms.
Task 14: incorrectly said that (atemayar, 6, ax5gghhj0o6g) and (tagalog, 5, a1s3njxhehcip) are the same in 1506 ms.
Task 15: correctly said that (tagalog, 5, ay0bjcgydhix) and (korean, 39, a39ivbdm1ekue) are not the same in 1565 ms.
Task 16: correctly said that (tagalog, 2, amynpl5xzw6q) and (inuktitut, 3, a12rfyr8dbn79) are not the same in 1979 ms.
Task 17: incorrectly said that (inuktitut, 2, aljsxt6v0a30) and (sylheti, 19, a1a25pyfoa9ge) are the same in 1374 ms.
Task 18: correctly said that (myanmar, 1, aljsxt6v0a30) and (tagalog, 6, a1l2fzyn31zvf) are not the same in 1659 ms.
Task 19: incorrectly said that (korean, 4, a1s3njxhehcip) and (korean, 4, ae0ibwt7asth) are not the same in 1838 ms.
Task 20: correctly said that (korean, 39, a3bb99mn3zy37) and (sylheti, 17, a2du3880jbrov) are not the same in 3047 ms.
Task 21: correctly said that (inuktitut, 3, a1manxzopzden) and (sylheti, 17, a3r9q9wqm1nyz) are not the same in 1126 ms.
Task 22: correctly said that (tagalog, 2, a12rfyr8dbn79) and (sylheti, 19, a1j8s7giyto4a) are not the same in 1240 ms.
Task 23: correctly said that (inuktitut, 1, a3mv1jtwep7xn) and (myanmar, 31, a4858n7ggst4) are not the same in 1102 ms.
Task 24: correctly said that (sylheti, 28, avlbkm6qpb1s) and (sylheti, 28, a3209gno9duil) are the same in 1375 ms.
Task 25: correctly said that (myanmar, 1, ajf66jwen7bx) and (myanmar, 1, aljsxt6v0a30) are the same in 2519 ms.
Task 26: correctly said that (myanmar, 1, a1j8s7giyto4a) and (myanmar, 1, a1s3njxhehcip) are the same in 833 ms.
Task 27: incorrectly said that (sylheti, 19, a6fjbvuzrhm6) and (sylheti, 19, a1l2fzyn31zvf) are not the same in 1896 ms.
Task 28: correctly said that (sylheti, 28, a4858n7ggst4) and (sylheti, 28, ay0bjcgydhix) are the same in 2459 ms.
Task 29: correctly said that (atemayar, 10, agst3co6usf8) and (tagalog, 5, amynpl5xzw6q) are not the same in 1385 ms.
Task 30: correctly said that (myanmar, 31, a1umhcrj9zfdp) and (myanmar, 31, a1clejhbx9yyn) are the same in 1760 ms.
Task 31: correctly said that (sylheti, 28, a1j8s7giyto4a) and (atemayar, 23, ae0ibwt7asth) are not the same in 1227 ms.
Task 32: correctly said that (atemayar, 6, a3m0kt6kjezd1) and (tagalog, 5, ay0bjcgydhix) are not the same in 1752 ms.
Task 33: correctly said that (myanmar, 32, ay0bjcgydhix) and (korean, 28, a1oh82lwr5hbg) are not the same in 1258 ms.
Task 34: correctly said that (atemayar, 23, a2pfktghg1ofp) and (atemayar, 23, ae0ibwt7asth) are the same in 2498 ms.
Task 35: correctly said that (atemayar, 25, a31e6jchki335) and (atemayar, 25, avlbkm6qpb1s) are the same in 1444 ms.
Task 36: correctly said that (inuktitut, 11, a3mv1jtwep7xn) and (korean, 39, a1umhcrj9zfdp) are not the same in 1292 ms.
Task 37: correctly said that (sylheti, 28, a1foggtlgo7n4) and (inuktitut, 1, a1manxzopzden) are not the same in 1170 ms.
Task 38: incorrectly said that (atemayar, 25, a1qku9mccvl35) and (atemayar, 25, azdw6062ia38) are not the same in 1241 ms.
Task 39: incorrectly said that (inuktitut, 11, a315xddd8j4x8) and (atemayar, 6, a1j8s7giyto4a) are the same in 1230 ms.
Task 40: correctly said that (tagalog, 5, a1s3njxhehcip) and (tagalog, 6, aljsxt6v0a30) are not the same in 1631 ms.
Task 41: correctly said that (sylheti, 20, atvyxlwe5n03) and (tagalog, 2, aljsxt6v0a30) are not the same in 1150 ms.
Task 42: correctly said that (myanmar, 31, a1stvgjqfl5kk) and (myanmar, 31, a1ehrxjplsq0r) are the same in 1405 ms.
Task 43: correctly said that (myanmar, 1, a2pfktghg1ofp) and (tagalog, 2, a3mv1jtwep7xn) are not the same in 1651 ms.
Task 44: incorrectly said that (myanmar, 1, ay0bjcgydhix) and (myanmar, 31, a1umhcrj9zfdp) are the same in 1323 ms.
Task 45: correctly said that (atemayar, 23, ae0ibwt7asth) and (atemayar, 23, a31e6jchki335) are the same in 1195 ms.
Task 46: correctly said that (korean, 29, a2px0aywxngce) and (myanmar, 32, adc8vbkoocrr) are not the same in 1536 ms.
Task 47: correctly said that (sylheti, 17, a3209gno9duil) and (tagalog, 10, a1j8s7giyto4a) are not the same in 2024 ms.
Task 48: correctly said that (atemayar, 10, a31xqftzcsia2) and (atemayar, 25, a3m0kt6kjezd1) are not the same in 2038 ms.
Task 49: correctly said that (tagalog, 10, asu813kb7bv1) and (tagalog, 2, asu813kb7bv1) are not the same in 989 ms.
Task 50: incorrectly said that (myanmar, 31, ay0bjcgydhix) and (myanmar, 25, a3j1pp96x14u0) are the same in 3922 ms.
Task 51: correctly said that (myanmar, 1, adc8vbkoocrr) and (myanmar, 1, a4858n7ggst4) are the same in 1306 ms.
Task 52: correctly said that (korean, 4, aw82oruamauz) and (inuktitut, 1, a1w3fo0d7cf5t) are not the same in 1362 ms.
Task 53: correctly said that (atemayar, 6, agst3co6usf8) and (atemayar, 6, a3m0kt6kjezd1) are the same in 1895 ms.
Task 54: correctly said that (sylheti, 20, avlbkm6qpb1s) and (inuktitut, 1, a2pfktghg1ofp) are not the same in 1416 ms.
Task 55: correctly said that (tagalog, 2, a1ehrxjplsq0r) and (tagalog, 6, ae0ibwt7asth) are not the same in 1730 ms.
Task 56: incorrectly said that (inuktitut, 2, a1manxzopzden) and (atemayar, 10, a1manxzopzden) are the same in 1317 ms.
Task 57: correctly said that (atemayar, 23, a1vzhyp8yvxkz) and (atemayar, 23, a31xqftzcsia2) are the same in 1154 ms.
Task 58: incorrectly said that (inuktitut, 1, a1manxzopzden) and (sylheti, 20, a4858n7ggst4) are the same in 1294 ms.
Task 59: correctly said that (tagalog, 5, ajf66jwen7bx) and (tagalog, 6, ae0ibwt7asth) are not the same in 2665 ms.
Task 60: incorrectly said that (sylheti, 19, a3np21apfa18b) and (sylheti, 19, a1s3njxhehcip) are not the same in 9031 ms.
Task 61: correctly said that (inuktitut, 1, a1manxzopzden) and (inuktitut, 1, a1z59eb5sfxpy) are the same in 1860 ms.
Task 62: incorrectly said that (atemayar, 10, a2pfktghg1ofp) and (sylheti, 28, a3209gno9duil) are the same in 3012 ms.
Task 63: correctly said that (korean, 39, a3dfh6dpi11ip) and (tagalog, 10, ae0ibwt7asth) are not the same in 1790 ms.
Task 64: incorrectly said that (atemayar, 25, ax5gghhj0o6g) and (atemayar, 25, avlbkm6qpb1s) are not the same in 1302 ms.
Task 65: incorrectly said that (sylheti, 19, a3np21apfa18b) and (sylheti, 20, a4858n7ggst4) are the same in 1440 ms.
Task 66: correctly said that (sylheti, 17, amynpl5xzw6q) and (inuktitut, 11, a3mv1jtwep7xn) are not the same in 1787 ms.
Task 67: correctly said that (korean, 4, ae0ibwt7asth) and (atemayar, 10, a31e6jchki335) are not the same in 1088 ms.
Task 68: correctly said that (atemayar, 10, a1j8s7giyto4a) and (tagalog, 10, a1i92aacrkacw) are not the same in 1869 ms.
Task 69: correctly said that (atemayar, 23, agst3co6usf8) and (atemayar, 23, a1z59eb5sfxpy) are the same in 1466 ms.
Task 70: correctly said that (korean, 29, a1s3njxhehcip) and (inuktitut, 1, ay0bjcgydhix) are not the same in 1590 ms.
Task 71: incorrectly said that (tagalog, 2, avlbkm6qpb1s) and (tagalog, 2, a3m0kt6kjezd1) are not the same in 1443 ms.
Task 72: correctly said that (myanmar, 25, ay0bjcgydhix) and (atemayar, 10, a1manxzopzden) are not the same in 985 ms.
Task 73: incorrectly said that (atemayar, 23, a1qku9mccvl35) and (korean, 29, aw82oruamauz) are the same in 1404 ms.
Task 74: correctly said that (sylheti, 28, a3m0kt6kjezd1) and (sylheti, 28, a1j8s7giyto4a) are the same in 948 ms.
Task 75: correctly said that (atemayar, 25, a1poymbentexx) and (sylheti, 17, a3r9q9wqm1nyz) are not the same in 2050 ms.
Task 76: correctly said that (sylheti, 28, a6fjbvuzrhm6) and (atemayar, 23, a1poymbentexx) are not the same in 1451 ms.
Task 77: correctly said that (tagalog, 5, amynpl5xzw6q) and (sylheti, 17, aljsxt6v0a30) are not the same in 1986 ms.
Task 78: incorrectly said that (atemayar, 10, a31e6jchki335) and (atemayar, 6, a6fjbvuzrhm6) are the same in 1812 ms.
Task 79: incorrectly said that (inuktitut, 2, a1qku9mccvl35) and (inuktitut, 2, a12xm86d2nbih) are not the same in 1285 ms.
Task 80: correctly said that (atemayar, 6, a3m0kt6kjezd1) and (inuktitut, 11, a2pfktghg1ofp) are not the same in 2649 ms.
Task 81: incorrectly said that (myanmar, 25, amynpl5xzw6q) and (myanmar, 25, a1s3njxhehcip) are not the same in 1552 ms.
Task 82: correctly said that (korean, 28, a5379luit3pc) and (tagalog, 2, a1ehrxjplsq0r) are not the same in 872 ms.
Task 83: correctly said that (inuktitut, 1, avlbkm6qpb1s) and (korean, 29, a1umhcrj9zfdp) are not the same in 1273 ms.
Task 84: incorrectly said that (atemayar, 10, a3m0kt6kjezd1) and (korean, 4, a1umhcrj9zfdp) are the same in 1746 ms.
Task 85: correctly said that (tagalog, 2, ay0bjcgydhix) and (sylheti, 20, a1foggtlgo7n4) are not the same in 1856 ms.
Task 86: incorrectly said that (korean, 39, a1v0p79fynz9j) and (atemayar, 6, a1yovlluv1c9h) are the same in 1755 ms.
Task 87: correctly said that (tagalog, 2, a1l2fzyn31zvf) and (sylheti, 19, a3np21apfa18b) are not the same in 983 ms.
Task 88: incorrectly said that (myanmar, 1, a1ehrxjplsq0r) and (myanmar, 1, a2pfktghg1ofp) are not the same in 1512 ms.
Task 89: correctly said that (myanmar, 32, a6fjbvuzrhm6) and (inuktitut, 3, a2pfktghg1ofp) are not the same in 1210 ms.
Task 90: correctly said that (myanmar, 1, a1j8s7giyto4a) and (myanmar, 1, a1s3njxhehcip) are the same in 1350 ms.
Task 91: correctly said that (inuktitut, 3, a2pfktghg1ofp) and (inuktitut, 3, a1s3njxhehcip) are the same in 2195 ms.
Task 92: correctly said that (myanmar, 31, a1foggtlgo7n4) and (korean, 4, a5379luit3pc) are not the same in 1428 ms.
Task 93: correctly said that (tagalog, 2, aghrkle9vf6z) and (inuktitut, 3, a2pgu4pi93b1q) are not the same in 1060 ms.
Task 94: incorrectly said that (atemayar, 25, a1yovlluv1c9h) and (atemayar, 25, a1vzhyp8yvxkz) are not the same in 960 ms.
Task 95: correctly said that (atemayar, 10, agst3co6usf8) and (sylheti, 17, atvyxlwe5n03) are not the same in 2103 ms.
Task 96: incorrectly said that (tagalog, 6, a12rfyr8dbn79) and (myanmar, 25, a2pfktghg1ofp) are the same in 1409 ms.
Task 97: correctly said that (sylheti, 20, a30xq7555spo8) and (inuktitut, 11, a1manxzopzden) are not the same in 2364 ms.
Task 98: incorrectly said that (korean, 4, a2ernxe6jlm4x) and (sylheti, 28, a1foggtlgo7n4) are the same in 1498 ms.
Task 99: correctly said that (inuktitut, 11, a1w3fo0d7cf5t) and (inuktitut, 11, a2ernxe6jlm4x) are the same in 1554 ms.
Task 100: correctly said that (korean, 39, aj6upe9mlarg) and (myanmar, 31, a1foggtlgo7n4) are not the same in 1362 ms.
Task 101: incorrectly said that (korean, 4, a3mv1jtwep7xn) and (korean, 29, a1oh82lwr5hbg) are the same in 1146 ms.
Task 102: incorrectly said that (atemayar, 10, azdw6062ia38) and (korean, 4, a2nm1qzuh14ls) are the same in 1576 ms.
Task 103: incorrectly said that (myanmar, 1, a1aegunf6e081) and (myanmar, 31, a1s3njxhehcip) are the same in 1113 ms.
Task 104: incorrectly said that (inuktitut, 1, avlbkm6qpb1s) and (inuktitut, 1, ay0bjcgydhix) are not the same in 1424 ms.
Task 105: correctly said that (inuktitut, 11, a2qnqd7m8zfbo) and (atemayar, 23, a1j8s7giyto4a) are not the same in 1493 ms.
Task 106: incorrectly said that (korean, 4, aav4pzxdoebd) and (sylheti, 20, a30xq7555spo8) are the same in 1336 ms.
Task 107: correctly said that (sylheti, 28, a3209gno9duil) and (tagalog, 6, avlbkm6qpb1s) are not the same in 1481 ms.
Task 108: correctly said that (inuktitut, 3, a3noo9k3y1yu5) and (inuktitut, 3, a2pgu4pi93b1q) are the same in 1433 ms.
Task 109: incorrectly said that (korean, 39, a1umhcrj9zfdp) and (korean, 39, ae0ibwt7asth) are not the same in 1460 ms.
Task 110: correctly said that (inuktitut, 3, a3qat9qkqumi2) and (tagalog, 2, avlbkm6qpb1s) are not the same in 1575 ms.
Task 111: correctly said that (inuktitut, 11, a12rfyr8dbn79) and (korean, 28, a3d7xwp0gand5) are not the same in 857 ms.
Task 112: incorrectly said that (korean, 39, ae0ibwt7asth) and (sylheti, 20, a1l2fzyn31zvf) are the same in 1616 ms.
Task 113: incorrectly said that (sylheti, 20, a1s3njxhehcip) and (tagalog, 5, a3qat9qkqumi2) are the same in 1601 ms.
Task 114: correctly said that (tagalog, 2, ae0ibwt7asth) and (atemayar, 6, a6fjbvuzrhm6) are not the same in 915 ms.
Task 115: incorrectly said that (tagalog, 10, a12rfyr8dbn79) and (tagalog, 10, a1i92aacrkacw) are not the same in 1154 ms.
Task 116: correctly said that (inuktitut, 11, a12xm86d2nbih) and (myanmar, 1, ay0bjcgydhix) are not the same in 2363 ms.
Task 117: incorrectly said that (korean, 4, a2px0aywxngce) and (korean, 4, a3mv1jtwep7xn) are not the same in 1032 ms.
Task 118: incorrectly said that (korean, 29, a2px0aywxngce) and (sylheti, 19, ay0bjcgydhix) are the same in 1929 ms.
Task 119: correctly said that (myanmar, 1, a3qat9qkqumi2) and (myanmar, 1, ay0bjcgydhix) are the same in 1696 ms.
Task 120: correctly said that (atemayar, 6, a1yovlluv1c9h) and (tagalog, 10, aghrkle9vf6z) are not the same in 1331 ms.
Task 121: correctly said that (atemayar, 10, a6fjbvuzrhm6) and (inuktitut, 11, a1z59eb5sfxpy) are not the same in 1641 ms.
Task 122: correctly said that (sylheti, 20, amynpl5xzw6q) and (atemayar, 6, awl8tng8e81d) are not the same in 1741 ms.
Task 123: incorrectly said that (tagalog, 2, aghrkle9vf6z) and (sylheti, 19, amynpl5xzw6q) are the same in 1557 ms.
Task 124: correctly said that (sylheti, 20, ay0bjcgydhix) and (sylheti, 20, a3pkun3onkucn) are the same in 1567 ms.
Task 125: correctly said that (korean, 29, a1umhcrj9zfdp) and (myanmar, 25, adc8vbkoocrr) are not the same in 2005 ms.
Task 126: correctly said that (korean, 28, a5379luit3pc) and (tagalog, 6, a2du3880jbrov) are not the same in 902 ms.
Task 127: correctly said that (myanmar, 1, a1stvgjqfl5kk) and (myanmar, 1, avlbkm6qpb1s) are the same in 1326 ms.
Task 128: correctly said that (myanmar, 31, a6fjbvuzrhm6) and (myanmar, 31, a1aegunf6e081) are the same in 1278 ms.
Task 129: correctly said that (korean, 28, a1oh82lwr5hbg) and (atemayar, 10, a31xqftzcsia2) are not the same in 977 ms.
Task 130: correctly said that (atemayar, 23, a6fjbvuzrhm6) and (sylheti, 17, a1qku9mccvl35) are not the same in 1681 ms.
Task 131: correctly said that (korean, 29, a3bb99mn3zy37) and (korean, 28, a1udqv4pfypkn) are not the same in 1085 ms.
Task 132: incorrectly said that (korean, 28, a1umhcrj9zfdp) and (korean, 4, a3mv1jtwep7xn) are the same in 1432 ms.
Task 133: correctly said that (myanmar, 1, a1clejhbx9yyn) and (myanmar, 31, aljsxt6v0a30) are not the same in 1431 ms.
Task 134: correctly said that (korean, 39, a3bb99mn3zy37) and (korean, 28, a1umhcrj9zfdp) are not the same in 1161 ms.
Task 135: correctly said that (atemayar, 23, a6fjbvuzrhm6) and (atemayar, 23, a1z59eb5sfxpy) are the same in 1120 ms.
Task 136: correctly said that (atemayar, 23, avlbkm6qpb1s) and (sylheti, 28, aljsxt6v0a30) are not the same in 1468 ms.
Task 137: correctly said that (myanmar, 32, a1clejhbx9yyn) and (inuktitut, 1, a1w3fo0d7cf5t) are not the same in 868 ms.
Task 138: correctly said that (myanmar, 32, a2pfktghg1ofp) and (myanmar, 32, a1aegunf6e081) are the same in 1744 ms.
Task 139: correctly said that (myanmar, 31, a1umhcrj9zfdp) and (korean, 39, a3bb99mn3zy37) are not the same in 1344 ms.
Task 140: correctly said that (inuktitut, 11, a3noo9k3y1yu5) and (inuktitut, 1, a2pgu4pi93b1q) are not the same in 3576 ms.
Task 141: correctly said that (inuktitut, 11, a2pfktghg1ofp) and (atemayar, 10, a3m0kt6kjezd1) are not the same in 1250 ms.
Task 142: incorrectly said that (myanmar, 31, a3qat9qkqumi2) and (myanmar, 32, ajf66jwen7bx) are the same in 1255 ms.
Task 143: correctly said that (inuktitut, 3, a1manxzopzden) and (inuktitut, 3, a1s3njxhehcip) are the same in 805 ms.
Task 144: incorrectly said that (sylheti, 19, a1a25pyfoa9ge) and (sylheti, 19, a1qku9mccvl35) are not the same in 2233 ms.
Task 145: correctly said that (myanmar, 25, a1foggtlgo7n4) and (myanmar, 25, a1ehrxjplsq0r) are the same in 1634 ms.
Task 146: correctly said that (inuktitut, 11, aljsxt6v0a30) and (korean, 28, a3mv1jtwep7xn) are not the same in 971 ms.
Task 147: incorrectly said that (atemayar, 10, a6fjbvuzrhm6) and (atemayar, 10, agst3co6usf8) are not the same in 2269 ms.
Task 148: correctly said that (atemayar, 6, a1j8s7giyto4a) and (atemayar, 6, a3m0kt6kjezd1) are the same in 1521 ms.
Task 149: correctly said that (tagalog, 5, a1s3njxhehcip) and (sylheti, 19, a1qku9mccvl35) are not the same in 3477 ms.
Task 150: correctly said that (tagalog, 10, a1a25pyfoa9ge) and (sylheti, 28, atvyxlwe5n03) are not the same in 1945 ms.
Task 151: correctly said that (sylheti, 28, a3pkun3onkucn) and (myanmar, 1, a6fjbvuzrhm6) are not the same in 1314 ms.
Task 152: incorrectly said that (myanmar, 31, a3pkun3onkucn) and (myanmar, 1, a3pkun3onkucn) are the same in 1186 ms.
Task 153: correctly said that (inuktitut, 1, a3noo9k3y1yu5) and (atemayar, 6, awl8tng8e81d) are not the same in 993 ms.
Task 154: incorrectly said that (tagalog, 6, asu813kb7bv1) and (tagalog, 6, a1i92aacrkacw) are not the same in 1377 ms.
Task 155: correctly said that (sylheti, 20, a3m0kt6kjezd1) and (myanmar, 31, avlbkm6qpb1s) are not the same in 858 ms.
Task 156: correctly said that (korean, 29, a2ernxe6jlm4x) and (myanmar, 31, ay0bjcgydhix) are not the same in 886 ms.
Task 157: correctly said that (myanmar, 1, a1j8s7giyto4a) and (tagalog, 10, avlbkm6qpb1s) are not the same in 961 ms.
Task 158: incorrectly said that (atemayar, 25, aljsxt6v0a30) and (sylheti, 19, a1j8s7giyto4a) are the same in 1259 ms.
Task 159: incorrectly said that (sylheti, 17, ay0bjcgydhix) and (inuktitut, 11, a3noo9k3y1yu5) are the same in 2119 ms.
Task 160: correctly said that (korean, 39, a1udqv4pfypkn) and (atemayar, 6, avlbkm6qpb1s) are not the same in 1565 ms.
Task 161: correctly said that (myanmar, 1, adc8vbkoocrr) and (myanmar, 1, amynpl5xzw6q) are the same in 1157 ms.
Task 162: incorrectly said that (tagalog, 2, a1l2fzyn31zvf) and (tagalog, 2, a3mv1jtwep7xn) are not the same in 3179 ms.
Task 163: correctly said that (sylheti, 28, a1j8s7giyto4a) and (sylheti, 28, a1a25pyfoa9ge) are the same in 1319 ms.
Task 164: correctly said that (myanmar, 1, a1clejhbx9yyn) and (myanmar, 1, a6fjbvuzrhm6) are the same in 1270 ms.
Task 165: correctly said that (inuktitut, 11, a1w3fo0d7cf5t) and (inuktitut, 11, a12xm86d2nbih) are the same in 1418 ms.
Task 166: incorrectly said that (atemayar, 23, a1qku9mccvl35) and (atemayar, 23, ae0ibwt7asth) are not the same in 2492 ms.
Task 167: correctly said that (atemayar, 6, agst3co6usf8) and (atemayar, 6, ax5gghhj0o6g) are the same in 1452 ms.
Task 168: correctly said that (myanmar, 31, a6fjbvuzrhm6) and (inuktitut, 2, avlbkm6qpb1s) are not the same in 1170 ms.
Task 169: correctly said that (inuktitut, 2, a315xddd8j4x8) and (inuktitut, 2, a3qat9qkqumi2) are the same in 1039 ms.
Task 170: correctly said that (korean, 29, a1v0p79fynz9j) and (inuktitut, 1, a1qku9mccvl35) are not the same in 2374 ms.
Task 171: correctly said that (tagalog, 10, a12rfyr8dbn79) and (korean, 4, a3mv1jtwep7xn) are not the same in 962 ms.
Task 172: correctly said that (tagalog, 2, asu813kb7bv1) and (myanmar, 25, a3pkun3onkucn) are not the same in 826 ms.
Task 173: correctly said that (tagalog, 6, a1l2fzyn31zvf) and (atemayar, 23, ax5gghhj0o6g) are not the same in 883 ms.
Task 174: incorrectly said that (tagalog, 6, aghrkle9vf6z) and (sylheti, 17, ay0bjcgydhix) are the same in 1506 ms.
Task 175: correctly said that (sylheti, 19, a3np21apfa18b) and (inuktitut, 11, a1manxzopzden) are not the same in 1653 ms.
Task 176: correctly said that (korean, 4, aj6upe9mlarg) and (tagalog, 6, a3mv1jtwep7xn) are not the same in 902 ms.
Task 177: incorrectly said that (sylheti, 28, a1foggtlgo7n4) and (inuktitut, 11, a3mv1jtwep7xn) are the same in 1916 ms.
Task 178: correctly said that (inuktitut, 11, a2ernxe6jlm4x) and (tagalog, 10, a3mv1jtwep7xn) are not the same in 2922 ms.
Task 179: correctly said that (sylheti, 20, aljsxt6v0a30) and (atemayar, 6, ae0ibwt7asth) are not the same in 861 ms.
Task 180: correctly said that (myanmar, 25, a6fjbvuzrhm6) and (tagalog, 5, ae0ibwt7asth) are not the same in 5323 ms.
Task 181: correctly said that (myanmar, 31, amynpl5xzw6q) and (atemayar, 10, ae0ibwt7asth) are not the same in 873 ms.
Task 182: incorrectly said that (sylheti, 20, a3m0kt6kjezd1) and (atemayar, 23, awl8tng8e81d) are the same in 1252 ms.
Task 183: correctly said that (tagalog, 6, a3mv1jtwep7xn) and (tagalog, 6, aghrkle9vf6z) are the same in 1568 ms.
Task 184: incorrectly said that (korean, 39, aav4pzxdoebd) and (atemayar, 6, a1vzhyp8yvxkz) are the same in 1650 ms.
Task 185: correctly said that (tagalog, 6, ae0ibwt7asth) and (tagalog, 2, aghrkle9vf6z) are not the same in 2774 ms.
Task 186: correctly said that (korean, 4, aw82oruamauz) and (korean, 4, a2ernxe6jlm4x) are the same in 1380 ms.
Task 187: correctly said that (atemayar, 10, a3m0kt6kjezd1) and (atemayar, 10, awl8tng8e81d) are the same in 1208 ms.
Task 188: correctly said that (tagalog, 6, ay0bjcgydhix) and (tagalog, 6, asu813kb7bv1) are the same in 1818 ms.
Task 189: incorrectly said that (myanmar, 32, a1j8s7giyto4a) and (myanmar, 31, a1s3njxhehcip) are the same in 1406 ms.
Task 190: correctly said that (sylheti, 17, a1j8s7giyto4a) and (tagalog, 6, ae0ibwt7asth) are not the same in 1583 ms.
Task 191: correctly said that (inuktitut, 2, a1j8s7giyto4a) and (tagalog, 6, amynpl5xzw6q) are not the same in 860 ms.
Task 192: correctly said that (inuktitut, 3, ae0ibwt7asth) and (inuktitut, 11, a315xddd8j4x8) are not the same in 866 ms.
Task 193: correctly said that (korean, 28, ae0ibwt7asth) and (korean, 28, a3mv1jtwep7xn) are the same in 1329 ms.
Task 194: correctly said that (atemayar, 23, a31e6jchki335) and (atemayar, 6, a1poymbentexx) are not the same in 1863 ms.
Task 195: incorrectly said that (tagalog, 6, a3mv1jtwep7xn) and (tagalog, 6, a1ehrxjplsq0r) are not the same in 1450 ms.
Task 196: correctly said that (myanmar, 1, a1foggtlgo7n4) and (myanmar, 1, ay0bjcgydhix) are the same in 1295 ms.
Task 197: correctly said that (tagalog, 10, a3m0kt6kjezd1) and (tagalog, 6, avlbkm6qpb1s) are not the same in 2119 ms.
Task 198: correctly said that (sylheti, 28, ay0bjcgydhix) and (korean, 4, a2nm1qzuh14ls) are not the same in 2195 ms.
Task 199: correctly said that (inuktitut, 1, a1j8s7giyto4a) and (sylheti, 20, a3m0kt6kjezd1) are not the same in 1211 ms.

Duration: 20m 48s 638ms
Comments: 