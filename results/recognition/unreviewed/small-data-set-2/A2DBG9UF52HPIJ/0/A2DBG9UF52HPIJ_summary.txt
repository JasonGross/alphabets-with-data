

Summary:Short Summary:
Correct: 111
Incorrect: 89
Percent Correct: 55

Same Correct: 48
Same Incorrect: 13
Percent Same Correct: 78

Different Correct: 63
Different Incorrect: 76
Percent Different Correct: 45

Long Summary:
Task 0: correctly said that (atemayar, 10, avlbkm6qpb1s) and (atemayar, 10, a1yovlluv1c9h) are the same in 7480 ms.
Task 1: incorrectly said that (myanmar, 32, ay0bjcgydhix) and (myanmar, 31, a1umhcrj9zfdp) are the same in 2876 ms.
Task 2: incorrectly said that (inuktitut, 11, a12xm86d2nbih) and (inuktitut, 11, a3qat9qkqumi2) are not the same in 2176 ms.
Task 3: correctly said that (inuktitut, 1, a1manxzopzden) and (inuktitut, 1, a3noo9k3y1yu5) are the same in 2537 ms.
Task 4: incorrectly said that (sylheti, 20, ay0bjcgydhix) and (sylheti, 28, a3pkun3onkucn) are the same in 1544 ms.
Task 5: correctly said that (atemayar, 6, a1vzhyp8yvxkz) and (sylheti, 28, a3m0kt6kjezd1) are not the same in 2219 ms.
Task 6: correctly said that (tagalog, 5, ay0bjcgydhix) and (tagalog, 5, ajf66jwen7bx) are the same in 2062 ms.
Task 7: correctly said that (inuktitut, 1, ae0ibwt7asth) and (korean, 4, a2nm1qzuh14ls) are not the same in 1638 ms.
Task 8: incorrectly said that (tagalog, 6, a1i92aacrkacw) and (tagalog, 2, asu813kb7bv1) are the same in 1746 ms.
Task 9: incorrectly said that (myanmar, 31, a1foggtlgo7n4) and (myanmar, 25, a1s3njxhehcip) are the same in 3056 ms.
Task 10: correctly said that (atemayar, 23, a1qku9mccvl35) and (tagalog, 6, a2du3880jbrov) are not the same in 1510 ms.
Task 11: incorrectly said that (myanmar, 25, a6fjbvuzrhm6) and (tagalog, 5, ay0bjcgydhix) are the same in 1974 ms.
Task 12: incorrectly said that (sylheti, 19, a3np21apfa18b) and (korean, 29, a1udqv4pfypkn) are the same in 2241 ms.
Task 13: correctly said that (myanmar, 25, aljsxt6v0a30) and (korean, 4, a1s3njxhehcip) are not the same in 2005 ms.
Task 14: correctly said that (atemayar, 25, a6fjbvuzrhm6) and (atemayar, 10, a3m0kt6kjezd1) are not the same in 3039 ms.
Task 15: correctly said that (korean, 4, a3dfh6dpi11ip) and (sylheti, 17, a3209gno9duil) are not the same in 2205 ms.
Task 16: incorrectly said that (tagalog, 2, asu813kb7bv1) and (tagalog, 5, a1j8s7giyto4a) are the same in 1097 ms.
Task 17: correctly said that (inuktitut, 1, a2pfktghg1ofp) and (inuktitut, 3, a1manxzopzden) are not the same in 1236 ms.
Task 18: correctly said that (inuktitut, 3, a2pgu4pi93b1q) and (inuktitut, 3, avlbkm6qpb1s) are the same in 1574 ms.
Task 19: correctly said that (inuktitut, 3, a1j8s7giyto4a) and (atemayar, 23, a2pfktghg1ofp) are not the same in 1213 ms.
Task 20: incorrectly said that (myanmar, 32, amynpl5xzw6q) and (myanmar, 31, a1aegunf6e081) are the same in 1278 ms.
Task 21: incorrectly said that (inuktitut, 2, a12xm86d2nbih) and (myanmar, 32, avlbkm6qpb1s) are the same in 1276 ms.
Task 22: correctly said that (myanmar, 32, a2pfktghg1ofp) and (korean, 29, a2px0aywxngce) are not the same in 1188 ms.
Task 23: correctly said that (myanmar, 1, a1j8s7giyto4a) and (atemayar, 25, avlbkm6qpb1s) are not the same in 915 ms.
Task 24: correctly said that (sylheti, 28, a1a25pyfoa9ge) and (sylheti, 28, a1foggtlgo7n4) are the same in 2165 ms.
Task 25: correctly said that (tagalog, 5, asu813kb7bv1) and (tagalog, 5, a1i92aacrkacw) are the same in 1333 ms.
Task 26: incorrectly said that (inuktitut, 11, a2ernxe6jlm4x) and (tagalog, 5, aljsxt6v0a30) are the same in 1456 ms.
Task 27: correctly said that (tagalog, 5, ajf66jwen7bx) and (sylheti, 20, a4858n7ggst4) are not the same in 1149 ms.
Task 28: correctly said that (atemayar, 6, a1z59eb5sfxpy) and (myanmar, 25, a1j8s7giyto4a) are not the same in 1145 ms.
Task 29: incorrectly said that (tagalog, 10, a2du3880jbrov) and (tagalog, 10, aghrkle9vf6z) are not the same in 1403 ms.
Task 30: incorrectly said that (atemayar, 25, a1manxzopzden) and (tagalog, 6, aghrkle9vf6z) are the same in 2800 ms.
Task 31: correctly said that (sylheti, 19, a3m0kt6kjezd1) and (inuktitut, 1, a3noo9k3y1yu5) are not the same in 1431 ms.
Task 32: incorrectly said that (sylheti, 19, a3209gno9duil) and (sylheti, 19, a1a25pyfoa9ge) are not the same in 1307 ms.
Task 33: incorrectly said that (myanmar, 31, a1umhcrj9zfdp) and (atemayar, 6, a1z59eb5sfxpy) are the same in 1816 ms.
Task 34: incorrectly said that (korean, 28, a2nm1qzuh14ls) and (korean, 28, a1s3njxhehcip) are not the same in 1144 ms.
Task 35: correctly said that (myanmar, 32, a3qat9qkqumi2) and (korean, 4, a2px0aywxngce) are not the same in 1529 ms.
Task 36: incorrectly said that (tagalog, 2, a3m0kt6kjezd1) and (sylheti, 17, a3m0kt6kjezd1) are the same in 2045 ms.
Task 37: correctly said that (atemayar, 23, a31xqftzcsia2) and (atemayar, 23, a31e6jchki335) are the same in 1593 ms.
Task 38: incorrectly said that (tagalog, 10, a1l2fzyn31zvf) and (sylheti, 20, a1qku9mccvl35) are the same in 1181 ms.
Task 39: correctly said that (atemayar, 25, azdw6062ia38) and (atemayar, 25, aljsxt6v0a30) are the same in 5112 ms.
Task 40: incorrectly said that (myanmar, 25, a1clejhbx9yyn) and (myanmar, 1, aljsxt6v0a30) are the same in 1608 ms.
Task 41: incorrectly said that (myanmar, 31, a1umhcrj9zfdp) and (myanmar, 1, ay0bjcgydhix) are the same in 1277 ms.
Task 42: incorrectly said that (atemayar, 23, agst3co6usf8) and (korean, 29, a1manxzopzden) are the same in 3635 ms.
Task 43: incorrectly said that (tagalog, 10, a1a25pyfoa9ge) and (korean, 28, a2px0aywxngce) are the same in 1514 ms.
Task 44: incorrectly said that (korean, 4, a1umhcrj9zfdp) and (korean, 4, a2ernxe6jlm4x) are not the same in 1527 ms.
Task 45: correctly said that (sylheti, 20, a1foggtlgo7n4) and (atemayar, 6, a31xqftzcsia2) are not the same in 1630 ms.
Task 46: incorrectly said that (sylheti, 20, avlbkm6qpb1s) and (sylheti, 28, a3209gno9duil) are the same in 1275 ms.
Task 47: correctly said that (myanmar, 31, avlbkm6qpb1s) and (atemayar, 6, a1j8s7giyto4a) are not the same in 3302 ms.
Task 48: incorrectly said that (myanmar, 31, a1foggtlgo7n4) and (myanmar, 31, avlbkm6qpb1s) are not the same in 1346 ms.
Task 49: correctly said that (sylheti, 28, a1a25pyfoa9ge) and (sylheti, 20, a1l2fzyn31zvf) are not the same in 918 ms.
Task 50: correctly said that (inuktitut, 1, ay0bjcgydhix) and (inuktitut, 1, a3qat9qkqumi2) are the same in 2148 ms.
Task 51: incorrectly said that (korean, 4, a2akud1gdstsd) and (korean, 29, a2px0aywxngce) are the same in 1874 ms.
Task 52: incorrectly said that (atemayar, 6, awl8tng8e81d) and (sylheti, 28, a3m0kt6kjezd1) are the same in 864 ms.
Task 53: incorrectly said that (tagalog, 2, a2du3880jbrov) and (myanmar, 25, aljsxt6v0a30) are the same in 2150 ms.
Task 54: incorrectly said that (inuktitut, 1, a3qat9qkqumi2) and (tagalog, 10, a1l2fzyn31zvf) are the same in 1208 ms.
Task 55: correctly said that (tagalog, 5, a1ehrxjplsq0r) and (tagalog, 5, a1yovlluv1c9h) are the same in 867 ms.
Task 56: correctly said that (tagalog, 6, a1i92aacrkacw) and (korean, 29, a2nm1qzuh14ls) are not the same in 1839 ms.
Task 57: correctly said that (sylheti, 17, a3pkun3onkucn) and (sylheti, 17, a1foggtlgo7n4) are the same in 1219 ms.
Task 58: correctly said that (myanmar, 32, a1umhcrj9zfdp) and (inuktitut, 2, a3qat9qkqumi2) are not the same in 2055 ms.
Task 59: correctly said that (sylheti, 28, a1j8s7giyto4a) and (sylheti, 28, ay0bjcgydhix) are the same in 856 ms.
Task 60: incorrectly said that (inuktitut, 11, avlbkm6qpb1s) and (inuktitut, 2, a1j8s7giyto4a) are the same in 2158 ms.
Task 61: incorrectly said that (inuktitut, 3, a2qnqd7m8zfbo) and (myanmar, 1, a2pfktghg1ofp) are the same in 1524 ms.
Task 62: correctly said that (sylheti, 28, a3m0kt6kjezd1) and (korean, 39, a2px0aywxngce) are not the same in 1300 ms.
Task 63: correctly said that (korean, 29, ae0ibwt7asth) and (tagalog, 5, a1s3njxhehcip) are not the same in 1264 ms.
Task 64: correctly said that (korean, 29, ae0ibwt7asth) and (inuktitut, 3, ae0ibwt7asth) are not the same in 1181 ms.
Task 65: incorrectly said that (atemayar, 10, a1z59eb5sfxpy) and (korean, 39, aw82oruamauz) are the same in 2232 ms.
Task 66: correctly said that (sylheti, 17, avlbkm6qpb1s) and (sylheti, 17, a30xq7555spo8) are the same in 4265 ms.
Task 67: incorrectly said that (myanmar, 25, a1clejhbx9yyn) and (inuktitut, 3, a12rfyr8dbn79) are the same in 3779 ms.
Task 68: correctly said that (myanmar, 1, a1clejhbx9yyn) and (myanmar, 1, aljsxt6v0a30) are the same in 1091 ms.
Task 69: correctly said that (atemayar, 23, aljsxt6v0a30) and (inuktitut, 2, a2pfktghg1ofp) are not the same in 1577 ms.
Task 70: incorrectly said that (sylheti, 20, a3pkun3onkucn) and (sylheti, 28, a3pkun3onkucn) are the same in 1125 ms.
Task 71: correctly said that (sylheti, 19, a6fjbvuzrhm6) and (tagalog, 10, a1a25pyfoa9ge) are not the same in 2285 ms.
Task 72: incorrectly said that (atemayar, 10, a1poymbentexx) and (atemayar, 10, ae0ibwt7asth) are the same in 1526 ms.
Task 73: correctly said that (inuktitut, 1, ae0ibwt7asth) and (atemayar, 25, a1qku9mccvl35) are not the same in 1298 ms.
Task 74: incorrectly said that (inuktitut, 2, a1manxzopzden) and (sylheti, 20, aljsxt6v0a30) are the same in 1634 ms.
Task 75: incorrectly said that (atemayar, 23, a1vzhyp8yvxkz) and (atemayar, 6, a23psx2z4f65q) are the same in 1398 ms.
Task 76: correctly said that (inuktitut, 11, aljsxt6v0a30) and (atemayar, 10, a1manxzopzden) are not the same in 2145 ms.
Task 77: correctly said that (atemayar, 23, avlbkm6qpb1s) and (tagalog, 6, aghrkle9vf6z) are not the same in 1495 ms.
Task 78: incorrectly said that (tagalog, 5, a1ehrxjplsq0r) and (atemayar, 6, agst3co6usf8) are the same in 1328 ms.
Task 79: incorrectly said that (tagalog, 10, a1a25pyfoa9ge) and (atemayar, 10, a3m0kt6kjezd1) are the same in 1362 ms.
Task 80: incorrectly said that (myanmar, 25, a4858n7ggst4) and (myanmar, 31, a1umhcrj9zfdp) are the same in 1550 ms.
Task 81: incorrectly said that (atemayar, 6, a23psx2z4f65q) and (korean, 4, a2px0aywxngce) are the same in 1812 ms.
Task 82: incorrectly said that (inuktitut, 11, a3mv1jtwep7xn) and (sylheti, 28, a1j8s7giyto4a) are the same in 1096 ms.
Task 83: incorrectly said that (tagalog, 6, a3qat9qkqumi2) and (tagalog, 6, a12rfyr8dbn79) are not the same in 1590 ms.
Task 84: incorrectly said that (inuktitut, 1, a2pgu4pi93b1q) and (sylheti, 28, a1j8s7giyto4a) are the same in 1313 ms.
Task 85: incorrectly said that (atemayar, 25, a1qku9mccvl35) and (atemayar, 25, a1manxzopzden) are not the same in 2332 ms.
Task 86: correctly said that (sylheti, 28, a2du3880jbrov) and (tagalog, 2, aghrkle9vf6z) are not the same in 1650 ms.
Task 87: correctly said that (atemayar, 25, a31e6jchki335) and (inuktitut, 11, a1z59eb5sfxpy) are not the same in 2090 ms.
Task 88: correctly said that (tagalog, 2, a1l2fzyn31zvf) and (myanmar, 32, a1umhcrj9zfdp) are not the same in 1185 ms.
Task 89: correctly said that (inuktitut, 11, a2qnqd7m8zfbo) and (tagalog, 2, avlbkm6qpb1s) are not the same in 1349 ms.
Task 90: incorrectly said that (sylheti, 19, a3m0kt6kjezd1) and (sylheti, 19, a6fjbvuzrhm6) are the same in 1057 ms.
Task 91: incorrectly said that (korean, 29, ae0ibwt7asth) and (sylheti, 19, a3r9q9wqm1nyz) are the same in 1333 ms.
Task 92: incorrectly said that (tagalog, 2, aghrkle9vf6z) and (tagalog, 6, a1j8s7giyto4a) are the same in 1147 ms.
Task 93: correctly said that (inuktitut, 1, a2pfktghg1ofp) and (atemayar, 23, a1yovlluv1c9h) are not the same in 1959 ms.
Task 94: incorrectly said that (korean, 39, ae0ibwt7asth) and (atemayar, 10, a2pfktghg1ofp) are the same in 1527 ms.
Task 95: correctly said that (sylheti, 20, a3209gno9duil) and (tagalog, 6, a1i92aacrkacw) are not the same in 2398 ms.
Task 96: correctly said that (korean, 4, a1manxzopzden) and (korean, 4, a5379luit3pc) are the same in 1196 ms.
Task 97: correctly said that (korean, 4, a2ernxe6jlm4x) and (korean, 4, aav4pzxdoebd) are the same in 1086 ms.
Task 98: incorrectly said that (korean, 29, a1s3njxhehcip) and (tagalog, 2, ay0bjcgydhix) are the same in 2916 ms.
Task 99: incorrectly said that (sylheti, 17, a3pkun3onkucn) and (atemayar, 6, a1yovlluv1c9h) are the same in 1301 ms.
Task 100: correctly said that (atemayar, 10, a6fjbvuzrhm6) and (atemayar, 10, a1poymbentexx) are the same in 1183 ms.
Task 101: correctly said that (atemayar, 23, a3m0kt6kjezd1) and (tagalog, 5, a1ehrxjplsq0r) are not the same in 1710 ms.
Task 102: correctly said that (inuktitut, 11, ae0ibwt7asth) and (inuktitut, 11, a1manxzopzden) are the same in 936 ms.
Task 103: correctly said that (tagalog, 6, ae0ibwt7asth) and (tagalog, 6, a1l2fzyn31zvf) are the same in 1196 ms.
Task 104: incorrectly said that (korean, 29, ae0ibwt7asth) and (myanmar, 1, ay0bjcgydhix) are the same in 1152 ms.
Task 105: incorrectly said that (sylheti, 20, atvyxlwe5n03) and (tagalog, 2, ay0bjcgydhix) are the same in 1749 ms.
Task 106: incorrectly said that (atemayar, 6, ae0ibwt7asth) and (tagalog, 6, ay0bjcgydhix) are the same in 2865 ms.
Task 107: correctly said that (sylheti, 20, a1a25pyfoa9ge) and (korean, 4, aav4pzxdoebd) are not the same in 1433 ms.
Task 108: incorrectly said that (sylheti, 28, a3np21apfa18b) and (sylheti, 17, ay0bjcgydhix) are the same in 2146 ms.
Task 109: correctly said that (atemayar, 6, awl8tng8e81d) and (atemayar, 6, a3m0kt6kjezd1) are the same in 1174 ms.
Task 110: correctly said that (atemayar, 23, awl8tng8e81d) and (myanmar, 25, amynpl5xzw6q) are not the same in 5796 ms.
Task 111: incorrectly said that (korean, 39, a2px0aywxngce) and (myanmar, 1, a1clejhbx9yyn) are the same in 1510 ms.
Task 112: incorrectly said that (atemayar, 10, a1qku9mccvl35) and (inuktitut, 3, a1s3njxhehcip) are the same in 4564 ms.
Task 113: correctly said that (inuktitut, 11, a3noo9k3y1yu5) and (inuktitut, 2, a3mv1jtwep7xn) are not the same in 1595 ms.
Task 114: correctly said that (sylheti, 17, a3r9q9wqm1nyz) and (sylheti, 17, a30xq7555spo8) are the same in 962 ms.
Task 115: correctly said that (myanmar, 25, amynpl5xzw6q) and (myanmar, 25, aljsxt6v0a30) are the same in 2849 ms.
Task 116: correctly said that (korean, 39, a3mv1jtwep7xn) and (korean, 39, a5379luit3pc) are the same in 1150 ms.
Task 117: correctly said that (korean, 29, a3d7xwp0gand5) and (tagalog, 6, ay0bjcgydhix) are not the same in 1625 ms.
Task 118: correctly said that (tagalog, 10, aghrkle9vf6z) and (tagalog, 10, a3qat9qkqumi2) are the same in 1061 ms.
Task 119: correctly said that (tagalog, 10, a1l2fzyn31zvf) and (atemayar, 10, a3m0kt6kjezd1) are not the same in 2373 ms.
Task 120: correctly said that (tagalog, 2, ae0ibwt7asth) and (atemayar, 23, a1z59eb5sfxpy) are not the same in 1353 ms.
Task 121: incorrectly said that (tagalog, 5, a3qat9qkqumi2) and (korean, 28, a2nm1qzuh14ls) are the same in 2052 ms.
Task 122: incorrectly said that (myanmar, 31, ajf66jwen7bx) and (atemayar, 10, ax5gghhj0o6g) are the same in 1288 ms.
Task 123: correctly said that (myanmar, 1, a1ehrxjplsq0r) and (myanmar, 1, a1umhcrj9zfdp) are the same in 807 ms.
Task 124: correctly said that (tagalog, 2, a3m0kt6kjezd1) and (tagalog, 2, a1yovlluv1c9h) are the same in 1550 ms.
Task 125: correctly said that (korean, 4, a3dfh6dpi11ip) and (korean, 4, a3bb99mn3zy37) are the same in 785 ms.
Task 126: correctly said that (korean, 28, ae0ibwt7asth) and (sylheti, 20, a1a25pyfoa9ge) are not the same in 1880 ms.
Task 127: correctly said that (korean, 39, a3d7xwp0gand5) and (korean, 39, a2ernxe6jlm4x) are the same in 4647 ms.
Task 128: incorrectly said that (atemayar, 23, azdw6062ia38) and (myanmar, 32, a4858n7ggst4) are the same in 1336 ms.
Task 129: incorrectly said that (inuktitut, 11, a1j8s7giyto4a) and (inuktitut, 11, avlbkm6qpb1s) are not the same in 1424 ms.
Task 130: correctly said that (korean, 29, a2ernxe6jlm4x) and (korean, 29, a1udqv4pfypkn) are the same in 1104 ms.
Task 131: correctly said that (myanmar, 31, ay0bjcgydhix) and (inuktitut, 11, a315xddd8j4x8) are not the same in 1015 ms.
Task 132: correctly said that (inuktitut, 11, ay0bjcgydhix) and (myanmar, 31, amynpl5xzw6q) are not the same in 1351 ms.
Task 133: correctly said that (inuktitut, 11, a2pfktghg1ofp) and (inuktitut, 1, a1s3njxhehcip) are not the same in 2885 ms.
Task 134: correctly said that (inuktitut, 3, a1manxzopzden) and (inuktitut, 3, a1w3fo0d7cf5t) are the same in 1089 ms.
Task 135: incorrectly said that (tagalog, 5, a2du3880jbrov) and (atemayar, 10, a31xqftzcsia2) are the same in 993 ms.
Task 136: correctly said that (myanmar, 32, ay0bjcgydhix) and (korean, 4, aj6upe9mlarg) are not the same in 3001 ms.
Task 137: incorrectly said that (inuktitut, 3, a1qku9mccvl35) and (inuktitut, 3, a2ernxe6jlm4x) are not the same in 773 ms.
Task 138: correctly said that (tagalog, 5, aghrkle9vf6z) and (inuktitut, 1, a12rfyr8dbn79) are not the same in 1257 ms.
Task 139: correctly said that (inuktitut, 11, a12rfyr8dbn79) and (inuktitut, 3, a1w3fo0d7cf5t) are not the same in 1615 ms.
Task 140: correctly said that (tagalog, 6, ajf66jwen7bx) and (sylheti, 17, a1foggtlgo7n4) are not the same in 3624 ms.
Task 141: correctly said that (myanmar, 31, amynpl5xzw6q) and (tagalog, 6, a3m0kt6kjezd1) are not the same in 4702 ms.
Task 142: incorrectly said that (korean, 39, a2nm1qzuh14ls) and (sylheti, 17, a1qku9mccvl35) are the same in 1828 ms.
Task 143: correctly said that (tagalog, 10, a12rfyr8dbn79) and (tagalog, 6, amynpl5xzw6q) are not the same in 1374 ms.
Task 144: correctly said that (sylheti, 20, ay0bjcgydhix) and (sylheti, 20, a3r9q9wqm1nyz) are the same in 1326 ms.
Task 145: incorrectly said that (atemayar, 23, a31e6jchki335) and (myanmar, 31, aljsxt6v0a30) are the same in 2760 ms.
Task 146: correctly said that (inuktitut, 1, a2pfktghg1ofp) and (inuktitut, 1, a2pgu4pi93b1q) are the same in 3082 ms.
Task 147: correctly said that (myanmar, 1, avlbkm6qpb1s) and (myanmar, 1, a1umhcrj9zfdp) are the same in 789 ms.
Task 148: incorrectly said that (tagalog, 5, a3mv1jtwep7xn) and (korean, 29, a1udqv4pfypkn) are the same in 1179 ms.
Task 149: incorrectly said that (sylheti, 20, a3r9q9wqm1nyz) and (korean, 28, a1oh82lwr5hbg) are the same in 3918 ms.
Task 150: correctly said that (sylheti, 28, a3pkun3onkucn) and (tagalog, 2, a3mv1jtwep7xn) are not the same in 1768 ms.
Task 151: incorrectly said that (sylheti, 19, a3209gno9duil) and (korean, 29, a1v0p79fynz9j) are the same in 3713 ms.
Task 152: correctly said that (korean, 39, a3d7xwp0gand5) and (korean, 39, a2akud1gdstsd) are the same in 1797 ms.
Task 153: incorrectly said that (inuktitut, 2, avlbkm6qpb1s) and (inuktitut, 2, a12rfyr8dbn79) are not the same in 902 ms.
Task 154: correctly said that (tagalog, 10, a1s3njxhehcip) and (tagalog, 10, ay0bjcgydhix) are the same in 1069 ms.
Task 155: correctly said that (tagalog, 5, a1i92aacrkacw) and (tagalog, 5, a1j8s7giyto4a) are the same in 1304 ms.
Task 156: incorrectly said that (inuktitut, 11, a2pfktghg1ofp) and (sylheti, 17, a3r9q9wqm1nyz) are the same in 1090 ms.
Task 157: incorrectly said that (korean, 28, a2nm1qzuh14ls) and (sylheti, 20, a1foggtlgo7n4) are the same in 916 ms.
Task 158: incorrectly said that (sylheti, 20, a1j8s7giyto4a) and (inuktitut, 1, ae0ibwt7asth) are the same in 2941 ms.
Task 159: incorrectly said that (korean, 29, a1v0p79fynz9j) and (atemayar, 6, a1yovlluv1c9h) are the same in 1164 ms.
Task 160: correctly said that (atemayar, 10, a1vzhyp8yvxkz) and (atemayar, 10, azdw6062ia38) are the same in 2605 ms.
Task 161: incorrectly said that (inuktitut, 3, a3noo9k3y1yu5) and (inuktitut, 2, a3mv1jtwep7xn) are the same in 1294 ms.
Task 162: correctly said that (myanmar, 1, a3j1pp96x14u0) and (inuktitut, 1, ay0bjcgydhix) are not the same in 1043 ms.
Task 163: correctly said that (korean, 4, a3dfh6dpi11ip) and (korean, 4, aav4pzxdoebd) are the same in 1042 ms.
Task 164: incorrectly said that (korean, 28, a1v0p79fynz9j) and (korean, 28, a3d7xwp0gand5) are not the same in 1286 ms.
Task 165: correctly said that (tagalog, 2, ae0ibwt7asth) and (atemayar, 23, a2pfktghg1ofp) are not the same in 986 ms.
Task 166: incorrectly said that (tagalog, 2, a1yovlluv1c9h) and (tagalog, 6, a3mv1jtwep7xn) are the same in 1223 ms.
Task 167: incorrectly said that (sylheti, 28, a2du3880jbrov) and (sylheti, 17, a3m0kt6kjezd1) are the same in 1061 ms.
Task 168: incorrectly said that (tagalog, 10, ay0bjcgydhix) and (sylheti, 19, a1a25pyfoa9ge) are the same in 1146 ms.
Task 169: incorrectly said that (korean, 29, a2ernxe6jlm4x) and (korean, 39, a1manxzopzden) are the same in 1171 ms.
Task 170: correctly said that (inuktitut, 2, a2pfktghg1ofp) and (inuktitut, 2, avlbkm6qpb1s) are the same in 1182 ms.
Task 171: incorrectly said that (atemayar, 10, ax5gghhj0o6g) and (sylheti, 28, aljsxt6v0a30) are the same in 3478 ms.
Task 172: correctly said that (atemayar, 10, a23psx2z4f65q) and (inuktitut, 3, a3mv1jtwep7xn) are not the same in 1207 ms.
Task 173: correctly said that (sylheti, 19, aljsxt6v0a30) and (tagalog, 10, avlbkm6qpb1s) are not the same in 1213 ms.
Task 174: correctly said that (myanmar, 25, adc8vbkoocrr) and (inuktitut, 3, a3noo9k3y1yu5) are not the same in 974 ms.
Task 175: incorrectly said that (korean, 29, aw82oruamauz) and (korean, 29, a1oh82lwr5hbg) are not the same in 1119 ms.
Task 176: incorrectly said that (myanmar, 32, a1umhcrj9zfdp) and (sylheti, 17, a1l2fzyn31zvf) are the same in 1584 ms.
Task 177: incorrectly said that (atemayar, 25, a31e6jchki335) and (sylheti, 17, avlbkm6qpb1s) are the same in 1033 ms.
Task 178: incorrectly said that (sylheti, 19, a3m0kt6kjezd1) and (sylheti, 20, a3209gno9duil) are the same in 910 ms.
Task 179: correctly said that (atemayar, 25, ae0ibwt7asth) and (atemayar, 25, a1z59eb5sfxpy) are the same in 1111 ms.
Task 180: correctly said that (inuktitut, 1, a12rfyr8dbn79) and (inuktitut, 1, a2qnqd7m8zfbo) are the same in 2094 ms.
Task 181: correctly said that (tagalog, 6, a3qat9qkqumi2) and (inuktitut, 2, a1j8s7giyto4a) are not the same in 1127 ms.
Task 182: incorrectly said that (korean, 4, aav4pzxdoebd) and (inuktitut, 3, ae0ibwt7asth) are the same in 1867 ms.
Task 183: correctly said that (myanmar, 25, a3qat9qkqumi2) and (inuktitut, 3, a2pgu4pi93b1q) are not the same in 1129 ms.
Task 184: incorrectly said that (tagalog, 2, a3m0kt6kjezd1) and (atemayar, 6, a1z59eb5sfxpy) are the same in 2784 ms.
Task 185: correctly said that (myanmar, 25, avlbkm6qpb1s) and (sylheti, 20, avlbkm6qpb1s) are not the same in 1668 ms.
Task 186: correctly said that (tagalog, 5, a1ehrxjplsq0r) and (tagalog, 5, ajf66jwen7bx) are the same in 987 ms.
Task 187: correctly said that (inuktitut, 3, a12rfyr8dbn79) and (inuktitut, 3, ay0bjcgydhix) are the same in 3272 ms.
Task 188: correctly said that (inuktitut, 1, a3qat9qkqumi2) and (inuktitut, 1, a1manxzopzden) are the same in 1015 ms.
Task 189: incorrectly said that (atemayar, 25, a6fjbvuzrhm6) and (inuktitut, 11, a2qnqd7m8zfbo) are the same in 2701 ms.
Task 190: correctly said that (inuktitut, 3, a1s3njxhehcip) and (inuktitut, 3, a1j8s7giyto4a) are the same in 1525 ms.
Task 191: correctly said that (myanmar, 31, a2pfktghg1ofp) and (korean, 39, a39ivbdm1ekue) are not the same in 1247 ms.
Task 192: correctly said that (myanmar, 1, a1aegunf6e081) and (myanmar, 1, a1j8s7giyto4a) are the same in 875 ms.
Task 193: correctly said that (tagalog, 10, a1a25pyfoa9ge) and (sylheti, 19, amynpl5xzw6q) are not the same in 1325 ms.
Task 194: incorrectly said that (sylheti, 20, a1qku9mccvl35) and (sylheti, 28, a3209gno9duil) are the same in 985 ms.
Task 195: correctly said that (atemayar, 23, a31xqftzcsia2) and (myanmar, 32, a1umhcrj9zfdp) are not the same in 5114 ms.
Task 196: correctly said that (myanmar, 31, a1s3njxhehcip) and (sylheti, 28, a1a25pyfoa9ge) are not the same in 1388 ms.
Task 197: correctly said that (myanmar, 25, ajf66jwen7bx) and (myanmar, 25, a1clejhbx9yyn) are the same in 992 ms.
Task 198: incorrectly said that (inuktitut, 1, a1s3njxhehcip) and (sylheti, 19, ay0bjcgydhix) are the same in 1219 ms.
Task 199: correctly said that (inuktitut, 11, a1z59eb5sfxpy) and (inuktitut, 11, a3mv1jtwep7xn) are the same in 1237 ms.

Duration: 20m 3s 552ms
Comments: It was a nice experience to complete this task and it was interesting too.