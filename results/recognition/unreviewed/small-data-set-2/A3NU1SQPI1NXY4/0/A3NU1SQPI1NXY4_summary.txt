

Summary:Short Summary:
Correct: 153
Incorrect: 47
Percent Correct: 76

Same Correct: 40
Same Incorrect: 21
Percent Same Correct: 65

Different Correct: 113
Different Incorrect: 26
Percent Different Correct: 81

Long Summary:
Task 0: incorrectly said that (sylheti, 19, a1a25pyfoa9ge) and (sylheti, 19, a2du3880jbrov) are not the same in 2044 ms.
Task 1: correctly said that (sylheti, 17, ay0bjcgydhix) and (inuktitut, 2, avlbkm6qpb1s) are not the same in 1449 ms.
Task 2: correctly said that (korean, 29, aw82oruamauz) and (inuktitut, 2, ay0bjcgydhix) are not the same in 1403 ms.
Task 3: incorrectly said that (inuktitut, 1, a1z59eb5sfxpy) and (inuktitut, 1, a12rfyr8dbn79) are not the same in 1101 ms.
Task 4: correctly said that (tagalog, 2, ajf66jwen7bx) and (myanmar, 32, a1aegunf6e081) are not the same in 918 ms.
Task 5: correctly said that (sylheti, 20, a1qku9mccvl35) and (inuktitut, 11, a2qnqd7m8zfbo) are not the same in 960 ms.
Task 6: correctly said that (korean, 4, a3d7xwp0gand5) and (korean, 39, a2nm1qzuh14ls) are not the same in 1123 ms.
Task 7: incorrectly said that (myanmar, 32, a1ehrxjplsq0r) and (myanmar, 32, a1clejhbx9yyn) are not the same in 1129 ms.
Task 8: correctly said that (atemayar, 10, avlbkm6qpb1s) and (inuktitut, 2, aljsxt6v0a30) are not the same in 1340 ms.
Task 9: incorrectly said that (atemayar, 25, awl8tng8e81d) and (atemayar, 25, avlbkm6qpb1s) are not the same in 1334 ms.
Task 10: correctly said that (sylheti, 28, a1foggtlgo7n4) and (korean, 4, a39ivbdm1ekue) are not the same in 1643 ms.
Task 11: correctly said that (inuktitut, 11, a1j8s7giyto4a) and (sylheti, 28, amynpl5xzw6q) are not the same in 1532 ms.
Task 12: incorrectly said that (korean, 29, ae0ibwt7asth) and (myanmar, 25, ajf66jwen7bx) are the same in 1307 ms.
Task 13: correctly said that (inuktitut, 1, a1w3fo0d7cf5t) and (inuktitut, 1, a2pgu4pi93b1q) are the same in 1417 ms.
Task 14: incorrectly said that (sylheti, 19, a4858n7ggst4) and (myanmar, 32, adc8vbkoocrr) are the same in 1276 ms.
Task 15: correctly said that (korean, 4, a1manxzopzden) and (myanmar, 32, a1foggtlgo7n4) are not the same in 1415 ms.
Task 16: incorrectly said that (atemayar, 23, a6fjbvuzrhm6) and (sylheti, 28, amynpl5xzw6q) are the same in 1447 ms.
Task 17: incorrectly said that (sylheti, 19, a1j8s7giyto4a) and (sylheti, 19, a3m0kt6kjezd1) are not the same in 1500 ms.
Task 18: incorrectly said that (sylheti, 17, a1j8s7giyto4a) and (inuktitut, 11, avlbkm6qpb1s) are the same in 1820 ms.
Task 19: correctly said that (inuktitut, 3, a3qat9qkqumi2) and (sylheti, 28, atvyxlwe5n03) are not the same in 1273 ms.
Task 20: correctly said that (korean, 28, aj6upe9mlarg) and (korean, 28, a3bb99mn3zy37) are the same in 1285 ms.
Task 21: correctly said that (myanmar, 25, a1clejhbx9yyn) and (myanmar, 25, a1umhcrj9zfdp) are the same in 1248 ms.
Task 22: correctly said that (tagalog, 5, a1l2fzyn31zvf) and (myanmar, 25, a1foggtlgo7n4) are not the same in 1316 ms.
Task 23: correctly said that (sylheti, 17, a3np21apfa18b) and (sylheti, 17, a1l2fzyn31zvf) are the same in 1289 ms.
Task 24: incorrectly said that (atemayar, 25, avlbkm6qpb1s) and (myanmar, 25, ay0bjcgydhix) are the same in 1257 ms.
Task 25: incorrectly said that (inuktitut, 2, a1manxzopzden) and (korean, 29, a2px0aywxngce) are the same in 1554 ms.
Task 26: correctly said that (korean, 28, a1v0p79fynz9j) and (korean, 28, a3d7xwp0gand5) are the same in 1125 ms.
Task 27: correctly said that (myanmar, 31, adc8vbkoocrr) and (myanmar, 31, a1aegunf6e081) are the same in 1057 ms.
Task 28: correctly said that (myanmar, 31, a1foggtlgo7n4) and (inuktitut, 3, a2ernxe6jlm4x) are not the same in 1238 ms.
Task 29: correctly said that (atemayar, 6, a1manxzopzden) and (atemayar, 6, azdw6062ia38) are the same in 1234 ms.
Task 30: correctly said that (tagalog, 2, a3mv1jtwep7xn) and (atemayar, 25, avlbkm6qpb1s) are not the same in 1579 ms.
Task 31: correctly said that (myanmar, 32, a3qat9qkqumi2) and (tagalog, 6, avlbkm6qpb1s) are not the same in 1321 ms.
Task 32: correctly said that (sylheti, 28, ay0bjcgydhix) and (inuktitut, 11, a315xddd8j4x8) are not the same in 923 ms.
Task 33: correctly said that (atemayar, 10, a3m0kt6kjezd1) and (myanmar, 1, a3j1pp96x14u0) are not the same in 1214 ms.
Task 34: incorrectly said that (atemayar, 25, a31e6jchki335) and (tagalog, 10, ajf66jwen7bx) are the same in 1597 ms.
Task 35: incorrectly said that (tagalog, 2, ay0bjcgydhix) and (tagalog, 6, aljsxt6v0a30) are the same in 1214 ms.
Task 36: correctly said that (atemayar, 23, a31xqftzcsia2) and (atemayar, 23, a23psx2z4f65q) are the same in 967 ms.
Task 37: correctly said that (tagalog, 6, a12rfyr8dbn79) and (tagalog, 6, a1j8s7giyto4a) are the same in 853 ms.
Task 38: correctly said that (korean, 28, a39ivbdm1ekue) and (atemayar, 23, agst3co6usf8) are not the same in 1594 ms.
Task 39: correctly said that (korean, 29, a3d7xwp0gand5) and (sylheti, 19, aljsxt6v0a30) are not the same in 1170 ms.
Task 40: correctly said that (sylheti, 28, aljsxt6v0a30) and (atemayar, 23, a23psx2z4f65q) are not the same in 1314 ms.
Task 41: correctly said that (korean, 4, a1umhcrj9zfdp) and (inuktitut, 3, a2pfktghg1ofp) are not the same in 1159 ms.
Task 42: incorrectly said that (atemayar, 25, agst3co6usf8) and (myanmar, 1, ajf66jwen7bx) are the same in 1383 ms.
Task 43: correctly said that (inuktitut, 2, a1s3njxhehcip) and (inuktitut, 2, a12rfyr8dbn79) are the same in 1079 ms.
Task 44: correctly said that (sylheti, 20, a1a25pyfoa9ge) and (korean, 39, a3d7xwp0gand5) are not the same in 951 ms.
Task 45: correctly said that (sylheti, 17, amynpl5xzw6q) and (tagalog, 5, a2du3880jbrov) are not the same in 962 ms.
Task 46: incorrectly said that (sylheti, 20, a1foggtlgo7n4) and (korean, 39, a1v0p79fynz9j) are the same in 1294 ms.
Task 47: incorrectly said that (korean, 4, a1s3njxhehcip) and (tagalog, 5, a1i92aacrkacw) are the same in 1546 ms.
Task 48: correctly said that (atemayar, 23, ax5gghhj0o6g) and (atemayar, 10, a1manxzopzden) are not the same in 1262 ms.
Task 49: correctly said that (tagalog, 10, a1ehrxjplsq0r) and (atemayar, 10, a3m0kt6kjezd1) are not the same in 959 ms.
Task 50: correctly said that (inuktitut, 11, a1s3njxhehcip) and (sylheti, 20, atvyxlwe5n03) are not the same in 1185 ms.
Task 51: incorrectly said that (atemayar, 25, agst3co6usf8) and (atemayar, 25, a23psx2z4f65q) are not the same in 946 ms.
Task 52: correctly said that (inuktitut, 3, a12xm86d2nbih) and (tagalog, 10, ay0bjcgydhix) are not the same in 867 ms.
Task 53: incorrectly said that (atemayar, 10, avlbkm6qpb1s) and (atemayar, 25, a1poymbentexx) are the same in 1227 ms.
Task 54: correctly said that (atemayar, 6, a1z59eb5sfxpy) and (atemayar, 6, a1poymbentexx) are the same in 1436 ms.
Task 55: correctly said that (sylheti, 20, a3r9q9wqm1nyz) and (tagalog, 2, ay0bjcgydhix) are not the same in 1283 ms.
Task 56: correctly said that (tagalog, 5, a3m0kt6kjezd1) and (tagalog, 10, ajf66jwen7bx) are not the same in 1275 ms.
Task 57: incorrectly said that (sylheti, 28, a1a25pyfoa9ge) and (sylheti, 19, ay0bjcgydhix) are the same in 1173 ms.
Task 58: correctly said that (sylheti, 20, a30xq7555spo8) and (korean, 4, a1oh82lwr5hbg) are not the same in 853 ms.
Task 59: correctly said that (tagalog, 10, ae0ibwt7asth) and (atemayar, 25, agst3co6usf8) are not the same in 754 ms.
Task 60: correctly said that (korean, 29, a3bb99mn3zy37) and (korean, 29, a2px0aywxngce) are the same in 1705 ms.
Task 61: correctly said that (myanmar, 1, a6fjbvuzrhm6) and (myanmar, 1, a3j1pp96x14u0) are the same in 1183 ms.
Task 62: correctly said that (inuktitut, 11, a1j8s7giyto4a) and (tagalog, 2, asu813kb7bv1) are not the same in 781 ms.
Task 63: correctly said that (inuktitut, 3, a2qnqd7m8zfbo) and (inuktitut, 1, avlbkm6qpb1s) are not the same in 773 ms.
Task 64: correctly said that (inuktitut, 3, a12rfyr8dbn79) and (inuktitut, 11, a1j8s7giyto4a) are not the same in 763 ms.
Task 65: correctly said that (atemayar, 23, a3m0kt6kjezd1) and (tagalog, 5, a1i92aacrkacw) are not the same in 766 ms.
Task 66: correctly said that (myanmar, 1, a6fjbvuzrhm6) and (inuktitut, 1, ay0bjcgydhix) are not the same in 651 ms.
Task 67: correctly said that (sylheti, 28, a3pkun3onkucn) and (myanmar, 25, a6fjbvuzrhm6) are not the same in 743 ms.
Task 68: correctly said that (korean, 29, a1s3njxhehcip) and (korean, 29, aav4pzxdoebd) are the same in 1448 ms.
Task 69: correctly said that (inuktitut, 2, aljsxt6v0a30) and (tagalog, 5, a1s3njxhehcip) are not the same in 1430 ms.
Task 70: correctly said that (tagalog, 10, ajf66jwen7bx) and (tagalog, 10, a3qat9qkqumi2) are the same in 1397 ms.
Task 71: correctly said that (inuktitut, 11, ay0bjcgydhix) and (tagalog, 5, a12rfyr8dbn79) are not the same in 822 ms.
Task 72: correctly said that (tagalog, 10, a1a25pyfoa9ge) and (atemayar, 6, ae0ibwt7asth) are not the same in 773 ms.
Task 73: correctly said that (korean, 4, a1oh82lwr5hbg) and (inuktitut, 11, a1z59eb5sfxpy) are not the same in 705 ms.
Task 74: correctly said that (tagalog, 10, a12rfyr8dbn79) and (korean, 39, a1udqv4pfypkn) are not the same in 739 ms.
Task 75: correctly said that (korean, 4, a39ivbdm1ekue) and (korean, 4, a2nm1qzuh14ls) are the same in 1195 ms.
Task 76: correctly said that (sylheti, 19, a1j8s7giyto4a) and (myanmar, 1, a1aegunf6e081) are not the same in 1167 ms.
Task 77: correctly said that (sylheti, 17, a2du3880jbrov) and (myanmar, 25, a4858n7ggst4) are not the same in 1534 ms.
Task 78: correctly said that (myanmar, 25, a1ehrxjplsq0r) and (myanmar, 32, a3pkun3onkucn) are not the same in 801 ms.
Task 79: incorrectly said that (sylheti, 19, a3209gno9duil) and (sylheti, 19, a1l2fzyn31zvf) are not the same in 760 ms.
Task 80: correctly said that (inuktitut, 3, a1w3fo0d7cf5t) and (atemayar, 6, aljsxt6v0a30) are not the same in 1245 ms.
Task 81: correctly said that (sylheti, 20, a1s3njxhehcip) and (atemayar, 6, ax5gghhj0o6g) are not the same in 1104 ms.
Task 82: incorrectly said that (myanmar, 1, a1aegunf6e081) and (myanmar, 32, adc8vbkoocrr) are the same in 1385 ms.
Task 83: incorrectly said that (atemayar, 6, agst3co6usf8) and (atemayar, 25, a1j8s7giyto4a) are the same in 1237 ms.
Task 84: correctly said that (korean, 29, a3bb99mn3zy37) and (korean, 29, a39ivbdm1ekue) are the same in 1166 ms.
Task 85: incorrectly said that (myanmar, 31, a3qat9qkqumi2) and (myanmar, 1, ajf66jwen7bx) are the same in 1039 ms.
Task 86: correctly said that (atemayar, 25, ae0ibwt7asth) and (atemayar, 25, aljsxt6v0a30) are the same in 1383 ms.
Task 87: incorrectly said that (tagalog, 5, a3mv1jtwep7xn) and (tagalog, 10, a1yovlluv1c9h) are the same in 1044 ms.
Task 88: correctly said that (atemayar, 23, a3m0kt6kjezd1) and (atemayar, 23, agst3co6usf8) are the same in 760 ms.
Task 89: incorrectly said that (korean, 4, a1v0p79fynz9j) and (tagalog, 5, ay0bjcgydhix) are the same in 879 ms.
Task 90: incorrectly said that (atemayar, 10, azdw6062ia38) and (atemayar, 10, a31e6jchki335) are not the same in 1392 ms.
Task 91: incorrectly said that (myanmar, 25, a1j8s7giyto4a) and (myanmar, 25, a1ehrxjplsq0r) are not the same in 1204 ms.
Task 92: correctly said that (korean, 4, a3dfh6dpi11ip) and (korean, 4, a2akud1gdstsd) are the same in 1524 ms.
Task 93: correctly said that (myanmar, 1, ay0bjcgydhix) and (atemayar, 25, awl8tng8e81d) are not the same in 1117 ms.
Task 94: correctly said that (korean, 39, a3bb99mn3zy37) and (tagalog, 10, asu813kb7bv1) are not the same in 1270 ms.
Task 95: correctly said that (tagalog, 2, a1l2fzyn31zvf) and (korean, 28, aav4pzxdoebd) are not the same in 944 ms.
Task 96: correctly said that (korean, 4, a3mv1jtwep7xn) and (korean, 4, a3dfh6dpi11ip) are the same in 1443 ms.
Task 97: correctly said that (inuktitut, 1, a2qnqd7m8zfbo) and (inuktitut, 11, a2pfktghg1ofp) are not the same in 1397 ms.
Task 98: correctly said that (myanmar, 32, a3j1pp96x14u0) and (atemayar, 23, a2pfktghg1ofp) are not the same in 926 ms.
Task 99: incorrectly said that (atemayar, 25, agst3co6usf8) and (atemayar, 25, ax5gghhj0o6g) are not the same in 825 ms.
Task 100: incorrectly said that (inuktitut, 1, a3mv1jtwep7xn) and (korean, 29, aav4pzxdoebd) are the same in 1329 ms.
Task 101: incorrectly said that (sylheti, 28, a3np21apfa18b) and (sylheti, 28, a1l2fzyn31zvf) are not the same in 1133 ms.
Task 102: correctly said that (tagalog, 5, a3qat9qkqumi2) and (myanmar, 32, a2pfktghg1ofp) are not the same in 779 ms.
Task 103: correctly said that (tagalog, 2, a12rfyr8dbn79) and (sylheti, 19, atvyxlwe5n03) are not the same in 815 ms.
Task 104: correctly said that (korean, 28, aav4pzxdoebd) and (korean, 28, aw82oruamauz) are the same in 1436 ms.
Task 105: correctly said that (myanmar, 32, a2pfktghg1ofp) and (atemayar, 23, a1vzhyp8yvxkz) are not the same in 758 ms.
Task 106: correctly said that (tagalog, 5, a1i92aacrkacw) and (atemayar, 10, a31xqftzcsia2) are not the same in 750 ms.
Task 107: correctly said that (sylheti, 19, amynpl5xzw6q) and (tagalog, 2, a3qat9qkqumi2) are not the same in 804 ms.
Task 108: correctly said that (atemayar, 23, agst3co6usf8) and (inuktitut, 2, a1s3njxhehcip) are not the same in 674 ms.
Task 109: correctly said that (tagalog, 6, aghrkle9vf6z) and (atemayar, 23, a1z59eb5sfxpy) are not the same in 671 ms.
Task 110: correctly said that (sylheti, 20, a1s3njxhehcip) and (atemayar, 23, aljsxt6v0a30) are not the same in 2004 ms.
Task 111: correctly said that (korean, 29, a3d7xwp0gand5) and (myanmar, 1, a1j8s7giyto4a) are not the same in 907 ms.
Task 112: correctly said that (myanmar, 1, ay0bjcgydhix) and (myanmar, 1, a1s3njxhehcip) are the same in 1490 ms.
Task 113: incorrectly said that (tagalog, 5, a1j8s7giyto4a) and (myanmar, 31, a4858n7ggst4) are the same in 1302 ms.
Task 114: correctly said that (myanmar, 32, a1umhcrj9zfdp) and (korean, 39, a1manxzopzden) are not the same in 1220 ms.
Task 115: correctly said that (sylheti, 17, a3r9q9wqm1nyz) and (sylheti, 19, ay0bjcgydhix) are not the same in 831 ms.
Task 116: correctly said that (myanmar, 31, a3pkun3onkucn) and (tagalog, 6, a1i92aacrkacw) are not the same in 761 ms.
Task 117: correctly said that (tagalog, 6, a1i92aacrkacw) and (inuktitut, 11, avlbkm6qpb1s) are not the same in 639 ms.
Task 118: incorrectly said that (myanmar, 31, adc8vbkoocrr) and (tagalog, 6, a2du3880jbrov) are the same in 1435 ms.
Task 119: correctly said that (inuktitut, 2, avlbkm6qpb1s) and (myanmar, 32, aljsxt6v0a30) are not the same in 771 ms.
Task 120: correctly said that (myanmar, 1, ay0bjcgydhix) and (myanmar, 31, a1umhcrj9zfdp) are not the same in 1462 ms.
Task 121: correctly said that (korean, 29, a3bb99mn3zy37) and (myanmar, 31, a1s3njxhehcip) are not the same in 733 ms.
Task 122: correctly said that (tagalog, 5, amynpl5xzw6q) and (tagalog, 5, a1i92aacrkacw) are the same in 1515 ms.
Task 123: incorrectly said that (sylheti, 19, avlbkm6qpb1s) and (sylheti, 28, atvyxlwe5n03) are the same in 1298 ms.
Task 124: correctly said that (sylheti, 20, a3r9q9wqm1nyz) and (atemayar, 6, a6fjbvuzrhm6) are not the same in 1878 ms.
Task 125: incorrectly said that (myanmar, 32, avlbkm6qpb1s) and (myanmar, 32, a1umhcrj9zfdp) are not the same in 2205 ms.
Task 126: correctly said that (korean, 29, a5379luit3pc) and (inuktitut, 1, a2ernxe6jlm4x) are not the same in 1204 ms.
Task 127: correctly said that (inuktitut, 1, a12rfyr8dbn79) and (inuktitut, 1, a2ernxe6jlm4x) are the same in 1190 ms.
Task 128: correctly said that (myanmar, 1, a2pfktghg1ofp) and (myanmar, 1, amynpl5xzw6q) are the same in 980 ms.
Task 129: correctly said that (korean, 39, ae0ibwt7asth) and (korean, 29, a1oh82lwr5hbg) are not the same in 1828 ms.
Task 130: correctly said that (atemayar, 25, avlbkm6qpb1s) and (tagalog, 10, avlbkm6qpb1s) are not the same in 1233 ms.
Task 131: correctly said that (korean, 28, a2nm1qzuh14ls) and (inuktitut, 11, a1qku9mccvl35) are not the same in 755 ms.
Task 132: correctly said that (tagalog, 2, aljsxt6v0a30) and (tagalog, 10, a1l2fzyn31zvf) are not the same in 1225 ms.
Task 133: incorrectly said that (korean, 39, a2nm1qzuh14ls) and (sylheti, 19, a1qku9mccvl35) are the same in 1989 ms.
Task 134: correctly said that (atemayar, 25, ax5gghhj0o6g) and (atemayar, 25, a1poymbentexx) are the same in 1671 ms.
Task 135: correctly said that (tagalog, 5, a1ehrxjplsq0r) and (myanmar, 1, ay0bjcgydhix) are not the same in 1136 ms.
Task 136: correctly said that (inuktitut, 2, a12rfyr8dbn79) and (atemayar, 23, a6fjbvuzrhm6) are not the same in 1259 ms.
Task 137: correctly said that (myanmar, 31, a1aegunf6e081) and (inuktitut, 3, ay0bjcgydhix) are not the same in 806 ms.
Task 138: correctly said that (sylheti, 28, a1j8s7giyto4a) and (tagalog, 5, a12rfyr8dbn79) are not the same in 950 ms.
Task 139: correctly said that (atemayar, 6, a6fjbvuzrhm6) and (inuktitut, 2, avlbkm6qpb1s) are not the same in 783 ms.
Task 140: correctly said that (tagalog, 10, aljsxt6v0a30) and (inuktitut, 2, ay0bjcgydhix) are not the same in 1754 ms.
Task 141: correctly said that (inuktitut, 11, a3mv1jtwep7xn) and (korean, 39, a1umhcrj9zfdp) are not the same in 783 ms.
Task 142: correctly said that (atemayar, 23, a1manxzopzden) and (atemayar, 25, a1j8s7giyto4a) are not the same in 1320 ms.
Task 143: correctly said that (sylheti, 28, avlbkm6qpb1s) and (korean, 39, a1manxzopzden) are not the same in 1410 ms.
Task 144: incorrectly said that (korean, 4, a3d7xwp0gand5) and (korean, 4, ae0ibwt7asth) are not the same in 1237 ms.
Task 145: incorrectly said that (sylheti, 19, amynpl5xzw6q) and (sylheti, 19, a6fjbvuzrhm6) are not the same in 1347 ms.
Task 146: correctly said that (myanmar, 1, avlbkm6qpb1s) and (myanmar, 1, a1clejhbx9yyn) are the same in 1278 ms.
Task 147: correctly said that (atemayar, 10, a1manxzopzden) and (sylheti, 17, a1a25pyfoa9ge) are not the same in 1305 ms.
Task 148: correctly said that (atemayar, 25, agst3co6usf8) and (inuktitut, 3, a2pgu4pi93b1q) are not the same in 900 ms.
Task 149: correctly said that (tagalog, 10, a12rfyr8dbn79) and (inuktitut, 11, a1j8s7giyto4a) are not the same in 914 ms.
Task 150: correctly said that (tagalog, 2, a2du3880jbrov) and (tagalog, 6, amynpl5xzw6q) are not the same in 1615 ms.
Task 151: incorrectly said that (sylheti, 17, a2du3880jbrov) and (sylheti, 17, a3m0kt6kjezd1) are not the same in 979 ms.
Task 152: correctly said that (sylheti, 17, a3m0kt6kjezd1) and (myanmar, 31, a6fjbvuzrhm6) are not the same in 733 ms.
Task 153: correctly said that (inuktitut, 3, a315xddd8j4x8) and (korean, 4, a1oh82lwr5hbg) are not the same in 715 ms.
Task 154: correctly said that (tagalog, 6, ajf66jwen7bx) and (myanmar, 25, a1s3njxhehcip) are not the same in 808 ms.
Task 155: correctly said that (sylheti, 28, aljsxt6v0a30) and (inuktitut, 1, a2qnqd7m8zfbo) are not the same in 627 ms.
Task 156: correctly said that (sylheti, 17, a3209gno9duil) and (myanmar, 25, aljsxt6v0a30) are not the same in 900 ms.
Task 157: correctly said that (tagalog, 6, a2du3880jbrov) and (tagalog, 6, ajf66jwen7bx) are the same in 1901 ms.
Task 158: correctly said that (atemayar, 6, avlbkm6qpb1s) and (tagalog, 5, a1j8s7giyto4a) are not the same in 1072 ms.
Task 159: correctly said that (tagalog, 5, a1j8s7giyto4a) and (myanmar, 32, a3j1pp96x14u0) are not the same in 894 ms.
Task 160: incorrectly said that (atemayar, 6, ae0ibwt7asth) and (atemayar, 6, a1z59eb5sfxpy) are not the same in 1682 ms.
Task 161: correctly said that (inuktitut, 3, a12rfyr8dbn79) and (sylheti, 17, a1a25pyfoa9ge) are not the same in 840 ms.
Task 162: incorrectly said that (tagalog, 10, ajf66jwen7bx) and (myanmar, 32, ay0bjcgydhix) are the same in 1317 ms.
Task 163: incorrectly said that (inuktitut, 2, a2ernxe6jlm4x) and (inuktitut, 2, a2pfktghg1ofp) are not the same in 1275 ms.
Task 164: correctly said that (sylheti, 19, a6fjbvuzrhm6) and (atemayar, 10, aljsxt6v0a30) are not the same in 852 ms.
Task 165: incorrectly said that (tagalog, 5, ay0bjcgydhix) and (tagalog, 5, asu813kb7bv1) are not the same in 2026 ms.
Task 166: incorrectly said that (korean, 39, a3d7xwp0gand5) and (korean, 39, a5379luit3pc) are not the same in 927 ms.
Task 167: correctly said that (myanmar, 32, a1j8s7giyto4a) and (inuktitut, 2, a3qat9qkqumi2) are not the same in 687 ms.
Task 168: correctly said that (myanmar, 1, a1foggtlgo7n4) and (myanmar, 1, avlbkm6qpb1s) are the same in 1165 ms.
Task 169: correctly said that (korean, 4, a3d7xwp0gand5) and (inuktitut, 3, a1manxzopzden) are not the same in 1253 ms.
Task 170: correctly said that (myanmar, 1, a2pfktghg1ofp) and (myanmar, 1, a1foggtlgo7n4) are the same in 1354 ms.
Task 171: correctly said that (korean, 28, ae0ibwt7asth) and (korean, 28, a2akud1gdstsd) are the same in 1549 ms.
Task 172: correctly said that (korean, 28, a39ivbdm1ekue) and (korean, 28, a5379luit3pc) are the same in 753 ms.
Task 173: correctly said that (myanmar, 25, ay0bjcgydhix) and (korean, 4, a2nm1qzuh14ls) are not the same in 1420 ms.
Task 174: correctly said that (inuktitut, 3, a315xddd8j4x8) and (inuktitut, 11, a3mv1jtwep7xn) are not the same in 767 ms.
Task 175: correctly said that (sylheti, 19, a1a25pyfoa9ge) and (korean, 29, a1umhcrj9zfdp) are not the same in 767 ms.
Task 176: incorrectly said that (sylheti, 20, avlbkm6qpb1s) and (sylheti, 20, a3209gno9duil) are not the same in 940 ms.
Task 177: correctly said that (myanmar, 1, a1clejhbx9yyn) and (sylheti, 28, a1l2fzyn31zvf) are not the same in 722 ms.
Task 178: incorrectly said that (atemayar, 10, avlbkm6qpb1s) and (atemayar, 6, a1yovlluv1c9h) are the same in 1634 ms.
Task 179: incorrectly said that (sylheti, 20, aljsxt6v0a30) and (sylheti, 19, atvyxlwe5n03) are the same in 1448 ms.
Task 180: correctly said that (korean, 29, aav4pzxdoebd) and (sylheti, 19, avlbkm6qpb1s) are not the same in 1344 ms.
Task 181: correctly said that (myanmar, 32, a3pkun3onkucn) and (sylheti, 17, a30xq7555spo8) are not the same in 968 ms.
Task 182: correctly said that (inuktitut, 11, a1j8s7giyto4a) and (inuktitut, 11, a1w3fo0d7cf5t) are the same in 1585 ms.
Task 183: correctly said that (myanmar, 25, amynpl5xzw6q) and (myanmar, 25, a3qat9qkqumi2) are the same in 1283 ms.
Task 184: correctly said that (atemayar, 10, a23psx2z4f65q) and (tagalog, 2, a3m0kt6kjezd1) are not the same in 1350 ms.
Task 185: correctly said that (tagalog, 5, ae0ibwt7asth) and (tagalog, 6, a1i92aacrkacw) are not the same in 835 ms.
Task 186: correctly said that (korean, 39, a3mv1jtwep7xn) and (myanmar, 31, a1s3njxhehcip) are not the same in 739 ms.
Task 187: incorrectly said that (atemayar, 23, avlbkm6qpb1s) and (atemayar, 23, a3m0kt6kjezd1) are not the same in 812 ms.
Task 188: correctly said that (inuktitut, 2, a2pfktghg1ofp) and (inuktitut, 3, aljsxt6v0a30) are not the same in 756 ms.
Task 189: correctly said that (korean, 39, a39ivbdm1ekue) and (korean, 39, aj6upe9mlarg) are the same in 1691 ms.
Task 190: correctly said that (sylheti, 19, a1j8s7giyto4a) and (atemayar, 10, agst3co6usf8) are not the same in 1189 ms.
Task 191: correctly said that (myanmar, 25, a1s3njxhehcip) and (tagalog, 6, a1ehrxjplsq0r) are not the same in 786 ms.
Task 192: correctly said that (myanmar, 1, a1stvgjqfl5kk) and (myanmar, 1, ay0bjcgydhix) are the same in 1158 ms.
Task 193: correctly said that (inuktitut, 1, a1s3njxhehcip) and (inuktitut, 1, a1manxzopzden) are the same in 976 ms.
Task 194: correctly said that (myanmar, 31, a6fjbvuzrhm6) and (myanmar, 31, ay0bjcgydhix) are the same in 865 ms.
Task 195: correctly said that (sylheti, 28, a3r9q9wqm1nyz) and (tagalog, 6, amynpl5xzw6q) are not the same in 1127 ms.
Task 196: correctly said that (inuktitut, 1, ae0ibwt7asth) and (inuktitut, 1, avlbkm6qpb1s) are the same in 932 ms.
Task 197: correctly said that (korean, 28, a2akud1gdstsd) and (myanmar, 1, a1aegunf6e081) are not the same in 1144 ms.
Task 198: correctly said that (tagalog, 2, a1yovlluv1c9h) and (sylheti, 20, a6fjbvuzrhm6) are not the same in 740 ms.
Task 199: correctly said that (myanmar, 32, amynpl5xzw6q) and (myanmar, 1, a4858n7ggst4) are not the same in 1174 ms.

Duration: 17m 3s 696ms
Comments: 