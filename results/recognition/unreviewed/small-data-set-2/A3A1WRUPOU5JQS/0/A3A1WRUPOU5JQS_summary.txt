

Summary:Short Summary:
Correct: 181
Incorrect: 19
Percent Correct: 90

Same Correct: 50
Same Incorrect: 5
Percent Same Correct: 90

Different Correct: 131
Different Incorrect: 14
Percent Different Correct: 90

Long Summary:
Task 0: incorrectly said that (tagalog, 10, asu813kb7bv1) and (korean, 28, a1s3njxhehcip) are the same in 3590 ms.
Task 1: correctly said that (inuktitut, 1, a1s3njxhehcip) and (inuktitut, 1, avlbkm6qpb1s) are the same in 1967 ms.
Task 2: correctly said that (inuktitut, 3, a1manxzopzden) and (korean, 28, a39ivbdm1ekue) are not the same in 2064 ms.
Task 3: incorrectly said that (sylheti, 28, a4858n7ggst4) and (korean, 29, a5379luit3pc) are the same in 3016 ms.
Task 4: correctly said that (tagalog, 6, a1j8s7giyto4a) and (tagalog, 5, a3qat9qkqumi2) are not the same in 5199 ms.
Task 5: correctly said that (myanmar, 31, a1stvgjqfl5kk) and (korean, 4, a2nm1qzuh14ls) are not the same in 1887 ms.
Task 6: incorrectly said that (tagalog, 10, a1s3njxhehcip) and (inuktitut, 11, a1manxzopzden) are the same in 2062 ms.
Task 7: correctly said that (tagalog, 10, a1yovlluv1c9h) and (atemayar, 10, agst3co6usf8) are not the same in 2257 ms.
Task 8: correctly said that (tagalog, 6, a3m0kt6kjezd1) and (tagalog, 6, a3mv1jtwep7xn) are the same in 1752 ms.
Task 9: incorrectly said that (tagalog, 2, a1i92aacrkacw) and (tagalog, 10, ajf66jwen7bx) are the same in 2026 ms.
Task 10: correctly said that (inuktitut, 3, a12rfyr8dbn79) and (korean, 39, a2ernxe6jlm4x) are not the same in 1146 ms.
Task 11: correctly said that (myanmar, 31, a1stvgjqfl5kk) and (tagalog, 10, aljsxt6v0a30) are not the same in 2591 ms.
Task 12: correctly said that (tagalog, 6, aljsxt6v0a30) and (korean, 28, a3bb99mn3zy37) are not the same in 1633 ms.
Task 13: incorrectly said that (myanmar, 32, a2pfktghg1ofp) and (tagalog, 10, asu813kb7bv1) are the same in 1033 ms.
Task 14: correctly said that (inuktitut, 3, ay0bjcgydhix) and (inuktitut, 3, aljsxt6v0a30) are the same in 1025 ms.
Task 15: correctly said that (sylheti, 19, a30xq7555spo8) and (korean, 4, a3dfh6dpi11ip) are not the same in 1041 ms.
Task 16: correctly said that (tagalog, 10, ae0ibwt7asth) and (korean, 39, aj6upe9mlarg) are not the same in 1147 ms.
Task 17: correctly said that (tagalog, 6, a3mv1jtwep7xn) and (tagalog, 6, a1l2fzyn31zvf) are the same in 1092 ms.
Task 18: correctly said that (atemayar, 23, a1vzhyp8yvxkz) and (tagalog, 10, avlbkm6qpb1s) are not the same in 1281 ms.
Task 19: correctly said that (tagalog, 2, a1s3njxhehcip) and (atemayar, 25, ae0ibwt7asth) are not the same in 1001 ms.
Task 20: incorrectly said that (inuktitut, 1, a3noo9k3y1yu5) and (sylheti, 17, a3209gno9duil) are the same in 1396 ms.
Task 21: correctly said that (myanmar, 31, a1stvgjqfl5kk) and (myanmar, 31, a1s3njxhehcip) are the same in 1406 ms.
Task 22: correctly said that (inuktitut, 3, a1z59eb5sfxpy) and (inuktitut, 3, a1manxzopzden) are the same in 1568 ms.
Task 23: correctly said that (inuktitut, 2, a12rfyr8dbn79) and (inuktitut, 2, a1j8s7giyto4a) are the same in 1216 ms.
Task 24: correctly said that (sylheti, 20, avlbkm6qpb1s) and (inuktitut, 2, ay0bjcgydhix) are not the same in 1114 ms.
Task 25: correctly said that (atemayar, 10, a1qku9mccvl35) and (tagalog, 5, a3qat9qkqumi2) are not the same in 1182 ms.
Task 26: correctly said that (inuktitut, 2, a3noo9k3y1yu5) and (sylheti, 19, aljsxt6v0a30) are not the same in 3337 ms.
Task 27: correctly said that (inuktitut, 11, a1s3njxhehcip) and (myanmar, 31, a3j1pp96x14u0) are not the same in 4481 ms.
Task 28: correctly said that (atemayar, 6, ax5gghhj0o6g) and (korean, 29, a3mv1jtwep7xn) are not the same in 4928 ms.
Task 29: correctly said that (myanmar, 32, a3j1pp96x14u0) and (atemayar, 6, a31e6jchki335) are not the same in 1425 ms.
Task 30: correctly said that (myanmar, 25, a1umhcrj9zfdp) and (inuktitut, 3, a1j8s7giyto4a) are not the same in 1675 ms.
Task 31: incorrectly said that (sylheti, 17, a3m0kt6kjezd1) and (sylheti, 17, aljsxt6v0a30) are not the same in 6660 ms.
Task 32: correctly said that (inuktitut, 1, a1w3fo0d7cf5t) and (inuktitut, 1, a3noo9k3y1yu5) are the same in 1145 ms.
Task 33: correctly said that (inuktitut, 3, ay0bjcgydhix) and (atemayar, 10, a2pfktghg1ofp) are not the same in 1520 ms.
Task 34: correctly said that (atemayar, 10, a23psx2z4f65q) and (atemayar, 10, a31xqftzcsia2) are the same in 1586 ms.
Task 35: incorrectly said that (korean, 4, a3d7xwp0gand5) and (korean, 29, a3d7xwp0gand5) are the same in 1757 ms.
Task 36: correctly said that (myanmar, 32, a1j8s7giyto4a) and (tagalog, 6, a3m0kt6kjezd1) are not the same in 1512 ms.
Task 37: correctly said that (korean, 28, a3d7xwp0gand5) and (korean, 28, aav4pzxdoebd) are the same in 1138 ms.
Task 38: correctly said that (atemayar, 6, a1poymbentexx) and (atemayar, 23, a1manxzopzden) are not the same in 1593 ms.
Task 39: correctly said that (inuktitut, 1, ae0ibwt7asth) and (atemayar, 23, ae0ibwt7asth) are not the same in 1184 ms.
Task 40: correctly said that (sylheti, 20, a6fjbvuzrhm6) and (sylheti, 20, a1a25pyfoa9ge) are the same in 1650 ms.
Task 41: correctly said that (inuktitut, 1, a1s3njxhehcip) and (myanmar, 32, a6fjbvuzrhm6) are not the same in 1097 ms.
Task 42: correctly said that (atemayar, 25, a31e6jchki335) and (myanmar, 1, amynpl5xzw6q) are not the same in 1273 ms.
Task 43: correctly said that (tagalog, 5, avlbkm6qpb1s) and (sylheti, 28, ay0bjcgydhix) are not the same in 1041 ms.
Task 44: correctly said that (inuktitut, 11, a2pgu4pi93b1q) and (sylheti, 17, avlbkm6qpb1s) are not the same in 1251 ms.
Task 45: correctly said that (tagalog, 10, a1i92aacrkacw) and (atemayar, 23, ax5gghhj0o6g) are not the same in 1122 ms.
Task 46: correctly said that (sylheti, 20, avlbkm6qpb1s) and (sylheti, 20, a1a25pyfoa9ge) are the same in 1423 ms.
Task 47: correctly said that (inuktitut, 2, a2pfktghg1ofp) and (inuktitut, 2, a2qnqd7m8zfbo) are the same in 1250 ms.
Task 48: correctly said that (myanmar, 32, a3pkun3onkucn) and (tagalog, 6, a2du3880jbrov) are not the same in 1594 ms.
Task 49: correctly said that (korean, 4, a1udqv4pfypkn) and (myanmar, 32, a1foggtlgo7n4) are not the same in 1010 ms.
Task 50: correctly said that (tagalog, 2, a3mv1jtwep7xn) and (myanmar, 31, a1s3njxhehcip) are not the same in 1930 ms.
Task 51: correctly said that (sylheti, 28, a30xq7555spo8) and (korean, 39, ae0ibwt7asth) are not the same in 1037 ms.
Task 52: correctly said that (inuktitut, 1, a2pgu4pi93b1q) and (korean, 29, a3d7xwp0gand5) are not the same in 1441 ms.
Task 53: correctly said that (tagalog, 2, amynpl5xzw6q) and (tagalog, 2, a3qat9qkqumi2) are the same in 1793 ms.
Task 54: correctly said that (atemayar, 10, a23psx2z4f65q) and (inuktitut, 11, a1z59eb5sfxpy) are not the same in 1266 ms.
Task 55: correctly said that (myanmar, 25, amynpl5xzw6q) and (korean, 28, ae0ibwt7asth) are not the same in 1020 ms.
Task 56: correctly said that (korean, 28, a2nm1qzuh14ls) and (korean, 28, a3mv1jtwep7xn) are the same in 1419 ms.
Task 57: correctly said that (inuktitut, 11, a2qnqd7m8zfbo) and (sylheti, 19, a1qku9mccvl35) are not the same in 1224 ms.
Task 58: correctly said that (sylheti, 17, a3209gno9duil) and (sylheti, 17, avlbkm6qpb1s) are the same in 4449 ms.
Task 59: correctly said that (myanmar, 32, a3pkun3onkucn) and (myanmar, 32, a1ehrxjplsq0r) are the same in 1225 ms.
Task 60: correctly said that (atemayar, 25, agst3co6usf8) and (sylheti, 28, a1l2fzyn31zvf) are not the same in 1211 ms.
Task 61: correctly said that (inuktitut, 2, ae0ibwt7asth) and (inuktitut, 2, a3qat9qkqumi2) are the same in 1564 ms.
Task 62: correctly said that (inuktitut, 11, a1z59eb5sfxpy) and (inuktitut, 11, a1manxzopzden) are the same in 1088 ms.
Task 63: correctly said that (myanmar, 25, a1aegunf6e081) and (korean, 4, aw82oruamauz) are not the same in 1018 ms.
Task 64: correctly said that (tagalog, 6, a3qat9qkqumi2) and (sylheti, 28, atvyxlwe5n03) are not the same in 1019 ms.
Task 65: correctly said that (myanmar, 31, ajf66jwen7bx) and (atemayar, 23, a1poymbentexx) are not the same in 1648 ms.
Task 66: correctly said that (korean, 28, a1v0p79fynz9j) and (tagalog, 2, a1a25pyfoa9ge) are not the same in 873 ms.
Task 67: correctly said that (myanmar, 31, amynpl5xzw6q) and (inuktitut, 3, a1w3fo0d7cf5t) are not the same in 1098 ms.
Task 68: correctly said that (atemayar, 10, a1manxzopzden) and (sylheti, 20, avlbkm6qpb1s) are not the same in 1204 ms.
Task 69: correctly said that (tagalog, 10, a1s3njxhehcip) and (inuktitut, 1, a2pfktghg1ofp) are not the same in 1121 ms.
Task 70: correctly said that (myanmar, 25, ajf66jwen7bx) and (atemayar, 25, awl8tng8e81d) are not the same in 1483 ms.
Task 71: correctly said that (inuktitut, 1, a2ernxe6jlm4x) and (atemayar, 6, a2pfktghg1ofp) are not the same in 1467 ms.
Task 72: correctly said that (myanmar, 1, adc8vbkoocrr) and (inuktitut, 1, a1w3fo0d7cf5t) are not the same in 937 ms.
Task 73: correctly said that (inuktitut, 11, aljsxt6v0a30) and (atemayar, 10, a6fjbvuzrhm6) are not the same in 858 ms.
Task 74: correctly said that (inuktitut, 2, a2ernxe6jlm4x) and (tagalog, 10, a1yovlluv1c9h) are not the same in 1009 ms.
Task 75: correctly said that (tagalog, 10, ajf66jwen7bx) and (myanmar, 1, a4858n7ggst4) are not the same in 921 ms.
Task 76: correctly said that (myanmar, 31, ajf66jwen7bx) and (tagalog, 5, a3mv1jtwep7xn) are not the same in 939 ms.
Task 77: correctly said that (atemayar, 6, agst3co6usf8) and (sylheti, 28, a3r9q9wqm1nyz) are not the same in 1457 ms.
Task 78: correctly said that (myanmar, 25, a1s3njxhehcip) and (korean, 28, a1manxzopzden) are not the same in 817 ms.
Task 79: correctly said that (tagalog, 10, a1yovlluv1c9h) and (myanmar, 1, a4858n7ggst4) are not the same in 1042 ms.
Task 80: correctly said that (sylheti, 28, a3209gno9duil) and (tagalog, 2, a1ehrxjplsq0r) are not the same in 1746 ms.
Task 81: correctly said that (korean, 28, a2px0aywxngce) and (sylheti, 28, a1foggtlgo7n4) are not the same in 1372 ms.
Task 82: correctly said that (tagalog, 6, ajf66jwen7bx) and (korean, 4, a1umhcrj9zfdp) are not the same in 970 ms.
Task 83: correctly said that (inuktitut, 11, a2pfktghg1ofp) and (inuktitut, 11, ay0bjcgydhix) are the same in 1634 ms.
Task 84: correctly said that (korean, 4, aw82oruamauz) and (korean, 4, a3bb99mn3zy37) are the same in 930 ms.
Task 85: correctly said that (tagalog, 10, a1i92aacrkacw) and (atemayar, 25, a6fjbvuzrhm6) are not the same in 1594 ms.
Task 86: correctly said that (myanmar, 31, a1foggtlgo7n4) and (tagalog, 10, amynpl5xzw6q) are not the same in 2936 ms.
Task 87: correctly said that (inuktitut, 2, a2pgu4pi93b1q) and (inuktitut, 2, a1s3njxhehcip) are the same in 1761 ms.
Task 88: correctly said that (tagalog, 6, aljsxt6v0a30) and (inuktitut, 3, a1manxzopzden) are not the same in 1521 ms.
Task 89: correctly said that (myanmar, 32, a1clejhbx9yyn) and (atemayar, 10, awl8tng8e81d) are not the same in 1162 ms.
Task 90: incorrectly said that (sylheti, 20, a4858n7ggst4) and (sylheti, 28, a3pkun3onkucn) are the same in 1740 ms.
Task 91: correctly said that (inuktitut, 3, a1qku9mccvl35) and (myanmar, 32, a6fjbvuzrhm6) are not the same in 1682 ms.
Task 92: correctly said that (inuktitut, 1, a2pgu4pi93b1q) and (korean, 39, aw82oruamauz) are not the same in 1976 ms.
Task 93: correctly said that (atemayar, 25, a1manxzopzden) and (atemayar, 6, a3m0kt6kjezd1) are not the same in 2193 ms.
Task 94: correctly said that (tagalog, 10, a3m0kt6kjezd1) and (korean, 28, a3dfh6dpi11ip) are not the same in 1009 ms.
Task 95: correctly said that (sylheti, 17, ay0bjcgydhix) and (myanmar, 25, a2pfktghg1ofp) are not the same in 938 ms.
Task 96: incorrectly said that (tagalog, 6, aljsxt6v0a30) and (tagalog, 6, a1a25pyfoa9ge) are not the same in 2665 ms.
Task 97: correctly said that (sylheti, 28, a1a25pyfoa9ge) and (sylheti, 19, avlbkm6qpb1s) are not the same in 3480 ms.
Task 98: correctly said that (inuktitut, 1, a2qnqd7m8zfbo) and (atemayar, 25, a2pfktghg1ofp) are not the same in 913 ms.
Task 99: correctly said that (myanmar, 1, adc8vbkoocrr) and (myanmar, 1, a4858n7ggst4) are the same in 1257 ms.
Task 100: correctly said that (korean, 39, a2nm1qzuh14ls) and (korean, 39, aj6upe9mlarg) are the same in 2411 ms.
Task 101: correctly said that (atemayar, 6, a1j8s7giyto4a) and (myanmar, 31, ay0bjcgydhix) are not the same in 1274 ms.
Task 102: correctly said that (tagalog, 10, aghrkle9vf6z) and (sylheti, 17, amynpl5xzw6q) are not the same in 1170 ms.
Task 103: incorrectly said that (sylheti, 20, a3m0kt6kjezd1) and (sylheti, 17, a3r9q9wqm1nyz) are the same in 1809 ms.
Task 104: correctly said that (tagalog, 6, a3mv1jtwep7xn) and (tagalog, 6, a3m0kt6kjezd1) are the same in 1945 ms.
Task 105: correctly said that (sylheti, 17, a1l2fzyn31zvf) and (sylheti, 17, a1a25pyfoa9ge) are the same in 1555 ms.
Task 106: correctly said that (myanmar, 32, a1stvgjqfl5kk) and (sylheti, 20, a3m0kt6kjezd1) are not the same in 2195 ms.
Task 107: correctly said that (tagalog, 10, avlbkm6qpb1s) and (myanmar, 1, ay0bjcgydhix) are not the same in 1305 ms.
Task 108: correctly said that (tagalog, 5, a1ehrxjplsq0r) and (sylheti, 20, ay0bjcgydhix) are not the same in 1026 ms.
Task 109: correctly said that (myanmar, 31, adc8vbkoocrr) and (myanmar, 31, ay0bjcgydhix) are the same in 1258 ms.
Task 110: correctly said that (myanmar, 31, a2pfktghg1ofp) and (myanmar, 31, a1foggtlgo7n4) are the same in 2273 ms.
Task 111: correctly said that (tagalog, 2, a12rfyr8dbn79) and (atemayar, 25, a1j8s7giyto4a) are not the same in 1217 ms.
Task 112: correctly said that (myanmar, 25, amynpl5xzw6q) and (sylheti, 19, amynpl5xzw6q) are not the same in 817 ms.
Task 113: correctly said that (tagalog, 6, a1s3njxhehcip) and (myanmar, 32, adc8vbkoocrr) are not the same in 1132 ms.
Task 114: correctly said that (inuktitut, 2, a2pfktghg1ofp) and (inuktitut, 3, a2ernxe6jlm4x) are not the same in 905 ms.
Task 115: correctly said that (sylheti, 19, a1l2fzyn31zvf) and (inuktitut, 2, a3qat9qkqumi2) are not the same in 881 ms.
Task 116: correctly said that (myanmar, 31, ay0bjcgydhix) and (atemayar, 6, ax5gghhj0o6g) are not the same in 721 ms.
Task 117: correctly said that (sylheti, 17, a1l2fzyn31zvf) and (myanmar, 31, a1ehrxjplsq0r) are not the same in 1273 ms.
Task 118: correctly said that (inuktitut, 2, a2pfktghg1ofp) and (inuktitut, 11, ay0bjcgydhix) are not the same in 1081 ms.
Task 119: correctly said that (sylheti, 19, ay0bjcgydhix) and (sylheti, 19, a3pkun3onkucn) are the same in 1265 ms.
Task 120: correctly said that (atemayar, 6, a1poymbentexx) and (sylheti, 20, aljsxt6v0a30) are not the same in 3685 ms.
Task 121: correctly said that (myanmar, 32, avlbkm6qpb1s) and (atemayar, 6, a1qku9mccvl35) are not the same in 1976 ms.
Task 122: correctly said that (korean, 4, a2akud1gdstsd) and (korean, 4, a39ivbdm1ekue) are the same in 1712 ms.
Task 123: correctly said that (sylheti, 20, a1a25pyfoa9ge) and (atemayar, 6, a2pfktghg1ofp) are not the same in 1328 ms.
Task 124: correctly said that (inuktitut, 1, a12xm86d2nbih) and (korean, 29, a3d7xwp0gand5) are not the same in 1283 ms.
Task 125: correctly said that (korean, 39, a1v0p79fynz9j) and (atemayar, 6, ax5gghhj0o6g) are not the same in 817 ms.
Task 126: correctly said that (inuktitut, 3, a1qku9mccvl35) and (sylheti, 20, a1j8s7giyto4a) are not the same in 1006 ms.
Task 127: correctly said that (atemayar, 25, a31e6jchki335) and (atemayar, 25, agst3co6usf8) are the same in 1441 ms.
Task 128: incorrectly said that (atemayar, 10, ax5gghhj0o6g) and (atemayar, 10, a1yovlluv1c9h) are not the same in 1425 ms.
Task 129: correctly said that (korean, 39, a2akud1gdstsd) and (atemayar, 23, a2pfktghg1ofp) are not the same in 937 ms.
Task 130: correctly said that (inuktitut, 1, a12xm86d2nbih) and (myanmar, 32, a3j1pp96x14u0) are not the same in 1268 ms.
Task 131: correctly said that (tagalog, 2, a2du3880jbrov) and (myanmar, 32, avlbkm6qpb1s) are not the same in 810 ms.
Task 132: correctly said that (atemayar, 10, a1manxzopzden) and (myanmar, 1, a1ehrxjplsq0r) are not the same in 801 ms.
Task 133: correctly said that (inuktitut, 11, ay0bjcgydhix) and (inuktitut, 11, a2qnqd7m8zfbo) are the same in 1625 ms.
Task 134: correctly said that (myanmar, 1, avlbkm6qpb1s) and (inuktitut, 1, a1s3njxhehcip) are not the same in 916 ms.
Task 135: correctly said that (tagalog, 2, asu813kb7bv1) and (sylheti, 19, a4858n7ggst4) are not the same in 1025 ms.
Task 136: correctly said that (myanmar, 31, avlbkm6qpb1s) and (inuktitut, 1, a1s3njxhehcip) are not the same in 825 ms.
Task 137: correctly said that (inuktitut, 2, a12rfyr8dbn79) and (inuktitut, 2, ae0ibwt7asth) are the same in 1498 ms.
Task 138: incorrectly said that (korean, 28, a3bb99mn3zy37) and (korean, 28, a1v0p79fynz9j) are not the same in 1522 ms.
Task 139: correctly said that (sylheti, 20, a3np21apfa18b) and (korean, 4, aav4pzxdoebd) are not the same in 986 ms.
Task 140: correctly said that (inuktitut, 3, a1w3fo0d7cf5t) and (tagalog, 6, a1yovlluv1c9h) are not the same in 3227 ms.
Task 141: correctly said that (atemayar, 6, avlbkm6qpb1s) and (atemayar, 6, ax5gghhj0o6g) are the same in 2019 ms.
Task 142: correctly said that (atemayar, 10, awl8tng8e81d) and (atemayar, 10, aljsxt6v0a30) are the same in 1064 ms.
Task 143: correctly said that (tagalog, 5, a1s3njxhehcip) and (tagalog, 5, a1a25pyfoa9ge) are the same in 785 ms.
Task 144: correctly said that (inuktitut, 1, a12rfyr8dbn79) and (tagalog, 5, ae0ibwt7asth) are not the same in 1745 ms.
Task 145: correctly said that (tagalog, 6, aljsxt6v0a30) and (tagalog, 6, aghrkle9vf6z) are the same in 1209 ms.
Task 146: correctly said that (atemayar, 23, a1j8s7giyto4a) and (atemayar, 23, a1manxzopzden) are the same in 1150 ms.
Task 147: correctly said that (myanmar, 32, ajf66jwen7bx) and (myanmar, 32, adc8vbkoocrr) are the same in 1393 ms.
Task 148: correctly said that (atemayar, 23, a1poymbentexx) and (atemayar, 23, avlbkm6qpb1s) are the same in 2530 ms.
Task 149: incorrectly said that (myanmar, 32, a1umhcrj9zfdp) and (sylheti, 20, a4858n7ggst4) are the same in 6002 ms.
Task 150: correctly said that (atemayar, 10, avlbkm6qpb1s) and (tagalog, 10, a3qat9qkqumi2) are not the same in 1153 ms.
Task 151: incorrectly said that (atemayar, 25, awl8tng8e81d) and (atemayar, 25, ax5gghhj0o6g) are not the same in 2331 ms.
Task 152: correctly said that (myanmar, 1, a1aegunf6e081) and (inuktitut, 11, a3noo9k3y1yu5) are not the same in 1018 ms.
Task 153: correctly said that (korean, 4, a2nm1qzuh14ls) and (myanmar, 25, a1aegunf6e081) are not the same in 2312 ms.
Task 154: correctly said that (tagalog, 2, avlbkm6qpb1s) and (atemayar, 10, awl8tng8e81d) are not the same in 1065 ms.
Task 155: correctly said that (korean, 4, a3d7xwp0gand5) and (korean, 4, a1manxzopzden) are the same in 1317 ms.
Task 156: correctly said that (atemayar, 6, agst3co6usf8) and (myanmar, 32, aljsxt6v0a30) are not the same in 1113 ms.
Task 157: correctly said that (tagalog, 6, a3m0kt6kjezd1) and (tagalog, 6, a2du3880jbrov) are the same in 1353 ms.
Task 158: correctly said that (sylheti, 20, a1qku9mccvl35) and (korean, 29, a3d7xwp0gand5) are not the same in 1010 ms.
Task 159: correctly said that (atemayar, 10, agst3co6usf8) and (myanmar, 25, a1ehrxjplsq0r) are not the same in 763 ms.
Task 160: correctly said that (korean, 29, a3mv1jtwep7xn) and (inuktitut, 11, a1manxzopzden) are not the same in 1524 ms.
Task 161: correctly said that (atemayar, 25, agst3co6usf8) and (korean, 29, a1s3njxhehcip) are not the same in 744 ms.
Task 162: correctly said that (inuktitut, 3, a1s3njxhehcip) and (inuktitut, 3, a1j8s7giyto4a) are the same in 969 ms.
Task 163: correctly said that (tagalog, 6, a1ehrxjplsq0r) and (sylheti, 20, a3np21apfa18b) are not the same in 1194 ms.
Task 164: correctly said that (atemayar, 10, a1z59eb5sfxpy) and (inuktitut, 2, a2pfktghg1ofp) are not the same in 769 ms.
Task 165: correctly said that (myanmar, 1, a1stvgjqfl5kk) and (inuktitut, 3, a12rfyr8dbn79) are not the same in 736 ms.
Task 166: correctly said that (tagalog, 5, a1yovlluv1c9h) and (inuktitut, 3, a315xddd8j4x8) are not the same in 1216 ms.
Task 167: incorrectly said that (korean, 4, aav4pzxdoebd) and (korean, 29, aav4pzxdoebd) are the same in 1442 ms.
Task 168: correctly said that (atemayar, 10, a3m0kt6kjezd1) and (korean, 39, aav4pzxdoebd) are not the same in 1099 ms.
Task 169: incorrectly said that (sylheti, 20, ay0bjcgydhix) and (sylheti, 19, avlbkm6qpb1s) are the same in 1105 ms.
Task 170: correctly said that (myanmar, 25, a3qat9qkqumi2) and (myanmar, 25, a1foggtlgo7n4) are the same in 1755 ms.
Task 171: correctly said that (tagalog, 5, a12rfyr8dbn79) and (inuktitut, 11, ay0bjcgydhix) are not the same in 1895 ms.
Task 172: correctly said that (sylheti, 28, a3r9q9wqm1nyz) and (inuktitut, 1, a1manxzopzden) are not the same in 888 ms.
Task 173: correctly said that (korean, 39, a3dfh6dpi11ip) and (inuktitut, 2, ae0ibwt7asth) are not the same in 680 ms.
Task 174: correctly said that (inuktitut, 2, a3mv1jtwep7xn) and (inuktitut, 3, ay0bjcgydhix) are not the same in 747 ms.
Task 175: correctly said that (inuktitut, 11, a1w3fo0d7cf5t) and (atemayar, 10, agst3co6usf8) are not the same in 1079 ms.
Task 176: correctly said that (atemayar, 25, awl8tng8e81d) and (inuktitut, 11, a1w3fo0d7cf5t) are not the same in 729 ms.
Task 177: correctly said that (sylheti, 19, aljsxt6v0a30) and (korean, 28, a3bb99mn3zy37) are not the same in 696 ms.
Task 178: incorrectly said that (sylheti, 28, a3np21apfa18b) and (sylheti, 17, aljsxt6v0a30) are the same in 2217 ms.
Task 179: correctly said that (korean, 4, a2ernxe6jlm4x) and (atemayar, 25, a6fjbvuzrhm6) are not the same in 2009 ms.
Task 180: correctly said that (atemayar, 6, a1poymbentexx) and (inuktitut, 11, a1z59eb5sfxpy) are not the same in 2203 ms.
Task 181: correctly said that (inuktitut, 11, a12xm86d2nbih) and (inuktitut, 2, a12rfyr8dbn79) are not the same in 1010 ms.
Task 182: correctly said that (tagalog, 2, a1ehrxjplsq0r) and (tagalog, 2, asu813kb7bv1) are the same in 1297 ms.
Task 183: incorrectly said that (atemayar, 10, a1manxzopzden) and (atemayar, 6, azdw6062ia38) are the same in 1584 ms.
Task 184: correctly said that (inuktitut, 11, avlbkm6qpb1s) and (myanmar, 32, a1aegunf6e081) are not the same in 1129 ms.
Task 185: correctly said that (tagalog, 6, a1ehrxjplsq0r) and (sylheti, 20, a3pkun3onkucn) are not the same in 729 ms.
Task 186: correctly said that (korean, 28, aav4pzxdoebd) and (atemayar, 23, ae0ibwt7asth) are not the same in 890 ms.
Task 187: correctly said that (korean, 28, a2akud1gdstsd) and (korean, 28, a5379luit3pc) are the same in 1239 ms.
Task 188: correctly said that (atemayar, 25, a31xqftzcsia2) and (tagalog, 10, ajf66jwen7bx) are not the same in 888 ms.
Task 189: correctly said that (tagalog, 6, ajf66jwen7bx) and (inuktitut, 11, a3mv1jtwep7xn) are not the same in 784 ms.
Task 190: correctly said that (korean, 39, a1v0p79fynz9j) and (korean, 39, a2px0aywxngce) are the same in 2374 ms.
Task 191: correctly said that (inuktitut, 3, ae0ibwt7asth) and (myanmar, 32, aljsxt6v0a30) are not the same in 1795 ms.
Task 192: correctly said that (atemayar, 25, a1z59eb5sfxpy) and (atemayar, 25, a2pfktghg1ofp) are the same in 1337 ms.
Task 193: correctly said that (sylheti, 20, a1j8s7giyto4a) and (inuktitut, 3, ay0bjcgydhix) are not the same in 1403 ms.
Task 194: correctly said that (myanmar, 25, a1stvgjqfl5kk) and (myanmar, 25, a6fjbvuzrhm6) are the same in 1872 ms.
Task 195: correctly said that (korean, 39, a2nm1qzuh14ls) and (atemayar, 25, a1poymbentexx) are not the same in 2040 ms.
Task 196: correctly said that (tagalog, 2, a12rfyr8dbn79) and (inuktitut, 11, a1w3fo0d7cf5t) are not the same in 857 ms.
Task 197: correctly said that (myanmar, 32, a1umhcrj9zfdp) and (myanmar, 32, a3qat9qkqumi2) are the same in 1377 ms.
Task 198: correctly said that (korean, 29, a1umhcrj9zfdp) and (sylheti, 20, a1j8s7giyto4a) are not the same in 1225 ms.
Task 199: correctly said that (atemayar, 23, a3m0kt6kjezd1) and (sylheti, 28, a3pkun3onkucn) are not the same in 1687 ms.

Duration: 18m 13s 926ms
Comments: What made it tricky sometimes was when it was the same character but looked very different in a way.