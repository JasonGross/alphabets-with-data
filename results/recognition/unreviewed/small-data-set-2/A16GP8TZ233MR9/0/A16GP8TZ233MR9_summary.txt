

Summary:Short Summary:
Correct: 163
Incorrect: 37
Percent Correct: 81

Same Correct: 37
Same Incorrect: 13
Percent Same Correct: 74

Different Correct: 126
Different Incorrect: 24
Percent Different Correct: 84

Long Summary:
Task 0: incorrectly said that (sylheti, 28, a3np21apfa18b) and (sylheti, 28, a1a25pyfoa9ge) are not the same in 3808 ms.
Task 1: correctly said that (inuktitut, 3, a12rfyr8dbn79) and (inuktitut, 3, avlbkm6qpb1s) are the same in 1028 ms.
Task 2: correctly said that (atemayar, 25, a1yovlluv1c9h) and (korean, 4, a5379luit3pc) are not the same in 1398 ms.
Task 3: incorrectly said that (sylheti, 28, avlbkm6qpb1s) and (myanmar, 1, a2pfktghg1ofp) are the same in 2136 ms.
Task 4: correctly said that (inuktitut, 3, a12rfyr8dbn79) and (korean, 4, a3bb99mn3zy37) are not the same in 1780 ms.
Task 5: correctly said that (myanmar, 31, a3qat9qkqumi2) and (inuktitut, 3, a1manxzopzden) are not the same in 1648 ms.
Task 6: incorrectly said that (tagalog, 5, a1i92aacrkacw) and (inuktitut, 2, a3qat9qkqumi2) are the same in 1037 ms.
Task 7: incorrectly said that (myanmar, 1, ajf66jwen7bx) and (inuktitut, 3, a2pfktghg1ofp) are the same in 1251 ms.
Task 8: incorrectly said that (tagalog, 2, a1ehrxjplsq0r) and (myanmar, 31, a1foggtlgo7n4) are the same in 1241 ms.
Task 9: correctly said that (tagalog, 10, a3qat9qkqumi2) and (tagalog, 10, a12rfyr8dbn79) are the same in 995 ms.
Task 10: correctly said that (sylheti, 28, a4858n7ggst4) and (tagalog, 6, aljsxt6v0a30) are not the same in 1705 ms.
Task 11: correctly said that (sylheti, 20, a30xq7555spo8) and (korean, 29, a2px0aywxngce) are not the same in 1224 ms.
Task 12: correctly said that (tagalog, 6, a1l2fzyn31zvf) and (tagalog, 6, a1ehrxjplsq0r) are the same in 1179 ms.
Task 13: correctly said that (inuktitut, 1, a2ernxe6jlm4x) and (sylheti, 20, avlbkm6qpb1s) are not the same in 2542 ms.
Task 14: correctly said that (inuktitut, 11, a3qat9qkqumi2) and (sylheti, 20, a1l2fzyn31zvf) are not the same in 1336 ms.
Task 15: correctly said that (atemayar, 25, a1j8s7giyto4a) and (atemayar, 25, aljsxt6v0a30) are the same in 1090 ms.
Task 16: correctly said that (myanmar, 31, ajf66jwen7bx) and (korean, 4, a3bb99mn3zy37) are not the same in 1179 ms.
Task 17: correctly said that (atemayar, 10, a31e6jchki335) and (atemayar, 10, a1j8s7giyto4a) are the same in 1436 ms.
Task 18: correctly said that (atemayar, 10, a1j8s7giyto4a) and (korean, 29, a2px0aywxngce) are not the same in 1126 ms.
Task 19: correctly said that (korean, 28, a1oh82lwr5hbg) and (korean, 28, a39ivbdm1ekue) are the same in 2387 ms.
Task 20: correctly said that (tagalog, 6, a3mv1jtwep7xn) and (inuktitut, 1, a2pfktghg1ofp) are not the same in 1064 ms.
Task 21: correctly said that (sylheti, 28, a1foggtlgo7n4) and (inuktitut, 2, a2pgu4pi93b1q) are not the same in 1117 ms.
Task 22: correctly said that (myanmar, 31, ay0bjcgydhix) and (atemayar, 6, a1poymbentexx) are not the same in 754 ms.
Task 23: correctly said that (korean, 28, a1manxzopzden) and (korean, 28, a39ivbdm1ekue) are the same in 1120 ms.
Task 24: correctly said that (sylheti, 28, a4858n7ggst4) and (sylheti, 28, a1s3njxhehcip) are the same in 4402 ms.
Task 25: correctly said that (inuktitut, 11, a315xddd8j4x8) and (atemayar, 10, a1j8s7giyto4a) are not the same in 1079 ms.
Task 26: incorrectly said that (atemayar, 25, a23psx2z4f65q) and (tagalog, 10, a1s3njxhehcip) are the same in 18436 ms.
Task 27: correctly said that (myanmar, 32, a2pfktghg1ofp) and (sylheti, 17, a1a25pyfoa9ge) are not the same in 5860 ms.
Task 28: incorrectly said that (tagalog, 6, amynpl5xzw6q) and (tagalog, 6, a2du3880jbrov) are not the same in 1087 ms.
Task 29: incorrectly said that (korean, 39, a2nm1qzuh14ls) and (korean, 39, a39ivbdm1ekue) are not the same in 2025 ms.
Task 30: incorrectly said that (tagalog, 2, a1a25pyfoa9ge) and (myanmar, 25, a4858n7ggst4) are the same in 1807 ms.
Task 31: correctly said that (myanmar, 25, aljsxt6v0a30) and (atemayar, 10, avlbkm6qpb1s) are not the same in 1157 ms.
Task 32: correctly said that (korean, 39, a3dfh6dpi11ip) and (inuktitut, 2, a1j8s7giyto4a) are not the same in 1487 ms.
Task 33: correctly said that (atemayar, 6, a1poymbentexx) and (korean, 29, a1s3njxhehcip) are not the same in 1118 ms.
Task 34: incorrectly said that (myanmar, 31, a1s3njxhehcip) and (sylheti, 20, a3r9q9wqm1nyz) are the same in 2062 ms.
Task 35: correctly said that (inuktitut, 2, ae0ibwt7asth) and (atemayar, 23, a23psx2z4f65q) are not the same in 1116 ms.
Task 36: correctly said that (tagalog, 10, a3qat9qkqumi2) and (tagalog, 2, a1yovlluv1c9h) are not the same in 2292 ms.
Task 37: correctly said that (atemayar, 25, a23psx2z4f65q) and (tagalog, 10, aljsxt6v0a30) are not the same in 908 ms.
Task 38: correctly said that (atemayar, 10, a2pfktghg1ofp) and (inuktitut, 11, a3qat9qkqumi2) are not the same in 1248 ms.
Task 39: correctly said that (korean, 28, a3d7xwp0gand5) and (korean, 28, a1umhcrj9zfdp) are the same in 1302 ms.
Task 40: incorrectly said that (atemayar, 23, awl8tng8e81d) and (atemayar, 23, a31xqftzcsia2) are not the same in 1363 ms.
Task 41: incorrectly said that (sylheti, 17, a1foggtlgo7n4) and (sylheti, 19, a30xq7555spo8) are the same in 1187 ms.
Task 42: incorrectly said that (myanmar, 1, amynpl5xzw6q) and (tagalog, 10, avlbkm6qpb1s) are the same in 2463 ms.
Task 43: correctly said that (inuktitut, 11, ae0ibwt7asth) and (korean, 29, a2ernxe6jlm4x) are not the same in 869 ms.
Task 44: correctly said that (korean, 39, aj6upe9mlarg) and (atemayar, 10, a2pfktghg1ofp) are not the same in 794 ms.
Task 45: incorrectly said that (sylheti, 28, a4858n7ggst4) and (sylheti, 20, a1a25pyfoa9ge) are the same in 1369 ms.
Task 46: correctly said that (tagalog, 10, avlbkm6qpb1s) and (korean, 29, aw82oruamauz) are not the same in 924 ms.
Task 47: correctly said that (tagalog, 6, a1i92aacrkacw) and (myanmar, 32, a2pfktghg1ofp) are not the same in 1145 ms.
Task 48: correctly said that (inuktitut, 11, a1w3fo0d7cf5t) and (inuktitut, 11, a1manxzopzden) are the same in 979 ms.
Task 49: correctly said that (korean, 28, a1udqv4pfypkn) and (atemayar, 25, ax5gghhj0o6g) are not the same in 1007 ms.
Task 50: correctly said that (sylheti, 28, a2du3880jbrov) and (korean, 4, a1umhcrj9zfdp) are not the same in 1231 ms.
Task 51: correctly said that (inuktitut, 11, a12rfyr8dbn79) and (tagalog, 6, a1yovlluv1c9h) are not the same in 1906 ms.
Task 52: incorrectly said that (korean, 29, a2ernxe6jlm4x) and (myanmar, 25, amynpl5xzw6q) are the same in 2149 ms.
Task 53: correctly said that (korean, 29, a3d7xwp0gand5) and (korean, 29, a1oh82lwr5hbg) are the same in 1459 ms.
Task 54: correctly said that (sylheti, 28, a3r9q9wqm1nyz) and (inuktitut, 11, a1s3njxhehcip) are not the same in 1560 ms.
Task 55: correctly said that (tagalog, 5, a1yovlluv1c9h) and (inuktitut, 3, avlbkm6qpb1s) are not the same in 1039 ms.
Task 56: correctly said that (korean, 39, a5379luit3pc) and (korean, 39, a3d7xwp0gand5) are the same in 976 ms.
Task 57: correctly said that (tagalog, 5, a1yovlluv1c9h) and (myanmar, 1, a1umhcrj9zfdp) are not the same in 892 ms.
Task 58: correctly said that (inuktitut, 3, a1qku9mccvl35) and (atemayar, 25, ae0ibwt7asth) are not the same in 1326 ms.
Task 59: correctly said that (sylheti, 17, a6fjbvuzrhm6) and (sylheti, 17, a3209gno9duil) are the same in 970 ms.
Task 60: correctly said that (korean, 39, a1v0p79fynz9j) and (atemayar, 10, a1qku9mccvl35) are not the same in 2448 ms.
Task 61: incorrectly said that (tagalog, 6, ay0bjcgydhix) and (tagalog, 6, a1i92aacrkacw) are not the same in 1364 ms.
Task 62: correctly said that (tagalog, 10, avlbkm6qpb1s) and (korean, 29, aav4pzxdoebd) are not the same in 882 ms.
Task 63: correctly said that (korean, 4, a3d7xwp0gand5) and (sylheti, 17, ay0bjcgydhix) are not the same in 1484 ms.
Task 64: incorrectly said that (tagalog, 2, a1a25pyfoa9ge) and (myanmar, 1, a1ehrxjplsq0r) are the same in 3493 ms.
Task 65: correctly said that (inuktitut, 11, a2pgu4pi93b1q) and (myanmar, 1, ajf66jwen7bx) are not the same in 912 ms.
Task 66: correctly said that (inuktitut, 3, a12rfyr8dbn79) and (korean, 28, a1oh82lwr5hbg) are not the same in 1079 ms.
Task 67: correctly said that (myanmar, 31, a1stvgjqfl5kk) and (sylheti, 28, atvyxlwe5n03) are not the same in 2411 ms.
Task 68: incorrectly said that (tagalog, 6, a3m0kt6kjezd1) and (tagalog, 5, a1yovlluv1c9h) are the same in 1075 ms.
Task 69: correctly said that (tagalog, 5, a1ehrxjplsq0r) and (inuktitut, 11, ay0bjcgydhix) are not the same in 946 ms.
Task 70: correctly said that (inuktitut, 11, a2qnqd7m8zfbo) and (atemayar, 10, a1z59eb5sfxpy) are not the same in 919 ms.
Task 71: correctly said that (korean, 39, ae0ibwt7asth) and (myanmar, 1, a4858n7ggst4) are not the same in 1190 ms.
Task 72: correctly said that (tagalog, 5, a1i92aacrkacw) and (inuktitut, 2, a2qnqd7m8zfbo) are not the same in 1435 ms.
Task 73: correctly said that (inuktitut, 11, a2pgu4pi93b1q) and (inuktitut, 11, a1manxzopzden) are the same in 1105 ms.
Task 74: correctly said that (atemayar, 10, a23psx2z4f65q) and (inuktitut, 2, a1w3fo0d7cf5t) are not the same in 2542 ms.
Task 75: correctly said that (tagalog, 2, a1a25pyfoa9ge) and (atemayar, 10, awl8tng8e81d) are not the same in 1012 ms.
Task 76: correctly said that (inuktitut, 2, a2pfktghg1ofp) and (sylheti, 28, ay0bjcgydhix) are not the same in 1705 ms.
Task 77: correctly said that (inuktitut, 11, a3qat9qkqumi2) and (sylheti, 17, a30xq7555spo8) are not the same in 910 ms.
Task 78: correctly said that (myanmar, 32, a3j1pp96x14u0) and (myanmar, 32, amynpl5xzw6q) are the same in 1200 ms.
Task 79: correctly said that (tagalog, 10, amynpl5xzw6q) and (tagalog, 10, ajf66jwen7bx) are the same in 1294 ms.
Task 80: correctly said that (tagalog, 6, a3qat9qkqumi2) and (atemayar, 23, ax5gghhj0o6g) are not the same in 1126 ms.
Task 81: correctly said that (korean, 28, aav4pzxdoebd) and (korean, 28, a1manxzopzden) are the same in 1297 ms.
Task 82: correctly said that (sylheti, 17, a3r9q9wqm1nyz) and (sylheti, 17, atvyxlwe5n03) are the same in 785 ms.
Task 83: incorrectly said that (atemayar, 23, azdw6062ia38) and (korean, 28, a2nm1qzuh14ls) are the same in 2375 ms.
Task 84: correctly said that (korean, 29, a1umhcrj9zfdp) and (tagalog, 5, a3m0kt6kjezd1) are not the same in 8055 ms.
Task 85: correctly said that (sylheti, 20, avlbkm6qpb1s) and (myanmar, 32, a2pfktghg1ofp) are not the same in 1126 ms.
Task 86: correctly said that (atemayar, 10, ae0ibwt7asth) and (tagalog, 6, a2du3880jbrov) are not the same in 1459 ms.
Task 87: correctly said that (atemayar, 23, ae0ibwt7asth) and (myanmar, 1, a1s3njxhehcip) are not the same in 4287 ms.
Task 88: correctly said that (inuktitut, 3, a3qat9qkqumi2) and (inuktitut, 3, a2qnqd7m8zfbo) are the same in 1879 ms.
Task 89: correctly said that (tagalog, 10, a1yovlluv1c9h) and (korean, 39, a1s3njxhehcip) are not the same in 2219 ms.
Task 90: incorrectly said that (myanmar, 31, a2pfktghg1ofp) and (inuktitut, 11, a1s3njxhehcip) are the same in 8519 ms.
Task 91: correctly said that (sylheti, 17, a1foggtlgo7n4) and (myanmar, 25, a1stvgjqfl5kk) are not the same in 1561 ms.
Task 92: correctly said that (atemayar, 6, a1yovlluv1c9h) and (atemayar, 6, agst3co6usf8) are the same in 1724 ms.
Task 93: correctly said that (inuktitut, 2, avlbkm6qpb1s) and (korean, 28, a1v0p79fynz9j) are not the same in 1535 ms.
Task 94: correctly said that (inuktitut, 3, a1z59eb5sfxpy) and (atemayar, 10, a31e6jchki335) are not the same in 1048 ms.
Task 95: correctly said that (atemayar, 10, ax5gghhj0o6g) and (tagalog, 6, avlbkm6qpb1s) are not the same in 1161 ms.
Task 96: correctly said that (korean, 39, a3d7xwp0gand5) and (atemayar, 10, a1poymbentexx) are not the same in 846 ms.
Task 97: incorrectly said that (korean, 4, a3bb99mn3zy37) and (myanmar, 31, a3pkun3onkucn) are the same in 1585 ms.
Task 98: correctly said that (atemayar, 6, a31e6jchki335) and (myanmar, 32, aljsxt6v0a30) are not the same in 1674 ms.
Task 99: correctly said that (tagalog, 5, a1ehrxjplsq0r) and (korean, 4, a1oh82lwr5hbg) are not the same in 1067 ms.
Task 100: correctly said that (tagalog, 10, a3m0kt6kjezd1) and (korean, 29, aav4pzxdoebd) are not the same in 1675 ms.
Task 101: correctly said that (inuktitut, 2, aljsxt6v0a30) and (inuktitut, 2, a1manxzopzden) are the same in 1093 ms.
Task 102: correctly said that (myanmar, 1, a1j8s7giyto4a) and (myanmar, 1, amynpl5xzw6q) are the same in 733 ms.
Task 103: correctly said that (atemayar, 6, a1poymbentexx) and (tagalog, 10, a1l2fzyn31zvf) are not the same in 2230 ms.
Task 104: correctly said that (tagalog, 5, a12rfyr8dbn79) and (myanmar, 31, ay0bjcgydhix) are not the same in 1276 ms.
Task 105: correctly said that (tagalog, 5, a2du3880jbrov) and (korean, 29, a39ivbdm1ekue) are not the same in 1200 ms.
Task 106: incorrectly said that (myanmar, 31, a2pfktghg1ofp) and (myanmar, 32, a4858n7ggst4) are the same in 1592 ms.
Task 107: correctly said that (inuktitut, 3, a1z59eb5sfxpy) and (myanmar, 31, a1umhcrj9zfdp) are not the same in 1476 ms.
Task 108: incorrectly said that (sylheti, 17, a3r9q9wqm1nyz) and (korean, 39, a1v0p79fynz9j) are the same in 1882 ms.
Task 109: incorrectly said that (korean, 4, a2nm1qzuh14ls) and (korean, 39, a5379luit3pc) are the same in 1705 ms.
Task 110: correctly said that (korean, 29, a2ernxe6jlm4x) and (korean, 29, a1udqv4pfypkn) are the same in 2094 ms.
Task 111: correctly said that (tagalog, 6, a1yovlluv1c9h) and (tagalog, 6, ajf66jwen7bx) are the same in 1246 ms.
Task 112: correctly said that (inuktitut, 1, ae0ibwt7asth) and (tagalog, 2, a1j8s7giyto4a) are not the same in 1364 ms.
Task 113: correctly said that (sylheti, 28, a3m0kt6kjezd1) and (myanmar, 32, a3qat9qkqumi2) are not the same in 1122 ms.
Task 114: correctly said that (sylheti, 28, a1l2fzyn31zvf) and (atemayar, 23, a31e6jchki335) are not the same in 1168 ms.
Task 115: incorrectly said that (sylheti, 17, a2du3880jbrov) and (sylheti, 17, a6fjbvuzrhm6) are not the same in 3479 ms.
Task 116: incorrectly said that (atemayar, 6, a3m0kt6kjezd1) and (atemayar, 23, azdw6062ia38) are the same in 5464 ms.
Task 117: correctly said that (sylheti, 28, avlbkm6qpb1s) and (sylheti, 28, a3m0kt6kjezd1) are the same in 1008 ms.
Task 118: correctly said that (tagalog, 6, avlbkm6qpb1s) and (tagalog, 6, a3qat9qkqumi2) are the same in 1595 ms.
Task 119: correctly said that (sylheti, 20, a3r9q9wqm1nyz) and (sylheti, 20, a3pkun3onkucn) are the same in 1272 ms.
Task 120: correctly said that (myanmar, 25, amynpl5xzw6q) and (sylheti, 28, a3209gno9duil) are not the same in 1533 ms.
Task 121: correctly said that (korean, 4, a3bb99mn3zy37) and (myanmar, 31, ajf66jwen7bx) are not the same in 1385 ms.
Task 122: correctly said that (myanmar, 32, a1s3njxhehcip) and (tagalog, 6, ae0ibwt7asth) are not the same in 1235 ms.
Task 123: correctly said that (sylheti, 20, a1j8s7giyto4a) and (atemayar, 6, a31xqftzcsia2) are not the same in 1044 ms.
Task 124: correctly said that (atemayar, 25, azdw6062ia38) and (korean, 28, a2ernxe6jlm4x) are not the same in 1719 ms.
Task 125: correctly said that (atemayar, 10, a1qku9mccvl35) and (tagalog, 6, avlbkm6qpb1s) are not the same in 1015 ms.
Task 126: incorrectly said that (sylheti, 28, a3r9q9wqm1nyz) and (sylheti, 28, a1s3njxhehcip) are not the same in 6190 ms.
Task 127: correctly said that (sylheti, 20, atvyxlwe5n03) and (tagalog, 5, aghrkle9vf6z) are not the same in 849 ms.
Task 128: correctly said that (tagalog, 5, a1s3njxhehcip) and (korean, 4, a3bb99mn3zy37) are not the same in 1056 ms.
Task 129: correctly said that (tagalog, 2, a3mv1jtwep7xn) and (inuktitut, 2, a1w3fo0d7cf5t) are not the same in 792 ms.
Task 130: correctly said that (tagalog, 10, a12rfyr8dbn79) and (tagalog, 2, a3mv1jtwep7xn) are not the same in 8938 ms.
Task 131: correctly said that (atemayar, 25, agst3co6usf8) and (atemayar, 10, a1z59eb5sfxpy) are not the same in 967 ms.
Task 132: incorrectly said that (inuktitut, 2, a2ernxe6jlm4x) and (inuktitut, 11, a12xm86d2nbih) are the same in 1852 ms.
Task 133: correctly said that (tagalog, 10, a1ehrxjplsq0r) and (inuktitut, 2, avlbkm6qpb1s) are not the same in 1314 ms.
Task 134: correctly said that (tagalog, 5, a12rfyr8dbn79) and (sylheti, 19, a4858n7ggst4) are not the same in 1250 ms.
Task 135: correctly said that (korean, 39, a1s3njxhehcip) and (korean, 28, a2akud1gdstsd) are not the same in 1562 ms.
Task 136: correctly said that (atemayar, 25, a1poymbentexx) and (korean, 4, a3dfh6dpi11ip) are not the same in 1269 ms.
Task 137: correctly said that (korean, 39, a3mv1jtwep7xn) and (korean, 29, a1umhcrj9zfdp) are not the same in 2123 ms.
Task 138: correctly said that (korean, 28, a1manxzopzden) and (myanmar, 32, avlbkm6qpb1s) are not the same in 948 ms.
Task 139: correctly said that (inuktitut, 11, a315xddd8j4x8) and (tagalog, 5, a1ehrxjplsq0r) are not the same in 896 ms.
Task 140: correctly said that (korean, 29, a3dfh6dpi11ip) and (korean, 28, aw82oruamauz) are not the same in 2840 ms.
Task 141: correctly said that (tagalog, 10, a1i92aacrkacw) and (tagalog, 10, avlbkm6qpb1s) are the same in 1542 ms.
Task 142: correctly said that (myanmar, 32, adc8vbkoocrr) and (korean, 39, a5379luit3pc) are not the same in 1471 ms.
Task 143: incorrectly said that (atemayar, 6, a1vzhyp8yvxkz) and (atemayar, 23, agst3co6usf8) are the same in 2082 ms.
Task 144: correctly said that (sylheti, 19, a6fjbvuzrhm6) and (myanmar, 32, a2pfktghg1ofp) are not the same in 1373 ms.
Task 145: correctly said that (myanmar, 32, a1foggtlgo7n4) and (myanmar, 1, amynpl5xzw6q) are not the same in 1405 ms.
Task 146: correctly said that (sylheti, 28, avlbkm6qpb1s) and (korean, 29, a2akud1gdstsd) are not the same in 1050 ms.
Task 147: correctly said that (korean, 4, a3dfh6dpi11ip) and (tagalog, 5, a1i92aacrkacw) are not the same in 848 ms.
Task 148: correctly said that (tagalog, 5, a3qat9qkqumi2) and (inuktitut, 2, avlbkm6qpb1s) are not the same in 857 ms.
Task 149: correctly said that (myanmar, 1, a3pkun3onkucn) and (myanmar, 1, a1stvgjqfl5kk) are the same in 1274 ms.
Task 150: correctly said that (atemayar, 10, a3m0kt6kjezd1) and (korean, 39, aw82oruamauz) are not the same in 2337 ms.
Task 151: correctly said that (atemayar, 23, a1j8s7giyto4a) and (myanmar, 32, ay0bjcgydhix) are not the same in 1625 ms.
Task 152: correctly said that (atemayar, 23, ax5gghhj0o6g) and (korean, 4, a39ivbdm1ekue) are not the same in 1177 ms.
Task 153: correctly said that (korean, 4, a2akud1gdstsd) and (tagalog, 10, a12rfyr8dbn79) are not the same in 887 ms.
Task 154: correctly said that (sylheti, 20, a4858n7ggst4) and (sylheti, 20, a2du3880jbrov) are the same in 1420 ms.
Task 155: incorrectly said that (atemayar, 25, a31e6jchki335) and (atemayar, 25, a1yovlluv1c9h) are not the same in 1442 ms.
Task 156: correctly said that (korean, 39, a39ivbdm1ekue) and (inuktitut, 1, a12xm86d2nbih) are not the same in 1032 ms.
Task 157: correctly said that (tagalog, 10, a1ehrxjplsq0r) and (inuktitut, 2, a1qku9mccvl35) are not the same in 1736 ms.
Task 158: incorrectly said that (tagalog, 10, amynpl5xzw6q) and (tagalog, 10, a2du3880jbrov) are not the same in 2379 ms.
Task 159: correctly said that (myanmar, 31, a2pfktghg1ofp) and (inuktitut, 2, a2pfktghg1ofp) are not the same in 1573 ms.
Task 160: incorrectly said that (atemayar, 6, a1vzhyp8yvxkz) and (atemayar, 6, a1poymbentexx) are not the same in 1946 ms.
Task 161: correctly said that (atemayar, 10, aljsxt6v0a30) and (tagalog, 10, a1yovlluv1c9h) are not the same in 1064 ms.
Task 162: correctly said that (atemayar, 6, a2pfktghg1ofp) and (sylheti, 28, a6fjbvuzrhm6) are not the same in 1474 ms.
Task 163: correctly said that (myanmar, 31, avlbkm6qpb1s) and (tagalog, 10, a1i92aacrkacw) are not the same in 2012 ms.
Task 164: correctly said that (myanmar, 31, a1stvgjqfl5kk) and (myanmar, 31, a2pfktghg1ofp) are the same in 1217 ms.
Task 165: correctly said that (inuktitut, 2, ay0bjcgydhix) and (tagalog, 10, aljsxt6v0a30) are not the same in 1430 ms.
Task 166: correctly said that (sylheti, 19, a2du3880jbrov) and (atemayar, 23, a1vzhyp8yvxkz) are not the same in 1034 ms.
Task 167: correctly said that (inuktitut, 3, a12xm86d2nbih) and (tagalog, 2, a1s3njxhehcip) are not the same in 883 ms.
Task 168: incorrectly said that (sylheti, 19, a3pkun3onkucn) and (sylheti, 28, a1foggtlgo7n4) are the same in 1259 ms.
Task 169: correctly said that (inuktitut, 11, ay0bjcgydhix) and (atemayar, 25, ae0ibwt7asth) are not the same in 1185 ms.
Task 170: correctly said that (tagalog, 6, aljsxt6v0a30) and (korean, 39, aav4pzxdoebd) are not the same in 1604 ms.
Task 171: correctly said that (tagalog, 10, avlbkm6qpb1s) and (tagalog, 6, a2du3880jbrov) are not the same in 1726 ms.
Task 172: correctly said that (korean, 39, a3dfh6dpi11ip) and (korean, 29, a2nm1qzuh14ls) are not the same in 1022 ms.
Task 173: correctly said that (sylheti, 28, a3r9q9wqm1nyz) and (sylheti, 19, a3pkun3onkucn) are not the same in 5617 ms.
Task 174: correctly said that (sylheti, 17, a3209gno9duil) and (myanmar, 1, a1aegunf6e081) are not the same in 952 ms.
Task 175: correctly said that (myanmar, 32, a1foggtlgo7n4) and (myanmar, 32, a3j1pp96x14u0) are the same in 1351 ms.
Task 176: correctly said that (sylheti, 17, a4858n7ggst4) and (tagalog, 5, aghrkle9vf6z) are not the same in 1282 ms.
Task 177: correctly said that (inuktitut, 3, a1s3njxhehcip) and (atemayar, 6, a6fjbvuzrhm6) are not the same in 1170 ms.
Task 178: correctly said that (sylheti, 20, a1s3njxhehcip) and (korean, 39, a1s3njxhehcip) are not the same in 838 ms.
Task 179: correctly said that (tagalog, 6, asu813kb7bv1) and (inuktitut, 11, ay0bjcgydhix) are not the same in 823 ms.
Task 180: correctly said that (inuktitut, 2, a3noo9k3y1yu5) and (sylheti, 20, a1a25pyfoa9ge) are not the same in 1236 ms.
Task 181: correctly said that (sylheti, 20, avlbkm6qpb1s) and (sylheti, 20, a1a25pyfoa9ge) are the same in 1340 ms.
Task 182: correctly said that (tagalog, 6, a3m0kt6kjezd1) and (tagalog, 6, asu813kb7bv1) are the same in 1703 ms.
Task 183: correctly said that (myanmar, 32, ay0bjcgydhix) and (sylheti, 20, a3pkun3onkucn) are not the same in 1267 ms.
Task 184: incorrectly said that (korean, 29, a39ivbdm1ekue) and (korean, 29, a2ernxe6jlm4x) are not the same in 1164 ms.
Task 185: correctly said that (inuktitut, 2, a1s3njxhehcip) and (tagalog, 5, a1yovlluv1c9h) are not the same in 1950 ms.
Task 186: incorrectly said that (atemayar, 23, a31xqftzcsia2) and (atemayar, 23, a1manxzopzden) are not the same in 795 ms.
Task 187: correctly said that (inuktitut, 11, ay0bjcgydhix) and (sylheti, 17, amynpl5xzw6q) are not the same in 945 ms.
Task 188: correctly said that (atemayar, 6, agst3co6usf8) and (korean, 29, aw82oruamauz) are not the same in 899 ms.
Task 189: incorrectly said that (tagalog, 10, a3mv1jtwep7xn) and (tagalog, 10, a1l2fzyn31zvf) are not the same in 953 ms.
Task 190: correctly said that (tagalog, 10, ajf66jwen7bx) and (tagalog, 10, a3mv1jtwep7xn) are the same in 1670 ms.
Task 191: correctly said that (inuktitut, 11, a2pfktghg1ofp) and (myanmar, 31, a1umhcrj9zfdp) are not the same in 1100 ms.
Task 192: correctly said that (sylheti, 19, ay0bjcgydhix) and (tagalog, 2, a1l2fzyn31zvf) are not the same in 996 ms.
Task 193: correctly said that (korean, 28, a1oh82lwr5hbg) and (tagalog, 5, avlbkm6qpb1s) are not the same in 934 ms.
Task 194: correctly said that (sylheti, 20, a1qku9mccvl35) and (tagalog, 5, a1i92aacrkacw) are not the same in 830 ms.
Task 195: correctly said that (atemayar, 23, a1z59eb5sfxpy) and (myanmar, 1, a3qat9qkqumi2) are not the same in 904 ms.
Task 196: correctly said that (atemayar, 25, a1yovlluv1c9h) and (myanmar, 32, a1aegunf6e081) are not the same in 1025 ms.
Task 197: incorrectly said that (korean, 39, a3d7xwp0gand5) and (atemayar, 23, a1manxzopzden) are the same in 1482 ms.
Task 198: correctly said that (korean, 28, aj6upe9mlarg) and (korean, 28, aav4pzxdoebd) are the same in 1782 ms.
Task 199: correctly said that (atemayar, 25, a1poymbentexx) and (atemayar, 25, a23psx2z4f65q) are the same in 1639 ms.

Duration: 18m 23s 420ms
Comments: Flashes were extremely quick, in some instances I'm not even sure an image was shown, because I was careful not to look away.