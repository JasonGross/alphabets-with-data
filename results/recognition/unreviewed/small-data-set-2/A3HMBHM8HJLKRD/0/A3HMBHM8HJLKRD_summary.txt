

Summary:Short Summary:
Correct: 158
Incorrect: 42
Percent Correct: 79

Same Correct: 57
Same Incorrect: 3
Percent Same Correct: 95

Different Correct: 101
Different Incorrect: 39
Percent Different Correct: 72

Long Summary:
Task 0: correctly said that (korean, 28, a1oh82lwr5hbg) and (myanmar, 1, ajf66jwen7bx) are not the same in 3700 ms.
Task 1: incorrectly said that (korean, 28, a1manxzopzden) and (korean, 28, a2nm1qzuh14ls) are not the same in 1323 ms.
Task 2: correctly said that (korean, 39, a1umhcrj9zfdp) and (sylheti, 20, atvyxlwe5n03) are not the same in 3141 ms.
Task 3: incorrectly said that (sylheti, 19, aljsxt6v0a30) and (korean, 28, a1umhcrj9zfdp) are the same in 3513 ms.
Task 4: correctly said that (sylheti, 28, a1s3njxhehcip) and (inuktitut, 2, a3noo9k3y1yu5) are not the same in 1427 ms.
Task 5: correctly said that (korean, 4, a2ernxe6jlm4x) and (inuktitut, 1, a315xddd8j4x8) are not the same in 942 ms.
Task 6: incorrectly said that (inuktitut, 11, a1s3njxhehcip) and (tagalog, 6, a1s3njxhehcip) are the same in 1840 ms.
Task 7: correctly said that (atemayar, 25, avlbkm6qpb1s) and (sylheti, 17, a6fjbvuzrhm6) are not the same in 2150 ms.
Task 8: correctly said that (inuktitut, 1, a3qat9qkqumi2) and (atemayar, 23, a31e6jchki335) are not the same in 1480 ms.
Task 9: incorrectly said that (sylheti, 17, a4858n7ggst4) and (sylheti, 28, a1j8s7giyto4a) are the same in 2056 ms.
Task 10: correctly said that (myanmar, 1, a4858n7ggst4) and (inuktitut, 1, avlbkm6qpb1s) are not the same in 1709 ms.
Task 11: correctly said that (myanmar, 31, a1ehrxjplsq0r) and (tagalog, 6, a12rfyr8dbn79) are not the same in 945 ms.
Task 12: incorrectly said that (myanmar, 31, avlbkm6qpb1s) and (korean, 4, aj6upe9mlarg) are the same in 1554 ms.
Task 13: incorrectly said that (sylheti, 19, a3m0kt6kjezd1) and (myanmar, 25, a1foggtlgo7n4) are the same in 724 ms.
Task 14: correctly said that (myanmar, 1, amynpl5xzw6q) and (inuktitut, 2, a1w3fo0d7cf5t) are not the same in 971 ms.
Task 15: incorrectly said that (myanmar, 32, aljsxt6v0a30) and (sylheti, 17, a1foggtlgo7n4) are the same in 2014 ms.
Task 16: incorrectly said that (tagalog, 2, aljsxt6v0a30) and (atemayar, 25, a1manxzopzden) are the same in 1405 ms.
Task 17: correctly said that (inuktitut, 2, a2pfktghg1ofp) and (inuktitut, 11, a2pgu4pi93b1q) are not the same in 2365 ms.
Task 18: incorrectly said that (inuktitut, 2, a2ernxe6jlm4x) and (inuktitut, 2, a315xddd8j4x8) are not the same in 948 ms.
Task 19: correctly said that (atemayar, 6, agst3co6usf8) and (myanmar, 25, a1ehrxjplsq0r) are not the same in 842 ms.
Task 20: correctly said that (inuktitut, 11, avlbkm6qpb1s) and (inuktitut, 11, a2ernxe6jlm4x) are the same in 1858 ms.
Task 21: correctly said that (atemayar, 25, a1vzhyp8yvxkz) and (atemayar, 25, a3m0kt6kjezd1) are the same in 838 ms.
Task 22: correctly said that (inuktitut, 11, a3qat9qkqumi2) and (sylheti, 19, a1s3njxhehcip) are not the same in 1964 ms.
Task 23: correctly said that (sylheti, 20, a1j8s7giyto4a) and (myanmar, 31, a2pfktghg1ofp) are not the same in 769 ms.
Task 24: correctly said that (atemayar, 25, avlbkm6qpb1s) and (korean, 28, a3d7xwp0gand5) are not the same in 753 ms.
Task 25: correctly said that (myanmar, 31, aljsxt6v0a30) and (myanmar, 31, a4858n7ggst4) are the same in 1959 ms.
Task 26: correctly said that (myanmar, 31, a1stvgjqfl5kk) and (inuktitut, 1, a1z59eb5sfxpy) are not the same in 1469 ms.
Task 27: correctly said that (sylheti, 20, a2du3880jbrov) and (tagalog, 10, asu813kb7bv1) are not the same in 998 ms.
Task 28: correctly said that (atemayar, 10, a2pfktghg1ofp) and (inuktitut, 1, a3mv1jtwep7xn) are not the same in 684 ms.
Task 29: correctly said that (atemayar, 6, a1poymbentexx) and (atemayar, 6, ae0ibwt7asth) are the same in 1085 ms.
Task 30: incorrectly said that (myanmar, 31, a1aegunf6e081) and (myanmar, 1, a2pfktghg1ofp) are the same in 1114 ms.
Task 31: incorrectly said that (atemayar, 10, a3m0kt6kjezd1) and (atemayar, 6, azdw6062ia38) are the same in 1038 ms.
Task 32: correctly said that (atemayar, 23, a2pfktghg1ofp) and (atemayar, 23, a3m0kt6kjezd1) are the same in 931 ms.
Task 33: correctly said that (sylheti, 20, a3pkun3onkucn) and (sylheti, 20, a2du3880jbrov) are the same in 935 ms.
Task 34: correctly said that (inuktitut, 2, a1s3njxhehcip) and (inuktitut, 2, a1qku9mccvl35) are the same in 1534 ms.
Task 35: correctly said that (korean, 4, a1umhcrj9zfdp) and (inuktitut, 2, a1s3njxhehcip) are not the same in 1732 ms.
Task 36: correctly said that (myanmar, 31, a1foggtlgo7n4) and (sylheti, 20, a30xq7555spo8) are not the same in 1511 ms.
Task 37: correctly said that (inuktitut, 2, aljsxt6v0a30) and (inuktitut, 2, ae0ibwt7asth) are the same in 1537 ms.
Task 38: correctly said that (korean, 28, a5379luit3pc) and (tagalog, 10, aghrkle9vf6z) are not the same in 1065 ms.
Task 39: correctly said that (atemayar, 23, azdw6062ia38) and (atemayar, 23, ae0ibwt7asth) are the same in 1377 ms.
Task 40: incorrectly said that (tagalog, 2, a1s3njxhehcip) and (tagalog, 6, a12rfyr8dbn79) are the same in 1942 ms.
Task 41: correctly said that (korean, 28, a1s3njxhehcip) and (inuktitut, 3, a1manxzopzden) are not the same in 2069 ms.
Task 42: correctly said that (atemayar, 23, ax5gghhj0o6g) and (atemayar, 23, avlbkm6qpb1s) are the same in 1559 ms.
Task 43: correctly said that (myanmar, 25, a3j1pp96x14u0) and (myanmar, 25, amynpl5xzw6q) are the same in 5427 ms.
Task 44: correctly said that (atemayar, 23, ae0ibwt7asth) and (atemayar, 23, awl8tng8e81d) are the same in 2435 ms.
Task 45: correctly said that (korean, 28, a2px0aywxngce) and (korean, 28, a39ivbdm1ekue) are the same in 790 ms.
Task 46: incorrectly said that (inuktitut, 2, a1j8s7giyto4a) and (inuktitut, 1, a1w3fo0d7cf5t) are the same in 799 ms.
Task 47: correctly said that (tagalog, 2, aghrkle9vf6z) and (inuktitut, 1, ay0bjcgydhix) are not the same in 1672 ms.
Task 48: correctly said that (inuktitut, 1, a2qnqd7m8zfbo) and (inuktitut, 1, a2pgu4pi93b1q) are the same in 1294 ms.
Task 49: incorrectly said that (korean, 4, a3d7xwp0gand5) and (atemayar, 6, a1poymbentexx) are the same in 869 ms.
Task 50: correctly said that (korean, 39, a1oh82lwr5hbg) and (myanmar, 25, adc8vbkoocrr) are not the same in 1389 ms.
Task 51: incorrectly said that (atemayar, 6, awl8tng8e81d) and (atemayar, 10, a1vzhyp8yvxkz) are the same in 1694 ms.
Task 52: incorrectly said that (korean, 39, a39ivbdm1ekue) and (atemayar, 10, a1z59eb5sfxpy) are the same in 837 ms.
Task 53: correctly said that (tagalog, 10, a1l2fzyn31zvf) and (inuktitut, 3, a1manxzopzden) are not the same in 1825 ms.
Task 54: correctly said that (myanmar, 32, ajf66jwen7bx) and (sylheti, 20, a1foggtlgo7n4) are not the same in 1016 ms.
Task 55: incorrectly said that (sylheti, 20, a3r9q9wqm1nyz) and (sylheti, 19, a2du3880jbrov) are the same in 1804 ms.
Task 56: correctly said that (myanmar, 31, a1stvgjqfl5kk) and (inuktitut, 1, a1j8s7giyto4a) are not the same in 1376 ms.
Task 57: incorrectly said that (sylheti, 20, a1s3njxhehcip) and (tagalog, 10, asu813kb7bv1) are the same in 1197 ms.
Task 58: correctly said that (tagalog, 5, a1a25pyfoa9ge) and (inuktitut, 3, a1qku9mccvl35) are not the same in 1560 ms.
Task 59: correctly said that (atemayar, 6, aljsxt6v0a30) and (myanmar, 25, a1j8s7giyto4a) are not the same in 1021 ms.
Task 60: correctly said that (sylheti, 20, a1foggtlgo7n4) and (sylheti, 20, a3np21apfa18b) are the same in 1767 ms.
Task 61: correctly said that (inuktitut, 11, a1z59eb5sfxpy) and (myanmar, 25, amynpl5xzw6q) are not the same in 1287 ms.
Task 62: correctly said that (sylheti, 19, amynpl5xzw6q) and (tagalog, 6, a1l2fzyn31zvf) are not the same in 1042 ms.
Task 63: correctly said that (myanmar, 1, avlbkm6qpb1s) and (inuktitut, 11, a3mv1jtwep7xn) are not the same in 1764 ms.
Task 64: correctly said that (korean, 39, a1udqv4pfypkn) and (myanmar, 1, a3pkun3onkucn) are not the same in 1040 ms.
Task 65: incorrectly said that (atemayar, 23, avlbkm6qpb1s) and (tagalog, 5, amynpl5xzw6q) are the same in 1584 ms.
Task 66: correctly said that (tagalog, 6, a3qat9qkqumi2) and (tagalog, 6, a1j8s7giyto4a) are the same in 1753 ms.
Task 67: correctly said that (atemayar, 23, azdw6062ia38) and (atemayar, 23, awl8tng8e81d) are the same in 849 ms.
Task 68: correctly said that (sylheti, 28, avlbkm6qpb1s) and (sylheti, 28, a1l2fzyn31zvf) are the same in 797 ms.
Task 69: incorrectly said that (myanmar, 32, ajf66jwen7bx) and (myanmar, 32, a3j1pp96x14u0) are not the same in 1227 ms.
Task 70: correctly said that (korean, 39, aw82oruamauz) and (myanmar, 25, aljsxt6v0a30) are not the same in 2350 ms.
Task 71: correctly said that (sylheti, 20, aljsxt6v0a30) and (sylheti, 20, amynpl5xzw6q) are the same in 1042 ms.
Task 72: incorrectly said that (tagalog, 10, ay0bjcgydhix) and (sylheti, 17, a3209gno9duil) are the same in 2306 ms.
Task 73: correctly said that (atemayar, 23, a1poymbentexx) and (myanmar, 25, a1j8s7giyto4a) are not the same in 1331 ms.
Task 74: correctly said that (sylheti, 17, a3np21apfa18b) and (atemayar, 6, awl8tng8e81d) are not the same in 1047 ms.
Task 75: correctly said that (tagalog, 6, a3mv1jtwep7xn) and (sylheti, 19, a1j8s7giyto4a) are not the same in 905 ms.
Task 76: correctly said that (myanmar, 25, ay0bjcgydhix) and (myanmar, 25, a1j8s7giyto4a) are the same in 1445 ms.
Task 77: correctly said that (sylheti, 19, a3pkun3onkucn) and (myanmar, 1, avlbkm6qpb1s) are not the same in 1219 ms.
Task 78: correctly said that (myanmar, 25, a1j8s7giyto4a) and (tagalog, 10, a1a25pyfoa9ge) are not the same in 1133 ms.
Task 79: incorrectly said that (atemayar, 6, a1poymbentexx) and (korean, 39, a3mv1jtwep7xn) are the same in 1520 ms.
Task 80: correctly said that (korean, 29, a1umhcrj9zfdp) and (myanmar, 31, ay0bjcgydhix) are not the same in 1661 ms.
Task 81: correctly said that (korean, 39, a3mv1jtwep7xn) and (korean, 39, a2nm1qzuh14ls) are the same in 1025 ms.
Task 82: correctly said that (atemayar, 23, a31xqftzcsia2) and (myanmar, 1, a3j1pp96x14u0) are not the same in 1308 ms.
Task 83: correctly said that (sylheti, 20, a4858n7ggst4) and (sylheti, 20, a3pkun3onkucn) are the same in 1244 ms.
Task 84: incorrectly said that (korean, 4, a1udqv4pfypkn) and (sylheti, 17, a1foggtlgo7n4) are the same in 971 ms.
Task 85: correctly said that (myanmar, 1, a1foggtlgo7n4) and (inuktitut, 11, a12rfyr8dbn79) are not the same in 1638 ms.
Task 86: correctly said that (inuktitut, 11, a12rfyr8dbn79) and (korean, 29, a3dfh6dpi11ip) are not the same in 809 ms.
Task 87: incorrectly said that (atemayar, 25, agst3co6usf8) and (korean, 28, a2akud1gdstsd) are the same in 1607 ms.
Task 88: correctly said that (atemayar, 6, a31e6jchki335) and (atemayar, 6, a1vzhyp8yvxkz) are the same in 884 ms.
Task 89: correctly said that (atemayar, 10, a1manxzopzden) and (inuktitut, 3, a315xddd8j4x8) are not the same in 1263 ms.
Task 90: correctly said that (tagalog, 6, a1yovlluv1c9h) and (atemayar, 25, awl8tng8e81d) are not the same in 1677 ms.
Task 91: correctly said that (korean, 39, aw82oruamauz) and (korean, 39, a3mv1jtwep7xn) are the same in 1398 ms.
Task 92: correctly said that (tagalog, 2, aghrkle9vf6z) and (sylheti, 28, a30xq7555spo8) are not the same in 1091 ms.
Task 93: correctly said that (inuktitut, 1, a2pgu4pi93b1q) and (inuktitut, 1, a3qat9qkqumi2) are the same in 1269 ms.
Task 94: correctly said that (atemayar, 10, awl8tng8e81d) and (myanmar, 31, a3qat9qkqumi2) are not the same in 1067 ms.
Task 95: correctly said that (korean, 29, a1udqv4pfypkn) and (korean, 29, a2px0aywxngce) are the same in 1715 ms.
Task 96: correctly said that (sylheti, 28, a1a25pyfoa9ge) and (atemayar, 23, ae0ibwt7asth) are not the same in 1968 ms.
Task 97: incorrectly said that (myanmar, 31, a3pkun3onkucn) and (tagalog, 5, ajf66jwen7bx) are the same in 1292 ms.
Task 98: correctly said that (korean, 4, a3mv1jtwep7xn) and (korean, 4, aav4pzxdoebd) are the same in 867 ms.
Task 99: correctly said that (sylheti, 19, a1l2fzyn31zvf) and (sylheti, 19, a3pkun3onkucn) are the same in 729 ms.
Task 100: incorrectly said that (korean, 28, a1manxzopzden) and (sylheti, 20, a3m0kt6kjezd1) are the same in 1949 ms.
Task 101: incorrectly said that (atemayar, 6, awl8tng8e81d) and (korean, 39, aav4pzxdoebd) are the same in 939 ms.
Task 102: correctly said that (tagalog, 2, a1yovlluv1c9h) and (inuktitut, 11, a12rfyr8dbn79) are not the same in 1374 ms.
Task 103: correctly said that (sylheti, 20, aljsxt6v0a30) and (sylheti, 20, a3pkun3onkucn) are the same in 1264 ms.
Task 104: correctly said that (myanmar, 1, a1clejhbx9yyn) and (myanmar, 1, a1stvgjqfl5kk) are the same in 914 ms.
Task 105: correctly said that (sylheti, 20, a3209gno9duil) and (sylheti, 20, a3np21apfa18b) are the same in 1557 ms.
Task 106: correctly said that (sylheti, 17, a1s3njxhehcip) and (sylheti, 17, a3r9q9wqm1nyz) are the same in 1243 ms.
Task 107: correctly said that (korean, 4, ae0ibwt7asth) and (tagalog, 10, ae0ibwt7asth) are not the same in 1384 ms.
Task 108: correctly said that (tagalog, 2, ay0bjcgydhix) and (tagalog, 2, aljsxt6v0a30) are the same in 1263 ms.
Task 109: correctly said that (myanmar, 1, ajf66jwen7bx) and (korean, 4, a5379luit3pc) are not the same in 1251 ms.
Task 110: correctly said that (myanmar, 31, aljsxt6v0a30) and (korean, 29, a39ivbdm1ekue) are not the same in 1274 ms.
Task 111: correctly said that (tagalog, 5, a3qat9qkqumi2) and (sylheti, 19, a1j8s7giyto4a) are not the same in 2004 ms.
Task 112: correctly said that (atemayar, 6, azdw6062ia38) and (tagalog, 10, a1a25pyfoa9ge) are not the same in 814 ms.
Task 113: correctly said that (myanmar, 25, ay0bjcgydhix) and (tagalog, 6, a1yovlluv1c9h) are not the same in 926 ms.
Task 114: correctly said that (korean, 29, a1s3njxhehcip) and (korean, 29, a5379luit3pc) are the same in 1109 ms.
Task 115: incorrectly said that (tagalog, 2, a1i92aacrkacw) and (sylheti, 19, a1s3njxhehcip) are the same in 1560 ms.
Task 116: correctly said that (sylheti, 17, a3m0kt6kjezd1) and (inuktitut, 3, a1j8s7giyto4a) are not the same in 2034 ms.
Task 117: correctly said that (sylheti, 20, a1foggtlgo7n4) and (myanmar, 1, avlbkm6qpb1s) are not the same in 689 ms.
Task 118: correctly said that (tagalog, 5, a2du3880jbrov) and (inuktitut, 3, avlbkm6qpb1s) are not the same in 1710 ms.
Task 119: incorrectly said that (atemayar, 23, azdw6062ia38) and (atemayar, 25, a1vzhyp8yvxkz) are the same in 1279 ms.
Task 120: correctly said that (atemayar, 10, a1j8s7giyto4a) and (tagalog, 6, ae0ibwt7asth) are not the same in 1320 ms.
Task 121: correctly said that (myanmar, 1, adc8vbkoocrr) and (myanmar, 1, a1j8s7giyto4a) are the same in 1384 ms.
Task 122: incorrectly said that (tagalog, 10, ae0ibwt7asth) and (sylheti, 19, a3209gno9duil) are the same in 2183 ms.
Task 123: correctly said that (inuktitut, 11, a315xddd8j4x8) and (myanmar, 31, a3qat9qkqumi2) are not the same in 1177 ms.
Task 124: incorrectly said that (korean, 28, a1v0p79fynz9j) and (korean, 29, a2nm1qzuh14ls) are the same in 979 ms.
Task 125: correctly said that (myanmar, 32, a3j1pp96x14u0) and (myanmar, 32, a2pfktghg1ofp) are the same in 1334 ms.
Task 126: correctly said that (inuktitut, 11, avlbkm6qpb1s) and (inuktitut, 11, a1z59eb5sfxpy) are the same in 578 ms.
Task 127: incorrectly said that (atemayar, 10, a1vzhyp8yvxkz) and (korean, 4, a1manxzopzden) are the same in 569 ms.
Task 128: correctly said that (korean, 39, a39ivbdm1ekue) and (korean, 39, aw82oruamauz) are the same in 765 ms.
Task 129: correctly said that (tagalog, 10, a3m0kt6kjezd1) and (tagalog, 10, ae0ibwt7asth) are the same in 962 ms.
Task 130: correctly said that (sylheti, 17, a1qku9mccvl35) and (tagalog, 5, a12rfyr8dbn79) are not the same in 1612 ms.
Task 131: correctly said that (myanmar, 32, a1j8s7giyto4a) and (tagalog, 10, a1l2fzyn31zvf) are not the same in 1785 ms.
Task 132: correctly said that (tagalog, 5, a1s3njxhehcip) and (atemayar, 10, awl8tng8e81d) are not the same in 995 ms.
Task 133: correctly said that (atemayar, 25, a1z59eb5sfxpy) and (atemayar, 25, a31e6jchki335) are the same in 1983 ms.
Task 134: correctly said that (inuktitut, 11, a12rfyr8dbn79) and (atemayar, 25, a1j8s7giyto4a) are not the same in 1720 ms.
Task 135: correctly said that (atemayar, 23, a2pfktghg1ofp) and (myanmar, 31, a1stvgjqfl5kk) are not the same in 753 ms.
Task 136: incorrectly said that (korean, 4, a1udqv4pfypkn) and (korean, 28, aw82oruamauz) are the same in 1285 ms.
Task 137: correctly said that (tagalog, 2, a1i92aacrkacw) and (korean, 4, a1oh82lwr5hbg) are not the same in 1848 ms.
Task 138: correctly said that (myanmar, 1, a1umhcrj9zfdp) and (tagalog, 5, aghrkle9vf6z) are not the same in 2053 ms.
Task 139: correctly said that (inuktitut, 1, ay0bjcgydhix) and (inuktitut, 1, avlbkm6qpb1s) are the same in 1698 ms.
Task 140: correctly said that (atemayar, 23, ae0ibwt7asth) and (atemayar, 23, a1poymbentexx) are the same in 1292 ms.
Task 141: correctly said that (sylheti, 17, a1l2fzyn31zvf) and (myanmar, 32, a1s3njxhehcip) are not the same in 1429 ms.
Task 142: correctly said that (atemayar, 6, ax5gghhj0o6g) and (tagalog, 2, a1s3njxhehcip) are not the same in 773 ms.
Task 143: correctly said that (myanmar, 31, a1aegunf6e081) and (korean, 29, a3dfh6dpi11ip) are not the same in 701 ms.
Task 144: correctly said that (myanmar, 1, ay0bjcgydhix) and (myanmar, 1, a1umhcrj9zfdp) are the same in 1272 ms.
Task 145: correctly said that (korean, 29, a2ernxe6jlm4x) and (korean, 29, a2nm1qzuh14ls) are the same in 710 ms.
Task 146: correctly said that (atemayar, 6, a31e6jchki335) and (atemayar, 6, a1yovlluv1c9h) are the same in 708 ms.
Task 147: correctly said that (atemayar, 25, a1poymbentexx) and (sylheti, 17, a1foggtlgo7n4) are not the same in 1852 ms.
Task 148: correctly said that (sylheti, 20, a30xq7555spo8) and (atemayar, 10, aljsxt6v0a30) are not the same in 732 ms.
Task 149: correctly said that (tagalog, 6, ay0bjcgydhix) and (tagalog, 6, ajf66jwen7bx) are the same in 1247 ms.
Task 150: incorrectly said that (sylheti, 17, a1j8s7giyto4a) and (sylheti, 28, avlbkm6qpb1s) are the same in 1541 ms.
Task 151: correctly said that (atemayar, 23, awl8tng8e81d) and (atemayar, 23, a31xqftzcsia2) are the same in 777 ms.
Task 152: correctly said that (myanmar, 1, ay0bjcgydhix) and (atemayar, 25, a1poymbentexx) are not the same in 1400 ms.
Task 153: correctly said that (sylheti, 28, a3pkun3onkucn) and (sylheti, 28, amynpl5xzw6q) are the same in 1134 ms.
Task 154: correctly said that (korean, 39, aav4pzxdoebd) and (korean, 39, a2akud1gdstsd) are the same in 747 ms.
Task 155: correctly said that (korean, 4, aj6upe9mlarg) and (sylheti, 19, aljsxt6v0a30) are not the same in 2929 ms.
Task 156: correctly said that (sylheti, 20, a3r9q9wqm1nyz) and (tagalog, 2, ae0ibwt7asth) are not the same in 670 ms.
Task 157: incorrectly said that (korean, 4, a3bb99mn3zy37) and (atemayar, 23, a1poymbentexx) are the same in 2148 ms.
Task 158: correctly said that (sylheti, 17, avlbkm6qpb1s) and (atemayar, 23, a1yovlluv1c9h) are not the same in 1781 ms.
Task 159: correctly said that (tagalog, 10, ay0bjcgydhix) and (tagalog, 10, amynpl5xzw6q) are the same in 1158 ms.
Task 160: correctly said that (korean, 29, a5379luit3pc) and (atemayar, 6, aljsxt6v0a30) are not the same in 4015 ms.
Task 161: incorrectly said that (atemayar, 6, a31e6jchki335) and (atemayar, 23, avlbkm6qpb1s) are the same in 1133 ms.
Task 162: correctly said that (tagalog, 10, ay0bjcgydhix) and (inuktitut, 2, ae0ibwt7asth) are not the same in 1130 ms.
Task 163: correctly said that (tagalog, 6, a1l2fzyn31zvf) and (tagalog, 6, ae0ibwt7asth) are the same in 1404 ms.
Task 164: correctly said that (inuktitut, 1, a12rfyr8dbn79) and (sylheti, 19, a3np21apfa18b) are not the same in 1260 ms.
Task 165: incorrectly said that (atemayar, 6, a1j8s7giyto4a) and (tagalog, 5, a1l2fzyn31zvf) are the same in 1428 ms.
Task 166: correctly said that (korean, 28, a2ernxe6jlm4x) and (korean, 28, a1oh82lwr5hbg) are the same in 569 ms.
Task 167: incorrectly said that (myanmar, 31, amynpl5xzw6q) and (atemayar, 25, a1qku9mccvl35) are the same in 843 ms.
Task 168: correctly said that (atemayar, 6, a1vzhyp8yvxkz) and (tagalog, 10, a1j8s7giyto4a) are not the same in 1668 ms.
Task 169: correctly said that (atemayar, 25, a1yovlluv1c9h) and (atemayar, 25, a31xqftzcsia2) are the same in 1854 ms.
Task 170: incorrectly said that (sylheti, 19, amynpl5xzw6q) and (sylheti, 17, a1a25pyfoa9ge) are the same in 2848 ms.
Task 171: correctly said that (atemayar, 25, a1j8s7giyto4a) and (korean, 28, a5379luit3pc) are not the same in 1197 ms.
Task 172: correctly said that (tagalog, 5, a3m0kt6kjezd1) and (sylheti, 20, a3pkun3onkucn) are not the same in 944 ms.
Task 173: correctly said that (sylheti, 19, a1qku9mccvl35) and (sylheti, 19, a3209gno9duil) are the same in 1286 ms.
Task 174: correctly said that (inuktitut, 3, a12rfyr8dbn79) and (atemayar, 23, a1vzhyp8yvxkz) are not the same in 1177 ms.
Task 175: correctly said that (inuktitut, 1, a1manxzopzden) and (atemayar, 23, ae0ibwt7asth) are not the same in 659 ms.
Task 176: correctly said that (korean, 4, a1oh82lwr5hbg) and (sylheti, 19, a3pkun3onkucn) are not the same in 753 ms.
Task 177: correctly said that (myanmar, 32, a1stvgjqfl5kk) and (korean, 28, a3d7xwp0gand5) are not the same in 756 ms.
Task 178: correctly said that (tagalog, 5, ajf66jwen7bx) and (sylheti, 28, a3np21apfa18b) are not the same in 1334 ms.
Task 179: correctly said that (tagalog, 10, asu813kb7bv1) and (atemayar, 10, a1vzhyp8yvxkz) are not the same in 643 ms.
Task 180: correctly said that (myanmar, 32, a4858n7ggst4) and (inuktitut, 2, a12rfyr8dbn79) are not the same in 1452 ms.
Task 181: correctly said that (tagalog, 2, a2du3880jbrov) and (korean, 39, a1umhcrj9zfdp) are not the same in 954 ms.
Task 182: incorrectly said that (tagalog, 2, aljsxt6v0a30) and (tagalog, 5, a1j8s7giyto4a) are the same in 1984 ms.
Task 183: correctly said that (korean, 4, a1umhcrj9zfdp) and (tagalog, 10, asu813kb7bv1) are not the same in 1677 ms.
Task 184: correctly said that (korean, 4, aav4pzxdoebd) and (korean, 4, a2nm1qzuh14ls) are the same in 1355 ms.
Task 185: correctly said that (tagalog, 6, ajf66jwen7bx) and (atemayar, 25, awl8tng8e81d) are not the same in 1444 ms.
Task 186: incorrectly said that (myanmar, 32, avlbkm6qpb1s) and (tagalog, 2, a1ehrxjplsq0r) are the same in 1654 ms.
Task 187: correctly said that (myanmar, 31, a1foggtlgo7n4) and (korean, 39, a1s3njxhehcip) are not the same in 1648 ms.
Task 188: correctly said that (myanmar, 32, a1foggtlgo7n4) and (korean, 4, a1oh82lwr5hbg) are not the same in 875 ms.
Task 189: incorrectly said that (korean, 29, a2nm1qzuh14ls) and (tagalog, 5, asu813kb7bv1) are the same in 1062 ms.
Task 190: correctly said that (sylheti, 20, a6fjbvuzrhm6) and (inuktitut, 2, a1s3njxhehcip) are not the same in 2198 ms.
Task 191: correctly said that (inuktitut, 2, aljsxt6v0a30) and (atemayar, 10, azdw6062ia38) are not the same in 930 ms.
Task 192: correctly said that (myanmar, 32, a1umhcrj9zfdp) and (myanmar, 32, a1clejhbx9yyn) are the same in 887 ms.
Task 193: correctly said that (atemayar, 6, a2pfktghg1ofp) and (inuktitut, 2, a1z59eb5sfxpy) are not the same in 1334 ms.
Task 194: correctly said that (inuktitut, 11, aljsxt6v0a30) and (inuktitut, 11, a12rfyr8dbn79) are the same in 1218 ms.
Task 195: correctly said that (inuktitut, 11, a1z59eb5sfxpy) and (atemayar, 23, ax5gghhj0o6g) are not the same in 1111 ms.
Task 196: correctly said that (inuktitut, 1, ae0ibwt7asth) and (tagalog, 6, ay0bjcgydhix) are not the same in 693 ms.
Task 197: correctly said that (sylheti, 20, amynpl5xzw6q) and (inuktitut, 3, a1qku9mccvl35) are not the same in 671 ms.
Task 198: correctly said that (tagalog, 5, a1i92aacrkacw) and (tagalog, 6, a1yovlluv1c9h) are not the same in 756 ms.
Task 199: correctly said that (inuktitut, 3, a3qat9qkqumi2) and (sylheti, 17, a6fjbvuzrhm6) are not the same in 685 ms.

Duration: 19m 15s 529ms
Comments: I wish the character was shown a bit longer each time.