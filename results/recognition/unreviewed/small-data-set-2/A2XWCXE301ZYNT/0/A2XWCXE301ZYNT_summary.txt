

Summary:Short Summary:
Correct: 133
Incorrect: 67
Percent Correct: 66

Same Correct: 11
Same Incorrect: 47
Percent Same Correct: 18

Different Correct: 122
Different Incorrect: 20
Percent Different Correct: 85

Long Summary:
Task 0: correctly said that (sylheti, 20, a2du3880jbrov) and (atemayar, 6, ae0ibwt7asth) are not the same in 10971 ms.
Task 1: correctly said that (tagalog, 10, asu813kb7bv1) and (sylheti, 19, a3r9q9wqm1nyz) are not the same in 6441 ms.
Task 2: incorrectly said that (sylheti, 17, a4858n7ggst4) and (sylheti, 17, a30xq7555spo8) are not the same in 3108 ms.
Task 3: incorrectly said that (tagalog, 6, aljsxt6v0a30) and (tagalog, 6, a1j8s7giyto4a) are not the same in 1443 ms.
Task 4: correctly said that (tagalog, 10, a1yovlluv1c9h) and (inuktitut, 1, a3qat9qkqumi2) are not the same in 2604 ms.
Task 5: correctly said that (myanmar, 25, amynpl5xzw6q) and (sylheti, 20, a4858n7ggst4) are not the same in 7457 ms.
Task 6: correctly said that (tagalog, 5, a3qat9qkqumi2) and (myanmar, 31, a6fjbvuzrhm6) are not the same in 3375 ms.
Task 7: correctly said that (inuktitut, 2, a1s3njxhehcip) and (myanmar, 1, a1foggtlgo7n4) are not the same in 2007 ms.
Task 8: incorrectly said that (myanmar, 32, a4858n7ggst4) and (myanmar, 32, a1stvgjqfl5kk) are not the same in 2655 ms.
Task 9: correctly said that (sylheti, 20, a1l2fzyn31zvf) and (inuktitut, 2, a3qat9qkqumi2) are not the same in 1304 ms.
Task 10: correctly said that (myanmar, 25, a1s3njxhehcip) and (myanmar, 31, aljsxt6v0a30) are not the same in 1448 ms.
Task 11: correctly said that (myanmar, 1, ay0bjcgydhix) and (tagalog, 6, ajf66jwen7bx) are not the same in 3863 ms.
Task 12: correctly said that (atemayar, 10, avlbkm6qpb1s) and (inuktitut, 11, a1j8s7giyto4a) are not the same in 3908 ms.
Task 13: incorrectly said that (inuktitut, 2, ae0ibwt7asth) and (sylheti, 17, aljsxt6v0a30) are the same in 2547 ms.
Task 14: correctly said that (atemayar, 23, awl8tng8e81d) and (korean, 4, a3mv1jtwep7xn) are not the same in 3898 ms.
Task 15: correctly said that (korean, 39, a1umhcrj9zfdp) and (atemayar, 25, a6fjbvuzrhm6) are not the same in 4830 ms.
Task 16: incorrectly said that (myanmar, 25, a3qat9qkqumi2) and (myanmar, 25, a1umhcrj9zfdp) are not the same in 3491 ms.
Task 17: correctly said that (tagalog, 6, a2du3880jbrov) and (inuktitut, 1, a1qku9mccvl35) are not the same in 1513 ms.
Task 18: incorrectly said that (myanmar, 1, a1s3njxhehcip) and (myanmar, 32, a1umhcrj9zfdp) are the same in 4306 ms.
Task 19: correctly said that (myanmar, 32, a3pkun3onkucn) and (myanmar, 25, a1aegunf6e081) are not the same in 4374 ms.
Task 20: incorrectly said that (korean, 29, a39ivbdm1ekue) and (korean, 29, a1umhcrj9zfdp) are not the same in 3576 ms.
Task 21: correctly said that (atemayar, 6, a23psx2z4f65q) and (inuktitut, 2, a2pgu4pi93b1q) are not the same in 12685 ms.
Task 22: incorrectly said that (inuktitut, 11, a3mv1jtwep7xn) and (sylheti, 17, a2du3880jbrov) are the same in 5443 ms.
Task 23: correctly said that (atemayar, 10, a2pfktghg1ofp) and (tagalog, 5, a3m0kt6kjezd1) are not the same in 2375 ms.
Task 24: correctly said that (sylheti, 17, a1l2fzyn31zvf) and (inuktitut, 2, a3qat9qkqumi2) are not the same in 1330 ms.
Task 25: correctly said that (atemayar, 25, a31xqftzcsia2) and (korean, 4, a3d7xwp0gand5) are not the same in 1635 ms.
Task 26: correctly said that (sylheti, 28, aljsxt6v0a30) and (atemayar, 25, a2pfktghg1ofp) are not the same in 1612 ms.
Task 27: correctly said that (atemayar, 25, agst3co6usf8) and (inuktitut, 3, a1manxzopzden) are not the same in 1509 ms.
Task 28: correctly said that (sylheti, 19, a3pkun3onkucn) and (tagalog, 2, aljsxt6v0a30) are not the same in 28027 ms.
Task 29: correctly said that (myanmar, 31, a1foggtlgo7n4) and (sylheti, 17, a2du3880jbrov) are not the same in 2834 ms.
Task 30: correctly said that (sylheti, 17, a30xq7555spo8) and (sylheti, 17, avlbkm6qpb1s) are the same in 4895 ms.
Task 31: incorrectly said that (korean, 29, a2ernxe6jlm4x) and (korean, 29, a1umhcrj9zfdp) are not the same in 5888 ms.
Task 32: incorrectly said that (atemayar, 23, a1qku9mccvl35) and (atemayar, 23, a1vzhyp8yvxkz) are not the same in 1066 ms.
Task 33: correctly said that (inuktitut, 1, a1w3fo0d7cf5t) and (atemayar, 6, a31e6jchki335) are not the same in 4105 ms.
Task 34: incorrectly said that (atemayar, 6, a1yovlluv1c9h) and (sylheti, 19, a3np21apfa18b) are the same in 3371 ms.
Task 35: incorrectly said that (inuktitut, 2, a1s3njxhehcip) and (inuktitut, 2, a1z59eb5sfxpy) are not the same in 2010 ms.
Task 36: incorrectly said that (myanmar, 31, ay0bjcgydhix) and (myanmar, 31, a1s3njxhehcip) are not the same in 2225 ms.
Task 37: incorrectly said that (atemayar, 25, a1manxzopzden) and (atemayar, 6, a1vzhyp8yvxkz) are the same in 2757 ms.
Task 38: incorrectly said that (atemayar, 23, azdw6062ia38) and (atemayar, 23, a31xqftzcsia2) are not the same in 1286 ms.
Task 39: correctly said that (tagalog, 10, a1yovlluv1c9h) and (atemayar, 6, a23psx2z4f65q) are not the same in 688 ms.
Task 40: correctly said that (myanmar, 1, ay0bjcgydhix) and (sylheti, 19, a3209gno9duil) are not the same in 4844 ms.
Task 41: incorrectly said that (atemayar, 10, a31xqftzcsia2) and (tagalog, 2, aljsxt6v0a30) are the same in 1515 ms.
Task 42: incorrectly said that (korean, 4, a5379luit3pc) and (korean, 4, a1oh82lwr5hbg) are not the same in 1638 ms.
Task 43: correctly said that (atemayar, 10, a31xqftzcsia2) and (sylheti, 17, a1qku9mccvl35) are not the same in 3497 ms.
Task 44: correctly said that (sylheti, 17, a3209gno9duil) and (sylheti, 17, ay0bjcgydhix) are the same in 1815 ms.
Task 45: correctly said that (korean, 39, a3bb99mn3zy37) and (atemayar, 25, a2pfktghg1ofp) are not the same in 1187 ms.
Task 46: correctly said that (korean, 39, a1s3njxhehcip) and (myanmar, 32, a1foggtlgo7n4) are not the same in 637 ms.
Task 47: incorrectly said that (tagalog, 6, a3qat9qkqumi2) and (tagalog, 6, a12rfyr8dbn79) are not the same in 2121 ms.
Task 48: incorrectly said that (tagalog, 5, a1a25pyfoa9ge) and (tagalog, 5, a3mv1jtwep7xn) are not the same in 1061 ms.
Task 49: correctly said that (tagalog, 6, ay0bjcgydhix) and (inuktitut, 2, a1w3fo0d7cf5t) are not the same in 1324 ms.
Task 50: correctly said that (tagalog, 5, a1i92aacrkacw) and (inuktitut, 2, ae0ibwt7asth) are not the same in 1132 ms.
Task 51: incorrectly said that (korean, 4, a3d7xwp0gand5) and (korean, 4, a2nm1qzuh14ls) are not the same in 964 ms.
Task 52: correctly said that (tagalog, 6, a1ehrxjplsq0r) and (myanmar, 31, a1umhcrj9zfdp) are not the same in 557 ms.
Task 53: incorrectly said that (korean, 28, a1udqv4pfypkn) and (korean, 28, a1oh82lwr5hbg) are not the same in 2285 ms.
Task 54: incorrectly said that (korean, 28, a1s3njxhehcip) and (korean, 39, a39ivbdm1ekue) are the same in 2667 ms.
Task 55: correctly said that (tagalog, 5, asu813kb7bv1) and (sylheti, 20, a4858n7ggst4) are not the same in 9039 ms.
Task 56: correctly said that (tagalog, 10, a12rfyr8dbn79) and (tagalog, 5, aljsxt6v0a30) are not the same in 3770 ms.
Task 57: incorrectly said that (atemayar, 6, a6fjbvuzrhm6) and (atemayar, 6, awl8tng8e81d) are not the same in 3875 ms.
Task 58: incorrectly said that (inuktitut, 11, avlbkm6qpb1s) and (sylheti, 20, a3209gno9duil) are the same in 2913 ms.
Task 59: correctly said that (korean, 29, a5379luit3pc) and (sylheti, 28, a1j8s7giyto4a) are not the same in 2383 ms.
Task 60: incorrectly said that (sylheti, 20, a1j8s7giyto4a) and (sylheti, 20, a1a25pyfoa9ge) are not the same in 1886 ms.
Task 61: correctly said that (inuktitut, 1, aljsxt6v0a30) and (inuktitut, 11, a3noo9k3y1yu5) are not the same in 5494 ms.
Task 62: incorrectly said that (atemayar, 6, a1vzhyp8yvxkz) and (atemayar, 6, avlbkm6qpb1s) are not the same in 7722 ms.
Task 63: incorrectly said that (korean, 28, aw82oruamauz) and (korean, 28, aav4pzxdoebd) are not the same in 14661 ms.
Task 64: correctly said that (korean, 28, a2ernxe6jlm4x) and (sylheti, 20, a3m0kt6kjezd1) are not the same in 1795 ms.
Task 65: correctly said that (tagalog, 6, a1i92aacrkacw) and (atemayar, 23, a1poymbentexx) are not the same in 10776 ms.
Task 66: correctly said that (inuktitut, 3, a1j8s7giyto4a) and (inuktitut, 3, ay0bjcgydhix) are the same in 3247 ms.
Task 67: correctly said that (korean, 28, aav4pzxdoebd) and (myanmar, 25, a1stvgjqfl5kk) are not the same in 2649 ms.
Task 68: correctly said that (myanmar, 32, a1clejhbx9yyn) and (myanmar, 31, a1stvgjqfl5kk) are not the same in 13982 ms.
Task 69: correctly said that (sylheti, 20, a3np21apfa18b) and (inuktitut, 1, a2pfktghg1ofp) are not the same in 1959 ms.
Task 70: correctly said that (atemayar, 10, a1qku9mccvl35) and (myanmar, 32, a2pfktghg1ofp) are not the same in 4186 ms.
Task 71: incorrectly said that (sylheti, 19, a3r9q9wqm1nyz) and (sylheti, 19, aljsxt6v0a30) are not the same in 3602 ms.
Task 72: correctly said that (inuktitut, 2, a1manxzopzden) and (inuktitut, 2, a1z59eb5sfxpy) are the same in 8887 ms.
Task 73: incorrectly said that (sylheti, 17, a6fjbvuzrhm6) and (tagalog, 2, aljsxt6v0a30) are the same in 5800 ms.
Task 74: incorrectly said that (sylheti, 28, a1a25pyfoa9ge) and (sylheti, 28, a3r9q9wqm1nyz) are not the same in 1078 ms.
Task 75: incorrectly said that (atemayar, 10, avlbkm6qpb1s) and (atemayar, 10, awl8tng8e81d) are not the same in 1127 ms.
Task 76: correctly said that (tagalog, 6, a1s3njxhehcip) and (korean, 4, a2px0aywxngce) are not the same in 1029 ms.
Task 77: correctly said that (sylheti, 20, a3np21apfa18b) and (inuktitut, 1, a315xddd8j4x8) are not the same in 794 ms.
Task 78: correctly said that (myanmar, 32, amynpl5xzw6q) and (sylheti, 28, a3209gno9duil) are not the same in 894 ms.
Task 79: correctly said that (inuktitut, 3, a1w3fo0d7cf5t) and (sylheti, 20, amynpl5xzw6q) are not the same in 8961 ms.
Task 80: correctly said that (atemayar, 25, ae0ibwt7asth) and (sylheti, 19, avlbkm6qpb1s) are not the same in 4209 ms.
Task 81: correctly said that (korean, 39, a2akud1gdstsd) and (atemayar, 23, awl8tng8e81d) are not the same in 4517 ms.
Task 82: correctly said that (atemayar, 25, azdw6062ia38) and (sylheti, 28, a1j8s7giyto4a) are not the same in 18302 ms.
Task 83: correctly said that (korean, 29, a2nm1qzuh14ls) and (sylheti, 28, avlbkm6qpb1s) are not the same in 2428 ms.
Task 84: correctly said that (korean, 4, a3d7xwp0gand5) and (atemayar, 23, aljsxt6v0a30) are not the same in 5902 ms.
Task 85: incorrectly said that (korean, 29, a2nm1qzuh14ls) and (korean, 29, a3mv1jtwep7xn) are not the same in 2625 ms.
Task 86: correctly said that (atemayar, 10, avlbkm6qpb1s) and (inuktitut, 1, aljsxt6v0a30) are not the same in 4482 ms.
Task 87: correctly said that (myanmar, 31, avlbkm6qpb1s) and (myanmar, 1, a1umhcrj9zfdp) are not the same in 3992 ms.
Task 88: correctly said that (myanmar, 31, a1umhcrj9zfdp) and (inuktitut, 1, a12rfyr8dbn79) are not the same in 4990 ms.
Task 89: correctly said that (myanmar, 1, a1aegunf6e081) and (myanmar, 1, ajf66jwen7bx) are the same in 2876 ms.
Task 90: incorrectly said that (korean, 39, a1manxzopzden) and (korean, 39, a2nm1qzuh14ls) are not the same in 2803 ms.
Task 91: incorrectly said that (sylheti, 17, atvyxlwe5n03) and (tagalog, 6, a1i92aacrkacw) are the same in 4805 ms.
Task 92: correctly said that (inuktitut, 3, a1qku9mccvl35) and (myanmar, 1, a6fjbvuzrhm6) are not the same in 4874 ms.
Task 93: correctly said that (atemayar, 25, a3m0kt6kjezd1) and (korean, 4, a1s3njxhehcip) are not the same in 2128 ms.
Task 94: incorrectly said that (tagalog, 5, a1yovlluv1c9h) and (tagalog, 5, a3m0kt6kjezd1) are not the same in 2142 ms.
Task 95: correctly said that (tagalog, 6, a1i92aacrkacw) and (korean, 4, a3mv1jtwep7xn) are not the same in 5077 ms.
Task 96: correctly said that (myanmar, 31, avlbkm6qpb1s) and (myanmar, 31, amynpl5xzw6q) are the same in 7440 ms.
Task 97: correctly said that (sylheti, 19, amynpl5xzw6q) and (inuktitut, 3, a12rfyr8dbn79) are not the same in 2771 ms.
Task 98: correctly said that (myanmar, 32, ajf66jwen7bx) and (sylheti, 28, atvyxlwe5n03) are not the same in 2395 ms.
Task 99: correctly said that (sylheti, 28, a2du3880jbrov) and (inuktitut, 3, a1qku9mccvl35) are not the same in 4299 ms.
Task 100: correctly said that (sylheti, 28, atvyxlwe5n03) and (atemayar, 23, a1poymbentexx) are not the same in 1705 ms.
Task 101: incorrectly said that (korean, 28, a1v0p79fynz9j) and (korean, 28, a5379luit3pc) are not the same in 1252 ms.
Task 102: correctly said that (tagalog, 10, a2du3880jbrov) and (sylheti, 19, ay0bjcgydhix) are not the same in 790 ms.
Task 103: correctly said that (atemayar, 6, a1yovlluv1c9h) and (korean, 28, aj6upe9mlarg) are not the same in 1279 ms.
Task 104: incorrectly said that (tagalog, 10, ay0bjcgydhix) and (tagalog, 6, a12rfyr8dbn79) are the same in 1993 ms.
Task 105: correctly said that (inuktitut, 1, a3qat9qkqumi2) and (korean, 4, aav4pzxdoebd) are not the same in 1056 ms.
Task 106: incorrectly said that (tagalog, 6, a1a25pyfoa9ge) and (myanmar, 31, a6fjbvuzrhm6) are the same in 1856 ms.
Task 107: correctly said that (inuktitut, 2, a3noo9k3y1yu5) and (sylheti, 17, a2du3880jbrov) are not the same in 1494 ms.
Task 108: incorrectly said that (myanmar, 32, a3j1pp96x14u0) and (korean, 39, a3bb99mn3zy37) are the same in 4290 ms.
Task 109: correctly said that (atemayar, 10, a31e6jchki335) and (atemayar, 25, azdw6062ia38) are not the same in 1207 ms.
Task 110: incorrectly said that (tagalog, 6, a3m0kt6kjezd1) and (tagalog, 6, amynpl5xzw6q) are not the same in 1900 ms.
Task 111: incorrectly said that (myanmar, 1, a1ehrxjplsq0r) and (tagalog, 10, a1ehrxjplsq0r) are the same in 1260 ms.
Task 112: correctly said that (myanmar, 31, amynpl5xzw6q) and (atemayar, 6, avlbkm6qpb1s) are not the same in 1706 ms.
Task 113: correctly said that (korean, 28, aw82oruamauz) and (inuktitut, 3, a1qku9mccvl35) are not the same in 2121 ms.
Task 114: correctly said that (korean, 4, a3d7xwp0gand5) and (atemayar, 23, awl8tng8e81d) are not the same in 2352 ms.
Task 115: incorrectly said that (myanmar, 31, a1s3njxhehcip) and (myanmar, 31, a1umhcrj9zfdp) are not the same in 8986 ms.
Task 116: correctly said that (korean, 4, a2akud1gdstsd) and (myanmar, 1, a1stvgjqfl5kk) are not the same in 2269 ms.
Task 117: correctly said that (tagalog, 10, a1s3njxhehcip) and (tagalog, 10, a1yovlluv1c9h) are the same in 1411 ms.
Task 118: correctly said that (tagalog, 10, ay0bjcgydhix) and (korean, 39, a2ernxe6jlm4x) are not the same in 1234 ms.
Task 119: correctly said that (myanmar, 25, amynpl5xzw6q) and (inuktitut, 11, ae0ibwt7asth) are not the same in 1870 ms.
Task 120: incorrectly said that (sylheti, 28, a1s3njxhehcip) and (inuktitut, 3, a1j8s7giyto4a) are the same in 3226 ms.
Task 121: correctly said that (tagalog, 10, amynpl5xzw6q) and (inuktitut, 11, ae0ibwt7asth) are not the same in 2929 ms.
Task 122: correctly said that (inuktitut, 2, a315xddd8j4x8) and (tagalog, 10, a1yovlluv1c9h) are not the same in 1402 ms.
Task 123: correctly said that (atemayar, 6, ae0ibwt7asth) and (sylheti, 19, aljsxt6v0a30) are not the same in 1310 ms.
Task 124: incorrectly said that (atemayar, 25, a3m0kt6kjezd1) and (atemayar, 25, a2pfktghg1ofp) are not the same in 1081 ms.
Task 125: incorrectly said that (myanmar, 25, aljsxt6v0a30) and (myanmar, 25, a1stvgjqfl5kk) are not the same in 2036 ms.
Task 126: incorrectly said that (inuktitut, 1, a12xm86d2nbih) and (tagalog, 10, ay0bjcgydhix) are the same in 1517 ms.
Task 127: correctly said that (tagalog, 5, ae0ibwt7asth) and (inuktitut, 11, avlbkm6qpb1s) are not the same in 7093 ms.
Task 128: incorrectly said that (myanmar, 25, amynpl5xzw6q) and (inuktitut, 11, avlbkm6qpb1s) are the same in 5444 ms.
Task 129: correctly said that (korean, 4, a1udqv4pfypkn) and (korean, 4, a2ernxe6jlm4x) are the same in 2527 ms.
Task 130: correctly said that (korean, 39, a2ernxe6jlm4x) and (atemayar, 6, ae0ibwt7asth) are not the same in 1098 ms.
Task 131: correctly said that (myanmar, 32, amynpl5xzw6q) and (korean, 28, aw82oruamauz) are not the same in 1163 ms.
Task 132: correctly said that (myanmar, 31, a3qat9qkqumi2) and (sylheti, 19, a3r9q9wqm1nyz) are not the same in 1348 ms.
Task 133: correctly said that (atemayar, 23, ae0ibwt7asth) and (atemayar, 10, a23psx2z4f65q) are not the same in 1589 ms.
Task 134: correctly said that (atemayar, 6, ae0ibwt7asth) and (tagalog, 2, a3mv1jtwep7xn) are not the same in 20681 ms.
Task 135: correctly said that (inuktitut, 11, a315xddd8j4x8) and (myanmar, 25, a1s3njxhehcip) are not the same in 1800 ms.
Task 136: incorrectly said that (sylheti, 19, avlbkm6qpb1s) and (sylheti, 19, atvyxlwe5n03) are not the same in 1100 ms.
Task 137: correctly said that (sylheti, 20, avlbkm6qpb1s) and (sylheti, 19, a2du3880jbrov) are not the same in 1243 ms.
Task 138: correctly said that (myanmar, 32, amynpl5xzw6q) and (sylheti, 19, avlbkm6qpb1s) are not the same in 1095 ms.
Task 139: correctly said that (korean, 28, a1s3njxhehcip) and (tagalog, 6, avlbkm6qpb1s) are not the same in 2114 ms.
Task 140: incorrectly said that (sylheti, 19, a1s3njxhehcip) and (myanmar, 25, ay0bjcgydhix) are the same in 1811 ms.
Task 141: incorrectly said that (tagalog, 5, ay0bjcgydhix) and (tagalog, 5, a2du3880jbrov) are not the same in 2380 ms.
Task 142: incorrectly said that (tagalog, 10, a1i92aacrkacw) and (tagalog, 10, a3m0kt6kjezd1) are not the same in 9364 ms.
Task 143: correctly said that (inuktitut, 11, a2pfktghg1ofp) and (sylheti, 20, a2du3880jbrov) are not the same in 8098 ms.
Task 144: correctly said that (atemayar, 23, ae0ibwt7asth) and (myanmar, 31, a2pfktghg1ofp) are not the same in 76146 ms.
Task 145: correctly said that (korean, 4, a2ernxe6jlm4x) and (tagalog, 5, a1i92aacrkacw) are not the same in 1272 ms.
Task 146: incorrectly said that (sylheti, 28, ay0bjcgydhix) and (sylheti, 28, a30xq7555spo8) are not the same in 1142 ms.
Task 147: correctly said that (korean, 39, a1v0p79fynz9j) and (myanmar, 32, a3pkun3onkucn) are not the same in 2558 ms.
Task 148: incorrectly said that (korean, 29, a3mv1jtwep7xn) and (korean, 29, a2px0aywxngce) are not the same in 48190 ms.
Task 149: correctly said that (tagalog, 2, ae0ibwt7asth) and (myanmar, 31, avlbkm6qpb1s) are not the same in 2369 ms.
Task 150: correctly said that (tagalog, 6, asu813kb7bv1) and (atemayar, 25, a1yovlluv1c9h) are not the same in 1714 ms.
Task 151: correctly said that (sylheti, 19, a3209gno9duil) and (sylheti, 28, avlbkm6qpb1s) are not the same in 38263 ms.
Task 152: incorrectly said that (korean, 4, a2px0aywxngce) and (tagalog, 2, a1ehrxjplsq0r) are the same in 1664 ms.
Task 153: correctly said that (tagalog, 10, a3m0kt6kjezd1) and (sylheti, 17, atvyxlwe5n03) are not the same in 5834 ms.
Task 154: incorrectly said that (korean, 28, a1s3njxhehcip) and (korean, 28, aw82oruamauz) are not the same in 1225 ms.
Task 155: correctly said that (inuktitut, 3, a2pgu4pi93b1q) and (inuktitut, 3, a2pfktghg1ofp) are the same in 3956 ms.
Task 156: correctly said that (sylheti, 19, amynpl5xzw6q) and (atemayar, 23, a3m0kt6kjezd1) are not the same in 1126 ms.
Task 157: incorrectly said that (tagalog, 6, ay0bjcgydhix) and (tagalog, 6, a1j8s7giyto4a) are not the same in 1390 ms.
Task 158: correctly said that (inuktitut, 11, a1z59eb5sfxpy) and (korean, 29, a2px0aywxngce) are not the same in 2522 ms.
Task 159: correctly said that (myanmar, 32, a4858n7ggst4) and (sylheti, 20, a2du3880jbrov) are not the same in 16393 ms.
Task 160: correctly said that (atemayar, 6, a6fjbvuzrhm6) and (myanmar, 32, a3pkun3onkucn) are not the same in 5767 ms.
Task 161: correctly said that (myanmar, 32, amynpl5xzw6q) and (sylheti, 28, amynpl5xzw6q) are not the same in 1476 ms.
Task 162: incorrectly said that (myanmar, 32, a3j1pp96x14u0) and (myanmar, 32, a1ehrxjplsq0r) are not the same in 1237 ms.
Task 163: correctly said that (inuktitut, 2, avlbkm6qpb1s) and (tagalog, 6, ae0ibwt7asth) are not the same in 1979 ms.
Task 164: correctly said that (sylheti, 20, a1j8s7giyto4a) and (inuktitut, 11, a1s3njxhehcip) are not the same in 2104 ms.
Task 165: correctly said that (atemayar, 6, a1qku9mccvl35) and (atemayar, 23, a2pfktghg1ofp) are not the same in 1544 ms.
Task 166: correctly said that (tagalog, 5, asu813kb7bv1) and (myanmar, 1, a1clejhbx9yyn) are not the same in 1514 ms.
Task 167: correctly said that (myanmar, 25, ay0bjcgydhix) and (sylheti, 20, avlbkm6qpb1s) are not the same in 1917 ms.
Task 168: correctly said that (myanmar, 1, a3j1pp96x14u0) and (korean, 39, aw82oruamauz) are not the same in 1643 ms.
Task 169: correctly said that (atemayar, 10, a1yovlluv1c9h) and (korean, 39, aj6upe9mlarg) are not the same in 15573 ms.
Task 170: correctly said that (atemayar, 25, a1poymbentexx) and (atemayar, 25, a6fjbvuzrhm6) are the same in 4286 ms.
Task 171: correctly said that (sylheti, 20, a3209gno9duil) and (tagalog, 6, aghrkle9vf6z) are not the same in 1193 ms.
Task 172: incorrectly said that (inuktitut, 3, a2pgu4pi93b1q) and (inuktitut, 3, a12xm86d2nbih) are not the same in 5863 ms.
Task 173: correctly said that (inuktitut, 2, a2pfktghg1ofp) and (atemayar, 6, a1poymbentexx) are not the same in 1235 ms.
Task 174: correctly said that (myanmar, 32, a3j1pp96x14u0) and (inuktitut, 1, a3mv1jtwep7xn) are not the same in 1526 ms.
Task 175: incorrectly said that (inuktitut, 11, a3noo9k3y1yu5) and (inuktitut, 11, a1s3njxhehcip) are not the same in 1133 ms.
Task 176: correctly said that (myanmar, 32, amynpl5xzw6q) and (atemayar, 6, avlbkm6qpb1s) are not the same in 3125 ms.
Task 177: incorrectly said that (tagalog, 6, a12rfyr8dbn79) and (tagalog, 6, a1yovlluv1c9h) are not the same in 13358 ms.
Task 178: incorrectly said that (atemayar, 25, awl8tng8e81d) and (atemayar, 25, a1yovlluv1c9h) are not the same in 1664 ms.
Task 179: correctly said that (inuktitut, 1, a12xm86d2nbih) and (atemayar, 10, a1poymbentexx) are not the same in 1626 ms.
Task 180: incorrectly said that (myanmar, 31, a3pkun3onkucn) and (myanmar, 31, a1stvgjqfl5kk) are not the same in 2149 ms.
Task 181: correctly said that (tagalog, 6, a12rfyr8dbn79) and (korean, 28, a3dfh6dpi11ip) are not the same in 1513 ms.
Task 182: correctly said that (korean, 4, a2px0aywxngce) and (inuktitut, 3, a3qat9qkqumi2) are not the same in 1543 ms.
Task 183: correctly said that (sylheti, 20, a30xq7555spo8) and (myanmar, 32, a1s3njxhehcip) are not the same in 1958 ms.
Task 184: correctly said that (myanmar, 25, a1clejhbx9yyn) and (sylheti, 28, a1s3njxhehcip) are not the same in 2342 ms.
Task 185: correctly said that (inuktitut, 1, a2pfktghg1ofp) and (atemayar, 6, a1vzhyp8yvxkz) are not the same in 1422 ms.
Task 186: incorrectly said that (sylheti, 19, a1foggtlgo7n4) and (sylheti, 19, a3m0kt6kjezd1) are not the same in 856 ms.
Task 187: correctly said that (sylheti, 28, a1qku9mccvl35) and (atemayar, 23, a1vzhyp8yvxkz) are not the same in 1089 ms.
Task 188: incorrectly said that (korean, 28, a2ernxe6jlm4x) and (myanmar, 32, a1foggtlgo7n4) are the same in 1327 ms.
Task 189: incorrectly said that (myanmar, 25, ajf66jwen7bx) and (myanmar, 25, a1s3njxhehcip) are not the same in 2933 ms.
Task 190: incorrectly said that (korean, 29, a2ernxe6jlm4x) and (korean, 29, a5379luit3pc) are not the same in 2950 ms.
Task 191: correctly said that (tagalog, 2, a1ehrxjplsq0r) and (tagalog, 6, a1l2fzyn31zvf) are not the same in 2997 ms.
Task 192: correctly said that (myanmar, 25, a1s3njxhehcip) and (tagalog, 5, a1s3njxhehcip) are not the same in 1620 ms.
Task 193: correctly said that (korean, 29, aav4pzxdoebd) and (korean, 4, a2ernxe6jlm4x) are not the same in 1428 ms.
Task 194: incorrectly said that (tagalog, 10, a1j8s7giyto4a) and (tagalog, 10, avlbkm6qpb1s) are not the same in 1574 ms.
Task 195: correctly said that (myanmar, 31, a1foggtlgo7n4) and (inuktitut, 11, a3qat9qkqumi2) are not the same in 3709 ms.
Task 196: correctly said that (atemayar, 23, aljsxt6v0a30) and (korean, 28, a1s3njxhehcip) are not the same in 1186 ms.
Task 197: correctly said that (atemayar, 10, a31e6jchki335) and (atemayar, 10, a1j8s7giyto4a) are the same in 6218 ms.
Task 198: correctly said that (sylheti, 20, a3pkun3onkucn) and (sylheti, 19, a1a25pyfoa9ge) are not the same in 2440 ms.
Task 199: correctly said that (myanmar, 1, adc8vbkoocrr) and (inuktitut, 11, a3mv1jtwep7xn) are not the same in 2952 ms.

Duration: 34m 22s 839ms
Comments: 