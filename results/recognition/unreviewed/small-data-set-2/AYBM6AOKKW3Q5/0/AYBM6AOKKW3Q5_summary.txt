

Summary:Short Summary:
Correct: 165
Incorrect: 35
Percent Correct: 82

Same Correct: 49
Same Incorrect: 10
Percent Same Correct: 83

Different Correct: 116
Different Incorrect: 25
Percent Different Correct: 82

Long Summary:
Task 0: incorrectly said that (myanmar, 32, ajf66jwen7bx) and (myanmar, 32, ay0bjcgydhix) are not the same in 2085 ms.
Task 1: correctly said that (atemayar, 10, awl8tng8e81d) and (atemayar, 6, a1manxzopzden) are not the same in 1352 ms.
Task 2: incorrectly said that (atemayar, 25, ae0ibwt7asth) and (atemayar, 25, a1yovlluv1c9h) are not the same in 1517 ms.
Task 3: correctly said that (tagalog, 6, a1yovlluv1c9h) and (sylheti, 17, a6fjbvuzrhm6) are not the same in 1869 ms.
Task 4: incorrectly said that (korean, 28, ae0ibwt7asth) and (korean, 28, a1s3njxhehcip) are not the same in 1613 ms.
Task 5: incorrectly said that (sylheti, 28, avlbkm6qpb1s) and (sylheti, 28, a3pkun3onkucn) are not the same in 1039 ms.
Task 6: correctly said that (inuktitut, 2, a315xddd8j4x8) and (korean, 28, a1s3njxhehcip) are not the same in 876 ms.
Task 7: correctly said that (korean, 39, a2px0aywxngce) and (myanmar, 31, a1ehrxjplsq0r) are not the same in 1245 ms.
Task 8: correctly said that (sylheti, 17, a3r9q9wqm1nyz) and (myanmar, 25, a1stvgjqfl5kk) are not the same in 3265 ms.
Task 9: correctly said that (sylheti, 17, atvyxlwe5n03) and (atemayar, 6, a1vzhyp8yvxkz) are not the same in 924 ms.
Task 10: correctly said that (tagalog, 10, a3qat9qkqumi2) and (tagalog, 10, asu813kb7bv1) are the same in 2219 ms.
Task 11: correctly said that (korean, 4, a3d7xwp0gand5) and (atemayar, 10, awl8tng8e81d) are not the same in 1356 ms.
Task 12: correctly said that (atemayar, 10, a3m0kt6kjezd1) and (inuktitut, 3, a2pfktghg1ofp) are not the same in 872 ms.
Task 13: correctly said that (myanmar, 25, a2pfktghg1ofp) and (atemayar, 23, ax5gghhj0o6g) are not the same in 945 ms.
Task 14: incorrectly said that (myanmar, 32, a2pfktghg1ofp) and (myanmar, 32, amynpl5xzw6q) are not the same in 771 ms.
Task 15: incorrectly said that (inuktitut, 1, a3mv1jtwep7xn) and (inuktitut, 1, ay0bjcgydhix) are not the same in 779 ms.
Task 16: correctly said that (sylheti, 20, a1l2fzyn31zvf) and (tagalog, 6, a3qat9qkqumi2) are not the same in 1273 ms.
Task 17: correctly said that (sylheti, 19, a1foggtlgo7n4) and (inuktitut, 1, a2qnqd7m8zfbo) are not the same in 2572 ms.
Task 18: incorrectly said that (inuktitut, 1, ae0ibwt7asth) and (inuktitut, 1, a2qnqd7m8zfbo) are not the same in 1803 ms.
Task 19: correctly said that (atemayar, 6, a1yovlluv1c9h) and (myanmar, 25, a1j8s7giyto4a) are not the same in 1226 ms.
Task 20: incorrectly said that (sylheti, 28, a3r9q9wqm1nyz) and (korean, 28, a1v0p79fynz9j) are the same in 3240 ms.
Task 21: correctly said that (sylheti, 20, a3np21apfa18b) and (atemayar, 25, a23psx2z4f65q) are not the same in 1739 ms.
Task 22: incorrectly said that (inuktitut, 1, a1manxzopzden) and (inuktitut, 1, a1w3fo0d7cf5t) are not the same in 1654 ms.
Task 23: correctly said that (tagalog, 10, a3qat9qkqumi2) and (inuktitut, 1, a3noo9k3y1yu5) are not the same in 2691 ms.
Task 24: correctly said that (korean, 28, aw82oruamauz) and (sylheti, 19, a3209gno9duil) are not the same in 3339 ms.
Task 25: correctly said that (inuktitut, 1, ay0bjcgydhix) and (inuktitut, 1, a12rfyr8dbn79) are the same in 2230 ms.
Task 26: correctly said that (myanmar, 32, ajf66jwen7bx) and (inuktitut, 1, a2pgu4pi93b1q) are not the same in 1707 ms.
Task 27: correctly said that (sylheti, 17, amynpl5xzw6q) and (myanmar, 32, adc8vbkoocrr) are not the same in 1127 ms.
Task 28: correctly said that (sylheti, 19, aljsxt6v0a30) and (myanmar, 1, a1ehrxjplsq0r) are not the same in 1515 ms.
Task 29: correctly said that (myanmar, 1, a1j8s7giyto4a) and (myanmar, 1, a1foggtlgo7n4) are the same in 1242 ms.
Task 30: correctly said that (myanmar, 31, a3pkun3onkucn) and (myanmar, 31, a1s3njxhehcip) are the same in 1715 ms.
Task 31: correctly said that (atemayar, 23, ax5gghhj0o6g) and (sylheti, 17, a3m0kt6kjezd1) are not the same in 1798 ms.
Task 32: correctly said that (korean, 28, a2ernxe6jlm4x) and (inuktitut, 3, a3noo9k3y1yu5) are not the same in 941 ms.
Task 33: correctly said that (sylheti, 28, a4858n7ggst4) and (atemayar, 6, a1poymbentexx) are not the same in 1169 ms.
Task 34: correctly said that (tagalog, 6, a3mv1jtwep7xn) and (tagalog, 6, asu813kb7bv1) are the same in 1344 ms.
Task 35: incorrectly said that (korean, 39, aw82oruamauz) and (sylheti, 17, a3r9q9wqm1nyz) are the same in 2181 ms.
Task 36: incorrectly said that (inuktitut, 3, a2ernxe6jlm4x) and (inuktitut, 3, a2pgu4pi93b1q) are not the same in 1960 ms.
Task 37: correctly said that (korean, 39, a1udqv4pfypkn) and (korean, 39, a2ernxe6jlm4x) are the same in 1632 ms.
Task 38: correctly said that (atemayar, 10, a31xqftzcsia2) and (atemayar, 10, a1z59eb5sfxpy) are the same in 1893 ms.
Task 39: correctly said that (myanmar, 32, avlbkm6qpb1s) and (atemayar, 23, a1poymbentexx) are not the same in 2098 ms.
Task 40: correctly said that (sylheti, 28, a1foggtlgo7n4) and (korean, 39, a2ernxe6jlm4x) are not the same in 1571 ms.
Task 41: incorrectly said that (sylheti, 17, ay0bjcgydhix) and (sylheti, 19, a3np21apfa18b) are the same in 1642 ms.
Task 42: correctly said that (inuktitut, 11, a1j8s7giyto4a) and (inuktitut, 11, a12rfyr8dbn79) are the same in 2092 ms.
Task 43: correctly said that (atemayar, 23, a23psx2z4f65q) and (inuktitut, 2, avlbkm6qpb1s) are not the same in 2917 ms.
Task 44: correctly said that (atemayar, 23, a2pfktghg1ofp) and (tagalog, 2, a3m0kt6kjezd1) are not the same in 1539 ms.
Task 45: correctly said that (korean, 4, aj6upe9mlarg) and (korean, 4, a1oh82lwr5hbg) are the same in 1465 ms.
Task 46: incorrectly said that (myanmar, 32, a6fjbvuzrhm6) and (sylheti, 19, a1l2fzyn31zvf) are the same in 2607 ms.
Task 47: correctly said that (myanmar, 25, ay0bjcgydhix) and (sylheti, 19, a1l2fzyn31zvf) are not the same in 1516 ms.
Task 48: correctly said that (sylheti, 19, avlbkm6qpb1s) and (tagalog, 5, aghrkle9vf6z) are not the same in 1438 ms.
Task 49: correctly said that (atemayar, 25, a31xqftzcsia2) and (atemayar, 25, a23psx2z4f65q) are the same in 1511 ms.
Task 50: correctly said that (myanmar, 25, ay0bjcgydhix) and (inuktitut, 11, a1j8s7giyto4a) are not the same in 1681 ms.
Task 51: correctly said that (myanmar, 25, a1aegunf6e081) and (inuktitut, 3, ae0ibwt7asth) are not the same in 1356 ms.
Task 52: correctly said that (tagalog, 10, a1s3njxhehcip) and (tagalog, 10, a1ehrxjplsq0r) are the same in 1238 ms.
Task 53: correctly said that (korean, 4, aj6upe9mlarg) and (myanmar, 25, a1clejhbx9yyn) are not the same in 1286 ms.
Task 54: correctly said that (myanmar, 1, aljsxt6v0a30) and (korean, 4, aav4pzxdoebd) are not the same in 1678 ms.
Task 55: correctly said that (inuktitut, 11, a1j8s7giyto4a) and (inuktitut, 11, a3mv1jtwep7xn) are the same in 1495 ms.
Task 56: incorrectly said that (inuktitut, 11, a1qku9mccvl35) and (tagalog, 10, a1i92aacrkacw) are the same in 1941 ms.
Task 57: correctly said that (atemayar, 10, a1j8s7giyto4a) and (inuktitut, 1, ay0bjcgydhix) are not the same in 1536 ms.
Task 58: correctly said that (sylheti, 20, a3r9q9wqm1nyz) and (sylheti, 20, a1qku9mccvl35) are the same in 1743 ms.
Task 59: correctly said that (korean, 29, a39ivbdm1ekue) and (sylheti, 20, avlbkm6qpb1s) are not the same in 1595 ms.
Task 60: correctly said that (tagalog, 5, a3m0kt6kjezd1) and (atemayar, 10, aljsxt6v0a30) are not the same in 1487 ms.
Task 61: correctly said that (sylheti, 17, a3209gno9duil) and (myanmar, 1, a1aegunf6e081) are not the same in 1244 ms.
Task 62: correctly said that (inuktitut, 11, a1z59eb5sfxpy) and (inuktitut, 11, a1w3fo0d7cf5t) are the same in 1351 ms.
Task 63: correctly said that (atemayar, 25, avlbkm6qpb1s) and (sylheti, 20, a3m0kt6kjezd1) are not the same in 2150 ms.
Task 64: incorrectly said that (myanmar, 32, avlbkm6qpb1s) and (tagalog, 10, a1a25pyfoa9ge) are the same in 1561 ms.
Task 65: correctly said that (korean, 29, aj6upe9mlarg) and (korean, 29, a1manxzopzden) are the same in 1529 ms.
Task 66: correctly said that (atemayar, 6, a1yovlluv1c9h) and (atemayar, 6, a1j8s7giyto4a) are the same in 1558 ms.
Task 67: incorrectly said that (korean, 28, aj6upe9mlarg) and (myanmar, 25, a3qat9qkqumi2) are the same in 2215 ms.
Task 68: correctly said that (myanmar, 1, a3j1pp96x14u0) and (sylheti, 17, a1qku9mccvl35) are not the same in 1850 ms.
Task 69: correctly said that (korean, 39, a3dfh6dpi11ip) and (atemayar, 10, a31xqftzcsia2) are not the same in 2100 ms.
Task 70: correctly said that (myanmar, 1, ajf66jwen7bx) and (sylheti, 20, a1j8s7giyto4a) are not the same in 2542 ms.
Task 71: correctly said that (korean, 4, a39ivbdm1ekue) and (korean, 28, aj6upe9mlarg) are not the same in 2254 ms.
Task 72: correctly said that (inuktitut, 3, a1s3njxhehcip) and (inuktitut, 3, aljsxt6v0a30) are the same in 1326 ms.
Task 73: correctly said that (korean, 39, a5379luit3pc) and (korean, 28, a3mv1jtwep7xn) are not the same in 1706 ms.
Task 74: incorrectly said that (korean, 39, a1manxzopzden) and (atemayar, 23, ae0ibwt7asth) are the same in 1636 ms.
Task 75: correctly said that (tagalog, 10, amynpl5xzw6q) and (myanmar, 31, a1j8s7giyto4a) are not the same in 1633 ms.
Task 76: incorrectly said that (tagalog, 5, a3mv1jtwep7xn) and (sylheti, 28, a4858n7ggst4) are the same in 1481 ms.
Task 77: correctly said that (myanmar, 31, a1s3njxhehcip) and (atemayar, 10, a1vzhyp8yvxkz) are not the same in 1829 ms.
Task 78: correctly said that (sylheti, 17, a1j8s7giyto4a) and (inuktitut, 2, a2pgu4pi93b1q) are not the same in 1591 ms.
Task 79: correctly said that (sylheti, 28, a3209gno9duil) and (tagalog, 2, a1yovlluv1c9h) are not the same in 2422 ms.
Task 80: correctly said that (inuktitut, 3, a1s3njxhehcip) and (inuktitut, 3, a1qku9mccvl35) are the same in 1526 ms.
Task 81: correctly said that (sylheti, 19, a3m0kt6kjezd1) and (myanmar, 25, a1ehrxjplsq0r) are not the same in 1695 ms.
Task 82: correctly said that (atemayar, 10, awl8tng8e81d) and (inuktitut, 1, a1w3fo0d7cf5t) are not the same in 1434 ms.
Task 83: correctly said that (inuktitut, 3, a3mv1jtwep7xn) and (tagalog, 10, a1yovlluv1c9h) are not the same in 1306 ms.
Task 84: correctly said that (inuktitut, 3, ay0bjcgydhix) and (tagalog, 10, ajf66jwen7bx) are not the same in 950 ms.
Task 85: correctly said that (atemayar, 6, a6fjbvuzrhm6) and (atemayar, 6, aljsxt6v0a30) are the same in 1574 ms.
Task 86: correctly said that (korean, 28, a1oh82lwr5hbg) and (korean, 28, a5379luit3pc) are the same in 1316 ms.
Task 87: correctly said that (inuktitut, 11, ae0ibwt7asth) and (tagalog, 6, a3m0kt6kjezd1) are not the same in 1397 ms.
Task 88: correctly said that (atemayar, 10, avlbkm6qpb1s) and (atemayar, 10, aljsxt6v0a30) are the same in 1387 ms.
Task 89: correctly said that (inuktitut, 1, a3noo9k3y1yu5) and (korean, 29, a1v0p79fynz9j) are not the same in 1013 ms.
Task 90: correctly said that (myanmar, 32, avlbkm6qpb1s) and (myanmar, 32, a1stvgjqfl5kk) are the same in 1568 ms.
Task 91: correctly said that (tagalog, 10, a3qat9qkqumi2) and (tagalog, 10, a3mv1jtwep7xn) are the same in 1740 ms.
Task 92: correctly said that (sylheti, 17, a1l2fzyn31zvf) and (atemayar, 6, azdw6062ia38) are not the same in 1418 ms.
Task 93: correctly said that (tagalog, 10, a2du3880jbrov) and (sylheti, 17, a4858n7ggst4) are not the same in 1292 ms.
Task 94: correctly said that (korean, 29, a3dfh6dpi11ip) and (myanmar, 32, a1s3njxhehcip) are not the same in 1616 ms.
Task 95: correctly said that (myanmar, 1, a1foggtlgo7n4) and (inuktitut, 2, a12rfyr8dbn79) are not the same in 1526 ms.
Task 96: incorrectly said that (korean, 39, aw82oruamauz) and (sylheti, 20, a4858n7ggst4) are the same in 1534 ms.
Task 97: correctly said that (sylheti, 20, a3209gno9duil) and (atemayar, 25, a1poymbentexx) are not the same in 1166 ms.
Task 98: correctly said that (korean, 4, a1udqv4pfypkn) and (tagalog, 5, a1a25pyfoa9ge) are not the same in 1855 ms.
Task 99: correctly said that (myanmar, 31, a1ehrxjplsq0r) and (myanmar, 31, a3j1pp96x14u0) are the same in 1832 ms.
Task 100: incorrectly said that (korean, 29, a1umhcrj9zfdp) and (korean, 39, a3d7xwp0gand5) are the same in 1364 ms.
Task 101: correctly said that (sylheti, 17, a1s3njxhehcip) and (myanmar, 25, adc8vbkoocrr) are not the same in 1284 ms.
Task 102: correctly said that (sylheti, 28, a3209gno9duil) and (sylheti, 28, a3pkun3onkucn) are the same in 1668 ms.
Task 103: correctly said that (atemayar, 10, a1qku9mccvl35) and (sylheti, 20, a30xq7555spo8) are not the same in 1443 ms.
Task 104: correctly said that (sylheti, 19, a3pkun3onkucn) and (sylheti, 19, a3r9q9wqm1nyz) are the same in 1096 ms.
Task 105: correctly said that (myanmar, 1, a1j8s7giyto4a) and (myanmar, 1, a2pfktghg1ofp) are the same in 1408 ms.
Task 106: correctly said that (korean, 28, a1s3njxhehcip) and (sylheti, 17, a1qku9mccvl35) are not the same in 849 ms.
Task 107: correctly said that (inuktitut, 1, a1w3fo0d7cf5t) and (inuktitut, 1, avlbkm6qpb1s) are the same in 1103 ms.
Task 108: incorrectly said that (tagalog, 2, a3qat9qkqumi2) and (sylheti, 17, a1s3njxhehcip) are the same in 2030 ms.
Task 109: correctly said that (myanmar, 32, a1foggtlgo7n4) and (myanmar, 32, a3j1pp96x14u0) are the same in 1742 ms.
Task 110: correctly said that (sylheti, 28, a3pkun3onkucn) and (sylheti, 28, a1foggtlgo7n4) are the same in 1652 ms.
Task 111: correctly said that (atemayar, 10, a31e6jchki335) and (atemayar, 10, a1qku9mccvl35) are the same in 1469 ms.
Task 112: correctly said that (korean, 39, a3d7xwp0gand5) and (sylheti, 28, avlbkm6qpb1s) are not the same in 1439 ms.
Task 113: correctly said that (sylheti, 28, a3r9q9wqm1nyz) and (sylheti, 28, atvyxlwe5n03) are the same in 1331 ms.
Task 114: incorrectly said that (tagalog, 10, a3mv1jtwep7xn) and (sylheti, 17, a2du3880jbrov) are the same in 2082 ms.
Task 115: correctly said that (atemayar, 6, a1poymbentexx) and (myanmar, 31, ay0bjcgydhix) are not the same in 1276 ms.
Task 116: correctly said that (sylheti, 28, atvyxlwe5n03) and (tagalog, 6, ay0bjcgydhix) are not the same in 1481 ms.
Task 117: incorrectly said that (myanmar, 31, a1umhcrj9zfdp) and (myanmar, 32, a3pkun3onkucn) are the same in 1412 ms.
Task 118: correctly said that (myanmar, 31, ajf66jwen7bx) and (inuktitut, 2, a2pfktghg1ofp) are not the same in 1391 ms.
Task 119: correctly said that (myanmar, 32, a1umhcrj9zfdp) and (tagalog, 2, a3m0kt6kjezd1) are not the same in 2557 ms.
Task 120: correctly said that (korean, 29, a2ernxe6jlm4x) and (korean, 29, a1udqv4pfypkn) are the same in 2053 ms.
Task 121: incorrectly said that (myanmar, 31, adc8vbkoocrr) and (tagalog, 5, a1a25pyfoa9ge) are the same in 1873 ms.
Task 122: correctly said that (sylheti, 20, a1foggtlgo7n4) and (korean, 29, a5379luit3pc) are not the same in 1089 ms.
Task 123: correctly said that (myanmar, 31, a1clejhbx9yyn) and (inuktitut, 1, a1j8s7giyto4a) are not the same in 1053 ms.
Task 124: incorrectly said that (myanmar, 31, a1ehrxjplsq0r) and (korean, 39, a3mv1jtwep7xn) are the same in 1423 ms.
Task 125: correctly said that (atemayar, 10, aljsxt6v0a30) and (inuktitut, 3, aljsxt6v0a30) are not the same in 1104 ms.
Task 126: correctly said that (inuktitut, 2, a315xddd8j4x8) and (sylheti, 19, amynpl5xzw6q) are not the same in 1436 ms.
Task 127: correctly said that (atemayar, 10, a3m0kt6kjezd1) and (myanmar, 32, a1s3njxhehcip) are not the same in 2169 ms.
Task 128: correctly said that (myanmar, 31, a1aegunf6e081) and (sylheti, 17, a3209gno9duil) are not the same in 1674 ms.
Task 129: correctly said that (korean, 4, ae0ibwt7asth) and (korean, 4, a39ivbdm1ekue) are the same in 1426 ms.
Task 130: correctly said that (atemayar, 23, a31xqftzcsia2) and (atemayar, 23, a1j8s7giyto4a) are the same in 1484 ms.
Task 131: correctly said that (myanmar, 25, a1clejhbx9yyn) and (inuktitut, 1, a1s3njxhehcip) are not the same in 1161 ms.
Task 132: correctly said that (korean, 4, a1s3njxhehcip) and (korean, 28, a3d7xwp0gand5) are not the same in 1324 ms.
Task 133: correctly said that (tagalog, 6, ay0bjcgydhix) and (tagalog, 6, a1s3njxhehcip) are the same in 1079 ms.
Task 134: correctly said that (sylheti, 28, a1l2fzyn31zvf) and (atemayar, 10, agst3co6usf8) are not the same in 1215 ms.
Task 135: correctly said that (tagalog, 5, aghrkle9vf6z) and (inuktitut, 11, a1qku9mccvl35) are not the same in 1375 ms.
Task 136: correctly said that (myanmar, 25, a1aegunf6e081) and (myanmar, 25, adc8vbkoocrr) are the same in 1529 ms.
Task 137: incorrectly said that (atemayar, 6, a1j8s7giyto4a) and (tagalog, 5, a3m0kt6kjezd1) are the same in 1578 ms.
Task 138: correctly said that (inuktitut, 3, a3mv1jtwep7xn) and (inuktitut, 3, ay0bjcgydhix) are the same in 1379 ms.
Task 139: correctly said that (myanmar, 32, a3j1pp96x14u0) and (tagalog, 5, amynpl5xzw6q) are not the same in 1258 ms.
Task 140: correctly said that (inuktitut, 2, a1qku9mccvl35) and (inuktitut, 2, a1manxzopzden) are the same in 1362 ms.
Task 141: correctly said that (tagalog, 5, aljsxt6v0a30) and (tagalog, 2, avlbkm6qpb1s) are not the same in 1129 ms.
Task 142: correctly said that (tagalog, 10, a3mv1jtwep7xn) and (korean, 28, a2akud1gdstsd) are not the same in 1295 ms.
Task 143: correctly said that (atemayar, 25, agst3co6usf8) and (inuktitut, 1, ae0ibwt7asth) are not the same in 1246 ms.
Task 144: correctly said that (sylheti, 28, a1foggtlgo7n4) and (inuktitut, 2, a2pfktghg1ofp) are not the same in 1704 ms.
Task 145: correctly said that (tagalog, 5, a1s3njxhehcip) and (tagalog, 5, a1i92aacrkacw) are the same in 1072 ms.
Task 146: correctly said that (myanmar, 32, amynpl5xzw6q) and (myanmar, 32, a3pkun3onkucn) are the same in 1421 ms.
Task 147: incorrectly said that (myanmar, 32, a3j1pp96x14u0) and (sylheti, 17, a3r9q9wqm1nyz) are the same in 1511 ms.
Task 148: correctly said that (inuktitut, 3, a2ernxe6jlm4x) and (inuktitut, 2, ae0ibwt7asth) are not the same in 1167 ms.
Task 149: correctly said that (korean, 29, a3d7xwp0gand5) and (inuktitut, 11, avlbkm6qpb1s) are not the same in 1385 ms.
Task 150: correctly said that (myanmar, 25, a4858n7ggst4) and (tagalog, 10, a12rfyr8dbn79) are not the same in 1667 ms.
Task 151: incorrectly said that (atemayar, 23, a1manxzopzden) and (atemayar, 6, ae0ibwt7asth) are the same in 1162 ms.
Task 152: correctly said that (myanmar, 25, amynpl5xzw6q) and (inuktitut, 1, ay0bjcgydhix) are not the same in 1242 ms.
Task 153: incorrectly said that (atemayar, 6, a1z59eb5sfxpy) and (korean, 4, a2nm1qzuh14ls) are the same in 1282 ms.
Task 154: incorrectly said that (sylheti, 19, a1qku9mccvl35) and (atemayar, 10, awl8tng8e81d) are the same in 1571 ms.
Task 155: correctly said that (sylheti, 17, a3209gno9duil) and (myanmar, 25, a1umhcrj9zfdp) are not the same in 1439 ms.
Task 156: correctly said that (inuktitut, 2, a12xm86d2nbih) and (tagalog, 5, a1a25pyfoa9ge) are not the same in 2246 ms.
Task 157: correctly said that (inuktitut, 3, a12rfyr8dbn79) and (myanmar, 32, ajf66jwen7bx) are not the same in 2232 ms.
Task 158: correctly said that (inuktitut, 11, a1z59eb5sfxpy) and (korean, 28, a2nm1qzuh14ls) are not the same in 1179 ms.
Task 159: correctly said that (myanmar, 25, a3qat9qkqumi2) and (korean, 29, a2ernxe6jlm4x) are not the same in 1188 ms.
Task 160: correctly said that (korean, 39, a1s3njxhehcip) and (korean, 4, ae0ibwt7asth) are not the same in 1783 ms.
Task 161: correctly said that (korean, 4, aj6upe9mlarg) and (korean, 4, a2ernxe6jlm4x) are the same in 1095 ms.
Task 162: correctly said that (tagalog, 5, a3mv1jtwep7xn) and (atemayar, 10, a23psx2z4f65q) are not the same in 1159 ms.
Task 163: correctly said that (tagalog, 6, a1yovlluv1c9h) and (sylheti, 17, a6fjbvuzrhm6) are not the same in 1618 ms.
Task 164: correctly said that (myanmar, 31, a1s3njxhehcip) and (myanmar, 31, a3pkun3onkucn) are the same in 1244 ms.
Task 165: correctly said that (inuktitut, 2, a3qat9qkqumi2) and (myanmar, 32, ay0bjcgydhix) are not the same in 1290 ms.
Task 166: correctly said that (tagalog, 6, a1s3njxhehcip) and (inuktitut, 11, aljsxt6v0a30) are not the same in 1356 ms.
Task 167: correctly said that (tagalog, 2, a1s3njxhehcip) and (korean, 39, a3mv1jtwep7xn) are not the same in 1726 ms.
Task 168: correctly said that (inuktitut, 1, a2ernxe6jlm4x) and (tagalog, 5, amynpl5xzw6q) are not the same in 1200 ms.
Task 169: correctly said that (tagalog, 2, a1ehrxjplsq0r) and (inuktitut, 11, a2pgu4pi93b1q) are not the same in 1375 ms.
Task 170: incorrectly said that (sylheti, 17, a1j8s7giyto4a) and (sylheti, 20, ay0bjcgydhix) are the same in 1445 ms.
Task 171: correctly said that (sylheti, 20, a1a25pyfoa9ge) and (atemayar, 10, a6fjbvuzrhm6) are not the same in 1420 ms.
Task 172: correctly said that (inuktitut, 2, a1s3njxhehcip) and (atemayar, 10, ae0ibwt7asth) are not the same in 1296 ms.
Task 173: correctly said that (myanmar, 1, a1foggtlgo7n4) and (inuktitut, 11, a12rfyr8dbn79) are not the same in 1289 ms.
Task 174: correctly said that (myanmar, 25, a1aegunf6e081) and (atemayar, 23, a31e6jchki335) are not the same in 1389 ms.
Task 175: correctly said that (korean, 29, a1udqv4pfypkn) and (korean, 29, a2px0aywxngce) are the same in 1428 ms.
Task 176: correctly said that (korean, 28, a3mv1jtwep7xn) and (korean, 28, aw82oruamauz) are the same in 1343 ms.
Task 177: correctly said that (inuktitut, 1, aljsxt6v0a30) and (inuktitut, 1, a1z59eb5sfxpy) are the same in 1459 ms.
Task 178: correctly said that (tagalog, 5, a1s3njxhehcip) and (tagalog, 5, a1ehrxjplsq0r) are the same in 1500 ms.
Task 179: correctly said that (tagalog, 2, a1j8s7giyto4a) and (myanmar, 32, ay0bjcgydhix) are not the same in 1288 ms.
Task 180: correctly said that (tagalog, 2, aghrkle9vf6z) and (myanmar, 25, a1s3njxhehcip) are not the same in 2295 ms.
Task 181: incorrectly said that (myanmar, 32, a1umhcrj9zfdp) and (tagalog, 6, a1ehrxjplsq0r) are the same in 1253 ms.
Task 182: correctly said that (inuktitut, 1, a1j8s7giyto4a) and (inuktitut, 1, ay0bjcgydhix) are the same in 1610 ms.
Task 183: correctly said that (inuktitut, 11, a1manxzopzden) and (myanmar, 1, a1clejhbx9yyn) are not the same in 1822 ms.
Task 184: correctly said that (atemayar, 10, a1qku9mccvl35) and (inuktitut, 3, a1qku9mccvl35) are not the same in 1329 ms.
Task 185: correctly said that (korean, 28, a39ivbdm1ekue) and (atemayar, 6, a31e6jchki335) are not the same in 1217 ms.
Task 186: correctly said that (atemayar, 25, a23psx2z4f65q) and (myanmar, 1, amynpl5xzw6q) are not the same in 1443 ms.
Task 187: correctly said that (atemayar, 10, azdw6062ia38) and (atemayar, 10, avlbkm6qpb1s) are the same in 1304 ms.
Task 188: incorrectly said that (tagalog, 6, a3qat9qkqumi2) and (tagalog, 5, a1a25pyfoa9ge) are the same in 1415 ms.
Task 189: correctly said that (inuktitut, 3, a1s3njxhehcip) and (sylheti, 17, a3pkun3onkucn) are not the same in 1170 ms.
Task 190: correctly said that (korean, 39, aw82oruamauz) and (myanmar, 32, a1stvgjqfl5kk) are not the same in 1392 ms.
Task 191: correctly said that (korean, 28, aj6upe9mlarg) and (atemayar, 6, a1yovlluv1c9h) are not the same in 1198 ms.
Task 192: correctly said that (inuktitut, 1, aljsxt6v0a30) and (sylheti, 19, a1l2fzyn31zvf) are not the same in 1454 ms.
Task 193: correctly said that (inuktitut, 1, a3mv1jtwep7xn) and (korean, 28, a3d7xwp0gand5) are not the same in 1079 ms.
Task 194: incorrectly said that (tagalog, 2, a1a25pyfoa9ge) and (tagalog, 2, avlbkm6qpb1s) are not the same in 1186 ms.
Task 195: correctly said that (myanmar, 25, a1j8s7giyto4a) and (tagalog, 6, ay0bjcgydhix) are not the same in 1476 ms.
Task 196: incorrectly said that (inuktitut, 2, a3mv1jtwep7xn) and (sylheti, 28, amynpl5xzw6q) are the same in 1335 ms.
Task 197: correctly said that (atemayar, 25, a6fjbvuzrhm6) and (tagalog, 6, a12rfyr8dbn79) are not the same in 1555 ms.
Task 198: correctly said that (korean, 29, a1manxzopzden) and (myanmar, 32, a1foggtlgo7n4) are not the same in 1515 ms.
Task 199: correctly said that (korean, 39, a1v0p79fynz9j) and (atemayar, 25, a1poymbentexx) are not the same in 1489 ms.

Duration: 17m 45s 348ms
Comments: Interesting test. I was able to very briefly see a shape in the Test image that I tried to use to match to the right image.  Sometimes it was enough of a hint of a pattern for me to make a good guess.