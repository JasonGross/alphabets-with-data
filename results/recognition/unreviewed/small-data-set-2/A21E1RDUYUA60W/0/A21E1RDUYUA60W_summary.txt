

Summary:Short Summary:
Correct: 178
Incorrect: 22
Percent Correct: 89

Same Correct: 50
Same Incorrect: 7
Percent Same Correct: 87

Different Correct: 128
Different Incorrect: 15
Percent Different Correct: 89

Long Summary:
Task 0: incorrectly said that (korean, 4, aav4pzxdoebd) and (korean, 4, a1udqv4pfypkn) are not the same in 1733 ms.
Task 1: incorrectly said that (tagalog, 6, aljsxt6v0a30) and (myanmar, 32, a3pkun3onkucn) are the same in 1394 ms.
Task 2: correctly said that (myanmar, 1, a2pfktghg1ofp) and (atemayar, 25, azdw6062ia38) are not the same in 1189 ms.
Task 3: correctly said that (myanmar, 31, adc8vbkoocrr) and (myanmar, 31, a6fjbvuzrhm6) are the same in 1517 ms.
Task 4: correctly said that (atemayar, 23, a1z59eb5sfxpy) and (myanmar, 25, a1s3njxhehcip) are not the same in 1192 ms.
Task 5: correctly said that (tagalog, 5, aghrkle9vf6z) and (tagalog, 5, a3m0kt6kjezd1) are the same in 1025 ms.
Task 6: correctly said that (myanmar, 32, a1aegunf6e081) and (sylheti, 28, aljsxt6v0a30) are not the same in 976 ms.
Task 7: incorrectly said that (korean, 29, ae0ibwt7asth) and (sylheti, 19, amynpl5xzw6q) are the same in 1340 ms.
Task 8: incorrectly said that (tagalog, 2, asu813kb7bv1) and (tagalog, 10, a3m0kt6kjezd1) are the same in 998 ms.
Task 9: incorrectly said that (korean, 39, a1udqv4pfypkn) and (korean, 39, a1s3njxhehcip) are not the same in 1617 ms.
Task 10: incorrectly said that (tagalog, 6, a1yovlluv1c9h) and (tagalog, 6, aghrkle9vf6z) are not the same in 1429 ms.
Task 11: correctly said that (inuktitut, 1, a2ernxe6jlm4x) and (tagalog, 5, ay0bjcgydhix) are not the same in 850 ms.
Task 12: correctly said that (inuktitut, 2, a1s3njxhehcip) and (tagalog, 6, a1ehrxjplsq0r) are not the same in 951 ms.
Task 13: correctly said that (myanmar, 32, a3j1pp96x14u0) and (myanmar, 32, a3qat9qkqumi2) are the same in 993 ms.
Task 14: correctly said that (inuktitut, 1, a1manxzopzden) and (atemayar, 25, azdw6062ia38) are not the same in 903 ms.
Task 15: correctly said that (sylheti, 28, a1s3njxhehcip) and (tagalog, 2, a1j8s7giyto4a) are not the same in 790 ms.
Task 16: correctly said that (inuktitut, 2, a12xm86d2nbih) and (inuktitut, 3, a1j8s7giyto4a) are not the same in 1095 ms.
Task 17: incorrectly said that (myanmar, 25, a1foggtlgo7n4) and (tagalog, 2, a12rfyr8dbn79) are the same in 1112 ms.
Task 18: correctly said that (sylheti, 28, a30xq7555spo8) and (sylheti, 28, a1a25pyfoa9ge) are the same in 1540 ms.
Task 19: correctly said that (tagalog, 2, a12rfyr8dbn79) and (inuktitut, 11, a2pfktghg1ofp) are not the same in 1037 ms.
Task 20: correctly said that (inuktitut, 1, a1s3njxhehcip) and (sylheti, 19, atvyxlwe5n03) are not the same in 1021 ms.
Task 21: correctly said that (sylheti, 20, a1j8s7giyto4a) and (korean, 28, a3mv1jtwep7xn) are not the same in 809 ms.
Task 22: correctly said that (myanmar, 32, a2pfktghg1ofp) and (tagalog, 6, a1a25pyfoa9ge) are not the same in 717 ms.
Task 23: correctly said that (tagalog, 10, a3m0kt6kjezd1) and (sylheti, 19, a3np21apfa18b) are not the same in 1678 ms.
Task 24: correctly said that (inuktitut, 3, a1qku9mccvl35) and (inuktitut, 3, a2pfktghg1ofp) are the same in 888 ms.
Task 25: correctly said that (korean, 29, a5379luit3pc) and (sylheti, 20, a6fjbvuzrhm6) are not the same in 1005 ms.
Task 26: correctly said that (inuktitut, 2, ay0bjcgydhix) and (inuktitut, 2, a315xddd8j4x8) are the same in 982 ms.
Task 27: correctly said that (korean, 29, aw82oruamauz) and (korean, 29, a5379luit3pc) are the same in 1214 ms.
Task 28: correctly said that (atemayar, 23, azdw6062ia38) and (tagalog, 5, aghrkle9vf6z) are not the same in 990 ms.
Task 29: incorrectly said that (sylheti, 28, ay0bjcgydhix) and (sylheti, 28, avlbkm6qpb1s) are not the same in 1419 ms.
Task 30: correctly said that (korean, 4, a1manxzopzden) and (korean, 4, a3d7xwp0gand5) are the same in 1140 ms.
Task 31: correctly said that (tagalog, 10, a1j8s7giyto4a) and (sylheti, 28, a4858n7ggst4) are not the same in 943 ms.
Task 32: correctly said that (myanmar, 32, a1ehrxjplsq0r) and (sylheti, 20, a1foggtlgo7n4) are not the same in 716 ms.
Task 33: incorrectly said that (tagalog, 6, a1l2fzyn31zvf) and (tagalog, 5, a1j8s7giyto4a) are the same in 2460 ms.
Task 34: correctly said that (sylheti, 19, a1qku9mccvl35) and (korean, 39, a1umhcrj9zfdp) are not the same in 763 ms.
Task 35: correctly said that (inuktitut, 1, aljsxt6v0a30) and (tagalog, 10, a3mv1jtwep7xn) are not the same in 647 ms.
Task 36: correctly said that (sylheti, 28, aljsxt6v0a30) and (myanmar, 1, a1s3njxhehcip) are not the same in 1008 ms.
Task 37: correctly said that (atemayar, 10, a3m0kt6kjezd1) and (sylheti, 28, avlbkm6qpb1s) are not the same in 696 ms.
Task 38: correctly said that (myanmar, 31, ajf66jwen7bx) and (inuktitut, 3, a1w3fo0d7cf5t) are not the same in 702 ms.
Task 39: correctly said that (myanmar, 1, a1ehrxjplsq0r) and (myanmar, 1, a3j1pp96x14u0) are the same in 1065 ms.
Task 40: correctly said that (inuktitut, 2, a2ernxe6jlm4x) and (inuktitut, 2, aljsxt6v0a30) are the same in 1245 ms.
Task 41: correctly said that (korean, 4, a2akud1gdstsd) and (atemayar, 25, avlbkm6qpb1s) are not the same in 1009 ms.
Task 42: correctly said that (korean, 4, aw82oruamauz) and (inuktitut, 1, a1w3fo0d7cf5t) are not the same in 817 ms.
Task 43: incorrectly said that (korean, 39, a3d7xwp0gand5) and (korean, 29, a39ivbdm1ekue) are the same in 1075 ms.
Task 44: correctly said that (tagalog, 2, amynpl5xzw6q) and (tagalog, 2, a1s3njxhehcip) are the same in 968 ms.
Task 45: correctly said that (sylheti, 19, ay0bjcgydhix) and (atemayar, 10, a31e6jchki335) are not the same in 1049 ms.
Task 46: correctly said that (myanmar, 1, ajf66jwen7bx) and (myanmar, 1, a3j1pp96x14u0) are the same in 970 ms.
Task 47: correctly said that (korean, 39, ae0ibwt7asth) and (atemayar, 25, a1manxzopzden) are not the same in 913 ms.
Task 48: correctly said that (atemayar, 10, a1yovlluv1c9h) and (sylheti, 28, a1l2fzyn31zvf) are not the same in 750 ms.
Task 49: correctly said that (inuktitut, 11, a2ernxe6jlm4x) and (inuktitut, 11, a12rfyr8dbn79) are the same in 1492 ms.
Task 50: correctly said that (inuktitut, 3, a315xddd8j4x8) and (inuktitut, 3, ay0bjcgydhix) are the same in 2544 ms.
Task 51: incorrectly said that (sylheti, 19, a1j8s7giyto4a) and (sylheti, 19, a6fjbvuzrhm6) are not the same in 1365 ms.
Task 52: correctly said that (sylheti, 19, a4858n7ggst4) and (sylheti, 19, a3m0kt6kjezd1) are the same in 1223 ms.
Task 53: correctly said that (korean, 4, a39ivbdm1ekue) and (inuktitut, 1, a1manxzopzden) are not the same in 856 ms.
Task 54: incorrectly said that (sylheti, 28, a1foggtlgo7n4) and (sylheti, 19, a4858n7ggst4) are the same in 1040 ms.
Task 55: correctly said that (korean, 4, aj6upe9mlarg) and (korean, 28, a3d7xwp0gand5) are not the same in 1347 ms.
Task 56: correctly said that (inuktitut, 11, a2pgu4pi93b1q) and (inuktitut, 1, a12rfyr8dbn79) are not the same in 1073 ms.
Task 57: correctly said that (myanmar, 31, a1ehrxjplsq0r) and (tagalog, 10, a2du3880jbrov) are not the same in 842 ms.
Task 58: correctly said that (sylheti, 20, a1foggtlgo7n4) and (inuktitut, 1, a12rfyr8dbn79) are not the same in 608 ms.
Task 59: correctly said that (tagalog, 5, a12rfyr8dbn79) and (tagalog, 5, aghrkle9vf6z) are the same in 1188 ms.
Task 60: correctly said that (sylheti, 17, atvyxlwe5n03) and (tagalog, 2, a1l2fzyn31zvf) are not the same in 1160 ms.
Task 61: correctly said that (sylheti, 28, a1a25pyfoa9ge) and (myanmar, 25, a2pfktghg1ofp) are not the same in 915 ms.
Task 62: correctly said that (myanmar, 32, a1ehrxjplsq0r) and (tagalog, 6, a1s3njxhehcip) are not the same in 774 ms.
Task 63: correctly said that (tagalog, 5, asu813kb7bv1) and (atemayar, 6, avlbkm6qpb1s) are not the same in 656 ms.
Task 64: correctly said that (atemayar, 10, ax5gghhj0o6g) and (korean, 39, a3d7xwp0gand5) are not the same in 861 ms.
Task 65: incorrectly said that (sylheti, 17, a1a25pyfoa9ge) and (sylheti, 17, a6fjbvuzrhm6) are not the same in 1016 ms.
Task 66: correctly said that (myanmar, 31, a1ehrxjplsq0r) and (sylheti, 17, a1j8s7giyto4a) are not the same in 852 ms.
Task 67: correctly said that (myanmar, 32, aljsxt6v0a30) and (myanmar, 32, a3qat9qkqumi2) are the same in 968 ms.
Task 68: correctly said that (sylheti, 19, a3np21apfa18b) and (atemayar, 25, a1z59eb5sfxpy) are not the same in 826 ms.
Task 69: correctly said that (inuktitut, 3, a1manxzopzden) and (tagalog, 5, a1ehrxjplsq0r) are not the same in 1257 ms.
Task 70: correctly said that (inuktitut, 11, a2pgu4pi93b1q) and (myanmar, 1, a4858n7ggst4) are not the same in 1220 ms.
Task 71: correctly said that (tagalog, 6, a1s3njxhehcip) and (tagalog, 6, ay0bjcgydhix) are the same in 1102 ms.
Task 72: correctly said that (atemayar, 6, ax5gghhj0o6g) and (atemayar, 6, a3m0kt6kjezd1) are the same in 1191 ms.
Task 73: correctly said that (tagalog, 2, asu813kb7bv1) and (tagalog, 2, a1s3njxhehcip) are the same in 1488 ms.
Task 74: correctly said that (atemayar, 10, a1yovlluv1c9h) and (sylheti, 19, ay0bjcgydhix) are not the same in 1168 ms.
Task 75: correctly said that (sylheti, 19, a4858n7ggst4) and (sylheti, 19, a1l2fzyn31zvf) are the same in 1143 ms.
Task 76: correctly said that (atemayar, 25, azdw6062ia38) and (korean, 29, a2akud1gdstsd) are not the same in 828 ms.
Task 77: correctly said that (myanmar, 31, a1s3njxhehcip) and (myanmar, 31, a1umhcrj9zfdp) are the same in 1360 ms.
Task 78: correctly said that (atemayar, 23, a6fjbvuzrhm6) and (inuktitut, 11, a3mv1jtwep7xn) are not the same in 1150 ms.
Task 79: incorrectly said that (atemayar, 23, agst3co6usf8) and (atemayar, 23, azdw6062ia38) are not the same in 1383 ms.
Task 80: incorrectly said that (myanmar, 1, a1clejhbx9yyn) and (myanmar, 25, amynpl5xzw6q) are the same in 1193 ms.
Task 81: correctly said that (atemayar, 6, a1z59eb5sfxpy) and (tagalog, 2, ajf66jwen7bx) are not the same in 1041 ms.
Task 82: correctly said that (atemayar, 25, a1qku9mccvl35) and (tagalog, 10, ay0bjcgydhix) are not the same in 680 ms.
Task 83: correctly said that (inuktitut, 1, a1s3njxhehcip) and (tagalog, 6, ae0ibwt7asth) are not the same in 634 ms.
Task 84: correctly said that (myanmar, 31, adc8vbkoocrr) and (sylheti, 20, a3np21apfa18b) are not the same in 726 ms.
Task 85: correctly said that (myanmar, 25, aljsxt6v0a30) and (korean, 29, ae0ibwt7asth) are not the same in 680 ms.
Task 86: correctly said that (myanmar, 1, a1umhcrj9zfdp) and (tagalog, 10, ay0bjcgydhix) are not the same in 608 ms.
Task 87: correctly said that (inuktitut, 1, a3noo9k3y1yu5) and (korean, 39, a3dfh6dpi11ip) are not the same in 604 ms.
Task 88: incorrectly said that (korean, 4, a3d7xwp0gand5) and (korean, 29, a3d7xwp0gand5) are the same in 868 ms.
Task 89: correctly said that (inuktitut, 11, a3noo9k3y1yu5) and (sylheti, 19, atvyxlwe5n03) are not the same in 796 ms.
Task 90: correctly said that (myanmar, 1, ay0bjcgydhix) and (inuktitut, 1, a1qku9mccvl35) are not the same in 827 ms.
Task 91: correctly said that (tagalog, 6, ae0ibwt7asth) and (inuktitut, 11, a2pgu4pi93b1q) are not the same in 828 ms.
Task 92: correctly said that (korean, 4, aj6upe9mlarg) and (inuktitut, 3, a3noo9k3y1yu5) are not the same in 642 ms.
Task 93: correctly said that (sylheti, 28, a6fjbvuzrhm6) and (sylheti, 28, a1j8s7giyto4a) are the same in 1085 ms.
Task 94: correctly said that (korean, 39, aav4pzxdoebd) and (korean, 4, a3bb99mn3zy37) are not the same in 1218 ms.
Task 95: correctly said that (sylheti, 20, a1a25pyfoa9ge) and (tagalog, 2, a1yovlluv1c9h) are not the same in 634 ms.
Task 96: correctly said that (inuktitut, 11, a2ernxe6jlm4x) and (inuktitut, 11, a1s3njxhehcip) are the same in 1186 ms.
Task 97: correctly said that (sylheti, 17, a1qku9mccvl35) and (atemayar, 25, avlbkm6qpb1s) are not the same in 1138 ms.
Task 98: correctly said that (sylheti, 20, a4858n7ggst4) and (tagalog, 6, a3qat9qkqumi2) are not the same in 717 ms.
Task 99: correctly said that (sylheti, 20, a2du3880jbrov) and (korean, 39, a3mv1jtwep7xn) are not the same in 688 ms.
Task 100: correctly said that (korean, 4, a5379luit3pc) and (inuktitut, 1, a2ernxe6jlm4x) are not the same in 1662 ms.
Task 101: correctly said that (korean, 28, a1umhcrj9zfdp) and (sylheti, 28, a4858n7ggst4) are not the same in 749 ms.
Task 102: correctly said that (inuktitut, 2, a1qku9mccvl35) and (myanmar, 1, a1umhcrj9zfdp) are not the same in 609 ms.
Task 103: incorrectly said that (sylheti, 28, a1a25pyfoa9ge) and (sylheti, 20, a1a25pyfoa9ge) are the same in 1162 ms.
Task 104: correctly said that (myanmar, 31, a1ehrxjplsq0r) and (inuktitut, 3, a2qnqd7m8zfbo) are not the same in 801 ms.
Task 105: correctly said that (korean, 39, aw82oruamauz) and (atemayar, 10, a1vzhyp8yvxkz) are not the same in 696 ms.
Task 106: correctly said that (inuktitut, 3, a3mv1jtwep7xn) and (myanmar, 32, avlbkm6qpb1s) are not the same in 678 ms.
Task 107: correctly said that (atemayar, 23, a1vzhyp8yvxkz) and (atemayar, 23, a1j8s7giyto4a) are the same in 1362 ms.
Task 108: correctly said that (myanmar, 1, a2pfktghg1ofp) and (korean, 4, a2akud1gdstsd) are not the same in 728 ms.
Task 109: correctly said that (myanmar, 31, a1j8s7giyto4a) and (korean, 39, aav4pzxdoebd) are not the same in 797 ms.
Task 110: correctly said that (atemayar, 25, agst3co6usf8) and (atemayar, 25, awl8tng8e81d) are the same in 1311 ms.
Task 111: correctly said that (myanmar, 31, ajf66jwen7bx) and (korean, 28, a2akud1gdstsd) are not the same in 1140 ms.
Task 112: correctly said that (myanmar, 1, a6fjbvuzrhm6) and (atemayar, 10, aljsxt6v0a30) are not the same in 697 ms.
Task 113: correctly said that (korean, 29, a3mv1jtwep7xn) and (myanmar, 1, amynpl5xzw6q) are not the same in 1062 ms.
Task 114: incorrectly said that (myanmar, 32, a1foggtlgo7n4) and (myanmar, 31, a1foggtlgo7n4) are the same in 1328 ms.
Task 115: correctly said that (atemayar, 10, ax5gghhj0o6g) and (inuktitut, 2, a2ernxe6jlm4x) are not the same in 699 ms.
Task 116: correctly said that (atemayar, 10, awl8tng8e81d) and (myanmar, 32, aljsxt6v0a30) are not the same in 811 ms.
Task 117: correctly said that (sylheti, 20, a1a25pyfoa9ge) and (tagalog, 5, asu813kb7bv1) are not the same in 739 ms.
Task 118: correctly said that (korean, 28, ae0ibwt7asth) and (atemayar, 25, a1poymbentexx) are not the same in 1534 ms.
Task 119: correctly said that (tagalog, 5, a1i92aacrkacw) and (sylheti, 17, a2du3880jbrov) are not the same in 858 ms.
Task 120: correctly said that (tagalog, 5, a2du3880jbrov) and (tagalog, 5, asu813kb7bv1) are the same in 1185 ms.
Task 121: correctly said that (inuktitut, 3, a2pgu4pi93b1q) and (inuktitut, 3, a2pfktghg1ofp) are the same in 1090 ms.
Task 122: correctly said that (atemayar, 10, awl8tng8e81d) and (atemayar, 10, azdw6062ia38) are the same in 1331 ms.
Task 123: correctly said that (korean, 39, a3dfh6dpi11ip) and (sylheti, 19, atvyxlwe5n03) are not the same in 938 ms.
Task 124: correctly said that (myanmar, 32, ay0bjcgydhix) and (inuktitut, 11, a1j8s7giyto4a) are not the same in 696 ms.
Task 125: correctly said that (atemayar, 23, awl8tng8e81d) and (atemayar, 23, a1j8s7giyto4a) are the same in 988 ms.
Task 126: correctly said that (korean, 39, aav4pzxdoebd) and (sylheti, 17, aljsxt6v0a30) are not the same in 739 ms.
Task 127: correctly said that (tagalog, 5, a3qat9qkqumi2) and (inuktitut, 3, a1manxzopzden) are not the same in 795 ms.
Task 128: correctly said that (inuktitut, 2, a1s3njxhehcip) and (sylheti, 20, a1j8s7giyto4a) are not the same in 1123 ms.
Task 129: correctly said that (korean, 4, a3bb99mn3zy37) and (sylheti, 19, a3r9q9wqm1nyz) are not the same in 994 ms.
Task 130: correctly said that (korean, 29, a3dfh6dpi11ip) and (sylheti, 28, a3209gno9duil) are not the same in 1634 ms.
Task 131: correctly said that (inuktitut, 1, a2pgu4pi93b1q) and (atemayar, 25, avlbkm6qpb1s) are not the same in 699 ms.
Task 132: correctly said that (atemayar, 6, a23psx2z4f65q) and (atemayar, 6, a31xqftzcsia2) are the same in 1023 ms.
Task 133: correctly said that (sylheti, 19, amynpl5xzw6q) and (tagalog, 10, a2du3880jbrov) are not the same in 1538 ms.
Task 134: correctly said that (atemayar, 23, a1z59eb5sfxpy) and (atemayar, 23, a1j8s7giyto4a) are the same in 1195 ms.
Task 135: correctly said that (tagalog, 10, a1i92aacrkacw) and (tagalog, 6, a1l2fzyn31zvf) are not the same in 767 ms.
Task 136: correctly said that (myanmar, 1, ay0bjcgydhix) and (myanmar, 1, a3j1pp96x14u0) are the same in 1257 ms.
Task 137: correctly said that (tagalog, 2, avlbkm6qpb1s) and (tagalog, 6, a1l2fzyn31zvf) are not the same in 1698 ms.
Task 138: correctly said that (tagalog, 6, ajf66jwen7bx) and (tagalog, 6, a1i92aacrkacw) are the same in 1182 ms.
Task 139: correctly said that (inuktitut, 11, a2pfktghg1ofp) and (korean, 39, a5379luit3pc) are not the same in 1136 ms.
Task 140: correctly said that (sylheti, 19, a3209gno9duil) and (sylheti, 19, a1l2fzyn31zvf) are the same in 1261 ms.
Task 141: correctly said that (atemayar, 25, a1poymbentexx) and (inuktitut, 1, a2qnqd7m8zfbo) are not the same in 1044 ms.
Task 142: correctly said that (inuktitut, 2, avlbkm6qpb1s) and (inuktitut, 2, ay0bjcgydhix) are the same in 1101 ms.
Task 143: correctly said that (tagalog, 5, a12rfyr8dbn79) and (myanmar, 32, adc8vbkoocrr) are not the same in 1136 ms.
Task 144: correctly said that (sylheti, 20, a1l2fzyn31zvf) and (inuktitut, 2, a2qnqd7m8zfbo) are not the same in 1112 ms.
Task 145: correctly said that (tagalog, 5, a2du3880jbrov) and (tagalog, 5, a12rfyr8dbn79) are the same in 1175 ms.
Task 146: correctly said that (korean, 28, aav4pzxdoebd) and (myanmar, 31, a3pkun3onkucn) are not the same in 835 ms.
Task 147: incorrectly said that (myanmar, 1, a1s3njxhehcip) and (myanmar, 32, a1s3njxhehcip) are the same in 1546 ms.
Task 148: correctly said that (korean, 39, a2px0aywxngce) and (myanmar, 25, aljsxt6v0a30) are not the same in 809 ms.
Task 149: correctly said that (myanmar, 1, a6fjbvuzrhm6) and (korean, 29, ae0ibwt7asth) are not the same in 898 ms.
Task 150: correctly said that (inuktitut, 3, a1s3njxhehcip) and (korean, 39, ae0ibwt7asth) are not the same in 1153 ms.
Task 151: correctly said that (tagalog, 5, a3qat9qkqumi2) and (tagalog, 5, aljsxt6v0a30) are the same in 1089 ms.
Task 152: correctly said that (atemayar, 10, a1j8s7giyto4a) and (inuktitut, 11, a2qnqd7m8zfbo) are not the same in 713 ms.
Task 153: correctly said that (inuktitut, 3, a12rfyr8dbn79) and (inuktitut, 3, a315xddd8j4x8) are the same in 988 ms.
Task 154: correctly said that (sylheti, 17, a6fjbvuzrhm6) and (myanmar, 31, a1stvgjqfl5kk) are not the same in 722 ms.
Task 155: correctly said that (tagalog, 6, a3qat9qkqumi2) and (tagalog, 6, a1i92aacrkacw) are the same in 1174 ms.
Task 156: correctly said that (inuktitut, 11, a1s3njxhehcip) and (tagalog, 5, a1yovlluv1c9h) are not the same in 929 ms.
Task 157: correctly said that (sylheti, 20, a1j8s7giyto4a) and (tagalog, 5, a3mv1jtwep7xn) are not the same in 821 ms.
Task 158: correctly said that (sylheti, 20, ay0bjcgydhix) and (korean, 28, aav4pzxdoebd) are not the same in 845 ms.
Task 159: correctly said that (myanmar, 25, aljsxt6v0a30) and (tagalog, 5, a1j8s7giyto4a) are not the same in 1116 ms.
Task 160: correctly said that (atemayar, 6, a31e6jchki335) and (atemayar, 6, a1z59eb5sfxpy) are the same in 1267 ms.
Task 161: correctly said that (atemayar, 23, aljsxt6v0a30) and (myanmar, 32, amynpl5xzw6q) are not the same in 1155 ms.
Task 162: incorrectly said that (tagalog, 2, asu813kb7bv1) and (tagalog, 5, a3m0kt6kjezd1) are the same in 1142 ms.
Task 163: correctly said that (myanmar, 31, a1aegunf6e081) and (sylheti, 17, a3np21apfa18b) are not the same in 805 ms.
Task 164: correctly said that (myanmar, 1, a3pkun3onkucn) and (tagalog, 10, a1ehrxjplsq0r) are not the same in 817 ms.
Task 165: correctly said that (korean, 39, a1umhcrj9zfdp) and (myanmar, 32, a1aegunf6e081) are not the same in 787 ms.
Task 166: correctly said that (sylheti, 19, a30xq7555spo8) and (atemayar, 6, a1manxzopzden) are not the same in 848 ms.
Task 167: correctly said that (inuktitut, 3, a1manxzopzden) and (inuktitut, 11, a12rfyr8dbn79) are not the same in 784 ms.
Task 168: correctly said that (atemayar, 10, ae0ibwt7asth) and (inuktitut, 1, ay0bjcgydhix) are not the same in 717 ms.
Task 169: correctly said that (korean, 39, a2nm1qzuh14ls) and (korean, 39, a2akud1gdstsd) are the same in 1268 ms.
Task 170: correctly said that (inuktitut, 2, a12xm86d2nbih) and (korean, 28, a3dfh6dpi11ip) are not the same in 1043 ms.
Task 171: correctly said that (atemayar, 10, a31xqftzcsia2) and (sylheti, 28, a4858n7ggst4) are not the same in 798 ms.
Task 172: correctly said that (tagalog, 2, a1a25pyfoa9ge) and (tagalog, 2, amynpl5xzw6q) are the same in 1090 ms.
Task 173: correctly said that (korean, 39, a1oh82lwr5hbg) and (korean, 4, ae0ibwt7asth) are not the same in 999 ms.
Task 174: correctly said that (korean, 4, a1s3njxhehcip) and (atemayar, 10, a1poymbentexx) are not the same in 812 ms.
Task 175: correctly said that (atemayar, 23, aljsxt6v0a30) and (sylheti, 20, a3209gno9duil) are not the same in 864 ms.
Task 176: correctly said that (inuktitut, 1, a3mv1jtwep7xn) and (myanmar, 31, a1j8s7giyto4a) are not the same in 718 ms.
Task 177: correctly said that (inuktitut, 3, a1j8s7giyto4a) and (inuktitut, 3, a2pgu4pi93b1q) are the same in 1335 ms.
Task 178: correctly said that (sylheti, 20, ay0bjcgydhix) and (myanmar, 25, a3qat9qkqumi2) are not the same in 1044 ms.
Task 179: correctly said that (myanmar, 1, a2pfktghg1ofp) and (myanmar, 1, aljsxt6v0a30) are the same in 1132 ms.
Task 180: correctly said that (korean, 39, a2px0aywxngce) and (tagalog, 5, a2du3880jbrov) are not the same in 1264 ms.
Task 181: incorrectly said that (sylheti, 20, ay0bjcgydhix) and (sylheti, 17, a1a25pyfoa9ge) are the same in 1449 ms.
Task 182: correctly said that (tagalog, 5, amynpl5xzw6q) and (tagalog, 2, amynpl5xzw6q) are not the same in 987 ms.
Task 183: correctly said that (korean, 4, a1manxzopzden) and (tagalog, 10, a3qat9qkqumi2) are not the same in 717 ms.
Task 184: correctly said that (myanmar, 31, a1j8s7giyto4a) and (tagalog, 6, asu813kb7bv1) are not the same in 947 ms.
Task 185: correctly said that (tagalog, 2, ay0bjcgydhix) and (atemayar, 10, a1qku9mccvl35) are not the same in 826 ms.
Task 186: correctly said that (tagalog, 10, a1ehrxjplsq0r) and (sylheti, 28, a1foggtlgo7n4) are not the same in 660 ms.
Task 187: correctly said that (myanmar, 32, a6fjbvuzrhm6) and (myanmar, 32, a2pfktghg1ofp) are the same in 1045 ms.
Task 188: correctly said that (sylheti, 17, a1s3njxhehcip) and (tagalog, 5, aljsxt6v0a30) are not the same in 1263 ms.
Task 189: correctly said that (sylheti, 19, a1a25pyfoa9ge) and (myanmar, 31, a4858n7ggst4) are not the same in 754 ms.
Task 190: correctly said that (sylheti, 17, aljsxt6v0a30) and (inuktitut, 1, a1qku9mccvl35) are not the same in 1169 ms.
Task 191: correctly said that (sylheti, 28, a1s3njxhehcip) and (tagalog, 10, ae0ibwt7asth) are not the same in 938 ms.
Task 192: correctly said that (myanmar, 25, a1aegunf6e081) and (myanmar, 25, a1stvgjqfl5kk) are the same in 1108 ms.
Task 193: correctly said that (tagalog, 2, avlbkm6qpb1s) and (inuktitut, 3, a1qku9mccvl35) are not the same in 636 ms.
Task 194: correctly said that (sylheti, 17, a30xq7555spo8) and (inuktitut, 1, a1z59eb5sfxpy) are not the same in 1348 ms.
Task 195: correctly said that (atemayar, 23, a6fjbvuzrhm6) and (atemayar, 23, a1qku9mccvl35) are the same in 1390 ms.
Task 196: correctly said that (sylheti, 17, a1s3njxhehcip) and (sylheti, 17, a3r9q9wqm1nyz) are the same in 1132 ms.
Task 197: correctly said that (korean, 28, a3mv1jtwep7xn) and (inuktitut, 1, a1manxzopzden) are not the same in 1106 ms.
Task 198: correctly said that (tagalog, 5, aghrkle9vf6z) and (tagalog, 5, a3m0kt6kjezd1) are the same in 1059 ms.
Task 199: incorrectly said that (sylheti, 17, a1l2fzyn31zvf) and (tagalog, 2, avlbkm6qpb1s) are the same in 2134 ms.

Duration: 17m 14s 105ms
Comments: 