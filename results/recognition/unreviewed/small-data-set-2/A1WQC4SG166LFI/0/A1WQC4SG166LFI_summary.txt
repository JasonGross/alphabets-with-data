

Summary:Short Summary:
Correct: 176
Incorrect: 24
Percent Correct: 88

Same Correct: 44
Same Incorrect: 16
Percent Same Correct: 73

Different Correct: 132
Different Incorrect: 8
Percent Different Correct: 94

Long Summary:
Task 0: correctly said that (tagalog, 2, a12rfyr8dbn79) and (myanmar, 32, a1umhcrj9zfdp) are not the same in 1619 ms.
Task 1: correctly said that (korean, 28, a1udqv4pfypkn) and (inuktitut, 2, a2qnqd7m8zfbo) are not the same in 1042 ms.
Task 2: correctly said that (tagalog, 5, a1yovlluv1c9h) and (korean, 4, a39ivbdm1ekue) are not the same in 1272 ms.
Task 3: incorrectly said that (korean, 39, a3bb99mn3zy37) and (korean, 39, a1s3njxhehcip) are not the same in 683 ms.
Task 4: correctly said that (inuktitut, 2, a1j8s7giyto4a) and (tagalog, 6, asu813kb7bv1) are not the same in 764 ms.
Task 5: correctly said that (sylheti, 19, a1j8s7giyto4a) and (atemayar, 10, a23psx2z4f65q) are not the same in 607 ms.
Task 6: correctly said that (myanmar, 32, ajf66jwen7bx) and (tagalog, 2, avlbkm6qpb1s) are not the same in 582 ms.
Task 7: correctly said that (korean, 29, a1manxzopzden) and (sylheti, 28, aljsxt6v0a30) are not the same in 570 ms.
Task 8: incorrectly said that (atemayar, 23, a1j8s7giyto4a) and (atemayar, 23, a1qku9mccvl35) are not the same in 926 ms.
Task 9: incorrectly said that (atemayar, 23, a1z59eb5sfxpy) and (atemayar, 23, a3m0kt6kjezd1) are not the same in 577 ms.
Task 10: correctly said that (tagalog, 5, ajf66jwen7bx) and (sylheti, 20, a3209gno9duil) are not the same in 2555 ms.
Task 11: correctly said that (korean, 4, aj6upe9mlarg) and (korean, 4, a2akud1gdstsd) are the same in 1577 ms.
Task 12: correctly said that (sylheti, 20, a2du3880jbrov) and (myanmar, 1, ay0bjcgydhix) are not the same in 1253 ms.
Task 13: correctly said that (korean, 4, a1v0p79fynz9j) and (atemayar, 25, a1j8s7giyto4a) are not the same in 1910 ms.
Task 14: correctly said that (sylheti, 19, a3pkun3onkucn) and (myanmar, 1, adc8vbkoocrr) are not the same in 950 ms.
Task 15: correctly said that (sylheti, 17, avlbkm6qpb1s) and (atemayar, 23, a1qku9mccvl35) are not the same in 1419 ms.
Task 16: correctly said that (tagalog, 10, ay0bjcgydhix) and (atemayar, 25, a1yovlluv1c9h) are not the same in 1153 ms.
Task 17: correctly said that (myanmar, 25, a1umhcrj9zfdp) and (korean, 28, a2ernxe6jlm4x) are not the same in 1190 ms.
Task 18: incorrectly said that (tagalog, 10, aljsxt6v0a30) and (tagalog, 10, a1ehrxjplsq0r) are not the same in 1692 ms.
Task 19: correctly said that (inuktitut, 2, ae0ibwt7asth) and (inuktitut, 2, a2pgu4pi93b1q) are the same in 1126 ms.
Task 20: correctly said that (sylheti, 19, a6fjbvuzrhm6) and (korean, 39, a1manxzopzden) are not the same in 1870 ms.
Task 21: correctly said that (korean, 28, a2akud1gdstsd) and (myanmar, 1, a1foggtlgo7n4) are not the same in 1197 ms.
Task 22: correctly said that (atemayar, 10, agst3co6usf8) and (inuktitut, 3, ae0ibwt7asth) are not the same in 1064 ms.
Task 23: correctly said that (sylheti, 19, a1j8s7giyto4a) and (sylheti, 19, a1foggtlgo7n4) are the same in 1185 ms.
Task 24: incorrectly said that (tagalog, 5, a1l2fzyn31zvf) and (atemayar, 25, a3m0kt6kjezd1) are the same in 1484 ms.
Task 25: correctly said that (korean, 4, a1umhcrj9zfdp) and (tagalog, 6, a1l2fzyn31zvf) are not the same in 1807 ms.
Task 26: correctly said that (atemayar, 6, a1j8s7giyto4a) and (korean, 4, a2akud1gdstsd) are not the same in 1742 ms.
Task 27: correctly said that (inuktitut, 11, a1s3njxhehcip) and (atemayar, 23, ae0ibwt7asth) are not the same in 1074 ms.
Task 28: correctly said that (atemayar, 25, a1poymbentexx) and (inuktitut, 3, a3qat9qkqumi2) are not the same in 1132 ms.
Task 29: correctly said that (atemayar, 10, a1manxzopzden) and (myanmar, 32, a3j1pp96x14u0) are not the same in 1459 ms.
Task 30: correctly said that (tagalog, 10, ajf66jwen7bx) and (tagalog, 10, ay0bjcgydhix) are the same in 1553 ms.
Task 31: correctly said that (tagalog, 5, a1s3njxhehcip) and (tagalog, 5, asu813kb7bv1) are the same in 1659 ms.
Task 32: incorrectly said that (korean, 29, a1umhcrj9zfdp) and (korean, 29, a39ivbdm1ekue) are not the same in 2107 ms.
Task 33: incorrectly said that (sylheti, 17, a3209gno9duil) and (sylheti, 20, a1l2fzyn31zvf) are the same in 1314 ms.
Task 34: correctly said that (sylheti, 19, a3m0kt6kjezd1) and (tagalog, 2, aljsxt6v0a30) are not the same in 1371 ms.
Task 35: correctly said that (korean, 4, a2ernxe6jlm4x) and (tagalog, 10, a1yovlluv1c9h) are not the same in 1296 ms.
Task 36: correctly said that (atemayar, 6, a1poymbentexx) and (sylheti, 17, a1s3njxhehcip) are not the same in 1758 ms.
Task 37: correctly said that (korean, 4, a3mv1jtwep7xn) and (myanmar, 25, a1clejhbx9yyn) are not the same in 1288 ms.
Task 38: correctly said that (myanmar, 1, a1clejhbx9yyn) and (myanmar, 1, ajf66jwen7bx) are the same in 1397 ms.
Task 39: correctly said that (tagalog, 2, a3m0kt6kjezd1) and (myanmar, 25, a3qat9qkqumi2) are not the same in 1572 ms.
Task 40: correctly said that (korean, 29, a39ivbdm1ekue) and (sylheti, 17, atvyxlwe5n03) are not the same in 1035 ms.
Task 41: correctly said that (sylheti, 28, a1l2fzyn31zvf) and (inuktitut, 3, a315xddd8j4x8) are not the same in 995 ms.
Task 42: correctly said that (sylheti, 19, a4858n7ggst4) and (tagalog, 5, amynpl5xzw6q) are not the same in 972 ms.
Task 43: correctly said that (inuktitut, 11, a12xm86d2nbih) and (myanmar, 1, a1umhcrj9zfdp) are not the same in 1065 ms.
Task 44: correctly said that (inuktitut, 3, a315xddd8j4x8) and (inuktitut, 3, a2ernxe6jlm4x) are the same in 1027 ms.
Task 45: incorrectly said that (myanmar, 25, aljsxt6v0a30) and (myanmar, 25, a1umhcrj9zfdp) are not the same in 2091 ms.
Task 46: correctly said that (atemayar, 23, agst3co6usf8) and (atemayar, 23, a31e6jchki335) are the same in 1204 ms.
Task 47: correctly said that (korean, 39, a3dfh6dpi11ip) and (myanmar, 31, ajf66jwen7bx) are not the same in 1128 ms.
Task 48: correctly said that (korean, 28, a3mv1jtwep7xn) and (korean, 28, a1manxzopzden) are the same in 1107 ms.
Task 49: correctly said that (atemayar, 6, a1z59eb5sfxpy) and (sylheti, 28, a3np21apfa18b) are not the same in 1093 ms.
Task 50: correctly said that (myanmar, 25, ay0bjcgydhix) and (korean, 39, a1v0p79fynz9j) are not the same in 1151 ms.
Task 51: correctly said that (atemayar, 25, a1qku9mccvl35) and (myanmar, 31, ay0bjcgydhix) are not the same in 855 ms.
Task 52: correctly said that (tagalog, 2, asu813kb7bv1) and (atemayar, 6, a1poymbentexx) are not the same in 1140 ms.
Task 53: incorrectly said that (korean, 28, a1udqv4pfypkn) and (korean, 28, a3bb99mn3zy37) are not the same in 1576 ms.
Task 54: correctly said that (inuktitut, 3, a315xddd8j4x8) and (myanmar, 1, a1j8s7giyto4a) are not the same in 1134 ms.
Task 55: correctly said that (myanmar, 25, a2pfktghg1ofp) and (myanmar, 25, a3pkun3onkucn) are the same in 1254 ms.
Task 56: correctly said that (sylheti, 17, atvyxlwe5n03) and (myanmar, 25, a4858n7ggst4) are not the same in 753 ms.
Task 57: correctly said that (korean, 29, a3bb99mn3zy37) and (korean, 29, aw82oruamauz) are the same in 1718 ms.
Task 58: correctly said that (atemayar, 6, a1j8s7giyto4a) and (inuktitut, 3, a2pfktghg1ofp) are not the same in 796 ms.
Task 59: correctly said that (tagalog, 5, ae0ibwt7asth) and (sylheti, 17, a3pkun3onkucn) are not the same in 2122 ms.
Task 60: correctly said that (inuktitut, 3, ae0ibwt7asth) and (inuktitut, 3, a12xm86d2nbih) are the same in 1457 ms.
Task 61: correctly said that (sylheti, 17, a2du3880jbrov) and (korean, 29, a39ivbdm1ekue) are not the same in 1112 ms.
Task 62: correctly said that (atemayar, 23, ae0ibwt7asth) and (myanmar, 25, a6fjbvuzrhm6) are not the same in 870 ms.
Task 63: correctly said that (inuktitut, 3, ae0ibwt7asth) and (inuktitut, 3, a12xm86d2nbih) are the same in 1149 ms.
Task 64: correctly said that (korean, 39, a1umhcrj9zfdp) and (korean, 39, a2ernxe6jlm4x) are the same in 3293 ms.
Task 65: correctly said that (atemayar, 6, aljsxt6v0a30) and (myanmar, 25, avlbkm6qpb1s) are not the same in 769 ms.
Task 66: correctly said that (atemayar, 10, avlbkm6qpb1s) and (korean, 29, a2nm1qzuh14ls) are not the same in 1478 ms.
Task 67: correctly said that (korean, 28, a1v0p79fynz9j) and (inuktitut, 3, a3mv1jtwep7xn) are not the same in 795 ms.
Task 68: correctly said that (sylheti, 17, a3m0kt6kjezd1) and (atemayar, 23, agst3co6usf8) are not the same in 928 ms.
Task 69: correctly said that (korean, 28, a2nm1qzuh14ls) and (atemayar, 6, a1z59eb5sfxpy) are not the same in 905 ms.
Task 70: correctly said that (korean, 4, a3dfh6dpi11ip) and (korean, 4, a2akud1gdstsd) are the same in 1216 ms.
Task 71: correctly said that (korean, 28, a2ernxe6jlm4x) and (myanmar, 32, a3pkun3onkucn) are not the same in 858 ms.
Task 72: incorrectly said that (atemayar, 25, a1vzhyp8yvxkz) and (atemayar, 23, a3m0kt6kjezd1) are the same in 1576 ms.
Task 73: correctly said that (myanmar, 32, a1umhcrj9zfdp) and (myanmar, 32, a1foggtlgo7n4) are the same in 1786 ms.
Task 74: correctly said that (myanmar, 31, a1clejhbx9yyn) and (myanmar, 31, ajf66jwen7bx) are the same in 1352 ms.
Task 75: correctly said that (inuktitut, 11, aljsxt6v0a30) and (inuktitut, 1, a315xddd8j4x8) are not the same in 963 ms.
Task 76: correctly said that (sylheti, 19, atvyxlwe5n03) and (myanmar, 25, a1s3njxhehcip) are not the same in 929 ms.
Task 77: correctly said that (tagalog, 10, a1j8s7giyto4a) and (tagalog, 6, ae0ibwt7asth) are not the same in 938 ms.
Task 78: correctly said that (sylheti, 17, a1l2fzyn31zvf) and (myanmar, 32, a1aegunf6e081) are not the same in 730 ms.
Task 79: correctly said that (atemayar, 10, a1z59eb5sfxpy) and (atemayar, 23, a1vzhyp8yvxkz) are not the same in 847 ms.
Task 80: correctly said that (inuktitut, 1, a12xm86d2nbih) and (inuktitut, 2, a2pgu4pi93b1q) are not the same in 2711 ms.
Task 81: incorrectly said that (atemayar, 6, agst3co6usf8) and (atemayar, 6, aljsxt6v0a30) are not the same in 1636 ms.
Task 82: correctly said that (atemayar, 25, ax5gghhj0o6g) and (tagalog, 2, a3qat9qkqumi2) are not the same in 1016 ms.
Task 83: correctly said that (inuktitut, 2, a1manxzopzden) and (myanmar, 1, a1ehrxjplsq0r) are not the same in 857 ms.
Task 84: correctly said that (korean, 4, ae0ibwt7asth) and (atemayar, 10, ae0ibwt7asth) are not the same in 923 ms.
Task 85: correctly said that (tagalog, 6, a1l2fzyn31zvf) and (korean, 28, aw82oruamauz) are not the same in 826 ms.
Task 86: correctly said that (korean, 39, a3bb99mn3zy37) and (atemayar, 10, aljsxt6v0a30) are not the same in 758 ms.
Task 87: correctly said that (sylheti, 17, a3r9q9wqm1nyz) and (sylheti, 17, avlbkm6qpb1s) are the same in 1371 ms.
Task 88: incorrectly said that (atemayar, 25, aljsxt6v0a30) and (atemayar, 6, a1z59eb5sfxpy) are the same in 1776 ms.
Task 89: correctly said that (myanmar, 25, a6fjbvuzrhm6) and (inuktitut, 11, a1j8s7giyto4a) are not the same in 2465 ms.
Task 90: correctly said that (tagalog, 2, a1yovlluv1c9h) and (inuktitut, 11, a3mv1jtwep7xn) are not the same in 1524 ms.
Task 91: correctly said that (korean, 29, aj6upe9mlarg) and (korean, 29, a1s3njxhehcip) are the same in 2553 ms.
Task 92: correctly said that (sylheti, 17, a3np21apfa18b) and (tagalog, 5, a3qat9qkqumi2) are not the same in 903 ms.
Task 93: correctly said that (myanmar, 31, a1j8s7giyto4a) and (sylheti, 19, a4858n7ggst4) are not the same in 917 ms.
Task 94: incorrectly said that (atemayar, 25, agst3co6usf8) and (atemayar, 25, ax5gghhj0o6g) are not the same in 954 ms.
Task 95: correctly said that (inuktitut, 3, a1w3fo0d7cf5t) and (myanmar, 1, ajf66jwen7bx) are not the same in 1115 ms.
Task 96: correctly said that (sylheti, 20, a1a25pyfoa9ge) and (sylheti, 20, a1qku9mccvl35) are the same in 1441 ms.
Task 97: correctly said that (tagalog, 6, a1s3njxhehcip) and (tagalog, 6, aghrkle9vf6z) are the same in 1556 ms.
Task 98: correctly said that (sylheti, 28, atvyxlwe5n03) and (atemayar, 6, a31xqftzcsia2) are not the same in 1594 ms.
Task 99: correctly said that (atemayar, 25, azdw6062ia38) and (myanmar, 31, aljsxt6v0a30) are not the same in 848 ms.
Task 100: correctly said that (tagalog, 5, amynpl5xzw6q) and (inuktitut, 1, a12xm86d2nbih) are not the same in 1277 ms.
Task 101: correctly said that (atemayar, 6, a3m0kt6kjezd1) and (atemayar, 6, agst3co6usf8) are the same in 1430 ms.
Task 102: correctly said that (myanmar, 25, a4858n7ggst4) and (myanmar, 25, a1ehrxjplsq0r) are the same in 1174 ms.
Task 103: incorrectly said that (myanmar, 32, a1s3njxhehcip) and (myanmar, 31, ajf66jwen7bx) are the same in 1439 ms.
Task 104: correctly said that (sylheti, 28, a2du3880jbrov) and (sylheti, 28, a3m0kt6kjezd1) are the same in 1404 ms.
Task 105: correctly said that (sylheti, 19, a1foggtlgo7n4) and (sylheti, 19, a1l2fzyn31zvf) are the same in 1557 ms.
Task 106: correctly said that (korean, 39, aj6upe9mlarg) and (tagalog, 5, a1s3njxhehcip) are not the same in 1572 ms.
Task 107: correctly said that (sylheti, 19, ay0bjcgydhix) and (korean, 39, a2nm1qzuh14ls) are not the same in 915 ms.
Task 108: correctly said that (inuktitut, 2, a12rfyr8dbn79) and (inuktitut, 1, a1z59eb5sfxpy) are not the same in 879 ms.
Task 109: correctly said that (atemayar, 23, a23psx2z4f65q) and (sylheti, 17, a1a25pyfoa9ge) are not the same in 3087 ms.
Task 110: correctly said that (inuktitut, 2, a1s3njxhehcip) and (sylheti, 19, avlbkm6qpb1s) are not the same in 1022 ms.
Task 111: incorrectly said that (atemayar, 10, a23psx2z4f65q) and (atemayar, 10, agst3co6usf8) are not the same in 1205 ms.
Task 112: correctly said that (atemayar, 23, azdw6062ia38) and (myanmar, 32, a1j8s7giyto4a) are not the same in 830 ms.
Task 113: correctly said that (myanmar, 31, a1aegunf6e081) and (sylheti, 19, a1l2fzyn31zvf) are not the same in 850 ms.
Task 114: correctly said that (myanmar, 32, avlbkm6qpb1s) and (korean, 28, a1umhcrj9zfdp) are not the same in 659 ms.
Task 115: correctly said that (inuktitut, 11, a1j8s7giyto4a) and (inuktitut, 11, a2qnqd7m8zfbo) are the same in 1432 ms.
Task 116: correctly said that (tagalog, 6, ay0bjcgydhix) and (atemayar, 25, agst3co6usf8) are not the same in 1261 ms.
Task 117: correctly said that (atemayar, 25, a23psx2z4f65q) and (korean, 28, a3dfh6dpi11ip) are not the same in 793 ms.
Task 118: correctly said that (tagalog, 6, aghrkle9vf6z) and (korean, 29, a1v0p79fynz9j) are not the same in 751 ms.
Task 119: correctly said that (tagalog, 5, a2du3880jbrov) and (korean, 29, a1umhcrj9zfdp) are not the same in 1592 ms.
Task 120: correctly said that (sylheti, 20, a3pkun3onkucn) and (myanmar, 31, a3pkun3onkucn) are not the same in 1158 ms.
Task 121: correctly said that (atemayar, 25, a1poymbentexx) and (myanmar, 1, ajf66jwen7bx) are not the same in 746 ms.
Task 122: correctly said that (myanmar, 1, amynpl5xzw6q) and (inuktitut, 3, a2pfktghg1ofp) are not the same in 802 ms.
Task 123: correctly said that (myanmar, 31, a1umhcrj9zfdp) and (myanmar, 31, a3j1pp96x14u0) are the same in 2914 ms.
Task 124: correctly said that (sylheti, 20, a3np21apfa18b) and (tagalog, 2, a12rfyr8dbn79) are not the same in 733 ms.
Task 125: correctly said that (myanmar, 32, a1foggtlgo7n4) and (myanmar, 32, a3qat9qkqumi2) are the same in 1921 ms.
Task 126: correctly said that (sylheti, 17, avlbkm6qpb1s) and (tagalog, 10, ae0ibwt7asth) are not the same in 884 ms.
Task 127: correctly said that (atemayar, 25, a3m0kt6kjezd1) and (atemayar, 23, a3m0kt6kjezd1) are not the same in 932 ms.
Task 128: correctly said that (atemayar, 23, a1j8s7giyto4a) and (tagalog, 10, a3qat9qkqumi2) are not the same in 884 ms.
Task 129: incorrectly said that (sylheti, 28, a1qku9mccvl35) and (sylheti, 28, a1s3njxhehcip) are not the same in 1298 ms.
Task 130: correctly said that (korean, 39, aw82oruamauz) and (korean, 39, a3d7xwp0gand5) are the same in 2017 ms.
Task 131: correctly said that (korean, 28, a1v0p79fynz9j) and (sylheti, 28, ay0bjcgydhix) are not the same in 1386 ms.
Task 132: correctly said that (tagalog, 2, avlbkm6qpb1s) and (inuktitut, 1, a3noo9k3y1yu5) are not the same in 891 ms.
Task 133: correctly said that (tagalog, 5, a1s3njxhehcip) and (atemayar, 23, a1vzhyp8yvxkz) are not the same in 861 ms.
Task 134: correctly said that (atemayar, 10, agst3co6usf8) and (sylheti, 17, amynpl5xzw6q) are not the same in 1032 ms.
Task 135: correctly said that (atemayar, 10, a23psx2z4f65q) and (inuktitut, 1, a1qku9mccvl35) are not the same in 778 ms.
Task 136: correctly said that (sylheti, 20, a1a25pyfoa9ge) and (atemayar, 25, a1manxzopzden) are not the same in 772 ms.
Task 137: correctly said that (korean, 29, aav4pzxdoebd) and (sylheti, 19, a6fjbvuzrhm6) are not the same in 727 ms.
Task 138: correctly said that (atemayar, 25, a1yovlluv1c9h) and (inuktitut, 2, a3noo9k3y1yu5) are not the same in 778 ms.
Task 139: correctly said that (myanmar, 1, a3j1pp96x14u0) and (atemayar, 23, a1z59eb5sfxpy) are not the same in 724 ms.
Task 140: correctly said that (myanmar, 32, a3j1pp96x14u0) and (inuktitut, 2, a12rfyr8dbn79) are not the same in 1221 ms.
Task 141: correctly said that (inuktitut, 1, a1s3njxhehcip) and (tagalog, 2, a1yovlluv1c9h) are not the same in 785 ms.
Task 142: correctly said that (inuktitut, 11, a1s3njxhehcip) and (myanmar, 32, a1s3njxhehcip) are not the same in 733 ms.
Task 143: correctly said that (myanmar, 31, a1ehrxjplsq0r) and (tagalog, 5, ajf66jwen7bx) are not the same in 738 ms.
Task 144: correctly said that (atemayar, 25, a1qku9mccvl35) and (atemayar, 25, a23psx2z4f65q) are the same in 2238 ms.
Task 145: correctly said that (inuktitut, 11, aljsxt6v0a30) and (inuktitut, 11, a12xm86d2nbih) are the same in 1220 ms.
Task 146: correctly said that (myanmar, 31, amynpl5xzw6q) and (myanmar, 32, aljsxt6v0a30) are not the same in 2636 ms.
Task 147: correctly said that (atemayar, 6, a31e6jchki335) and (inuktitut, 1, a1z59eb5sfxpy) are not the same in 804 ms.
Task 148: correctly said that (tagalog, 10, asu813kb7bv1) and (korean, 28, a1v0p79fynz9j) are not the same in 758 ms.
Task 149: correctly said that (atemayar, 23, a2pfktghg1ofp) and (atemayar, 23, a1manxzopzden) are the same in 1364 ms.
Task 150: correctly said that (myanmar, 25, ay0bjcgydhix) and (korean, 29, a3mv1jtwep7xn) are not the same in 1292 ms.
Task 151: correctly said that (tagalog, 10, a1a25pyfoa9ge) and (korean, 4, a1s3njxhehcip) are not the same in 747 ms.
Task 152: correctly said that (sylheti, 28, a3m0kt6kjezd1) and (myanmar, 25, a3qat9qkqumi2) are not the same in 729 ms.
Task 153: correctly said that (sylheti, 17, a1qku9mccvl35) and (atemayar, 6, a1z59eb5sfxpy) are not the same in 631 ms.
Task 154: correctly said that (atemayar, 23, a31xqftzcsia2) and (atemayar, 23, a1j8s7giyto4a) are the same in 1175 ms.
Task 155: correctly said that (myanmar, 25, a2pfktghg1ofp) and (atemayar, 25, agst3co6usf8) are not the same in 667 ms.
Task 156: correctly said that (myanmar, 25, amynpl5xzw6q) and (atemayar, 25, a31e6jchki335) are not the same in 681 ms.
Task 157: correctly said that (atemayar, 10, a1yovlluv1c9h) and (myanmar, 31, adc8vbkoocrr) are not the same in 694 ms.
Task 158: incorrectly said that (myanmar, 25, a1umhcrj9zfdp) and (myanmar, 25, amynpl5xzw6q) are not the same in 978 ms.
Task 159: correctly said that (myanmar, 25, a1umhcrj9zfdp) and (korean, 4, a2akud1gdstsd) are not the same in 664 ms.
Task 160: correctly said that (korean, 29, aj6upe9mlarg) and (korean, 29, a2akud1gdstsd) are the same in 1165 ms.
Task 161: correctly said that (tagalog, 10, aghrkle9vf6z) and (atemayar, 6, a6fjbvuzrhm6) are not the same in 801 ms.
Task 162: incorrectly said that (sylheti, 17, a1qku9mccvl35) and (sylheti, 17, aljsxt6v0a30) are not the same in 860 ms.
Task 163: incorrectly said that (atemayar, 10, ax5gghhj0o6g) and (atemayar, 10, a1vzhyp8yvxkz) are not the same in 769 ms.
Task 164: correctly said that (atemayar, 6, a31e6jchki335) and (tagalog, 5, a1a25pyfoa9ge) are not the same in 798 ms.
Task 165: correctly said that (sylheti, 17, a3pkun3onkucn) and (sylheti, 17, a4858n7ggst4) are the same in 1468 ms.
Task 166: incorrectly said that (atemayar, 23, a1poymbentexx) and (atemayar, 23, aljsxt6v0a30) are not the same in 1767 ms.
Task 167: correctly said that (korean, 4, a3bb99mn3zy37) and (inuktitut, 3, a1qku9mccvl35) are not the same in 657 ms.
Task 168: correctly said that (inuktitut, 3, a12xm86d2nbih) and (sylheti, 19, a1foggtlgo7n4) are not the same in 783 ms.
Task 169: correctly said that (inuktitut, 3, a1j8s7giyto4a) and (atemayar, 6, a1j8s7giyto4a) are not the same in 780 ms.
Task 170: correctly said that (sylheti, 20, a3pkun3onkucn) and (tagalog, 6, a1s3njxhehcip) are not the same in 1017 ms.
Task 171: correctly said that (korean, 4, a1oh82lwr5hbg) and (korean, 4, a2nm1qzuh14ls) are the same in 1183 ms.
Task 172: correctly said that (sylheti, 28, a3r9q9wqm1nyz) and (korean, 39, a1oh82lwr5hbg) are not the same in 760 ms.
Task 173: correctly said that (korean, 28, a1manxzopzden) and (atemayar, 10, a1z59eb5sfxpy) are not the same in 831 ms.
Task 174: correctly said that (inuktitut, 1, a2qnqd7m8zfbo) and (korean, 39, a3dfh6dpi11ip) are not the same in 839 ms.
Task 175: correctly said that (sylheti, 20, a4858n7ggst4) and (myanmar, 31, a1ehrxjplsq0r) are not the same in 606 ms.
Task 176: correctly said that (atemayar, 6, ax5gghhj0o6g) and (atemayar, 6, aljsxt6v0a30) are the same in 1392 ms.
Task 177: incorrectly said that (korean, 28, a5379luit3pc) and (korean, 28, a3d7xwp0gand5) are not the same in 1075 ms.
Task 178: incorrectly said that (sylheti, 28, a3r9q9wqm1nyz) and (sylheti, 20, a3r9q9wqm1nyz) are the same in 1486 ms.
Task 179: correctly said that (korean, 39, aw82oruamauz) and (korean, 29, a5379luit3pc) are not the same in 734 ms.
Task 180: correctly said that (korean, 28, a2akud1gdstsd) and (myanmar, 25, adc8vbkoocrr) are not the same in 1125 ms.
Task 181: correctly said that (korean, 28, a3bb99mn3zy37) and (korean, 28, aw82oruamauz) are the same in 1399 ms.
Task 182: correctly said that (atemayar, 6, a1manxzopzden) and (tagalog, 6, a1yovlluv1c9h) are not the same in 754 ms.
Task 183: incorrectly said that (sylheti, 20, avlbkm6qpb1s) and (sylheti, 28, a3209gno9duil) are the same in 1638 ms.
Task 184: correctly said that (sylheti, 17, a3209gno9duil) and (sylheti, 17, ay0bjcgydhix) are the same in 1205 ms.
Task 185: correctly said that (inuktitut, 3, a2pfktghg1ofp) and (atemayar, 10, a3m0kt6kjezd1) are not the same in 790 ms.
Task 186: incorrectly said that (atemayar, 6, a3m0kt6kjezd1) and (sylheti, 17, atvyxlwe5n03) are the same in 1619 ms.
Task 187: correctly said that (tagalog, 5, a1yovlluv1c9h) and (tagalog, 5, a1i92aacrkacw) are the same in 1098 ms.
Task 188: correctly said that (inuktitut, 11, a2pfktghg1ofp) and (inuktitut, 11, aljsxt6v0a30) are the same in 1350 ms.
Task 189: correctly said that (sylheti, 17, atvyxlwe5n03) and (korean, 28, a2ernxe6jlm4x) are not the same in 1412 ms.
Task 190: correctly said that (tagalog, 5, aljsxt6v0a30) and (atemayar, 25, a1yovlluv1c9h) are not the same in 937 ms.
Task 191: correctly said that (inuktitut, 1, a315xddd8j4x8) and (myanmar, 25, amynpl5xzw6q) are not the same in 858 ms.
Task 192: correctly said that (tagalog, 5, a1a25pyfoa9ge) and (sylheti, 19, a2du3880jbrov) are not the same in 1072 ms.
Task 193: correctly said that (atemayar, 25, a23psx2z4f65q) and (inuktitut, 3, a1manxzopzden) are not the same in 647 ms.
Task 194: correctly said that (myanmar, 31, a2pfktghg1ofp) and (myanmar, 31, a1clejhbx9yyn) are the same in 2300 ms.
Task 195: correctly said that (inuktitut, 11, a1qku9mccvl35) and (inuktitut, 11, a1z59eb5sfxpy) are the same in 1188 ms.
Task 196: correctly said that (sylheti, 20, a1foggtlgo7n4) and (inuktitut, 1, a12xm86d2nbih) are not the same in 778 ms.
Task 197: correctly said that (myanmar, 32, a2pfktghg1ofp) and (korean, 39, a1v0p79fynz9j) are not the same in 798 ms.
Task 198: correctly said that (tagalog, 10, ae0ibwt7asth) and (tagalog, 5, aljsxt6v0a30) are not the same in 893 ms.
Task 199: correctly said that (inuktitut, 2, ay0bjcgydhix) and (inuktitut, 2, a2ernxe6jlm4x) are the same in 1383 ms.

Duration: 18m 21s 129ms
Comments: 