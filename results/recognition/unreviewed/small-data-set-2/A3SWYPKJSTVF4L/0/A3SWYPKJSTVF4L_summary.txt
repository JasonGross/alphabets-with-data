

Summary:Short Summary:
Correct: 111
Incorrect: 89
Percent Correct: 55

Same Correct: 11
Same Incorrect: 39
Percent Same Correct: 22

Different Correct: 100
Different Incorrect: 50
Percent Different Correct: 66

Long Summary:
Task 0: incorrectly said that (korean, 4, aav4pzxdoebd) and (myanmar, 25, a6fjbvuzrhm6) are the same in 11116 ms.
Task 1: correctly said that (sylheti, 28, atvyxlwe5n03) and (sylheti, 28, avlbkm6qpb1s) are the same in 5909 ms.
Task 2: incorrectly said that (tagalog, 5, a3m0kt6kjezd1) and (korean, 28, a2nm1qzuh14ls) are the same in 3182 ms.
Task 3: incorrectly said that (sylheti, 20, a2du3880jbrov) and (atemayar, 10, azdw6062ia38) are the same in 3904 ms.
Task 4: incorrectly said that (tagalog, 5, ajf66jwen7bx) and (inuktitut, 2, a1manxzopzden) are the same in 1762 ms.
Task 5: incorrectly said that (atemayar, 25, azdw6062ia38) and (atemayar, 10, ax5gghhj0o6g) are the same in 4080 ms.
Task 6: correctly said that (inuktitut, 2, a2qnqd7m8zfbo) and (korean, 4, a39ivbdm1ekue) are not the same in 18921 ms.
Task 7: correctly said that (myanmar, 32, ay0bjcgydhix) and (sylheti, 20, avlbkm6qpb1s) are not the same in 6449 ms.
Task 8: incorrectly said that (atemayar, 10, a1j8s7giyto4a) and (inuktitut, 1, avlbkm6qpb1s) are the same in 3741 ms.
Task 9: incorrectly said that (tagalog, 5, a1ehrxjplsq0r) and (tagalog, 10, a1ehrxjplsq0r) are the same in 1823 ms.
Task 10: incorrectly said that (myanmar, 32, a4858n7ggst4) and (inuktitut, 1, a1qku9mccvl35) are the same in 2833 ms.
Task 11: incorrectly said that (myanmar, 32, a4858n7ggst4) and (myanmar, 1, a1stvgjqfl5kk) are the same in 6505 ms.
Task 12: incorrectly said that (myanmar, 31, a6fjbvuzrhm6) and (myanmar, 31, a2pfktghg1ofp) are not the same in 7589 ms.
Task 13: incorrectly said that (korean, 39, a5379luit3pc) and (korean, 39, a2akud1gdstsd) are not the same in 3881 ms.
Task 14: correctly said that (myanmar, 25, a2pfktghg1ofp) and (tagalog, 2, ae0ibwt7asth) are not the same in 9463 ms.
Task 15: incorrectly said that (tagalog, 2, a3mv1jtwep7xn) and (atemayar, 10, a1manxzopzden) are the same in 2911 ms.
Task 16: correctly said that (atemayar, 25, aljsxt6v0a30) and (inuktitut, 11, a3mv1jtwep7xn) are not the same in 3337 ms.
Task 17: correctly said that (myanmar, 1, a1aegunf6e081) and (korean, 28, a39ivbdm1ekue) are not the same in 2813 ms.
Task 18: incorrectly said that (myanmar, 1, a1j8s7giyto4a) and (sylheti, 20, a1a25pyfoa9ge) are the same in 8700 ms.
Task 19: incorrectly said that (tagalog, 6, a12rfyr8dbn79) and (tagalog, 6, asu813kb7bv1) are not the same in 6738 ms.
Task 20: incorrectly said that (tagalog, 5, a1yovlluv1c9h) and (korean, 39, aw82oruamauz) are the same in 3671 ms.
Task 21: correctly said that (korean, 28, a2nm1qzuh14ls) and (sylheti, 17, a1j8s7giyto4a) are not the same in 8518 ms.
Task 22: incorrectly said that (sylheti, 19, a30xq7555spo8) and (tagalog, 2, ay0bjcgydhix) are the same in 6300 ms.
Task 23: correctly said that (atemayar, 25, a2pfktghg1ofp) and (korean, 4, a2akud1gdstsd) are not the same in 5996 ms.
Task 24: incorrectly said that (inuktitut, 11, a2pfktghg1ofp) and (atemayar, 10, a1manxzopzden) are the same in 3256 ms.
Task 25: incorrectly said that (myanmar, 32, a4858n7ggst4) and (atemayar, 10, awl8tng8e81d) are the same in 4112 ms.
Task 26: incorrectly said that (korean, 39, a1umhcrj9zfdp) and (inuktitut, 1, avlbkm6qpb1s) are the same in 6702 ms.
Task 27: incorrectly said that (tagalog, 5, ajf66jwen7bx) and (myanmar, 31, ay0bjcgydhix) are the same in 2934 ms.
Task 28: incorrectly said that (sylheti, 28, ay0bjcgydhix) and (myanmar, 32, a4858n7ggst4) are the same in 3889 ms.
Task 29: correctly said that (korean, 4, aj6upe9mlarg) and (korean, 28, a5379luit3pc) are not the same in 4505 ms.
Task 30: correctly said that (myanmar, 32, a4858n7ggst4) and (sylheti, 28, a4858n7ggst4) are not the same in 4302 ms.
Task 31: incorrectly said that (tagalog, 5, a3qat9qkqumi2) and (tagalog, 10, a1j8s7giyto4a) are the same in 2172 ms.
Task 32: incorrectly said that (inuktitut, 11, a2pgu4pi93b1q) and (inuktitut, 2, a3mv1jtwep7xn) are the same in 2325 ms.
Task 33: incorrectly said that (inuktitut, 3, a2ernxe6jlm4x) and (inuktitut, 2, a2ernxe6jlm4x) are the same in 1744 ms.
Task 34: incorrectly said that (sylheti, 20, a1s3njxhehcip) and (korean, 4, a2akud1gdstsd) are the same in 4189 ms.
Task 35: incorrectly said that (korean, 29, a1s3njxhehcip) and (korean, 29, ae0ibwt7asth) are not the same in 8786 ms.
Task 36: correctly said that (atemayar, 6, a31e6jchki335) and (myanmar, 31, a3pkun3onkucn) are not the same in 3902 ms.
Task 37: correctly said that (sylheti, 28, a1l2fzyn31zvf) and (myanmar, 32, a1aegunf6e081) are not the same in 1154 ms.
Task 38: correctly said that (inuktitut, 1, a12xm86d2nbih) and (korean, 4, a2akud1gdstsd) are not the same in 5603 ms.
Task 39: correctly said that (tagalog, 10, ae0ibwt7asth) and (atemayar, 6, a23psx2z4f65q) are not the same in 7222 ms.
Task 40: incorrectly said that (myanmar, 25, a1j8s7giyto4a) and (atemayar, 25, aljsxt6v0a30) are the same in 10294 ms.
Task 41: correctly said that (myanmar, 32, adc8vbkoocrr) and (sylheti, 19, a3209gno9duil) are not the same in 5206 ms.
Task 42: correctly said that (korean, 29, a3dfh6dpi11ip) and (atemayar, 6, a23psx2z4f65q) are not the same in 4455 ms.
Task 43: incorrectly said that (korean, 4, ae0ibwt7asth) and (atemayar, 10, a1manxzopzden) are the same in 3570 ms.
Task 44: incorrectly said that (tagalog, 5, ajf66jwen7bx) and (tagalog, 5, ay0bjcgydhix) are not the same in 11007 ms.
Task 45: incorrectly said that (myanmar, 31, a3qat9qkqumi2) and (tagalog, 2, amynpl5xzw6q) are the same in 4723 ms.
Task 46: incorrectly said that (korean, 4, aw82oruamauz) and (inuktitut, 3, ae0ibwt7asth) are the same in 2868 ms.
Task 47: correctly said that (korean, 39, a3mv1jtwep7xn) and (sylheti, 19, ay0bjcgydhix) are not the same in 4751 ms.
Task 48: correctly said that (myanmar, 32, adc8vbkoocrr) and (atemayar, 23, avlbkm6qpb1s) are not the same in 8135 ms.
Task 49: correctly said that (tagalog, 10, a1l2fzyn31zvf) and (korean, 4, aav4pzxdoebd) are not the same in 3313 ms.
Task 50: correctly said that (tagalog, 10, a1j8s7giyto4a) and (korean, 39, a3d7xwp0gand5) are not the same in 3578 ms.
Task 51: incorrectly said that (tagalog, 10, ay0bjcgydhix) and (tagalog, 5, a3m0kt6kjezd1) are the same in 8418 ms.
Task 52: correctly said that (atemayar, 6, a31xqftzcsia2) and (korean, 29, a2akud1gdstsd) are not the same in 3879 ms.
Task 53: incorrectly said that (atemayar, 10, a1qku9mccvl35) and (inuktitut, 3, a2ernxe6jlm4x) are the same in 3488 ms.
Task 54: correctly said that (myanmar, 1, a1j8s7giyto4a) and (sylheti, 28, avlbkm6qpb1s) are not the same in 2388 ms.
Task 55: correctly said that (myanmar, 25, a1foggtlgo7n4) and (atemayar, 10, a3m0kt6kjezd1) are not the same in 5241 ms.
Task 56: correctly said that (atemayar, 10, a31xqftzcsia2) and (korean, 4, a1s3njxhehcip) are not the same in 1868 ms.
Task 57: correctly said that (inuktitut, 1, a1z59eb5sfxpy) and (tagalog, 2, a3m0kt6kjezd1) are not the same in 7218 ms.
Task 58: correctly said that (inuktitut, 1, a2pfktghg1ofp) and (korean, 4, a3dfh6dpi11ip) are not the same in 2151 ms.
Task 59: incorrectly said that (korean, 39, a2akud1gdstsd) and (korean, 39, aav4pzxdoebd) are not the same in 4650 ms.
Task 60: incorrectly said that (sylheti, 20, a1foggtlgo7n4) and (sylheti, 20, a1a25pyfoa9ge) are not the same in 2037 ms.
Task 61: correctly said that (myanmar, 32, a1j8s7giyto4a) and (atemayar, 6, a31xqftzcsia2) are not the same in 2623 ms.
Task 62: incorrectly said that (atemayar, 23, aljsxt6v0a30) and (atemayar, 23, ae0ibwt7asth) are not the same in 3026 ms.
Task 63: incorrectly said that (myanmar, 1, ajf66jwen7bx) and (myanmar, 1, a3qat9qkqumi2) are not the same in 5896 ms.
Task 64: correctly said that (sylheti, 20, avlbkm6qpb1s) and (atemayar, 10, a31e6jchki335) are not the same in 3228 ms.
Task 65: correctly said that (inuktitut, 11, a3qat9qkqumi2) and (tagalog, 2, a1l2fzyn31zvf) are not the same in 16992 ms.
Task 66: correctly said that (myanmar, 25, aljsxt6v0a30) and (myanmar, 31, a3j1pp96x14u0) are not the same in 6629 ms.
Task 67: correctly said that (atemayar, 6, aljsxt6v0a30) and (inuktitut, 1, a3qat9qkqumi2) are not the same in 7910 ms.
Task 68: correctly said that (tagalog, 10, a1a25pyfoa9ge) and (sylheti, 20, a4858n7ggst4) are not the same in 1694 ms.
Task 69: incorrectly said that (myanmar, 1, a1foggtlgo7n4) and (myanmar, 1, ay0bjcgydhix) are not the same in 3569 ms.
Task 70: correctly said that (sylheti, 17, a1s3njxhehcip) and (inuktitut, 3, a315xddd8j4x8) are not the same in 5005 ms.
Task 71: correctly said that (myanmar, 31, aljsxt6v0a30) and (atemayar, 10, a1z59eb5sfxpy) are not the same in 8759 ms.
Task 72: correctly said that (sylheti, 20, a1a25pyfoa9ge) and (atemayar, 6, a31xqftzcsia2) are not the same in 5151 ms.
Task 73: incorrectly said that (sylheti, 28, amynpl5xzw6q) and (sylheti, 28, a1qku9mccvl35) are not the same in 1599 ms.
Task 74: correctly said that (atemayar, 23, awl8tng8e81d) and (tagalog, 5, a3m0kt6kjezd1) are not the same in 5144 ms.
Task 75: incorrectly said that (sylheti, 19, a3r9q9wqm1nyz) and (sylheti, 19, a6fjbvuzrhm6) are not the same in 3102 ms.
Task 76: incorrectly said that (tagalog, 10, a1j8s7giyto4a) and (inuktitut, 2, a3qat9qkqumi2) are the same in 6072 ms.
Task 77: correctly said that (inuktitut, 3, a3qat9qkqumi2) and (myanmar, 1, ajf66jwen7bx) are not the same in 19335 ms.
Task 78: incorrectly said that (atemayar, 25, aljsxt6v0a30) and (atemayar, 25, awl8tng8e81d) are not the same in 4945 ms.
Task 79: correctly said that (tagalog, 2, asu813kb7bv1) and (atemayar, 10, ae0ibwt7asth) are not the same in 10379 ms.
Task 80: correctly said that (myanmar, 1, a6fjbvuzrhm6) and (tagalog, 10, a2du3880jbrov) are not the same in 4023 ms.
Task 81: correctly said that (tagalog, 6, a3qat9qkqumi2) and (inuktitut, 1, ay0bjcgydhix) are not the same in 2667 ms.
Task 82: correctly said that (myanmar, 25, a3qat9qkqumi2) and (myanmar, 32, a1clejhbx9yyn) are not the same in 4575 ms.
Task 83: correctly said that (inuktitut, 3, a3noo9k3y1yu5) and (atemayar, 25, a1z59eb5sfxpy) are not the same in 2513 ms.
Task 84: correctly said that (inuktitut, 1, aljsxt6v0a30) and (korean, 28, a1v0p79fynz9j) are not the same in 1200 ms.
Task 85: incorrectly said that (korean, 4, aj6upe9mlarg) and (korean, 4, a3mv1jtwep7xn) are not the same in 929 ms.
Task 86: correctly said that (inuktitut, 3, a1s3njxhehcip) and (inuktitut, 3, avlbkm6qpb1s) are the same in 6337 ms.
Task 87: incorrectly said that (myanmar, 25, a6fjbvuzrhm6) and (inuktitut, 2, avlbkm6qpb1s) are the same in 3565 ms.
Task 88: correctly said that (tagalog, 2, a1yovlluv1c9h) and (atemayar, 23, agst3co6usf8) are not the same in 4645 ms.
Task 89: correctly said that (korean, 29, a3bb99mn3zy37) and (atemayar, 25, ae0ibwt7asth) are not the same in 7302 ms.
Task 90: correctly said that (tagalog, 10, aljsxt6v0a30) and (sylheti, 28, avlbkm6qpb1s) are not the same in 11296 ms.
Task 91: incorrectly said that (inuktitut, 11, a1z59eb5sfxpy) and (inuktitut, 11, a1manxzopzden) are not the same in 3939 ms.
Task 92: correctly said that (tagalog, 10, aljsxt6v0a30) and (atemayar, 10, a3m0kt6kjezd1) are not the same in 7503 ms.
Task 93: incorrectly said that (korean, 4, a1manxzopzden) and (inuktitut, 1, ay0bjcgydhix) are the same in 3817 ms.
Task 94: incorrectly said that (atemayar, 25, a3m0kt6kjezd1) and (atemayar, 25, a1z59eb5sfxpy) are not the same in 3469 ms.
Task 95: incorrectly said that (sylheti, 17, a1l2fzyn31zvf) and (sylheti, 17, a3209gno9duil) are not the same in 4093 ms.
Task 96: incorrectly said that (tagalog, 10, a1l2fzyn31zvf) and (myanmar, 1, ajf66jwen7bx) are the same in 2314 ms.
Task 97: correctly said that (sylheti, 28, atvyxlwe5n03) and (inuktitut, 11, ae0ibwt7asth) are not the same in 4491 ms.
Task 98: incorrectly said that (sylheti, 20, a3np21apfa18b) and (sylheti, 20, a2du3880jbrov) are not the same in 1819 ms.
Task 99: incorrectly said that (sylheti, 19, ay0bjcgydhix) and (sylheti, 19, a1j8s7giyto4a) are not the same in 2103 ms.
Task 100: correctly said that (tagalog, 10, a1l2fzyn31zvf) and (korean, 39, a2akud1gdstsd) are not the same in 4027 ms.
Task 101: incorrectly said that (myanmar, 25, a1ehrxjplsq0r) and (myanmar, 25, ay0bjcgydhix) are not the same in 3921 ms.
Task 102: incorrectly said that (atemayar, 25, a1j8s7giyto4a) and (atemayar, 25, a1z59eb5sfxpy) are not the same in 2493 ms.
Task 103: correctly said that (atemayar, 23, a1yovlluv1c9h) and (atemayar, 23, aljsxt6v0a30) are the same in 17341 ms.
Task 104: correctly said that (sylheti, 19, a3r9q9wqm1nyz) and (atemayar, 6, ax5gghhj0o6g) are not the same in 4078 ms.
Task 105: correctly said that (inuktitut, 11, a12xm86d2nbih) and (korean, 29, a3dfh6dpi11ip) are not the same in 7673 ms.
Task 106: incorrectly said that (myanmar, 25, a1foggtlgo7n4) and (myanmar, 25, adc8vbkoocrr) are not the same in 7122 ms.
Task 107: incorrectly said that (atemayar, 6, a1qku9mccvl35) and (atemayar, 6, a2pfktghg1ofp) are not the same in 7510 ms.
Task 108: incorrectly said that (inuktitut, 1, a2pgu4pi93b1q) and (inuktitut, 1, ae0ibwt7asth) are not the same in 4388 ms.
Task 109: incorrectly said that (atemayar, 23, a1j8s7giyto4a) and (inuktitut, 2, a1manxzopzden) are the same in 5107 ms.
Task 110: correctly said that (inuktitut, 3, a3qat9qkqumi2) and (inuktitut, 3, ae0ibwt7asth) are the same in 2602 ms.
Task 111: incorrectly said that (myanmar, 25, a4858n7ggst4) and (myanmar, 25, adc8vbkoocrr) are not the same in 14299 ms.
Task 112: incorrectly said that (myanmar, 32, a1s3njxhehcip) and (myanmar, 32, a1j8s7giyto4a) are not the same in 11191 ms.
Task 113: incorrectly said that (myanmar, 32, a1foggtlgo7n4) and (inuktitut, 3, a1w3fo0d7cf5t) are the same in 5851 ms.
Task 114: incorrectly said that (sylheti, 20, avlbkm6qpb1s) and (inuktitut, 1, a315xddd8j4x8) are the same in 2776 ms.
Task 115: correctly said that (inuktitut, 3, a2ernxe6jlm4x) and (sylheti, 17, a1j8s7giyto4a) are not the same in 6027 ms.
Task 116: incorrectly said that (sylheti, 20, aljsxt6v0a30) and (sylheti, 20, a3pkun3onkucn) are not the same in 7178 ms.
Task 117: correctly said that (myanmar, 1, ajf66jwen7bx) and (myanmar, 31, a1umhcrj9zfdp) are not the same in 16816 ms.
Task 118: correctly said that (atemayar, 23, awl8tng8e81d) and (inuktitut, 1, a1s3njxhehcip) are not the same in 6221 ms.
Task 119: incorrectly said that (sylheti, 19, a3r9q9wqm1nyz) and (sylheti, 19, a1qku9mccvl35) are not the same in 10228 ms.
Task 120: correctly said that (atemayar, 25, a2pfktghg1ofp) and (myanmar, 31, a6fjbvuzrhm6) are not the same in 12431 ms.
Task 121: incorrectly said that (sylheti, 19, a1a25pyfoa9ge) and (myanmar, 25, a3j1pp96x14u0) are the same in 4481 ms.
Task 122: correctly said that (inuktitut, 1, a1s3njxhehcip) and (inuktitut, 1, a12rfyr8dbn79) are the same in 11133 ms.
Task 123: correctly said that (inuktitut, 3, a3noo9k3y1yu5) and (inuktitut, 3, aljsxt6v0a30) are the same in 7589 ms.
Task 124: incorrectly said that (korean, 29, a3bb99mn3zy37) and (inuktitut, 11, a1z59eb5sfxpy) are the same in 11335 ms.
Task 125: correctly said that (myanmar, 25, a1s3njxhehcip) and (sylheti, 28, a4858n7ggst4) are not the same in 4279 ms.
Task 126: correctly said that (korean, 29, a1oh82lwr5hbg) and (atemayar, 23, a2pfktghg1ofp) are not the same in 5859 ms.
Task 127: incorrectly said that (sylheti, 19, a3m0kt6kjezd1) and (sylheti, 19, a3np21apfa18b) are not the same in 3690 ms.
Task 128: incorrectly said that (sylheti, 17, atvyxlwe5n03) and (sylheti, 17, a1s3njxhehcip) are not the same in 6238 ms.
Task 129: correctly said that (atemayar, 10, a1yovlluv1c9h) and (myanmar, 25, a2pfktghg1ofp) are not the same in 2822 ms.
Task 130: correctly said that (inuktitut, 2, a1z59eb5sfxpy) and (atemayar, 10, a1manxzopzden) are not the same in 7387 ms.
Task 131: correctly said that (atemayar, 6, azdw6062ia38) and (inuktitut, 1, a1manxzopzden) are not the same in 3421 ms.
Task 132: incorrectly said that (inuktitut, 1, a2pfktghg1ofp) and (inuktitut, 1, a1qku9mccvl35) are not the same in 4091 ms.
Task 133: correctly said that (inuktitut, 2, a315xddd8j4x8) and (korean, 4, a2ernxe6jlm4x) are not the same in 6022 ms.
Task 134: correctly said that (korean, 4, a1udqv4pfypkn) and (korean, 39, a2akud1gdstsd) are not the same in 4043 ms.
Task 135: correctly said that (sylheti, 28, amynpl5xzw6q) and (inuktitut, 2, a1j8s7giyto4a) are not the same in 4634 ms.
Task 136: correctly said that (atemayar, 6, azdw6062ia38) and (myanmar, 31, a1clejhbx9yyn) are not the same in 5015 ms.
Task 137: correctly said that (myanmar, 1, ay0bjcgydhix) and (tagalog, 6, a1a25pyfoa9ge) are not the same in 4979 ms.
Task 138: incorrectly said that (inuktitut, 3, ae0ibwt7asth) and (inuktitut, 3, a12rfyr8dbn79) are not the same in 4208 ms.
Task 139: correctly said that (sylheti, 19, a1j8s7giyto4a) and (myanmar, 31, a1foggtlgo7n4) are not the same in 2543 ms.
Task 140: correctly said that (korean, 39, a1oh82lwr5hbg) and (atemayar, 23, ae0ibwt7asth) are not the same in 2799 ms.
Task 141: incorrectly said that (sylheti, 19, amynpl5xzw6q) and (sylheti, 19, avlbkm6qpb1s) are not the same in 1529 ms.
Task 142: incorrectly said that (atemayar, 6, a1qku9mccvl35) and (atemayar, 10, aljsxt6v0a30) are the same in 15570 ms.
Task 143: correctly said that (inuktitut, 3, avlbkm6qpb1s) and (korean, 29, a1manxzopzden) are not the same in 3635 ms.
Task 144: incorrectly said that (inuktitut, 2, a1z59eb5sfxpy) and (myanmar, 25, a4858n7ggst4) are the same in 8028 ms.
Task 145: correctly said that (inuktitut, 2, a1s3njxhehcip) and (inuktitut, 2, a2pfktghg1ofp) are the same in 8573 ms.
Task 146: correctly said that (atemayar, 6, agst3co6usf8) and (tagalog, 2, a1yovlluv1c9h) are not the same in 8610 ms.
Task 147: incorrectly said that (tagalog, 10, ajf66jwen7bx) and (tagalog, 10, a1a25pyfoa9ge) are not the same in 6725 ms.
Task 148: incorrectly said that (sylheti, 17, a1a25pyfoa9ge) and (tagalog, 2, a3m0kt6kjezd1) are the same in 23147 ms.
Task 149: correctly said that (korean, 28, a3bb99mn3zy37) and (sylheti, 20, a1l2fzyn31zvf) are not the same in 3685 ms.
Task 150: correctly said that (sylheti, 28, atvyxlwe5n03) and (myanmar, 32, a4858n7ggst4) are not the same in 4181 ms.
Task 151: incorrectly said that (atemayar, 25, ae0ibwt7asth) and (tagalog, 6, a3qat9qkqumi2) are the same in 13837 ms.
Task 152: correctly said that (atemayar, 25, agst3co6usf8) and (sylheti, 17, a3np21apfa18b) are not the same in 4036 ms.
Task 153: correctly said that (korean, 29, ae0ibwt7asth) and (myanmar, 25, a1j8s7giyto4a) are not the same in 6040 ms.
Task 154: correctly said that (korean, 28, a1v0p79fynz9j) and (korean, 29, a1v0p79fynz9j) are not the same in 1706 ms.
Task 155: correctly said that (myanmar, 1, avlbkm6qpb1s) and (tagalog, 5, aghrkle9vf6z) are not the same in 8316 ms.
Task 156: correctly said that (atemayar, 23, a1qku9mccvl35) and (sylheti, 17, a6fjbvuzrhm6) are not the same in 2446 ms.
Task 157: correctly said that (atemayar, 6, ax5gghhj0o6g) and (myanmar, 25, a1foggtlgo7n4) are not the same in 10050 ms.
Task 158: correctly said that (inuktitut, 2, a3qat9qkqumi2) and (sylheti, 20, a3r9q9wqm1nyz) are not the same in 2928 ms.
Task 159: correctly said that (atemayar, 10, awl8tng8e81d) and (sylheti, 19, a1j8s7giyto4a) are not the same in 2931 ms.
Task 160: correctly said that (inuktitut, 1, a1z59eb5sfxpy) and (inuktitut, 1, avlbkm6qpb1s) are the same in 2808 ms.
Task 161: correctly said that (myanmar, 32, a1umhcrj9zfdp) and (myanmar, 31, a6fjbvuzrhm6) are not the same in 12391 ms.
Task 162: incorrectly said that (korean, 29, a2px0aywxngce) and (inuktitut, 2, a12rfyr8dbn79) are the same in 2752 ms.
Task 163: correctly said that (korean, 4, a3d7xwp0gand5) and (atemayar, 10, a1yovlluv1c9h) are not the same in 5805 ms.
Task 164: correctly said that (sylheti, 17, a1j8s7giyto4a) and (tagalog, 5, asu813kb7bv1) are not the same in 7366 ms.
Task 165: correctly said that (inuktitut, 3, a2pgu4pi93b1q) and (myanmar, 25, a2pfktghg1ofp) are not the same in 7386 ms.
Task 166: incorrectly said that (sylheti, 28, a3m0kt6kjezd1) and (tagalog, 10, ajf66jwen7bx) are the same in 7645 ms.
Task 167: incorrectly said that (atemayar, 10, a3m0kt6kjezd1) and (atemayar, 10, a6fjbvuzrhm6) are not the same in 2789 ms.
Task 168: correctly said that (atemayar, 25, a1z59eb5sfxpy) and (korean, 4, a2px0aywxngce) are not the same in 3457 ms.
Task 169: incorrectly said that (sylheti, 20, a2du3880jbrov) and (sylheti, 20, a1j8s7giyto4a) are not the same in 1947 ms.
Task 170: correctly said that (inuktitut, 2, ae0ibwt7asth) and (inuktitut, 2, a1j8s7giyto4a) are the same in 4087 ms.
Task 171: correctly said that (sylheti, 28, a1qku9mccvl35) and (myanmar, 1, adc8vbkoocrr) are not the same in 5568 ms.
Task 172: correctly said that (sylheti, 20, a1l2fzyn31zvf) and (sylheti, 19, a2du3880jbrov) are not the same in 1667 ms.
Task 173: correctly said that (inuktitut, 3, aljsxt6v0a30) and (atemayar, 23, a6fjbvuzrhm6) are not the same in 7431 ms.
Task 174: incorrectly said that (tagalog, 10, a1i92aacrkacw) and (tagalog, 10, a3mv1jtwep7xn) are not the same in 3394 ms.
Task 175: correctly said that (atemayar, 6, a31xqftzcsia2) and (sylheti, 28, avlbkm6qpb1s) are not the same in 3663 ms.
Task 176: incorrectly said that (sylheti, 20, a1j8s7giyto4a) and (sylheti, 20, a1a25pyfoa9ge) are not the same in 1973 ms.
Task 177: incorrectly said that (atemayar, 25, agst3co6usf8) and (inuktitut, 11, a2pfktghg1ofp) are the same in 5757 ms.
Task 178: correctly said that (myanmar, 25, amynpl5xzw6q) and (inuktitut, 1, a1manxzopzden) are not the same in 5931 ms.
Task 179: correctly said that (inuktitut, 3, a12rfyr8dbn79) and (sylheti, 20, a3209gno9duil) are not the same in 10228 ms.
Task 180: incorrectly said that (tagalog, 6, a1yovlluv1c9h) and (tagalog, 2, a1yovlluv1c9h) are the same in 2748 ms.
Task 181: correctly said that (inuktitut, 11, a2qnqd7m8zfbo) and (atemayar, 10, a1yovlluv1c9h) are not the same in 9566 ms.
Task 182: correctly said that (atemayar, 23, a3m0kt6kjezd1) and (sylheti, 28, a3np21apfa18b) are not the same in 2138 ms.
Task 183: correctly said that (sylheti, 19, a1qku9mccvl35) and (korean, 39, ae0ibwt7asth) are not the same in 12428 ms.
Task 184: correctly said that (inuktitut, 3, a3mv1jtwep7xn) and (myanmar, 31, ay0bjcgydhix) are not the same in 7873 ms.
Task 185: correctly said that (myanmar, 31, a3pkun3onkucn) and (sylheti, 20, a3r9q9wqm1nyz) are not the same in 3061 ms.
Task 186: correctly said that (korean, 29, a1v0p79fynz9j) and (atemayar, 25, avlbkm6qpb1s) are not the same in 3525 ms.
Task 187: correctly said that (inuktitut, 2, a1z59eb5sfxpy) and (inuktitut, 2, ae0ibwt7asth) are the same in 5605 ms.
Task 188: incorrectly said that (korean, 29, a1manxzopzden) and (tagalog, 5, ay0bjcgydhix) are the same in 4157 ms.
Task 189: correctly said that (tagalog, 6, ay0bjcgydhix) and (atemayar, 23, agst3co6usf8) are not the same in 4465 ms.
Task 190: incorrectly said that (tagalog, 6, ajf66jwen7bx) and (tagalog, 5, a1l2fzyn31zvf) are the same in 2405 ms.
Task 191: incorrectly said that (sylheti, 19, a30xq7555spo8) and (myanmar, 32, a1clejhbx9yyn) are the same in 3882 ms.
Task 192: correctly said that (inuktitut, 2, aljsxt6v0a30) and (inuktitut, 2, ae0ibwt7asth) are the same in 2021 ms.
Task 193: incorrectly said that (sylheti, 19, a3m0kt6kjezd1) and (tagalog, 6, ajf66jwen7bx) are the same in 1794 ms.
Task 194: correctly said that (korean, 28, a3bb99mn3zy37) and (sylheti, 28, aljsxt6v0a30) are not the same in 6005 ms.
Task 195: correctly said that (sylheti, 17, a3np21apfa18b) and (sylheti, 28, a1a25pyfoa9ge) are not the same in 3094 ms.
Task 196: incorrectly said that (sylheti, 19, a1l2fzyn31zvf) and (inuktitut, 3, avlbkm6qpb1s) are the same in 5317 ms.
Task 197: incorrectly said that (inuktitut, 3, a12xm86d2nbih) and (inuktitut, 3, a1qku9mccvl35) are not the same in 8095 ms.
Task 198: correctly said that (sylheti, 19, atvyxlwe5n03) and (korean, 28, a5379luit3pc) are not the same in 5569 ms.
Task 199: correctly said that (atemayar, 25, avlbkm6qpb1s) and (myanmar, 25, ay0bjcgydhix) are not the same in 5900 ms.

Duration: 32m 49s 910ms
Comments: eye power, observation skill improving game