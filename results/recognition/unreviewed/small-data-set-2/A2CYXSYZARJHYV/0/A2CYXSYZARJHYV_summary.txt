

Summary:Short Summary:
Correct: 156
Incorrect: 44
Percent Correct: 78

Same Correct: 33
Same Incorrect: 26
Percent Same Correct: 55

Different Correct: 123
Different Incorrect: 18
Percent Different Correct: 87

Long Summary:
Task 0: correctly said that (atemayar, 10, a1z59eb5sfxpy) and (myanmar, 25, a1ehrxjplsq0r) are not the same in 10350 ms.
Task 1: incorrectly said that (sylheti, 17, a3r9q9wqm1nyz) and (sylheti, 17, amynpl5xzw6q) are not the same in 4126 ms.
Task 2: incorrectly said that (sylheti, 17, amynpl5xzw6q) and (tagalog, 5, aljsxt6v0a30) are the same in 5966 ms.
Task 3: correctly said that (atemayar, 10, a1j8s7giyto4a) and (atemayar, 10, a1z59eb5sfxpy) are the same in 1485 ms.
Task 4: correctly said that (korean, 4, a3d7xwp0gand5) and (sylheti, 20, ay0bjcgydhix) are not the same in 1890 ms.
Task 5: incorrectly said that (myanmar, 31, amynpl5xzw6q) and (myanmar, 31, a1j8s7giyto4a) are not the same in 2241 ms.
Task 6: correctly said that (tagalog, 2, a3mv1jtwep7xn) and (inuktitut, 3, a1qku9mccvl35) are not the same in 1245 ms.
Task 7: correctly said that (sylheti, 17, aljsxt6v0a30) and (sylheti, 17, amynpl5xzw6q) are the same in 6147 ms.
Task 8: correctly said that (sylheti, 17, amynpl5xzw6q) and (inuktitut, 1, a3mv1jtwep7xn) are not the same in 1526 ms.
Task 9: incorrectly said that (atemayar, 6, a2pfktghg1ofp) and (korean, 4, a3d7xwp0gand5) are the same in 1349 ms.
Task 10: correctly said that (korean, 39, a5379luit3pc) and (korean, 28, aj6upe9mlarg) are not the same in 1306 ms.
Task 11: correctly said that (tagalog, 2, ae0ibwt7asth) and (tagalog, 2, a1l2fzyn31zvf) are the same in 1220 ms.
Task 12: correctly said that (korean, 4, a2px0aywxngce) and (korean, 4, a39ivbdm1ekue) are the same in 1587 ms.
Task 13: correctly said that (inuktitut, 3, a1s3njxhehcip) and (atemayar, 10, a1z59eb5sfxpy) are not the same in 1176 ms.
Task 14: correctly said that (atemayar, 10, a1yovlluv1c9h) and (atemayar, 10, avlbkm6qpb1s) are the same in 1056 ms.
Task 15: incorrectly said that (korean, 39, a1manxzopzden) and (korean, 39, a2ernxe6jlm4x) are not the same in 1024 ms.
Task 16: incorrectly said that (inuktitut, 11, a12rfyr8dbn79) and (inuktitut, 11, a3noo9k3y1yu5) are not the same in 853 ms.
Task 17: correctly said that (sylheti, 28, a1s3njxhehcip) and (tagalog, 10, a12rfyr8dbn79) are not the same in 1037 ms.
Task 18: incorrectly said that (sylheti, 28, a3r9q9wqm1nyz) and (korean, 29, a1v0p79fynz9j) are the same in 1039 ms.
Task 19: correctly said that (atemayar, 6, a1yovlluv1c9h) and (sylheti, 20, a4858n7ggst4) are not the same in 1053 ms.
Task 20: correctly said that (atemayar, 10, a6fjbvuzrhm6) and (tagalog, 5, a1j8s7giyto4a) are not the same in 1610 ms.
Task 21: correctly said that (myanmar, 1, avlbkm6qpb1s) and (myanmar, 31, a3pkun3onkucn) are not the same in 1398 ms.
Task 22: correctly said that (sylheti, 28, a3pkun3onkucn) and (sylheti, 19, a1s3njxhehcip) are not the same in 2215 ms.
Task 23: correctly said that (myanmar, 32, a1s3njxhehcip) and (inuktitut, 1, a1j8s7giyto4a) are not the same in 1282 ms.
Task 24: correctly said that (myanmar, 1, a6fjbvuzrhm6) and (tagalog, 2, a1s3njxhehcip) are not the same in 1065 ms.
Task 25: incorrectly said that (atemayar, 23, aljsxt6v0a30) and (myanmar, 1, a1aegunf6e081) are the same in 1345 ms.
Task 26: correctly said that (sylheti, 28, a3r9q9wqm1nyz) and (korean, 29, a3dfh6dpi11ip) are not the same in 1718 ms.
Task 27: correctly said that (myanmar, 32, a1s3njxhehcip) and (myanmar, 31, a1umhcrj9zfdp) are not the same in 1503 ms.
Task 28: correctly said that (atemayar, 23, ax5gghhj0o6g) and (sylheti, 28, avlbkm6qpb1s) are not the same in 1247 ms.
Task 29: correctly said that (tagalog, 10, a3qat9qkqumi2) and (sylheti, 28, a1qku9mccvl35) are not the same in 1034 ms.
Task 30: correctly said that (sylheti, 28, atvyxlwe5n03) and (atemayar, 6, a23psx2z4f65q) are not the same in 1367 ms.
Task 31: incorrectly said that (tagalog, 2, aljsxt6v0a30) and (tagalog, 10, asu813kb7bv1) are the same in 1639 ms.
Task 32: incorrectly said that (myanmar, 1, adc8vbkoocrr) and (korean, 39, a39ivbdm1ekue) are the same in 1138 ms.
Task 33: correctly said that (korean, 4, a1oh82lwr5hbg) and (tagalog, 5, a12rfyr8dbn79) are not the same in 981 ms.
Task 34: correctly said that (myanmar, 31, a1umhcrj9zfdp) and (inuktitut, 11, a12xm86d2nbih) are not the same in 753 ms.
Task 35: correctly said that (tagalog, 10, a1a25pyfoa9ge) and (myanmar, 1, a1clejhbx9yyn) are not the same in 781 ms.
Task 36: correctly said that (atemayar, 6, ax5gghhj0o6g) and (myanmar, 25, a1stvgjqfl5kk) are not the same in 953 ms.
Task 37: correctly said that (sylheti, 19, a1foggtlgo7n4) and (korean, 29, a3dfh6dpi11ip) are not the same in 1194 ms.
Task 38: correctly said that (myanmar, 32, ajf66jwen7bx) and (korean, 39, a1manxzopzden) are not the same in 947 ms.
Task 39: incorrectly said that (atemayar, 25, a31xqftzcsia2) and (sylheti, 20, a3209gno9duil) are the same in 1556 ms.
Task 40: incorrectly said that (atemayar, 25, a1j8s7giyto4a) and (inuktitut, 11, ay0bjcgydhix) are the same in 2006 ms.
Task 41: correctly said that (atemayar, 23, ax5gghhj0o6g) and (atemayar, 10, avlbkm6qpb1s) are not the same in 1360 ms.
Task 42: correctly said that (tagalog, 5, a1a25pyfoa9ge) and (atemayar, 6, a31e6jchki335) are not the same in 728 ms.
Task 43: correctly said that (inuktitut, 3, a2qnqd7m8zfbo) and (korean, 39, a3mv1jtwep7xn) are not the same in 723 ms.
Task 44: correctly said that (atemayar, 6, a1poymbentexx) and (atemayar, 23, awl8tng8e81d) are not the same in 1093 ms.
Task 45: correctly said that (tagalog, 2, ae0ibwt7asth) and (atemayar, 10, a1vzhyp8yvxkz) are not the same in 923 ms.
Task 46: correctly said that (inuktitut, 2, a12rfyr8dbn79) and (inuktitut, 2, a1j8s7giyto4a) are the same in 1123 ms.
Task 47: incorrectly said that (inuktitut, 3, a1w3fo0d7cf5t) and (atemayar, 6, a1j8s7giyto4a) are the same in 2512 ms.
Task 48: correctly said that (myanmar, 25, aljsxt6v0a30) and (myanmar, 25, a1foggtlgo7n4) are the same in 4267 ms.
Task 49: correctly said that (inuktitut, 2, a12rfyr8dbn79) and (myanmar, 32, a6fjbvuzrhm6) are not the same in 1111 ms.
Task 50: correctly said that (tagalog, 10, a1yovlluv1c9h) and (tagalog, 10, a3mv1jtwep7xn) are the same in 2801 ms.
Task 51: correctly said that (tagalog, 6, a1s3njxhehcip) and (korean, 29, ae0ibwt7asth) are not the same in 1203 ms.
Task 52: incorrectly said that (myanmar, 31, a1j8s7giyto4a) and (myanmar, 25, adc8vbkoocrr) are the same in 1351 ms.
Task 53: correctly said that (sylheti, 20, aljsxt6v0a30) and (sylheti, 28, a3m0kt6kjezd1) are not the same in 886 ms.
Task 54: correctly said that (sylheti, 17, a3np21apfa18b) and (myanmar, 31, a4858n7ggst4) are not the same in 906 ms.
Task 55: correctly said that (tagalog, 5, a2du3880jbrov) and (tagalog, 5, a3m0kt6kjezd1) are the same in 1309 ms.
Task 56: incorrectly said that (myanmar, 25, ay0bjcgydhix) and (myanmar, 31, a4858n7ggst4) are the same in 904 ms.
Task 57: correctly said that (inuktitut, 3, a2pfktghg1ofp) and (myanmar, 1, a1j8s7giyto4a) are not the same in 1440 ms.
Task 58: correctly said that (myanmar, 32, a2pfktghg1ofp) and (tagalog, 6, a1a25pyfoa9ge) are not the same in 843 ms.
Task 59: correctly said that (atemayar, 10, a1yovlluv1c9h) and (atemayar, 10, awl8tng8e81d) are the same in 1558 ms.
Task 60: correctly said that (korean, 28, a1v0p79fynz9j) and (inuktitut, 1, a2ernxe6jlm4x) are not the same in 2237 ms.
Task 61: correctly said that (korean, 28, a2akud1gdstsd) and (korean, 28, a3bb99mn3zy37) are the same in 918 ms.
Task 62: correctly said that (tagalog, 6, amynpl5xzw6q) and (inuktitut, 3, a12xm86d2nbih) are not the same in 1225 ms.
Task 63: correctly said that (inuktitut, 1, a2ernxe6jlm4x) and (tagalog, 6, ajf66jwen7bx) are not the same in 966 ms.
Task 64: correctly said that (sylheti, 20, a30xq7555spo8) and (myanmar, 25, a1aegunf6e081) are not the same in 1090 ms.
Task 65: correctly said that (korean, 4, a2akud1gdstsd) and (korean, 28, ae0ibwt7asth) are not the same in 1024 ms.
Task 66: correctly said that (sylheti, 28, a30xq7555spo8) and (korean, 39, a1umhcrj9zfdp) are not the same in 1374 ms.
Task 67: correctly said that (inuktitut, 11, a315xddd8j4x8) and (atemayar, 23, avlbkm6qpb1s) are not the same in 825 ms.
Task 68: correctly said that (myanmar, 31, avlbkm6qpb1s) and (tagalog, 6, aghrkle9vf6z) are not the same in 652 ms.
Task 69: incorrectly said that (sylheti, 17, a1s3njxhehcip) and (sylheti, 17, ay0bjcgydhix) are not the same in 839 ms.
Task 70: correctly said that (atemayar, 6, avlbkm6qpb1s) and (sylheti, 17, a3r9q9wqm1nyz) are not the same in 1422 ms.
Task 71: incorrectly said that (myanmar, 1, amynpl5xzw6q) and (myanmar, 1, a1clejhbx9yyn) are not the same in 1092 ms.
Task 72: correctly said that (korean, 39, a1udqv4pfypkn) and (tagalog, 6, aghrkle9vf6z) are not the same in 873 ms.
Task 73: correctly said that (tagalog, 2, a1j8s7giyto4a) and (atemayar, 25, azdw6062ia38) are not the same in 952 ms.
Task 74: correctly said that (tagalog, 10, a3mv1jtwep7xn) and (korean, 4, a3dfh6dpi11ip) are not the same in 1544 ms.
Task 75: incorrectly said that (myanmar, 31, a1umhcrj9zfdp) and (myanmar, 31, adc8vbkoocrr) are not the same in 896 ms.
Task 76: correctly said that (sylheti, 17, avlbkm6qpb1s) and (inuktitut, 3, a1z59eb5sfxpy) are not the same in 4360 ms.
Task 77: incorrectly said that (atemayar, 6, aljsxt6v0a30) and (atemayar, 10, a3m0kt6kjezd1) are the same in 1357 ms.
Task 78: correctly said that (sylheti, 17, amynpl5xzw6q) and (sylheti, 19, a4858n7ggst4) are not the same in 1318 ms.
Task 79: incorrectly said that (sylheti, 28, a3209gno9duil) and (sylheti, 28, a3np21apfa18b) are not the same in 1845 ms.
Task 80: incorrectly said that (myanmar, 25, adc8vbkoocrr) and (myanmar, 25, a1foggtlgo7n4) are not the same in 1284 ms.
Task 81: correctly said that (inuktitut, 1, a2ernxe6jlm4x) and (atemayar, 6, ae0ibwt7asth) are not the same in 1345 ms.
Task 82: correctly said that (myanmar, 31, a3j1pp96x14u0) and (myanmar, 31, a1foggtlgo7n4) are the same in 1168 ms.
Task 83: correctly said that (atemayar, 23, a1yovlluv1c9h) and (atemayar, 23, a31e6jchki335) are the same in 1399 ms.
Task 84: incorrectly said that (tagalog, 10, a1s3njxhehcip) and (tagalog, 10, a1ehrxjplsq0r) are not the same in 1060 ms.
Task 85: correctly said that (inuktitut, 2, a1qku9mccvl35) and (inuktitut, 2, a1manxzopzden) are the same in 994 ms.
Task 86: correctly said that (korean, 29, a2akud1gdstsd) and (atemayar, 25, aljsxt6v0a30) are not the same in 1268 ms.
Task 87: correctly said that (atemayar, 23, a3m0kt6kjezd1) and (sylheti, 20, a3np21apfa18b) are not the same in 1231 ms.
Task 88: correctly said that (myanmar, 1, a1foggtlgo7n4) and (myanmar, 25, a1s3njxhehcip) are not the same in 1013 ms.
Task 89: correctly said that (tagalog, 6, avlbkm6qpb1s) and (myanmar, 31, a3pkun3onkucn) are not the same in 934 ms.
Task 90: correctly said that (inuktitut, 1, a12rfyr8dbn79) and (sylheti, 17, a1s3njxhehcip) are not the same in 1118 ms.
Task 91: incorrectly said that (atemayar, 25, a1vzhyp8yvxkz) and (sylheti, 19, a4858n7ggst4) are the same in 3871 ms.
Task 92: correctly said that (tagalog, 10, a1i92aacrkacw) and (sylheti, 28, a1l2fzyn31zvf) are not the same in 868 ms.
Task 93: correctly said that (tagalog, 5, ay0bjcgydhix) and (atemayar, 10, awl8tng8e81d) are not the same in 688 ms.
Task 94: correctly said that (sylheti, 19, a3209gno9duil) and (korean, 4, a2px0aywxngce) are not the same in 864 ms.
Task 95: correctly said that (tagalog, 5, a1s3njxhehcip) and (sylheti, 17, a3r9q9wqm1nyz) are not the same in 753 ms.
Task 96: correctly said that (myanmar, 32, a1stvgjqfl5kk) and (inuktitut, 3, a1z59eb5sfxpy) are not the same in 1033 ms.
Task 97: correctly said that (atemayar, 25, a1manxzopzden) and (tagalog, 5, asu813kb7bv1) are not the same in 1823 ms.
Task 98: correctly said that (korean, 29, ae0ibwt7asth) and (atemayar, 23, a6fjbvuzrhm6) are not the same in 731 ms.
Task 99: correctly said that (atemayar, 6, azdw6062ia38) and (myanmar, 31, ay0bjcgydhix) are not the same in 759 ms.
Task 100: correctly said that (atemayar, 10, a6fjbvuzrhm6) and (korean, 39, aav4pzxdoebd) are not the same in 1139 ms.
Task 101: correctly said that (atemayar, 25, a1manxzopzden) and (myanmar, 31, a1foggtlgo7n4) are not the same in 855 ms.
Task 102: incorrectly said that (atemayar, 25, ax5gghhj0o6g) and (atemayar, 25, a1z59eb5sfxpy) are not the same in 1854 ms.
Task 103: correctly said that (sylheti, 19, aljsxt6v0a30) and (inuktitut, 1, aljsxt6v0a30) are not the same in 780 ms.
Task 104: correctly said that (tagalog, 5, a3m0kt6kjezd1) and (tagalog, 2, a1ehrxjplsq0r) are not the same in 744 ms.
Task 105: correctly said that (sylheti, 28, a30xq7555spo8) and (sylheti, 28, a3pkun3onkucn) are the same in 1351 ms.
Task 106: incorrectly said that (inuktitut, 3, a1manxzopzden) and (inuktitut, 3, aljsxt6v0a30) are not the same in 2042 ms.
Task 107: correctly said that (korean, 29, a3d7xwp0gand5) and (sylheti, 20, amynpl5xzw6q) are not the same in 689 ms.
Task 108: correctly said that (sylheti, 17, a1s3njxhehcip) and (atemayar, 10, a1j8s7giyto4a) are not the same in 694 ms.
Task 109: correctly said that (korean, 28, a1s3njxhehcip) and (inuktitut, 1, a3mv1jtwep7xn) are not the same in 659 ms.
Task 110: correctly said that (atemayar, 6, awl8tng8e81d) and (sylheti, 20, a1a25pyfoa9ge) are not the same in 1553 ms.
Task 111: correctly said that (inuktitut, 11, a315xddd8j4x8) and (korean, 28, aw82oruamauz) are not the same in 745 ms.
Task 112: correctly said that (korean, 28, a2px0aywxngce) and (korean, 28, a1v0p79fynz9j) are the same in 1185 ms.
Task 113: correctly said that (inuktitut, 11, a1manxzopzden) and (atemayar, 25, aljsxt6v0a30) are not the same in 826 ms.
Task 114: correctly said that (atemayar, 25, a1yovlluv1c9h) and (myanmar, 25, a1umhcrj9zfdp) are not the same in 759 ms.
Task 115: correctly said that (myanmar, 25, a3pkun3onkucn) and (inuktitut, 3, a2pfktghg1ofp) are not the same in 771 ms.
Task 116: correctly said that (inuktitut, 1, a1s3njxhehcip) and (inuktitut, 2, a2qnqd7m8zfbo) are not the same in 889 ms.
Task 117: correctly said that (tagalog, 2, ajf66jwen7bx) and (sylheti, 17, a1j8s7giyto4a) are not the same in 951 ms.
Task 118: correctly said that (tagalog, 10, a1j8s7giyto4a) and (tagalog, 10, a1i92aacrkacw) are the same in 1263 ms.
Task 119: correctly said that (myanmar, 32, adc8vbkoocrr) and (korean, 28, a1oh82lwr5hbg) are not the same in 1098 ms.
Task 120: correctly said that (myanmar, 1, a1ehrxjplsq0r) and (tagalog, 5, a1j8s7giyto4a) are not the same in 1525 ms.
Task 121: correctly said that (tagalog, 10, a1s3njxhehcip) and (tagalog, 10, aljsxt6v0a30) are the same in 1321 ms.
Task 122: correctly said that (myanmar, 31, ajf66jwen7bx) and (korean, 39, a1manxzopzden) are not the same in 721 ms.
Task 123: correctly said that (tagalog, 5, avlbkm6qpb1s) and (sylheti, 17, a1j8s7giyto4a) are not the same in 671 ms.
Task 124: correctly said that (atemayar, 25, a3m0kt6kjezd1) and (tagalog, 6, a1a25pyfoa9ge) are not the same in 1273 ms.
Task 125: incorrectly said that (atemayar, 23, a1j8s7giyto4a) and (atemayar, 23, a31e6jchki335) are not the same in 1121 ms.
Task 126: incorrectly said that (tagalog, 10, a1ehrxjplsq0r) and (tagalog, 10, a1a25pyfoa9ge) are not the same in 1092 ms.
Task 127: correctly said that (atemayar, 6, a23psx2z4f65q) and (atemayar, 6, agst3co6usf8) are the same in 1263 ms.
Task 128: correctly said that (inuktitut, 11, a3noo9k3y1yu5) and (atemayar, 23, azdw6062ia38) are not the same in 825 ms.
Task 129: correctly said that (inuktitut, 1, a3noo9k3y1yu5) and (korean, 39, aw82oruamauz) are not the same in 718 ms.
Task 130: correctly said that (myanmar, 1, ay0bjcgydhix) and (myanmar, 1, amynpl5xzw6q) are the same in 1557 ms.
Task 131: correctly said that (inuktitut, 2, a2qnqd7m8zfbo) and (myanmar, 25, a6fjbvuzrhm6) are not the same in 1098 ms.
Task 132: correctly said that (myanmar, 31, a1clejhbx9yyn) and (sylheti, 17, aljsxt6v0a30) are not the same in 1054 ms.
Task 133: correctly said that (tagalog, 10, a12rfyr8dbn79) and (tagalog, 6, a1i92aacrkacw) are not the same in 1679 ms.
Task 134: correctly said that (myanmar, 31, a3qat9qkqumi2) and (korean, 28, a1v0p79fynz9j) are not the same in 828 ms.
Task 135: correctly said that (korean, 28, a3d7xwp0gand5) and (korean, 28, ae0ibwt7asth) are the same in 1595 ms.
Task 136: correctly said that (atemayar, 10, agst3co6usf8) and (sylheti, 17, ay0bjcgydhix) are not the same in 937 ms.
Task 137: correctly said that (myanmar, 31, avlbkm6qpb1s) and (inuktitut, 11, a3mv1jtwep7xn) are not the same in 800 ms.
Task 138: incorrectly said that (atemayar, 6, ax5gghhj0o6g) and (inuktitut, 1, avlbkm6qpb1s) are the same in 1231 ms.
Task 139: correctly said that (inuktitut, 2, a2ernxe6jlm4x) and (sylheti, 20, a1qku9mccvl35) are not the same in 1180 ms.
Task 140: correctly said that (atemayar, 10, a1vzhyp8yvxkz) and (atemayar, 10, avlbkm6qpb1s) are the same in 3395 ms.
Task 141: correctly said that (myanmar, 25, avlbkm6qpb1s) and (inuktitut, 1, a2pfktghg1ofp) are not the same in 1408 ms.
Task 142: incorrectly said that (inuktitut, 1, a1manxzopzden) and (inuktitut, 11, a3noo9k3y1yu5) are the same in 1471 ms.
Task 143: correctly said that (korean, 28, aw82oruamauz) and (korean, 29, a1s3njxhehcip) are not the same in 918 ms.
Task 144: correctly said that (atemayar, 23, azdw6062ia38) and (sylheti, 20, a1l2fzyn31zvf) are not the same in 712 ms.
Task 145: correctly said that (tagalog, 10, aghrkle9vf6z) and (inuktitut, 3, a1z59eb5sfxpy) are not the same in 861 ms.
Task 146: correctly said that (sylheti, 19, a30xq7555spo8) and (sylheti, 19, a1foggtlgo7n4) are the same in 2071 ms.
Task 147: correctly said that (sylheti, 28, a1l2fzyn31zvf) and (sylheti, 28, a3209gno9duil) are the same in 2168 ms.
Task 148: correctly said that (inuktitut, 11, a12rfyr8dbn79) and (sylheti, 17, a3r9q9wqm1nyz) are not the same in 1753 ms.
Task 149: correctly said that (korean, 29, a1umhcrj9zfdp) and (korean, 29, a1manxzopzden) are the same in 1359 ms.
Task 150: correctly said that (korean, 28, a39ivbdm1ekue) and (sylheti, 19, ay0bjcgydhix) are not the same in 1813 ms.
Task 151: incorrectly said that (tagalog, 5, amynpl5xzw6q) and (tagalog, 5, a12rfyr8dbn79) are not the same in 908 ms.
Task 152: incorrectly said that (tagalog, 2, a3m0kt6kjezd1) and (tagalog, 2, avlbkm6qpb1s) are not the same in 794 ms.
Task 153: correctly said that (sylheti, 20, atvyxlwe5n03) and (inuktitut, 11, a2ernxe6jlm4x) are not the same in 738 ms.
Task 154: correctly said that (atemayar, 6, aljsxt6v0a30) and (tagalog, 6, a3mv1jtwep7xn) are not the same in 768 ms.
Task 155: correctly said that (korean, 28, a1v0p79fynz9j) and (inuktitut, 1, a3noo9k3y1yu5) are not the same in 767 ms.
Task 156: correctly said that (korean, 4, ae0ibwt7asth) and (korean, 4, a3bb99mn3zy37) are the same in 1630 ms.
Task 157: incorrectly said that (myanmar, 32, a3qat9qkqumi2) and (tagalog, 6, avlbkm6qpb1s) are the same in 1171 ms.
Task 158: incorrectly said that (tagalog, 10, aljsxt6v0a30) and (tagalog, 10, a3m0kt6kjezd1) are not the same in 1174 ms.
Task 159: incorrectly said that (atemayar, 25, a3m0kt6kjezd1) and (atemayar, 25, a23psx2z4f65q) are not the same in 1046 ms.
Task 160: correctly said that (inuktitut, 3, a12xm86d2nbih) and (atemayar, 25, azdw6062ia38) are not the same in 1157 ms.
Task 161: correctly said that (myanmar, 1, ajf66jwen7bx) and (tagalog, 5, a1a25pyfoa9ge) are not the same in 1266 ms.
Task 162: correctly said that (korean, 4, a1udqv4pfypkn) and (tagalog, 10, a1i92aacrkacw) are not the same in 729 ms.
Task 163: incorrectly said that (myanmar, 31, a1foggtlgo7n4) and (myanmar, 31, a1j8s7giyto4a) are not the same in 850 ms.
Task 164: correctly said that (inuktitut, 2, a1w3fo0d7cf5t) and (sylheti, 28, avlbkm6qpb1s) are not the same in 1640 ms.
Task 165: incorrectly said that (sylheti, 17, a1s3njxhehcip) and (sylheti, 20, a1j8s7giyto4a) are the same in 1343 ms.
Task 166: correctly said that (myanmar, 1, a4858n7ggst4) and (myanmar, 1, a3qat9qkqumi2) are the same in 1461 ms.
Task 167: incorrectly said that (atemayar, 6, awl8tng8e81d) and (atemayar, 6, a31xqftzcsia2) are not the same in 759 ms.
Task 168: incorrectly said that (myanmar, 25, a1ehrxjplsq0r) and (atemayar, 6, a6fjbvuzrhm6) are the same in 1911 ms.
Task 169: correctly said that (atemayar, 25, a1manxzopzden) and (sylheti, 17, a30xq7555spo8) are not the same in 797 ms.
Task 170: correctly said that (atemayar, 23, azdw6062ia38) and (tagalog, 2, a1ehrxjplsq0r) are not the same in 1293 ms.
Task 171: correctly said that (inuktitut, 1, a1z59eb5sfxpy) and (korean, 29, a2akud1gdstsd) are not the same in 1045 ms.
Task 172: incorrectly said that (myanmar, 32, a4858n7ggst4) and (myanmar, 32, ay0bjcgydhix) are not the same in 1088 ms.
Task 173: incorrectly said that (korean, 39, a5379luit3pc) and (korean, 39, a2akud1gdstsd) are not the same in 1006 ms.
Task 174: correctly said that (sylheti, 28, a3r9q9wqm1nyz) and (korean, 4, aj6upe9mlarg) are not the same in 979 ms.
Task 175: correctly said that (atemayar, 23, ae0ibwt7asth) and (atemayar, 23, awl8tng8e81d) are the same in 1003 ms.
Task 176: correctly said that (sylheti, 28, a1foggtlgo7n4) and (korean, 29, a1v0p79fynz9j) are not the same in 1952 ms.
Task 177: correctly said that (sylheti, 19, a1l2fzyn31zvf) and (sylheti, 19, a4858n7ggst4) are the same in 1402 ms.
Task 178: incorrectly said that (sylheti, 20, a1l2fzyn31zvf) and (sylheti, 20, a1s3njxhehcip) are not the same in 695 ms.
Task 179: correctly said that (myanmar, 1, a2pfktghg1ofp) and (korean, 29, a1oh82lwr5hbg) are not the same in 839 ms.
Task 180: incorrectly said that (atemayar, 23, a23psx2z4f65q) and (atemayar, 23, a31e6jchki335) are not the same in 1030 ms.
Task 181: correctly said that (tagalog, 10, a1ehrxjplsq0r) and (korean, 29, a3bb99mn3zy37) are not the same in 1016 ms.
Task 182: correctly said that (tagalog, 2, a1s3njxhehcip) and (tagalog, 2, a1ehrxjplsq0r) are the same in 1945 ms.
Task 183: incorrectly said that (korean, 29, aav4pzxdoebd) and (korean, 29, a3d7xwp0gand5) are not the same in 1329 ms.
Task 184: correctly said that (myanmar, 32, amynpl5xzw6q) and (inuktitut, 11, avlbkm6qpb1s) are not the same in 687 ms.
Task 185: correctly said that (atemayar, 23, avlbkm6qpb1s) and (sylheti, 19, a1a25pyfoa9ge) are not the same in 1366 ms.
Task 186: correctly said that (sylheti, 19, a3np21apfa18b) and (inuktitut, 3, a315xddd8j4x8) are not the same in 872 ms.
Task 187: correctly said that (tagalog, 5, a2du3880jbrov) and (korean, 4, a2akud1gdstsd) are not the same in 733 ms.
Task 188: correctly said that (sylheti, 17, a3np21apfa18b) and (sylheti, 17, a1j8s7giyto4a) are the same in 2238 ms.
Task 189: correctly said that (sylheti, 28, a4858n7ggst4) and (inuktitut, 2, a12rfyr8dbn79) are not the same in 1018 ms.
Task 190: correctly said that (myanmar, 1, a1clejhbx9yyn) and (inuktitut, 1, a2pfktghg1ofp) are not the same in 1600 ms.
Task 191: correctly said that (atemayar, 23, a31xqftzcsia2) and (tagalog, 2, a1l2fzyn31zvf) are not the same in 5268 ms.
Task 192: correctly said that (atemayar, 23, a2pfktghg1ofp) and (sylheti, 20, avlbkm6qpb1s) are not the same in 831 ms.
Task 193: incorrectly said that (tagalog, 6, a1yovlluv1c9h) and (tagalog, 6, a2du3880jbrov) are not the same in 695 ms.
Task 194: correctly said that (korean, 4, aj6upe9mlarg) and (atemayar, 6, a31e6jchki335) are not the same in 925 ms.
Task 195: correctly said that (atemayar, 10, a6fjbvuzrhm6) and (inuktitut, 11, a1s3njxhehcip) are not the same in 855 ms.
Task 196: correctly said that (inuktitut, 1, a12rfyr8dbn79) and (sylheti, 20, a3np21apfa18b) are not the same in 1078 ms.
Task 197: correctly said that (inuktitut, 3, a315xddd8j4x8) and (inuktitut, 3, a1manxzopzden) are the same in 1344 ms.
Task 198: correctly said that (sylheti, 28, ay0bjcgydhix) and (sylheti, 28, a1a25pyfoa9ge) are the same in 1494 ms.
Task 199: correctly said that (korean, 29, a1umhcrj9zfdp) and (myanmar, 32, a3pkun3onkucn) are not the same in 1181 ms.

Duration: 26m 46s 784ms
Comments: 