

Summary:Short Summary:
Correct: 181
Incorrect: 19
Percent Correct: 90

Same Correct: 60
Same Incorrect: 15
Percent Same Correct: 80

Different Correct: 121
Different Incorrect: 4
Percent Different Correct: 96

Long Summary:
Task 0: correctly said that (sylheti, 15, a3pkun3onkucn) and (myanmar, 4, a1s3njxhehcip) are not the same in 1472 ms.
Task 1: correctly said that (sylheti, 3, a2du3880jbrov) and (sylheti, 3, a1j8s7giyto4a) are the same in 1419 ms.
Task 2: correctly said that (korean, 4, a3bb99mn3zy37) and (tagalog, 6, amynpl5xzw6q) are not the same in 2150 ms.
Task 3: correctly said that (sylheti, 3, a1foggtlgo7n4) and (sylheti, 3, a3pkun3onkucn) are the same in 1497 ms.
Task 4: correctly said that (inuktitut, 3, a2pgu4pi93b1q) and (inuktitut, 3, a315xddd8j4x8) are the same in 1653 ms.
Task 5: correctly said that (inuktitut, 3, avlbkm6qpb1s) and (atemayar, 6, avlbkm6qpb1s) are not the same in 1441 ms.
Task 6: correctly said that (korean, 17, a3mv1jtwep7xn) and (tagalog, 15, a1l2fzyn31zvf) are not the same in 1492 ms.
Task 7: correctly said that (tagalog, 10, avlbkm6qpb1s) and (korean, 39, a39ivbdm1ekue) are not the same in 1195 ms.
Task 8: correctly said that (atemayar, 6, a1vzhyp8yvxkz) and (myanmar, 34, a2pfktghg1ofp) are not the same in 880 ms.
Task 9: correctly said that (inuktitut, 11, a315xddd8j4x8) and (myanmar, 34, a3j1pp96x14u0) are not the same in 1636 ms.
Task 10: correctly said that (tagalog, 6, a1l2fzyn31zvf) and (atemayar, 10, a6fjbvuzrhm6) are not the same in 1286 ms.
Task 11: correctly said that (sylheti, 4, a6fjbvuzrhm6) and (atemayar, 12, ax5gghhj0o6g) are not the same in 936 ms.
Task 12: correctly said that (atemayar, 25, a1poymbentexx) and (atemayar, 25, avlbkm6qpb1s) are the same in 2356 ms.
Task 13: correctly said that (atemayar, 10, agst3co6usf8) and (sylheti, 3, amynpl5xzw6q) are not the same in 1386 ms.
Task 14: correctly said that (korean, 3, a3dfh6dpi11ip) and (korean, 3, aav4pzxdoebd) are the same in 1381 ms.
Task 15: correctly said that (sylheti, 3, a1j8s7giyto4a) and (inuktitut, 2, ay0bjcgydhix) are not the same in 802 ms.
Task 16: correctly said that (korean, 3, a2nm1qzuh14ls) and (tagalog, 1, a12rfyr8dbn79) are not the same in 880 ms.
Task 17: incorrectly said that (myanmar, 8, a1stvgjqfl5kk) and (myanmar, 8, adc8vbkoocrr) are not the same in 1163 ms.
Task 18: correctly said that (tagalog, 6, a1j8s7giyto4a) and (tagalog, 6, ajf66jwen7bx) are the same in 1167 ms.
Task 19: correctly said that (sylheti, 3, a3209gno9duil) and (korean, 3, a1udqv4pfypkn) are not the same in 1071 ms.
Task 20: correctly said that (myanmar, 8, avlbkm6qpb1s) and (sylheti, 15, a1a25pyfoa9ge) are not the same in 1223 ms.
Task 21: correctly said that (inuktitut, 12, a1z59eb5sfxpy) and (tagalog, 15, a1i92aacrkacw) are not the same in 932 ms.
Task 22: correctly said that (sylheti, 4, a3r9q9wqm1nyz) and (korean, 4, a1oh82lwr5hbg) are not the same in 689 ms.
Task 23: correctly said that (korean, 39, a2akud1gdstsd) and (tagalog, 6, aghrkle9vf6z) are not the same in 851 ms.
Task 24: correctly said that (myanmar, 4, a1ehrxjplsq0r) and (myanmar, 4, a3qat9qkqumi2) are the same in 950 ms.
Task 25: correctly said that (inuktitut, 11, a2pgu4pi93b1q) and (korean, 17, a2akud1gdstsd) are not the same in 1177 ms.
Task 26: correctly said that (atemayar, 12, ax5gghhj0o6g) and (atemayar, 12, ae0ibwt7asth) are the same in 1436 ms.
Task 27: correctly said that (atemayar, 25, a31e6jchki335) and (atemayar, 25, a6fjbvuzrhm6) are the same in 1009 ms.
Task 28: correctly said that (inuktitut, 12, a2ernxe6jlm4x) and (inuktitut, 2, a3noo9k3y1yu5) are not the same in 1193 ms.
Task 29: correctly said that (tagalog, 15, aljsxt6v0a30) and (myanmar, 4, a3j1pp96x14u0) are not the same in 1337 ms.
Task 30: correctly said that (inuktitut, 3, a12xm86d2nbih) and (inuktitut, 3, avlbkm6qpb1s) are the same in 1222 ms.
Task 31: incorrectly said that (atemayar, 12, a31e6jchki335) and (atemayar, 12, agst3co6usf8) are not the same in 1001 ms.
Task 32: correctly said that (tagalog, 10, ay0bjcgydhix) and (atemayar, 12, awl8tng8e81d) are not the same in 961 ms.
Task 33: correctly said that (inuktitut, 12, a1j8s7giyto4a) and (myanmar, 4, a1s3njxhehcip) are not the same in 799 ms.
Task 34: correctly said that (korean, 3, aw82oruamauz) and (myanmar, 34, avlbkm6qpb1s) are not the same in 763 ms.
Task 35: correctly said that (atemayar, 25, agst3co6usf8) and (inuktitut, 2, a1manxzopzden) are not the same in 732 ms.
Task 36: correctly said that (korean, 39, a2nm1qzuh14ls) and (myanmar, 34, a1aegunf6e081) are not the same in 685 ms.
Task 37: correctly said that (sylheti, 3, a1qku9mccvl35) and (tagalog, 1, a1l2fzyn31zvf) are not the same in 662 ms.
Task 38: correctly said that (korean, 3, a2akud1gdstsd) and (korean, 3, a5379luit3pc) are the same in 1006 ms.
Task 39: incorrectly said that (korean, 4, a1oh82lwr5hbg) and (korean, 4, a2px0aywxngce) are not the same in 1194 ms.
Task 40: correctly said that (sylheti, 15, a6fjbvuzrhm6) and (tagalog, 10, aljsxt6v0a30) are not the same in 1456 ms.
Task 41: correctly said that (sylheti, 3, ay0bjcgydhix) and (tagalog, 6, asu813kb7bv1) are not the same in 880 ms.
Task 42: correctly said that (korean, 17, a5379luit3pc) and (korean, 17, a3bb99mn3zy37) are the same in 1237 ms.
Task 43: correctly said that (tagalog, 6, a1s3njxhehcip) and (korean, 17, a3dfh6dpi11ip) are not the same in 857 ms.
Task 44: correctly said that (myanmar, 34, a1foggtlgo7n4) and (inuktitut, 11, a3qat9qkqumi2) are not the same in 762 ms.
Task 45: correctly said that (inuktitut, 12, a2ernxe6jlm4x) and (sylheti, 3, a3pkun3onkucn) are not the same in 686 ms.
Task 46: correctly said that (myanmar, 34, avlbkm6qpb1s) and (korean, 39, a3dfh6dpi11ip) are not the same in 761 ms.
Task 47: correctly said that (korean, 17, a39ivbdm1ekue) and (tagalog, 10, a1l2fzyn31zvf) are not the same in 611 ms.
Task 48: correctly said that (inuktitut, 11, aljsxt6v0a30) and (myanmar, 4, a1ehrxjplsq0r) are not the same in 691 ms.
Task 49: incorrectly said that (atemayar, 12, azdw6062ia38) and (atemayar, 12, ax5gghhj0o6g) are not the same in 1696 ms.
Task 50: correctly said that (tagalog, 1, aljsxt6v0a30) and (atemayar, 6, a1vzhyp8yvxkz) are not the same in 1319 ms.
Task 51: correctly said that (atemayar, 6, ax5gghhj0o6g) and (sylheti, 15, a3pkun3onkucn) are not the same in 1136 ms.
Task 52: correctly said that (sylheti, 3, a3pkun3onkucn) and (myanmar, 4, a6fjbvuzrhm6) are not the same in 1031 ms.
Task 53: correctly said that (atemayar, 6, a1yovlluv1c9h) and (tagalog, 10, a1a25pyfoa9ge) are not the same in 789 ms.
Task 54: correctly said that (atemayar, 10, a2pfktghg1ofp) and (myanmar, 34, a1umhcrj9zfdp) are not the same in 621 ms.
Task 55: correctly said that (tagalog, 1, a2du3880jbrov) and (tagalog, 1, ay0bjcgydhix) are the same in 1268 ms.
Task 56: correctly said that (inuktitut, 11, a3mv1jtwep7xn) and (atemayar, 6, a1z59eb5sfxpy) are not the same in 960 ms.
Task 57: correctly said that (inuktitut, 3, a3noo9k3y1yu5) and (sylheti, 4, a4858n7ggst4) are not the same in 838 ms.
Task 58: incorrectly said that (atemayar, 6, ae0ibwt7asth) and (korean, 39, a1umhcrj9zfdp) are the same in 1218 ms.
Task 59: correctly said that (tagalog, 1, a1ehrxjplsq0r) and (myanmar, 34, a1ehrxjplsq0r) are not the same in 811 ms.
Task 60: incorrectly said that (korean, 4, ae0ibwt7asth) and (korean, 4, a3d7xwp0gand5) are not the same in 1401 ms.
Task 61: correctly said that (myanmar, 8, a1j8s7giyto4a) and (tagalog, 15, ay0bjcgydhix) are not the same in 830 ms.
Task 62: correctly said that (inuktitut, 11, a2pfktghg1ofp) and (inuktitut, 11, a2ernxe6jlm4x) are the same in 1035 ms.
Task 63: correctly said that (korean, 4, a3mv1jtwep7xn) and (korean, 4, a1umhcrj9zfdp) are the same in 951 ms.
Task 64: correctly said that (tagalog, 1, a1s3njxhehcip) and (tagalog, 1, a3qat9qkqumi2) are the same in 829 ms.
Task 65: incorrectly said that (myanmar, 8, adc8vbkoocrr) and (inuktitut, 11, aljsxt6v0a30) are the same in 1770 ms.
Task 66: correctly said that (atemayar, 25, a1manxzopzden) and (korean, 17, a2akud1gdstsd) are not the same in 1156 ms.
Task 67: correctly said that (atemayar, 10, a6fjbvuzrhm6) and (atemayar, 10, ax5gghhj0o6g) are the same in 2646 ms.
Task 68: correctly said that (myanmar, 34, a1ehrxjplsq0r) and (myanmar, 34, adc8vbkoocrr) are the same in 2742 ms.
Task 69: correctly said that (atemayar, 12, ae0ibwt7asth) and (atemayar, 12, azdw6062ia38) are the same in 1602 ms.
Task 70: correctly said that (atemayar, 25, a31xqftzcsia2) and (tagalog, 6, a1i92aacrkacw) are not the same in 1514 ms.
Task 71: incorrectly said that (korean, 17, aav4pzxdoebd) and (atemayar, 12, a23psx2z4f65q) are the same in 1518 ms.
Task 72: correctly said that (atemayar, 10, awl8tng8e81d) and (inuktitut, 11, ae0ibwt7asth) are not the same in 7085 ms.
Task 73: correctly said that (inuktitut, 2, a1manxzopzden) and (inuktitut, 12, a2pfktghg1ofp) are not the same in 2301 ms.
Task 74: correctly said that (sylheti, 15, a3pkun3onkucn) and (sylheti, 15, a1qku9mccvl35) are the same in 6656 ms.
Task 75: correctly said that (tagalog, 6, a1yovlluv1c9h) and (sylheti, 13, a2du3880jbrov) are not the same in 3342 ms.
Task 76: correctly said that (inuktitut, 11, a2qnqd7m8zfbo) and (inuktitut, 11, a1s3njxhehcip) are the same in 2036 ms.
Task 77: correctly said that (inuktitut, 3, a2pgu4pi93b1q) and (sylheti, 13, atvyxlwe5n03) are not the same in 953 ms.
Task 78: correctly said that (inuktitut, 11, a1s3njxhehcip) and (myanmar, 16, a6fjbvuzrhm6) are not the same in 863 ms.
Task 79: correctly said that (tagalog, 6, a1j8s7giyto4a) and (atemayar, 25, ae0ibwt7asth) are not the same in 1609 ms.
Task 80: correctly said that (inuktitut, 11, ay0bjcgydhix) and (inuktitut, 11, a2ernxe6jlm4x) are the same in 1341 ms.
Task 81: correctly said that (myanmar, 34, a6fjbvuzrhm6) and (atemayar, 6, a1z59eb5sfxpy) are not the same in 1197 ms.
Task 82: correctly said that (sylheti, 13, a1a25pyfoa9ge) and (sylheti, 13, a3np21apfa18b) are the same in 1103 ms.
Task 83: correctly said that (korean, 39, a39ivbdm1ekue) and (sylheti, 4, ay0bjcgydhix) are not the same in 973 ms.
Task 84: correctly said that (atemayar, 25, aljsxt6v0a30) and (korean, 3, aav4pzxdoebd) are not the same in 869 ms.
Task 85: correctly said that (sylheti, 13, ay0bjcgydhix) and (sylheti, 13, a3209gno9duil) are the same in 1087 ms.
Task 86: correctly said that (inuktitut, 11, a3noo9k3y1yu5) and (atemayar, 12, avlbkm6qpb1s) are not the same in 773 ms.
Task 87: correctly said that (inuktitut, 11, a2qnqd7m8zfbo) and (korean, 39, a39ivbdm1ekue) are not the same in 834 ms.
Task 88: correctly said that (korean, 39, a2px0aywxngce) and (sylheti, 4, amynpl5xzw6q) are not the same in 812 ms.
Task 89: correctly said that (myanmar, 8, a6fjbvuzrhm6) and (tagalog, 10, avlbkm6qpb1s) are not the same in 1113 ms.
Task 90: incorrectly said that (myanmar, 16, avlbkm6qpb1s) and (myanmar, 16, a3j1pp96x14u0) are not the same in 1660 ms.
Task 91: correctly said that (atemayar, 25, a1yovlluv1c9h) and (sylheti, 4, amynpl5xzw6q) are not the same in 888 ms.
Task 92: correctly said that (korean, 39, a2px0aywxngce) and (sylheti, 13, a1l2fzyn31zvf) are not the same in 697 ms.
Task 93: correctly said that (inuktitut, 12, ae0ibwt7asth) and (inuktitut, 12, a3qat9qkqumi2) are the same in 973 ms.
Task 94: correctly said that (inuktitut, 3, a1s3njxhehcip) and (inuktitut, 3, a12rfyr8dbn79) are the same in 1296 ms.
Task 95: correctly said that (korean, 3, aw82oruamauz) and (korean, 3, a2nm1qzuh14ls) are the same in 1060 ms.
Task 96: correctly said that (sylheti, 13, avlbkm6qpb1s) and (sylheti, 3, ay0bjcgydhix) are not the same in 926 ms.
Task 97: correctly said that (atemayar, 25, ax5gghhj0o6g) and (tagalog, 1, a1i92aacrkacw) are not the same in 840 ms.
Task 98: correctly said that (korean, 4, aj6upe9mlarg) and (korean, 4, a2akud1gdstsd) are the same in 1762 ms.
Task 99: correctly said that (sylheti, 15, a4858n7ggst4) and (korean, 4, a1oh82lwr5hbg) are not the same in 941 ms.
Task 100: correctly said that (myanmar, 34, a1j8s7giyto4a) and (tagalog, 10, ae0ibwt7asth) are not the same in 2278 ms.
Task 101: correctly said that (korean, 39, a1oh82lwr5hbg) and (korean, 39, a3mv1jtwep7xn) are the same in 1322 ms.
Task 102: correctly said that (sylheti, 13, a1l2fzyn31zvf) and (sylheti, 13, a6fjbvuzrhm6) are the same in 1004 ms.
Task 103: correctly said that (korean, 4, a3d7xwp0gand5) and (korean, 4, a5379luit3pc) are the same in 963 ms.
Task 104: correctly said that (sylheti, 13, a3r9q9wqm1nyz) and (sylheti, 13, a3np21apfa18b) are the same in 907 ms.
Task 105: correctly said that (tagalog, 10, a2du3880jbrov) and (myanmar, 16, a3qat9qkqumi2) are not the same in 1020 ms.
Task 106: correctly said that (inuktitut, 11, a2pgu4pi93b1q) and (atemayar, 6, aljsxt6v0a30) are not the same in 631 ms.
Task 107: correctly said that (sylheti, 3, a1j8s7giyto4a) and (sylheti, 3, a1a25pyfoa9ge) are the same in 921 ms.
Task 108: correctly said that (tagalog, 6, a1j8s7giyto4a) and (tagalog, 6, a3mv1jtwep7xn) are the same in 2032 ms.
Task 109: correctly said that (atemayar, 25, ax5gghhj0o6g) and (atemayar, 25, awl8tng8e81d) are the same in 2958 ms.
Task 110: correctly said that (inuktitut, 2, a1w3fo0d7cf5t) and (atemayar, 12, a6fjbvuzrhm6) are not the same in 1052 ms.
Task 111: incorrectly said that (tagalog, 1, a12rfyr8dbn79) and (tagalog, 1, ay0bjcgydhix) are not the same in 1498 ms.
Task 112: correctly said that (sylheti, 3, ay0bjcgydhix) and (sylheti, 13, a1foggtlgo7n4) are not the same in 810 ms.
Task 113: correctly said that (korean, 17, a2ernxe6jlm4x) and (sylheti, 15, a30xq7555spo8) are not the same in 699 ms.
Task 114: correctly said that (korean, 4, a2akud1gdstsd) and (korean, 17, a1umhcrj9zfdp) are not the same in 692 ms.
Task 115: correctly said that (sylheti, 15, amynpl5xzw6q) and (sylheti, 15, ay0bjcgydhix) are the same in 1108 ms.
Task 116: correctly said that (myanmar, 16, ay0bjcgydhix) and (myanmar, 4, a3j1pp96x14u0) are not the same in 1788 ms.
Task 117: correctly said that (myanmar, 4, a1ehrxjplsq0r) and (sylheti, 4, a3pkun3onkucn) are not the same in 753 ms.
Task 118: correctly said that (sylheti, 13, a3r9q9wqm1nyz) and (atemayar, 10, a1poymbentexx) are not the same in 686 ms.
Task 119: correctly said that (inuktitut, 12, a2qnqd7m8zfbo) and (atemayar, 6, a1z59eb5sfxpy) are not the same in 657 ms.
Task 120: correctly said that (atemayar, 6, a1yovlluv1c9h) and (sylheti, 3, atvyxlwe5n03) are not the same in 1378 ms.
Task 121: correctly said that (tagalog, 1, a3m0kt6kjezd1) and (sylheti, 3, avlbkm6qpb1s) are not the same in 3054 ms.
Task 122: correctly said that (atemayar, 12, awl8tng8e81d) and (atemayar, 12, a6fjbvuzrhm6) are the same in 4214 ms.
Task 123: correctly said that (inuktitut, 11, avlbkm6qpb1s) and (atemayar, 25, a1poymbentexx) are not the same in 1062 ms.
Task 124: correctly said that (tagalog, 6, a1i92aacrkacw) and (tagalog, 6, a1a25pyfoa9ge) are the same in 1048 ms.
Task 125: correctly said that (sylheti, 3, a2du3880jbrov) and (tagalog, 10, a1yovlluv1c9h) are not the same in 797 ms.
Task 126: correctly said that (tagalog, 6, a3m0kt6kjezd1) and (tagalog, 10, a3m0kt6kjezd1) are not the same in 855 ms.
Task 127: correctly said that (myanmar, 34, aljsxt6v0a30) and (korean, 39, a5379luit3pc) are not the same in 671 ms.
Task 128: correctly said that (sylheti, 13, a30xq7555spo8) and (sylheti, 13, a1l2fzyn31zvf) are the same in 1600 ms.
Task 129: incorrectly said that (atemayar, 25, agst3co6usf8) and (atemayar, 25, a31e6jchki335) are not the same in 891 ms.
Task 130: correctly said that (myanmar, 34, a3j1pp96x14u0) and (inuktitut, 12, a3noo9k3y1yu5) are not the same in 900 ms.
Task 131: correctly said that (sylheti, 15, avlbkm6qpb1s) and (myanmar, 4, a3qat9qkqumi2) are not the same in 804 ms.
Task 132: incorrectly said that (sylheti, 13, aljsxt6v0a30) and (sylheti, 13, ay0bjcgydhix) are not the same in 2672 ms.
Task 133: incorrectly said that (atemayar, 10, azdw6062ia38) and (atemayar, 10, aljsxt6v0a30) are not the same in 694 ms.
Task 134: correctly said that (atemayar, 6, a1vzhyp8yvxkz) and (myanmar, 16, ajf66jwen7bx) are not the same in 676 ms.
Task 135: incorrectly said that (korean, 39, a2px0aywxngce) and (korean, 39, a1manxzopzden) are not the same in 1665 ms.
Task 136: correctly said that (korean, 39, aw82oruamauz) and (korean, 39, a3d7xwp0gand5) are the same in 1123 ms.
Task 137: correctly said that (inuktitut, 12, a1qku9mccvl35) and (sylheti, 13, a1qku9mccvl35) are not the same in 1067 ms.
Task 138: correctly said that (inuktitut, 3, a1s3njxhehcip) and (inuktitut, 3, aljsxt6v0a30) are the same in 953 ms.
Task 139: correctly said that (inuktitut, 2, a2pgu4pi93b1q) and (inuktitut, 11, a1z59eb5sfxpy) are not the same in 3759 ms.
Task 140: correctly said that (inuktitut, 11, a3qat9qkqumi2) and (myanmar, 34, aljsxt6v0a30) are not the same in 1324 ms.
Task 141: correctly said that (korean, 17, a2akud1gdstsd) and (korean, 17, a3dfh6dpi11ip) are the same in 1017 ms.
Task 142: incorrectly said that (atemayar, 25, a1poymbentexx) and (atemayar, 25, avlbkm6qpb1s) are not the same in 2973 ms.
Task 143: correctly said that (tagalog, 6, a1l2fzyn31zvf) and (sylheti, 4, a6fjbvuzrhm6) are not the same in 897 ms.
Task 144: correctly said that (myanmar, 34, a6fjbvuzrhm6) and (inuktitut, 11, a2ernxe6jlm4x) are not the same in 744 ms.
Task 145: correctly said that (sylheti, 13, a4858n7ggst4) and (korean, 17, a1udqv4pfypkn) are not the same in 624 ms.
Task 146: correctly said that (tagalog, 15, ae0ibwt7asth) and (atemayar, 12, a3m0kt6kjezd1) are not the same in 886 ms.
Task 147: correctly said that (atemayar, 6, azdw6062ia38) and (atemayar, 6, a6fjbvuzrhm6) are the same in 1255 ms.
Task 148: correctly said that (atemayar, 12, a1j8s7giyto4a) and (atemayar, 12, aljsxt6v0a30) are the same in 1271 ms.
Task 149: correctly said that (korean, 17, a2px0aywxngce) and (korean, 17, a1umhcrj9zfdp) are the same in 1072 ms.
Task 150: correctly said that (sylheti, 13, a3209gno9duil) and (sylheti, 13, a1s3njxhehcip) are the same in 1207 ms.
Task 151: correctly said that (korean, 4, a5379luit3pc) and (myanmar, 34, a1aegunf6e081) are not the same in 790 ms.
Task 152: correctly said that (atemayar, 12, a1vzhyp8yvxkz) and (atemayar, 12, azdw6062ia38) are the same in 1037 ms.
Task 153: correctly said that (korean, 17, a1s3njxhehcip) and (inuktitut, 11, ay0bjcgydhix) are not the same in 755 ms.
Task 154: correctly said that (inuktitut, 3, a1j8s7giyto4a) and (inuktitut, 3, a2pgu4pi93b1q) are the same in 1241 ms.
Task 155: correctly said that (inuktitut, 12, a12rfyr8dbn79) and (inuktitut, 3, a12xm86d2nbih) are not the same in 1143 ms.
Task 156: correctly said that (atemayar, 12, a1qku9mccvl35) and (atemayar, 6, a23psx2z4f65q) are not the same in 1398 ms.
Task 157: correctly said that (sylheti, 13, a1s3njxhehcip) and (myanmar, 34, a3qat9qkqumi2) are not the same in 730 ms.
Task 158: incorrectly said that (atemayar, 12, a1qku9mccvl35) and (atemayar, 12, a3m0kt6kjezd1) are not the same in 1026 ms.
Task 159: incorrectly said that (atemayar, 6, a31e6jchki335) and (atemayar, 10, a1j8s7giyto4a) are the same in 1129 ms.
Task 160: correctly said that (tagalog, 1, a1l2fzyn31zvf) and (tagalog, 1, a1yovlluv1c9h) are the same in 2113 ms.
Task 161: correctly said that (sylheti, 13, a3m0kt6kjezd1) and (inuktitut, 3, avlbkm6qpb1s) are not the same in 907 ms.
Task 162: correctly said that (atemayar, 12, a1yovlluv1c9h) and (atemayar, 12, a23psx2z4f65q) are the same in 1099 ms.
Task 163: correctly said that (myanmar, 34, aljsxt6v0a30) and (myanmar, 16, a3j1pp96x14u0) are not the same in 950 ms.
Task 164: correctly said that (myanmar, 16, a1j8s7giyto4a) and (tagalog, 1, a3m0kt6kjezd1) are not the same in 886 ms.
Task 165: correctly said that (tagalog, 1, avlbkm6qpb1s) and (inuktitut, 2, a1j8s7giyto4a) are not the same in 760 ms.
Task 166: incorrectly said that (myanmar, 8, a1stvgjqfl5kk) and (myanmar, 8, a3qat9qkqumi2) are not the same in 1385 ms.
Task 167: correctly said that (inuktitut, 11, a3noo9k3y1yu5) and (atemayar, 10, ae0ibwt7asth) are not the same in 834 ms.
Task 168: correctly said that (tagalog, 15, a1j8s7giyto4a) and (tagalog, 15, a1l2fzyn31zvf) are the same in 1001 ms.
Task 169: correctly said that (myanmar, 34, a1ehrxjplsq0r) and (sylheti, 15, a3pkun3onkucn) are not the same in 796 ms.
Task 170: correctly said that (sylheti, 3, a1foggtlgo7n4) and (atemayar, 6, a1vzhyp8yvxkz) are not the same in 1130 ms.
Task 171: correctly said that (korean, 17, a1manxzopzden) and (atemayar, 25, ax5gghhj0o6g) are not the same in 1325 ms.
Task 172: correctly said that (myanmar, 34, a1s3njxhehcip) and (tagalog, 15, asu813kb7bv1) are not the same in 692 ms.
Task 173: correctly said that (atemayar, 10, a2pfktghg1ofp) and (myanmar, 16, aljsxt6v0a30) are not the same in 623 ms.
Task 174: correctly said that (sylheti, 13, a3m0kt6kjezd1) and (tagalog, 6, a1s3njxhehcip) are not the same in 730 ms.
Task 175: correctly said that (atemayar, 6, agst3co6usf8) and (atemayar, 12, a1poymbentexx) are not the same in 2917 ms.
Task 176: correctly said that (myanmar, 34, ajf66jwen7bx) and (myanmar, 4, a1stvgjqfl5kk) are not the same in 773 ms.
Task 177: correctly said that (atemayar, 25, a1manxzopzden) and (myanmar, 34, adc8vbkoocrr) are not the same in 839 ms.
Task 178: correctly said that (tagalog, 1, a1s3njxhehcip) and (myanmar, 16, a1ehrxjplsq0r) are not the same in 663 ms.
Task 179: correctly said that (tagalog, 10, aljsxt6v0a30) and (tagalog, 10, a1l2fzyn31zvf) are the same in 2130 ms.
Task 180: correctly said that (sylheti, 13, a1foggtlgo7n4) and (sylheti, 13, amynpl5xzw6q) are the same in 1128 ms.
Task 181: correctly said that (tagalog, 6, ae0ibwt7asth) and (sylheti, 15, avlbkm6qpb1s) are not the same in 972 ms.
Task 182: correctly said that (sylheti, 3, a2du3880jbrov) and (sylheti, 3, a4858n7ggst4) are the same in 1078 ms.
Task 183: correctly said that (inuktitut, 11, a1manxzopzden) and (inuktitut, 11, a1j8s7giyto4a) are the same in 1133 ms.
Task 184: correctly said that (tagalog, 10, amynpl5xzw6q) and (sylheti, 15, a3209gno9duil) are not the same in 950 ms.
Task 185: correctly said that (sylheti, 15, a1j8s7giyto4a) and (atemayar, 25, ax5gghhj0o6g) are not the same in 648 ms.
Task 186: correctly said that (inuktitut, 2, a2pfktghg1ofp) and (inuktitut, 2, a2pgu4pi93b1q) are the same in 928 ms.
Task 187: correctly said that (sylheti, 3, a6fjbvuzrhm6) and (tagalog, 10, a2du3880jbrov) are not the same in 786 ms.
Task 188: correctly said that (inuktitut, 11, a1w3fo0d7cf5t) and (inuktitut, 11, avlbkm6qpb1s) are the same in 1099 ms.
Task 189: correctly said that (inuktitut, 3, a315xddd8j4x8) and (inuktitut, 3, a3noo9k3y1yu5) are the same in 1022 ms.
Task 190: correctly said that (tagalog, 10, aljsxt6v0a30) and (sylheti, 15, a1qku9mccvl35) are not the same in 1098 ms.
Task 191: correctly said that (tagalog, 6, a1yovlluv1c9h) and (inuktitut, 3, aljsxt6v0a30) are not the same in 957 ms.
Task 192: correctly said that (korean, 4, aav4pzxdoebd) and (korean, 4, a1v0p79fynz9j) are the same in 1534 ms.
Task 193: correctly said that (tagalog, 15, a1s3njxhehcip) and (atemayar, 25, a31e6jchki335) are not the same in 971 ms.
Task 194: correctly said that (myanmar, 34, amynpl5xzw6q) and (myanmar, 4, a1stvgjqfl5kk) are not the same in 738 ms.
Task 195: correctly said that (korean, 3, aav4pzxdoebd) and (atemayar, 25, avlbkm6qpb1s) are not the same in 803 ms.
Task 196: incorrectly said that (atemayar, 10, ae0ibwt7asth) and (atemayar, 10, a31e6jchki335) are not the same in 2241 ms.
Task 197: correctly said that (sylheti, 4, atvyxlwe5n03) and (sylheti, 4, a2du3880jbrov) are the same in 1005 ms.
Task 198: correctly said that (korean, 3, a3mv1jtwep7xn) and (atemayar, 25, a31xqftzcsia2) are not the same in 1133 ms.
Task 199: correctly said that (atemayar, 25, a1yovlluv1c9h) and (inuktitut, 12, a3noo9k3y1yu5) are not the same in 840 ms.

Duration: 17m 20s 188ms
Comments: It was challenging and I enjoyed it