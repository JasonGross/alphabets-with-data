results.for_m5100446896490349538(17).duration = '0y 0d 0h 0m 31s 399ms';
results.for_m5100446896490349538(17).form_load_time = int64(1309909303374);
results.for_m5100446896490349538(17).form_submit_time = int64(1309909335208);
results.for_m5100446896490349538(17).jQuery.browser.safari = true;
results.for_m5100446896490349538(17).jQuery.browser.version = '535.1';
results.for_m5100446896490349538(17).jQuery.browser.webkit = true;
results.for_m5100446896490349538(17).navigator.appCodeName = 'Mozilla';
results.for_m5100446896490349538(17).navigator.appName = 'Netscape';
results.for_m5100446896490349538(17).navigator.appVersion = '5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.1 (KHTML, like Gecko) Chrome/13.0.782.99 Safari/535.1';
results.for_m5100446896490349538(17).navigator.platform = 'Win32';
results.for_m5100446896490349538(17).navigator.userAgent = 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.1 (KHTML, like Gecko) Chrome/13.0.782.99 Safari/535.1';
results.for_m5100446896490349538(17).navigator.vendor = 'Google Inc.';
results.for_m5100446896490349538(17).screenAavilHeight = int16(760);
results.for_m5100446896490349538(17).screenAavilWidth = int16(1280);
results.for_m5100446896490349538(17).screenHeight = int16(800);
results.for_m5100446896490349538(17).screenWidth = int16(1280);
results.for_m5100446896490349538(17).submitButton = 'Submit';
results.for_m5100446896490349538(17).task(1).image = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkAQAAAABYmaj5AAAAY0lEQVR4Xu3TMQ6AIBBEUQwFpUfwKBwNEi+23oQjUFqQXdv9jbGUhOleOclMMJf+Xy1p9hpQgwSqUIA2c9LkNSDZX3R41QwVr9PmUITSlIqf216QFCh5NaynQzekkAlkeX36Ae0C2cOKnOiVAAAAAElFTkSuQmCC';
results.for_m5100446896490349538(17).task(1).image_alphabet = 'mkhedruli';
results.for_m5100446896490349538(17).task(1).image_anonymous_url = 'https://jgross.scripts.mit.edu/alphabets/results/accepted-images/a3m7lkvchjee4148gkgu.png';
results.for_m5100446896490349538(17).task(1).image_character_number = int8(41);
results.for_m5100446896490349538(17).task(1).image_id = 'a37lvcje48gkgu';
results.for_m5100446896490349538(17).task(1).image_show_half = 'right';
results.for_m5100446896490349538(17).task(1).seen_before = 'never';
results.for_m5100446896490349538(17).task(1).strokes = '[[{''x'':51,''y'':80,''t'':1311378126928},{''x'':50,''y'':79,''t'':1311378126930},{''x'':50,''y'':79,''t'':1311378126935},{''x'':49,''y'':79,''t'':1311378126976},{''x'':48,''y'':79,''t'':1311378127015},{''x'':47,''y'':79,''t'':1311378127040},{''x'':45,''y'':79,''t'':1311378127065},{''x'':44,''y'':79,''t'':1311378127103},{''x'':44,''y'':78,''t'':1311378127116},{''x'':43,''y'':78,''t'':1311378127142},{''x'':42,''y'':78,''t'':1311378127166},{''x'':41,''y'':78,''t'':1311378127204},{''x'':40,''y'':77,''t'':1311378127228},{''x'':38,''y'':76,''t'':1311378127255},{''x'':37,''y'':75,''t'':1311378127279},{''x'':36,''y'':74,''t'':1311378127304},{''x'':36,''y'':72,''t'':1311378127317},{''x'':35,''y'':71,''t'':1311378127342},{''x'':35,''y'':70,''t'':1311378127406},{''x'':35,''y'':69,''t'':1311378127470},{''x'':35,''y'':68,''t'':1311378127481},{''x'':35,''y'':67,''t'':1311378127494},{''x'':35,''y'':65,''t'':1311378127519},{''x'':35,''y'':64,''t'':1311378127532},{''x'':35,''y'':63,''t'':1311378127545},{''x'':35,''y'':62,''t'':1311378127560},{''x'':35,''y'':61,''t'':1311378127569},{''x'':35,''y'':60,''t'':1311378127608},{''x'':35,''y'':58,''t'':1311378127621},{''x'':35,''y'':58,''t'':1311378127702},{''x'':35,''y'':57,''t'':1311378127861},{''x'':35,''y'':56,''t'':1311378127924},{''x'':36,''y'':42,''t'':1311378128368},{''x'':36,''y'':42,''t'':1311378128686},{''x'':37,''y'':42,''t'':1311378128752},{''x'':39,''y'':42,''t'':1311378128782},{''x'':39,''y'':41,''t'':1311378128807},{''x'':40,''y'':41,''t'':1311378128820},{''x'':40,''y'':40,''t'':1311378128850},{''x'':41,''y'':40,''t'':1311378128896},{''x'':41,''y'':39,''t'':1311378128923},{''x'':42,''y'':39,''t'':1311378128973},{''x'':42,''y'':37,''t'':1311378129010},{''x'':43,''y'':37,''t'':1311378129097},{''x'':43,''y'':36,''t'':1311378129124},{''x'':44,''y'':35,''t'':1311378129150},{''x'':46,''y'':34,''t'':1311378129187},{''x'':47,''y'':34,''t'':1311378129264},{''x'':48,''y'':34,''t'':1311378129301},{''x'':48,''y'':33,''t'':1311378129313},{''x'':49,''y'':33,''t'':1311378129351},{''x'':55,''y'':33,''t'':1311378129818}]]';
results.for_m5100446896490349538(17).task(1).time_of_do_task = int64(1309909306087);
results.for_m5100446896490349538(17).task(1).time_of_finish_task = int64(1309909332740);
results.for_m5100446896490349538(17).url_parameter_characterSize = '100px';
results.for_m5100446896490349538(17).url_parameter_imageHalf = 'left,right';
results.for_m5100446896490349538(17).url_parameter_taskCount = int8(1);
results.for_m5100446896490349538(17).windowHeight = int16(333);
results.for_m5100446896490349538(17).windowWidth = int16(1263);




r
e
s
u
l
t
s
.
f
o
r
_
m
5
1
0
0
4
4
6
8
9
6
4
9
0
3
4
9
5
3
8
(
1
7
)
.
t
a
s
k
(
1
)
.
d
r
a
w
n
_
i
m
a
g
e
 
=
 
i
m
r
e
a
d
(
'
/
a
f
s
/
a
t
h
e
n
a
.
m
i
t
.
e
d
u
/
u
s
e
r
/
j
/
g
/
j
g
r
o
s
s
/
w
e
b
_
s
c
r
i
p
t
s
/
a
l
p
h
a
b
e
t
s
/
r
e
s
u
l
t
s
/
c
o
m
p
l
e
t
i
o
n
/
u
n
r
e
v
i
e
w
e
d
/
m
5
1
0
0
4
4
6
8
9
6
4
9
0
3
4
9
5
3
8
/
1
6
/
m
5
1
0
0
4
4
6
8
9
6
4
9
0
3
4
9
5
3
8
_
i
m
a
g
e
_
1
.
p
n
g
'
)
;