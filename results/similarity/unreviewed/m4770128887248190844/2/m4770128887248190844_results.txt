duration: 0y 0d 0h 0m 10s 121ms
form-load-time: 1309477963689
form-submit-time: 1309477974500
jQuery.browser.safari: true
jQuery.browser.version: 535.1
jQuery.browser.webkit: true
navigator.appCodeName: Mozilla
navigator.appName: Netscape
navigator.appVersion: 5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.1 (KHTML, like Gecko) Chrome/13.0.782.41 Safari/535.1
navigator.platform: Win32
navigator.userAgent: Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.1 (KHTML, like Gecko) Chrome/13.0.782.41 Safari/535.1
navigator.vendor: Google Inc.
screenAavilHeight: 760
screenAavilWidth: 1280
screenHeight: 800
screenWidth: 1280
submitButton: Submit
task-0-answer: 7
task-0-are-same: true
task-0-character-0-alphabet: katakana
task-0-character-0-anonymous_url: https://jgross.scripts.mit.edu/alphabets/results/accepted-images/a2kpxa0atywa25txngce.png
task-0-character-0-character-number: 25
task-0-character-0-id: a2px0aywxngce
task-0-character-1-alphabet: katakana
task-0-character-1-anonymous_url: https://jgross.scripts.mit.edu/alphabets/results/accepted-images/axkgfa0utu5a25po5jh.png
task-0-character-1-character-number: 25
task-0-character-1-id: axgf0uu5o5jh
task-0-time-of-do-task: ['1309477965698', '1309477965704']
task-0-time-of-finish-task: 1309477970740
task-1-answer: 1
task-1-are-same: false
task-1-character-0-alphabet: cyrillic
task-1-character-0-anonymous_url: https://jgross.scripts.mit.edu/alphabets/results/accepted-images/a3cstyxercei30wj2ykp.png
task-1-character-0-character-number: 30
task-1-character-0-id: a3stxecej2ykp
task-1-character-1-alphabet: mongolian
task-1-character-1-anonymous_url: https://jgross.scripts.mit.edu/alphabets/results/accepted-images/a1mfooggntlg11wgo7n4.png
task-1-character-1-character-number: 11
task-1-character-1-id: a1foggtlgo7n4
task-1-time-of-do-task: ['1309477970749', '1309477970755']
task-1-time-of-finish-task: 1309477972804
url-parameter-allowDidNotSeeCount: 1,0
url-parameter-characterSet: large_experiment
url-parameter-characterSize: 100px
url-parameter-differentAlphabetFraction: -1
url-parameter-displayProgressBarDuringTask: false
url-parameter-pauseToNextGroup: -1,0
url-parameter-sameFraction: 0.5
url-parameter-taskCount: 2
url-parameter-tasksPerFeedbackGroup: 10,0
url-parameter-unique: true
weirdness: ('task-0-time-of-do-task', <type 'list'>, "['1309477965698', '1309477965704']");('task-1-time-of-do-task', <type 'list'>, "['1309477970749', '1309477970755']")
windowHeight: 442
windowWidth: 1263
