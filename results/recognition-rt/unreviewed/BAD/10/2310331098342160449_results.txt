calibration_task-0-keypress: {"altKey":false,"charCode":32,"ctrlKey":false,"keyCode":32,"metaKey":false,"shiftKey":false,"timeStamp":1300752703886,"which":32}
calibration_task-0-keypress_before_show_prompt_count: 0
calibration_task-0-time-of-doTask: 1299024702479
calibration_task-0-time-of-keypress: 1299024703886
calibration_task-0-time-of-showCharacters: 1299024703197
duration: 0y 0d 0h 0m 22s 421ms
form-load-time: 1299024696894
form-submit-time: 1299024720511
jQuery.browser.safari: true
jQuery.browser.version: 534.16
jQuery.browser.webkit: true
navigator.appCodeName: Mozilla
navigator.appName: Netscape
navigator.appVersion: 5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/534.16 (KHTML, like Gecko) Chrome/10.0.648.151 Safari/534.16
navigator.platform: Win32
navigator.userAgent: Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/534.16 (KHTML, like Gecko) Chrome/10.0.648.151 Safari/534.16
navigator.vendor: Google Inc.
screenAavilHeight: 760
screenAavilWidth: 1280
screenHeight: 800
screenWidth: 1280
submitButton: Submit
task-0-are-same: false
task-0-character-0-alphabet: korean
task-0-character-0-anonymous_url: http://scripts.mit.edu/~jgross/alphabets/results/accepted-images/a1koho82rlwe28dr5hbg.png
task-0-character-0-character-number: 28
task-0-character-0-id: a1oh82lwr5hbg
task-0-character-1-alphabet: myanmar
task-0-character-1-anonymous_url: http://scripts.mit.edu/~jgross/alphabets/results/accepted-images/a1mehyrxajpn32klsq0r.png
task-0-character-1-character-number: 32
task-0-character-1-id: a1ehrxjplsq0r
task-0-is-correct-answer: false
task-0-noise-image-0-url: http://jgross.scripts.mit.edu/alphabets/images/strokeNoise3.png
task-0-noise-image-1-url: http://jgross.scripts.mit.edu/alphabets/images/strokeNoise5.png
task-0-noise-image-2-url: http://jgross.scripts.mit.edu/alphabets/images/strokeNoise3.png
task-0-noise-image-3-url: http://jgross.scripts.mit.edu/alphabets/images/strokeNoise2.png
task-0-noise-image-4-url: http://jgross.scripts.mit.edu/alphabets/images/strokeNoise7.png
task-0-time-of-doTask: 1299024704940
task-0-time-of-showCharacters: 1299024706452
task-1-are-same: false
task-1-character-0-alphabet: inuktitut
task-1-character-0-anonymous_url: http://scripts.mit.edu/~jgross/alphabets/results/accepted-images/a1imannxuzok01lpzden.png
task-1-character-0-character-number: 01
task-1-character-0-id: a1manxzopzden
task-1-character-1-alphabet: atemayar
task-1-character-1-anonymous_url: http://scripts.mit.edu/~jgross/alphabets/results/accepted-images/a1avzthyep8m23byvxkz.png
task-1-character-1-character-number: 23
task-1-character-1-id: a1vzhyp8yvxkz
task-1-is-correct-answer: true
task-1-keypress: {"altKey":false,"charCode":32,"ctrlKey":false,"keyCode":32,"metaKey":false,"shiftKey":false,"timeStamp":1300752715558,"which":32}
task-1-keypress_before_show_prompt_count: 0
task-1-noise-image-0-url: http://jgross.scripts.mit.edu/alphabets/images/strokeNoise7.png
task-1-noise-image-1-url: http://jgross.scripts.mit.edu/alphabets/images/strokeNoise6.png
task-1-noise-image-2-url: http://jgross.scripts.mit.edu/alphabets/images/strokeNoise1.png
task-1-noise-image-3-url: http://jgross.scripts.mit.edu/alphabets/images/strokeNoise2.png
task-1-noise-image-4-url: http://jgross.scripts.mit.edu/alphabets/images/strokeNoise2.png
task-1-time-of-doTask: 1299024711515
task-1-time-of-keypress: 1299024715559
task-1-time-of-showCharacters: 1299024712749
url-parameter-calibrationTaskCount: 1
url-parameter-characterSet: small_experiment2
url-parameter-displayProgressBarDuringTask: false
url-parameter-fractionSame: 0.25
url-parameter-pauseToGroup: -1,0
url-parameter-random: true
url-parameter-tasksPerFeedbackGroup: -1,0
url-parameter-trialsPerExperiment: 2
windowHeight: 632
windowWidth: 1263
