Summary:
Calibration:
6050
1026
770
621
409
494
336
880
720
1078
406
316
762
381
687
325
331
374
308
427
354
323
372
637
1360
Average: 789


Tasks:
Task 0: atemayar, 25, agst3co6usf8 VS. sylheti, 20, a1j8s7giyto4a
Task 0: Wrong, Are Different
Task 1: inuktitut, 11, a12xm86d2nbih VS. inuktitut, 11, a315xddd8j4x8
Task 1: Wrong, Are Same
Task 1 RT: 970
Task 2: korean, 28, a1udqv4pfypkn VS. inuktitut, 01, a1qku9mccvl35
Task 2: Right, Are Different
Task 2 RT: 1217
Task 3: korean, 29, a1manxzopzden VS. myanmar, 32, amynpl5xzw6q
Task 3: Right, Are Different
Task 3 RT: 1416
Task 4: inuktitut, 02, avlbkm6qpb1s VS. inuktitut, 02, a2pfktghg1ofp
Task 4: Right, Are Same
Task 5: sylheti, 20, a1l2fzyn31zvf VS. atemayar, 25, a1vzhyp8yvxkz
Task 5: Right, Are Different
Task 5 RT: 934
Task 6: korean, 04, a3d7xwp0gand5 VS. sylheti, 17, a1l2fzyn31zvf
Task 6: Right, Are Different
Task 6 RT: 1320
Task 7: sylheti, 17, a1l2fzyn31zvf VS. korean, 29, a3mv1jtwep7xn
Task 7: Right, Are Different
Task 7 RT: 1300
Task 8: inuktitut, 02, a3mv1jtwep7xn VS. sylheti, 19, a1l2fzyn31zvf
Task 8: Right, Are Different
Task 8 RT: 1224
Task 9: myanmar, 32, avlbkm6qpb1s VS. tagalog, 02, a1j8s7giyto4a
Task 9: Right, Are Different
Task 9 RT: 1553
Task 10: inuktitut, 11, ay0bjcgydhix VS. inuktitut, 11, a1manxzopzden
Task 10: Right, Are Same
Task 11: tagalog, 06, avlbkm6qpb1s VS. sylheti, 17, a3209gno9duil
Task 11: Right, Are Different
Task 11 RT: 1073
Task 12: inuktitut, 01, a2pfktghg1ofp VS. inuktitut, 01, a12rfyr8dbn79
Task 12: Right, Are Same
Task 13: korean, 39, a1oh82lwr5hbg VS. myanmar, 32, aljsxt6v0a30
Task 13: Right, Are Different
Task 13 RT: 1199
Task 14: myanmar, 25, a1umhcrj9zfdp VS. myanmar, 25, a1clejhbx9yyn
Task 14: Right, Are Same
Task 15: inuktitut, 01, a2ernxe6jlm4x VS. atemayar, 10, avlbkm6qpb1s
Task 15: Right, Are Different
Task 15 RT: 875
Task 16: inuktitut, 01, a1j8s7giyto4a VS. inuktitut, 01, a2ernxe6jlm4x
Task 16: Right, Are Same
Task 17: korean, 39, a1umhcrj9zfdp VS. korean, 39, aav4pzxdoebd
Task 17: Right, Are Same
Task 18: inuktitut, 11, ay0bjcgydhix VS. inuktitut, 01, a3mv1jtwep7xn
Task 18: Right, Are Different
Task 18 RT: 2066
Task 19: korean, 28, a2nm1qzuh14ls VS. korean, 28, ae0ibwt7asth
Task 19: Wrong, Are Same
Task 19 RT: 3160
Task 20: sylheti, 20, a3209gno9duil VS. korean, 29, a2nm1qzuh14ls
Task 20: Right, Are Different
Task 20 RT: 1282
Task 21: myanmar, 32, a1umhcrj9zfdp VS. atemayar, 25, a31xqftzcsia2
Task 21: Right, Are Different
Task 21 RT: 1103
Task 22: tagalog, 10, avlbkm6qpb1s VS. tagalog, 10, amynpl5xzw6q
Task 22: Wrong, Are Same
Task 22 RT: 2595
Task 23: tagalog, 05, avlbkm6qpb1s VS. inuktitut, 01, a2pgu4pi93b1q
Task 23: Wrong, Are Different
Task 24: inuktitut, 02, a3mv1jtwep7xn VS. inuktitut, 01, a12xm86d2nbih
Task 24: Right, Are Different
Task 24 RT: 973
Task 25: atemayar, 23, a1qku9mccvl35 VS. inuktitut, 11, a1j8s7giyto4a
Task 25: Right, Are Different
Task 25 RT: 941
Task 26: tagalog, 06, a1l2fzyn31zvf VS. inuktitut, 11, a1z59eb5sfxpy
Task 26: Right, Are Different
Task 26 RT: 1145
Task 27: sylheti, 19, ay0bjcgydhix VS. sylheti, 19, a3r9q9wqm1nyz
Task 27: Wrong, Are Same
Task 27 RT: 2045
Task 28: atemayar, 25, a1yovlluv1c9h VS. atemayar, 25, awl8tng8e81d
Task 28: Wrong, Are Same
Task 28 RT: 1752
Task 29: atemayar, 06, a6fjbvuzrhm6 VS. myanmar, 31, a4858n7ggst4
Task 29: Right, Are Different
Task 29 RT: 839
Task 30: myanmar, 32, avlbkm6qpb1s VS. tagalog, 05, a1s3njxhehcip
Task 30: Right, Are Different
Task 30 RT: 1584
Task 31: atemayar, 06, ae0ibwt7asth VS. atemayar, 06, a31e6jchki335
Task 31: Right, Are Same
Task 32: tagalog, 06, a2du3880jbrov VS. tagalog, 06, a1yovlluv1c9h
Task 32: Wrong, Are Same
Task 32 RT: 1700
Task 33: sylheti, 19, a4858n7ggst4 VS. sylheti, 19, a3r9q9wqm1nyz
Task 33: Wrong, Are Same
Task 33 RT: 2738
Task 34: tagalog, 05, a1a25pyfoa9ge VS. tagalog, 05, a3m0kt6kjezd1
Task 34: Right, Are Same
Task 35: sylheti, 28, a3m0kt6kjezd1 VS. sylheti, 28, a30xq7555spo8
Task 35: Wrong, Are Same
Task 35 RT: 2687
Task 36: inuktitut, 11, a315xddd8j4x8 VS. inuktitut, 11, ay0bjcgydhix
Task 36: Right, Are Same
Task 37: tagalog, 06, a1ehrxjplsq0r VS. tagalog, 02, a1j8s7giyto4a
Task 37: Right, Are Different
Task 37 RT: 1251
Task 38: korean, 39, a3bb99mn3zy37 VS. korean, 39, a2akud1gdstsd
Task 38: Right, Are Same
Task 39: sylheti, 19, aljsxt6v0a30 VS. sylheti, 19, a1foggtlgo7n4
Task 39: Right, Are Same
Task 40: inuktitut, 03, a2qnqd7m8zfbo VS. korean, 29, a1v0p79fynz9j
Task 40: Right, Are Different
Task 40 RT: 1222
Task 41: inuktitut, 01, a1s3njxhehcip VS. tagalog, 05, avlbkm6qpb1s
Task 41: Right, Are Different
Task 41 RT: 775
Task 42: myanmar, 31, a1ehrxjplsq0r VS. myanmar, 31, a1aegunf6e081
Task 42: Right, Are Same
Task 43: sylheti, 19, a1j8s7giyto4a VS. sylheti, 19, a3m0kt6kjezd1
Task 43: Right, Are Same
Task 44: inuktitut, 11, a1j8s7giyto4a VS. inuktitut, 11, a3qat9qkqumi2
Task 44: Right, Are Same
Task 45: inuktitut, 01, a1w3fo0d7cf5t VS. inuktitut, 03, a3qat9qkqumi2
Task 45: Right, Are Different
Task 45 RT: 741
Task 46: atemayar, 06, a6fjbvuzrhm6 VS. atemayar, 06, awl8tng8e81d
Task 46: Right, Are Same
Task 47: tagalog, 05, amynpl5xzw6q VS. atemayar, 06, ae0ibwt7asth
Task 47: Right, Are Different
Task 47 RT: 868
Task 48: atemayar, 25, a1qku9mccvl35 VS. atemayar, 25, ax5gghhj0o6g
Task 48: Wrong, Are Same
Task 48 RT: 2861
Task 49: myanmar, 01, adc8vbkoocrr VS. atemayar, 06, agst3co6usf8
Task 49: Right, Are Different
Task 49 RT: 1180
Task 50: inuktitut, 01, a3mv1jtwep7xn VS. inuktitut, 01, a1z59eb5sfxpy
Task 50: Wrong, Are Same
Task 50 RT: 1309
Task 51: korean, 28, a2nm1qzuh14ls VS. korean, 28, a1oh82lwr5hbg
Task 51: Wrong, Are Same
Task 51 RT: 1512
Task 52: atemayar, 23, a1manxzopzden VS. tagalog, 05, ajf66jwen7bx
Task 52: Right, Are Different
Task 52 RT: 1037
Task 53: korean, 39, a39ivbdm1ekue VS. korean, 39, aav4pzxdoebd
Task 53: Wrong, Are Same
Task 53 RT: 1746
Task 54: inuktitut, 01, a315xddd8j4x8 VS. inuktitut, 01, aljsxt6v0a30
Task 54: Right, Are Same
Task 55: myanmar, 32, a1foggtlgo7n4 VS. myanmar, 32, a1clejhbx9yyn
Task 55: Right, Are Same
Task 56: inuktitut, 01, a12rfyr8dbn79 VS. atemayar, 23, a1qku9mccvl35
Task 56: Right, Are Different
Task 56 RT: 849
Task 57: sylheti, 20, a4858n7ggst4 VS. sylheti, 20, a1l2fzyn31zvf
Task 57: Right, Are Same
Task 58: atemayar, 06, a1qku9mccvl35 VS. inuktitut, 01, a3noo9k3y1yu5
Task 58: Right, Are Different
Task 58 RT: 831
Task 59: tagalog, 10, a3m0kt6kjezd1 VS. tagalog, 10, aljsxt6v0a30
Task 59: Right, Are Same
Task 60: sylheti, 17, a1l2fzyn31zvf VS. sylheti, 17, a30xq7555spo8
Task 60: Wrong, Are Same
Task 60 RT: 2439
Task 61: atemayar, 25, azdw6062ia38 VS. korean, 39, ae0ibwt7asth
Task 61: Right, Are Different
Task 61 RT: 987
Task 62: myanmar, 32, a3qat9qkqumi2 VS. tagalog, 02, a1yovlluv1c9h
Task 62: Right, Are Different
Task 62 RT: 998
Task 63: korean, 39, a1manxzopzden VS. atemayar, 25, a31xqftzcsia2
Task 63: Right, Are Different
Task 63 RT: 800
Task 64: inuktitut, 03, a3mv1jtwep7xn VS. inuktitut, 03, a2ernxe6jlm4x
Task 64: Right, Are Same
Task 65: korean, 28, a1s3njxhehcip VS. korean, 28, a1oh82lwr5hbg
Task 65: Wrong, Are Same
Task 65 RT: 1337
Task 66: inuktitut, 01, ae0ibwt7asth VS. inuktitut, 02, ae0ibwt7asth
Task 66: Right, Are Different
Task 66 RT: 1149
Task 67: tagalog, 02, a3mv1jtwep7xn VS. tagalog, 02, aghrkle9vf6z
Task 67: Wrong, Are Same
Task 67 RT: 2504
Task 68: korean, 29, a2ernxe6jlm4x VS. myanmar, 31, a3qat9qkqumi2
Task 68: Right, Are Different
Task 68 RT: 927
Task 69: inuktitut, 11, a3mv1jtwep7xn VS. inuktitut, 11, a12xm86d2nbih
Task 69: Right, Are Same
Task 70: sylheti, 20, a6fjbvuzrhm6 VS. sylheti, 20, a3r9q9wqm1nyz
Task 70: Right, Are Same
Task 71: sylheti, 20, a30xq7555spo8 VS. atemayar, 23, a1j8s7giyto4a
Task 71: Right, Are Different
Task 71 RT: 809
Task 72: atemayar, 25, a6fjbvuzrhm6 VS. sylheti, 19, a3m0kt6kjezd1
Task 72: Right, Are Different
Task 72 RT: 884
Task 73: tagalog, 10, aljsxt6v0a30 VS. myanmar, 01, ay0bjcgydhix
Task 73: Right, Are Different
Task 73 RT: 851
Task 74: sylheti, 20, a3r9q9wqm1nyz VS. inuktitut, 11, a1j8s7giyto4a
Task 74: Right, Are Different
Task 74 RT: 963
Task 75: inuktitut, 02, a1qku9mccvl35 VS. inuktitut, 02, a1s3njxhehcip
Task 75: Right, Are Same
Task 76: tagalog, 06, a1a25pyfoa9ge VS. tagalog, 10, a1l2fzyn31zvf
Task 76: Right, Are Different
Task 76 RT: 676
Task 77: sylheti, 28, a6fjbvuzrhm6 VS. myanmar, 01, a2pfktghg1ofp
Task 77: Right, Are Different
Task 77 RT: 814
Task 78: inuktitut, 01, a1w3fo0d7cf5t VS. inuktitut, 01, a1j8s7giyto4a
Task 78: Right, Are Same
Task 79: inuktitut, 11, aljsxt6v0a30 VS. myanmar, 32, a3qat9qkqumi2
Task 79: Right, Are Different
Task 79 RT: 726
Task 80: myanmar, 01, amynpl5xzw6q VS. myanmar, 01, a3qat9qkqumi2
Task 80: Right, Are Same
Task 81: inuktitut, 02, a2pfktghg1ofp VS. myanmar, 01, aljsxt6v0a30
Task 81: Right, Are Different
Task 81 RT: 722
Task 82: sylheti, 28, a2du3880jbrov VS. sylheti, 28, avlbkm6qpb1s
Task 82: Right, Are Same
Task 83: korean, 39, a1oh82lwr5hbg VS. tagalog, 10, amynpl5xzw6q
Task 83: Right, Are Different
Task 83 RT: 858
Task 84: korean, 04, a1v0p79fynz9j VS. korean, 04, a1udqv4pfypkn
Task 84: Wrong, Are Same
Task 84 RT: 2051
Task 85: korean, 04, ae0ibwt7asth VS. atemayar, 10, ae0ibwt7asth
Task 85: Right, Are Different
Task 85 RT: 1021
Task 86: myanmar, 32, a1umhcrj9zfdp VS. myanmar, 32, a4858n7ggst4
Task 86: Right, Are Same
Task 87: atemayar, 10, a6fjbvuzrhm6 VS. atemayar, 10, azdw6062ia38
Task 87: Right, Are Same
Task 88: tagalog, 02, a12rfyr8dbn79 VS. inuktitut, 02, a12rfyr8dbn79
Task 88: Right, Are Different
Task 88 RT: 690
Task 89: atemayar, 10, ae0ibwt7asth VS. atemayar, 10, a1j8s7giyto4a
Task 89: Right, Are Same
Task 90: myanmar, 31, a1j8s7giyto4a VS. myanmar, 31, a1foggtlgo7n4
Task 90: Wrong, Are Same
Task 90 RT: 1658
Task 91: tagalog, 02, a1l2fzyn31zvf VS. myanmar, 01, a1clejhbx9yyn
Task 91: Right, Are Different
Task 91 RT: 889
Task 92: myanmar, 01, a1aegunf6e081 VS. atemayar, 23, azdw6062ia38
Task 92: Right, Are Different
Task 92 RT: 860
Task 93: sylheti, 20, a6fjbvuzrhm6 VS. atemayar, 10, ae0ibwt7asth
Task 93: Right, Are Different
Task 93 RT: 733
Task 94: sylheti, 20, a3np21apfa18b VS. korean, 29, a1v0p79fynz9j
Task 94: Right, Are Different
Task 94 RT: 687
Task 95: inuktitut, 03, ae0ibwt7asth VS. korean, 28, a3d7xwp0gand5
Task 95: Right, Are Different
Task 95 RT: 709
Task 96: myanmar, 32, a1ehrxjplsq0r VS. korean, 04, a1v0p79fynz9j
Task 96: Right, Are Different
Task 96 RT: 812
Task 97: korean, 04, aav4pzxdoebd VS. korean, 04, a5379luit3pc
Task 97: Right, Are Same
Task 98: inuktitut, 01, a3mv1jtwep7xn VS. atemayar, 25, avlbkm6qpb1s
Task 98: Right, Are Different
Task 98 RT: 2586
Task 99: inuktitut, 01, a2ernxe6jlm4x VS. myanmar, 01, a1stvgjqfl5kk
Task 99: Right, Are Different
Task 99 RT: 1029
Task 100: inuktitut, 11, a3mv1jtwep7xn VS. atemayar, 06, a31e6jchki335
Task 100: Right, Are Different
Task 100 RT: 3214
Task 101: tagalog, 10, ajf66jwen7bx VS. korean, 29, a3bb99mn3zy37
Task 101: Right, Are Different
Task 101 RT: 1387
Task 102: sylheti, 17, avlbkm6qpb1s VS. inuktitut, 11, a2qnqd7m8zfbo
Task 102: Right, Are Different
Task 102 RT: 683
Task 103: korean, 04, a3dfh6dpi11ip VS. myanmar, 31, a6fjbvuzrhm6
Task 103: Right, Are Different
Task 103 RT: 863
Task 104: korean, 39, a1s3njxhehcip VS. sylheti, 17, amynpl5xzw6q
Task 104: Right, Are Different
Task 104 RT: 1018
Task 105: korean, 28, a1umhcrj9zfdp VS. korean, 39, a1v0p79fynz9j
Task 105: Right, Are Different
Task 105 RT: 937
Task 106: korean, 04, a3bb99mn3zy37 VS. korean, 04, a2ernxe6jlm4x
Task 106: Wrong, Are Same
Task 106 RT: 1320
Task 107: inuktitut, 01, a1z59eb5sfxpy VS. inuktitut, 11, avlbkm6qpb1s
Task 107: Right, Are Different
Task 107 RT: 651
Task 108: atemayar, 10, a2pfktghg1ofp VS. atemayar, 10, a1yovlluv1c9h
Task 108: Right, Are Same
Task 109: tagalog, 10, avlbkm6qpb1s VS. tagalog, 05, aljsxt6v0a30
Task 109: Right, Are Different
Task 109 RT: 670
Task 110: myanmar, 01, avlbkm6qpb1s VS. myanmar, 01, ay0bjcgydhix
Task 110: Right, Are Same
Task 111: tagalog, 02, asu813kb7bv1 VS. tagalog, 06, a12rfyr8dbn79
Task 111: Right, Are Different
Task 111 RT: 892
Task 112: myanmar, 31, a3pkun3onkucn VS. tagalog, 06, a1a25pyfoa9ge
Task 112: Right, Are Different
Task 112 RT: 640
Task 113: atemayar, 23, a31xqftzcsia2 VS. sylheti, 17, avlbkm6qpb1s
Task 113: Right, Are Different
Task 113 RT: 770
Task 114: inuktitut, 03, a3mv1jtwep7xn VS. atemayar, 23, a1manxzopzden
Task 114: Right, Are Different
Task 114 RT: 560
Task 115: atemayar, 25, a3m0kt6kjezd1 VS. sylheti, 28, a1s3njxhehcip
Task 115: Right, Are Different
Task 115 RT: 717
Task 116: inuktitut, 01, a2pgu4pi93b1q VS. inuktitut, 01, ae0ibwt7asth
Task 116: Right, Are Same
Task 117: myanmar, 01, a3j1pp96x14u0 VS. myanmar, 01, a1s3njxhehcip
Task 117: Wrong, Are Same
Task 117 RT: 2112
Task 118: korean, 04, a2nm1qzuh14ls VS. korean, 04, a1manxzopzden
Task 118: Right, Are Same
Task 119: myanmar, 25, a3qat9qkqumi2 VS. myanmar, 25, a1s3njxhehcip
Task 119: Right, Are Same
Task 120: tagalog, 05, aghrkle9vf6z VS. korean, 29, ae0ibwt7asth
Task 120: Right, Are Different
Task 120 RT: 2671
Task 121: tagalog, 10, a1j8s7giyto4a VS. tagalog, 10, a1l2fzyn31zvf
Task 121: Right, Are Same
Task 122: sylheti, 28, a2du3880jbrov VS. sylheti, 28, a1foggtlgo7n4
Task 122: Right, Are Same
Task 123: atemayar, 10, ae0ibwt7asth VS. atemayar, 10, a1z59eb5sfxpy
Task 123: Right, Are Same
Task 124: myanmar, 32, a1aegunf6e081 VS. myanmar, 32, a1j8s7giyto4a
Task 124: Right, Are Same
Task 125: inuktitut, 02, a2ernxe6jlm4x VS. inuktitut, 11, a12xm86d2nbih
Task 125: Right, Are Different
Task 125 RT: 842
Task 126: myanmar, 25, a1aegunf6e081 VS. myanmar, 25, a2pfktghg1ofp
Task 126: Right, Are Same
Task 127: atemayar, 06, a31xqftzcsia2 VS. inuktitut, 01, a315xddd8j4x8
Task 127: Right, Are Different
Task 127 RT: 781
Task 128: sylheti, 17, aljsxt6v0a30 VS. sylheti, 17, a3np21apfa18b
Task 128: Right, Are Same
Task 129: inuktitut, 03, ae0ibwt7asth VS. inuktitut, 03, a12xm86d2nbih
Task 129: Right, Are Same
Task 130: korean, 04, a1udqv4pfypkn VS. korean, 04, a3bb99mn3zy37
Task 130: Right, Are Same
Task 131: inuktitut, 02, ae0ibwt7asth VS. inuktitut, 02, a1z59eb5sfxpy
Task 131: Right, Are Same
Task 132: inuktitut, 11, a1j8s7giyto4a VS. atemayar, 10, a1j8s7giyto4a
Task 132: Right, Are Different
Task 132 RT: 901
Task 133: atemayar, 06, a1manxzopzden VS. korean, 28, a2ernxe6jlm4x
Task 133: Right, Are Different
Task 133 RT: 680
Task 134: inuktitut, 01, ae0ibwt7asth VS. inuktitut, 01, a2pgu4pi93b1q
Task 134: Right, Are Same
Task 135: atemayar, 23, agst3co6usf8 VS. atemayar, 23, avlbkm6qpb1s
Task 135: Right, Are Same
Task 136: korean, 39, aav4pzxdoebd VS. tagalog, 02, ajf66jwen7bx
Task 136: Right, Are Different
Task 136 RT: 863
Task 137: tagalog, 05, aljsxt6v0a30 VS. tagalog, 05, a1ehrxjplsq0r
Task 137: Right, Are Same
Task 138: tagalog, 05, a1a25pyfoa9ge VS. inuktitut, 02, a3qat9qkqumi2
Task 138: Right, Are Different
Task 138 RT: 621
Task 139: sylheti, 20, a1s3njxhehcip VS. sylheti, 20, a1a25pyfoa9ge
Task 139: Wrong, Are Same
Task 139 RT: 2047
Task 140: korean, 39, a2ernxe6jlm4x VS. sylheti, 20, a2du3880jbrov
Task 140: Right, Are Different
Task 140 RT: 1097
Task 141: korean, 29, aj6upe9mlarg VS. tagalog, 10, amynpl5xzw6q
Task 141: Right, Are Different
Task 141 RT: 657
Task 142: tagalog, 02, a1s3njxhehcip VS. atemayar, 25, a23psx2z4f65q
Task 142: Right, Are Different
Task 142 RT: 628
Task 143: tagalog, 06, a1s3njxhehcip VS. tagalog, 06, avlbkm6qpb1s
Task 143: Wrong, Are Same
Task 143 RT: 660
Task 144: tagalog, 10, ay0bjcgydhix VS. tagalog, 10, aljsxt6v0a30
Task 144: Right, Are Same
Task 145: myanmar, 31, a1j8s7giyto4a VS. myanmar, 31, amynpl5xzw6q
Task 145: Right, Are Same
Task 146: myanmar, 01, a1umhcrj9zfdp VS. myanmar, 01, a6fjbvuzrhm6
Task 146: Wrong, Are Same
Task 146 RT: 1604
Task 147: korean, 39, a1v0p79fynz9j VS. korean, 39, aw82oruamauz
Task 147: Right, Are Same
Task 148: atemayar, 10, a3m0kt6kjezd1 VS. atemayar, 06, a1vzhyp8yvxkz
Task 148: Right, Are Different
Task 148 RT: 1430
Task 149: sylheti, 17, a30xq7555spo8 VS. sylheti, 17, ay0bjcgydhix
Task 149: Wrong, Are Same
Task 149 RT: 2550
Task 150: inuktitut, 11, ay0bjcgydhix VS. sylheti, 20, ay0bjcgydhix
Task 150: Right, Are Different
Task 150 RT: 2134
Task 151: sylheti, 17, a30xq7555spo8 VS. atemayar, 10, a1manxzopzden
Task 151: Right, Are Different
Task 151 RT: 759
Task 152: tagalog, 05, a3mv1jtwep7xn VS. tagalog, 05, a1j8s7giyto4a
Task 152: Wrong, Are Same
Task 152 RT: 935
Task 153: tagalog, 02, a1yovlluv1c9h VS. atemayar, 06, ae0ibwt7asth
Task 153: Right, Are Different
Task 153 RT: 783
Task 154: atemayar, 06, avlbkm6qpb1s VS. atemayar, 06, azdw6062ia38
Task 154: Wrong, Are Same
Task 154 RT: 1352
Task 155: korean, 04, a2px0aywxngce VS. atemayar, 06, azdw6062ia38
Task 155: Right, Are Different
Task 155 RT: 1112
Task 156: myanmar, 31, a3pkun3onkucn VS. myanmar, 31, a3qat9qkqumi2
Task 156: Wrong, Are Same
Task 156 RT: 1416
Task 157: myanmar, 32, a1umhcrj9zfdp VS. korean, 39, a1s3njxhehcip
Task 157: Right, Are Different
Task 157 RT: 699
Task 158: atemayar, 25, a1qku9mccvl35 VS. atemayar, 25, a31e6jchki335
Task 158: Right, Are Same
Task 159: myanmar, 31, a3qat9qkqumi2 VS. myanmar, 31, a1j8s7giyto4a
Task 159: Wrong, Are Same
Task 159 RT: 1390
Task 160: tagalog, 02, aghrkle9vf6z VS. tagalog, 02, a1j8s7giyto4a
Task 160: Wrong, Are Same
Task 160 RT: 2021
Task 161: korean, 04, a39ivbdm1ekue VS. korean, 04, aj6upe9mlarg
Task 161: Wrong, Are Same
Task 161 RT: 2023
Task 162: atemayar, 25, a3m0kt6kjezd1 VS. atemayar, 25, a6fjbvuzrhm6
Task 162: Wrong, Are Same
Task 162 RT: 2118
Task 163: tagalog, 05, aghrkle9vf6z VS. tagalog, 05, amynpl5xzw6q
Task 163: Wrong, Are Same
Task 163 RT: 1163
Task 164: atemayar, 23, a6fjbvuzrhm6 VS. atemayar, 23, a1yovlluv1c9h
Task 164: Wrong, Are Same
Task 164 RT: 2378
Task 165: myanmar, 25, aljsxt6v0a30 VS. tagalog, 02, amynpl5xzw6q
Task 165: Right, Are Different
Task 165 RT: 856
Task 166: korean, 28, a1umhcrj9zfdp VS. korean, 28, a1v0p79fynz9j
Task 166: Wrong, Are Same
Task 166 RT: 782
Task 167: tagalog, 10, ajf66jwen7bx VS. tagalog, 10, asu813kb7bv1
Task 167: Wrong, Are Same
Task 167 RT: 675
Task 168: inuktitut, 02, a3noo9k3y1yu5 VS. inuktitut, 02, a1w3fo0d7cf5t
Task 168: Right, Are Same
Task 169: inuktitut, 03, a1s3njxhehcip VS. inuktitut, 03, a3qat9qkqumi2
Task 169: Right, Are Same
Task 170: sylheti, 19, a3np21apfa18b VS. sylheti, 19, a1j8s7giyto4a
Task 170: Wrong, Are Same
Task 170 RT: 2956
Task 171: atemayar, 25, a31e6jchki335 VS. atemayar, 25, a23psx2z4f65q
Task 171: Right, Are Same
Task 172: tagalog, 02, a3m0kt6kjezd1 VS. tagalog, 02, asu813kb7bv1
Task 172: Wrong, Are Same
Task 172 RT: 2719
Task 173: myanmar, 31, a1s3njxhehcip VS. myanmar, 01, ajf66jwen7bx
Task 173: Wrong, Are Different
Task 174: korean, 28, a1v0p79fynz9j VS. korean, 28, a3bb99mn3zy37
Task 174: Right, Are Same
Task 175: korean, 39, aj6upe9mlarg VS. inuktitut, 03, ae0ibwt7asth
Task 175: Right, Are Different
Task 175 RT: 771
Task 176: tagalog, 05, aljsxt6v0a30 VS. tagalog, 05, aghrkle9vf6z
Task 176: Wrong, Are Same
Task 176 RT: 3233
Task 177: atemayar, 10, azdw6062ia38 VS. inuktitut, 01, a2pfktghg1ofp
Task 177: Right, Are Different
Task 177 RT: 793
Task 178: atemayar, 06, aljsxt6v0a30 VS. atemayar, 06, agst3co6usf8
Task 178: Wrong, Are Same
Task 178 RT: 1079
Task 179: atemayar, 23, aljsxt6v0a30 VS. atemayar, 23, a6fjbvuzrhm6
Task 179: Wrong, Are Same
Task 179 RT: 928
Task 180: myanmar, 31, a1foggtlgo7n4 VS. atemayar, 23, a31xqftzcsia2
Task 180: Right, Are Different
Task 180 RT: 1697
Task 181: korean, 29, a3dfh6dpi11ip VS. atemayar, 23, a23psx2z4f65q
Task 181: Right, Are Different
Task 181 RT: 729
Task 182: tagalog, 10, a1j8s7giyto4a VS. myanmar, 32, ajf66jwen7bx
Task 182: Right, Are Different
Task 182 RT: 850
Task 183: myanmar, 01, ay0bjcgydhix VS. myanmar, 01, a4858n7ggst4
Task 183: Right, Are Same
Task 184: tagalog, 10, a2du3880jbrov VS. tagalog, 10, avlbkm6qpb1s
Task 184: Right, Are Same
Task 185: atemayar, 10, a1qku9mccvl35 VS. atemayar, 10, a1vzhyp8yvxkz
Task 185: Wrong, Are Same
Task 185 RT: 921
Task 186: myanmar, 31, amynpl5xzw6q VS. atemayar, 10, aljsxt6v0a30
Task 186: Right, Are Different
Task 186 RT: 667
Task 187: myanmar, 32, avlbkm6qpb1s VS. myanmar, 32, a3qat9qkqumi2
Task 187: Right, Are Same
Task 188: atemayar, 06, a1qku9mccvl35 VS. atemayar, 06, a31e6jchki335
Task 188: Right, Are Same
Task 189: atemayar, 25, a1j8s7giyto4a VS. inuktitut, 11, a1j8s7giyto4a
Task 189: Right, Are Different
Task 189 RT: 2479
Task 190: sylheti, 19, a1foggtlgo7n4 VS. tagalog, 05, amynpl5xzw6q
Task 190: Wrong, Are Different
Task 191: inuktitut, 11, avlbkm6qpb1s VS. myanmar, 32, a1s3njxhehcip
Task 191: Right, Are Different
Task 191 RT: 1190
Task 192: atemayar, 25, ae0ibwt7asth VS. korean, 28, a2nm1qzuh14ls
Task 192: Right, Are Different
Task 192 RT: 676
Task 193: atemayar, 06, aljsxt6v0a30 VS. atemayar, 06, a1qku9mccvl35
Task 193: Wrong, Are Same
Task 193 RT: 809
Task 194: tagalog, 02, amynpl5xzw6q VS. tagalog, 02, a1s3njxhehcip
Task 194: Wrong, Are Same
Task 194 RT: 684
Task 195: tagalog, 10, ae0ibwt7asth VS. tagalog, 10, a1yovlluv1c9h
Task 195: Wrong, Are Same
Task 195 RT: 1882
Task 196: inuktitut, 03, ae0ibwt7asth VS. sylheti, 28, a30xq7555spo8
Task 196: Right, Are Different
Task 196 RT: 1352
Task 197: korean, 28, a1manxzopzden VS. korean, 28, a5379luit3pc
Task 197: Wrong, Are Same
Task 197 RT: 1368
Task 198: tagalog, 05, a1a25pyfoa9ge VS. korean, 04, aw82oruamauz
Task 198: Right, Are Different
Task 198 RT: 1279
Task 199: korean, 04, a1umhcrj9zfdp VS. myanmar, 32, a3qat9qkqumi2
Task 199: Right, Are Different
Task 199 RT: 976
Task 200: sylheti, 28, a3r9q9wqm1nyz VS. sylheti, 28, a6fjbvuzrhm6
Task 200: Wrong, Are Same
Task 200 RT: 655
Task 201: sylheti, 28, a2du3880jbrov VS. sylheti, 28, a3r9q9wqm1nyz
Task 201: Right, Are Same
Task 202: sylheti, 20, a1foggtlgo7n4 VS. sylheti, 20, a1qku9mccvl35
Task 202: Right, Are Same
Task 203: inuktitut, 01, a12xm86d2nbih VS. sylheti, 28, a6fjbvuzrhm6
Task 203: Wrong, Are Different
Task 204: tagalog, 02, a1yovlluv1c9h VS. myanmar, 25, a3pkun3onkucn
Task 204: Right, Are Different
Task 204 RT: 1718
Task 205: myanmar, 01, a3j1pp96x14u0 VS. myanmar, 01, a1s3njxhehcip
Task 205: Wrong, Are Same
Task 205 RT: 1533
Task 206: tagalog, 06, ajf66jwen7bx VS. tagalog, 06, a1l2fzyn31zvf
Task 206: Right, Are Same
Task 207: sylheti, 20, aljsxt6v0a30 VS. sylheti, 20, a3pkun3onkucn
Task 207: Wrong, Are Same
Task 207 RT: 2430
Task 208: myanmar, 32, a1s3njxhehcip VS. myanmar, 32, a3j1pp96x14u0
Task 208: Right, Are Same
Task 209: myanmar, 01, a3pkun3onkucn VS. myanmar, 25, a1umhcrj9zfdp
Task 209: Right, Are Different
Task 209 RT: 1366